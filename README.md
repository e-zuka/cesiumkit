CesiumKit
=========

iOS/OS X port of the [Cesium](http://cesiumjs.org) WebGL virtual globe project.
Based on https://github.com/tokyovigilante/CesiumKit

Status
------
Updated to use Cesium Ion base architecture. Internal usage of Resource class.

Licence
-------

[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

Credits
-------

CesiumKit is based on the [Cesium WebGL Virtual Globe and Map Engine](http://cesiumjs.org) by AGI.
GLSL->Metal shader real-time translation performed by the [glsl-optimizer library](https://github.com/aras-p/glsl-optimizer) by Brian Paul, Aras Pranckevičius and Unity Technologies.
JSON parsing performed using the [PMJSON library](https://github.com/postmates/PMJSON) by Postmates.
For more efficient networking, changed to [Alamofire library](https://github.com/Alamofire/Alamofire)
[Hydra library](https://github.com/malcommac/Hydra) using for order handling of critical asynchronous processings. 