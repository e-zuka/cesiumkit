//
//  SCNQuaternion+matrix.swift
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2019/11/13.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation
import SceneKit

extension SCNQuaternion {
    func rotationMatrix() -> SCNMatrix4 {
        var matrix = SCNMatrix4Identity
        var c = self.x
        var d = self.y
        var e = self.z
        var f = self.w
        var g = c + c
        var h = d + d
        let k = e + e
        let a = c * g
        let l = c * h
        c = c * k
        let m = d * h
        d = d * k
        e = e * k
        g = f * g
        h = f * h
        f = f * k
        matrix.m11 = 1 - (m + e)
        matrix.m21 = l - f
        matrix.m31 = c + h
        matrix.m12 = l + f
        matrix.m22 = 1 - (a + e)
        matrix.m32 = d - g
        matrix.m13 = c - h
        matrix.m23 = d + g
        matrix.m33 = 1 - (a + m)
        matrix.m14 = 0
        matrix.m24 = 0
        matrix.m34 = 0
        matrix.m41 = 0
        matrix.m42 = 0
        matrix.m43 = 0
        matrix.m44 = 1
        /* https://www.andre-gaschler.com/rotationconverter/three-onlymath.min.js */
        return matrix
    }
    var simd_rotationMatrix: float4x4 {
        var matrix = SCNMatrix4Identity
        var c = self.x
        var d = self.y
        var e = self.z
        var f = self.w
        var g = c + c
        var h = d + d
        let k = e + e
        let a = c * g
        let l = c * h
        c = c * k
        let m = d * h
        d = d * k
        e = e * k
        g = f * g
        h = f * h
        f = f * k
        matrix.m11 = 1 - (m + e)
        matrix.m21 = l - f
        matrix.m31 = c + h
        matrix.m12 = l + f
        matrix.m22 = 1 - (a + e)
        matrix.m32 = d - g
        matrix.m13 = c - h
        matrix.m23 = d + g
        matrix.m33 = 1 - (a + m)
        matrix.m14 = 0
        matrix.m24 = 0
        matrix.m34 = 0
        matrix.m41 = 0
        matrix.m42 = 0
        matrix.m43 = 0
        matrix.m44 = 1
        /* https://www.andre-gaschler.com/rotationconverter/three-onlymath.min.js */
        return float4x4(matrix)
    }
    var angle: Float {
        return 2 * acosf(self.w)
    }
}

extension SCNVector3 {
    var quaternion: simd_quatf {
        // Abbreviations for the various angular functions
        let cy = cos(self.y * 0.5)
        let sy = sin(self.y * 0.5)
        let cp = cos(self.x * 0.5)
        let sp = sin(self.x * 0.5)
        let cr = cos(self.z * 0.5)
        let sr = sin(self.z * 0.5)

        let q = simd_quatf(ix: cy * cp * sr - sy * sp * cr,
                           iy: sy * cp * sr + cy * sp * cr,
                           iz: sy * cp * cr - cy * sp * sr,
                           r: cy * cp * cr + sy * sp * sr)
        /*
        q.w = cy * cp * cr + sy * sp * sr;
        q.x = cy * cp * sr - sy * sp * cr;
        q.y = sy * cp * sr + cy * sp * cr;
        q.z = sy * cp * cr - cy * sp * sr;
        */

        return q;
    }
}

extension SCNMatrix4 {
    var eulerAngles: SCNVector3 {
        let matrix = self
        let a = matrix.m11
        let f = matrix.m21
        let g = matrix.m31
        let h = matrix.m12
        let k = matrix.m22
        let l = matrix.m32
        let m = matrix.m13
        let n = matrix.m23
        let e = matrix.m33
    
        func clamp(_ a: Float, _ b: Float, _ c: Float) -> Float {
            return max(b, min(c, a))
        }
        /*
        // XYZ
        var y: Float = asinf(clamp(g, -1, 1))
        var x: Float = 0
        var z: Float = 0
        if abs(g) < 0.99999 {
            x = atan2f(-l, e)
            z = atan2f(-f, a)
        } else {
            x = atan2f(n, k)
            z = 0
        }
        //return SCNVector3(x, y, z)
        // https://www.andre-gaschler.com/rotationconverter/three-onlymath.min.js
    
        // XZY
        z = asinf(-clamp(f, -1, 1))
        if abs(f) < 0.99999 {
            x = atan2f(n, k)
            y = atan2f(g, a)
        } else {
            x = atan2f(-l, e)
            y = 0
        }
        */
    
        // YXZ
        let x = asinf(-clamp(l, -1, 1))
        var y: Float = 0
        var z: Float = 0
        if abs(l) < 0.99999 {
            y = atan2f(g, e)
            z = atan2f(h, k)
        } else {
            y = atan2f(-m, a)
            z = 0
        }
        return SCNVector3(x, y, z)
    }

}
