//
//  Shadow.metal
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2019/11/13.
//  Copyright © 2019 Test Toast. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct AutomaticUniform {
    float3x3    czm_a_viewRotation;
    float3x3    czm_a_temeToPseudoFixed;
    float3 czm_a_sunDirectionEC;
    float3 czm_a_sunDirectionWC;
    float3 czm_a_moonDirectionEC;
    float3 czm_a_viewerPositionWC;
    float3 czm_a_sunPositionWC;
    float3 czm_a_sunPositionColumbusView;
    float czm_a_morphTime;
    float czm_a_fogDensity;
    float czm_a_frameNumber;
};

// Checks if a shadow ray hit something on the way to the light source. If not,
// the point the shadow ray started from was not in shadow so it's color should
// be added to the output image.
kernel void shadowKernel(uint2 tid [[thread_position_in_grid]],
                         constant uint2 & viewport,
                         texture2d_array<float> shadowRays,
                         texture2d_array<float> intersections,
                         texture2d<float, access::write> dstTex)
{
    //if (tid.x < viewport.x && tid.y < viewport.y) {
        float maxDistance = shadowRays.read(tid, 1).w;
        float intersectionDistance = intersections.read(tid, 0).x;
        
        // If the shadow ray was disabled (max distance >= 0) or the shadow ray
        // didn't hit anything on the way to the light source, the original point
        // wasn't in shadow
        if (maxDistance < 0.0f || intersectionDistance < 0.0f)
            dstTex.write(float4(1.0f, 1.0f, 1.0f, 1.0f), tid);
        else
            dstTex.write(float4(0.0f, 0.0f, 0.0f, 1.0f), tid);
    //}
}

// Combines the (denoised) shadow image and original shaded image and
// applies a simple tone mapping operator
kernel void compositeKernel(uint2 tid [[thread_position_in_grid]],
                            constant uint2 & viewport [[ buffer(0) ]],
                            constant AutomaticUniform& autoUniform [[ buffer(1) ]],
                            texture2d<float> renderTex [[ texture(0) ]],
                            texture2d<float> shadowTex [[ texture(1) ]],
                            texture2d<float, access::write> accumTex [[ texture(2) ]])
{
    //accumTex.write(renderTex.read(tid), tid);
    //if (tid.x < viewport.x && tid.y < viewport.y) {
        float intensity = dot(normalize(autoUniform.czm_a_viewerPositionWC), normalize(autoUniform.czm_a_sunPositionWC));
        float shadow = shadowTex.read(tid).x;
        float density = (intensity < 0) ? shadow/* + 0.5*/ : shadow;
        float3 color = renderTex.read(tid).xyz * clamp(density, 0.0, 1.0);
                                                  
        // Apply a very simple tone mapping function to reduce the dynamic range of the
        // input image into a range which can be displayed on screen.
        //color = color / (1.0f + color);
        
        accumTex.write(float4(color, 1.0f), tid);
    //}
}
