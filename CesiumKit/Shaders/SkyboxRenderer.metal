//
//  SkyboxRenderer.metal
//  CesiumKitTests
//
//  Created by 飯塚淳 on 2018/08/08.
//  Copyright © 2018 Test Toast. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;


struct AutomaticUniform
{
    float3x3 czm_a_viewRotation;
    float3x3 czm_a_temeToPseudoFixed;
    float3 czm_a_sunDirectionEC;
    float3 czm_a_sunDirectionWC;
    float3 czm_a_moonDirectionEC;
    float3 czm_a_viewerPositionWC;
    float3 czm_a_sunPositionWC;
    float3 czm_a_sunPositionColumbusView;
    float czm_a_morphTime;
    float czm_a_fogDensity;
    float czm_a_frameNumber;
};
struct FrustumUniform
{
    float4x4 czm_f_viewportOrthographic;
    float4x4 czm_f_viewportTransformation;
    float4x4 czm_f_projection;
    float4x4 czm_f_inverseProjection;
    float4x4 czm_f_view;
    float4x4 czm_f_modelView;
    float4x4 czm_f_modelView3D;
    float4x4 czm_f_inverseModelView;
    float4x4 czm_f_modelViewProjection;
    float4 czm_f_viewport;
    float3x3 czm_f_normal;
    float3x3 czm_f_normal3D;
    float2 czm_f_entireFrustum;
};

struct xlatMtlShaderUniform {
};

struct SkyboxVertexInput {
    float3 position [[attribute(0)]];
};
struct SkyboxVertexOutput {
    float4 position [[position]];
    float3 v_texCoord;
};

vertex SkyboxVertexOutput skybox_vertex (SkyboxVertexInput in [[stage_in]],
                                         constant AutomaticUniform& autoUniform [[buffer(0)]],
                                         constant FrustumUniform& frustumUniform [[buffer(1)]],
                                         constant xlatMtlShaderUniform& _mtl_u [[buffer(2)]])
{
    SkyboxVertexOutput out;
    float3 p = autoUniform.czm_a_viewRotation * (autoUniform.czm_a_temeToPseudoFixed * (frustumUniform.czm_f_entireFrustum.y * in.position));
    out.position = frustumUniform.czm_f_projection * float4(p, 1.0);
    out.v_texCoord = in.position.xyz;
    
    return out;
}

fragment float4 skybox_fragment (SkyboxVertexOutput in [[stage_in]],
                                constant AutomaticUniform& autoUniform [[buffer(0)]],
                                constant xlatMtlShaderUniform& _mtl_u [[buffer(2)]],
                                texturecube<float> cubeMap [[texture(0)]],
                                sampler cubeMap_sampler [[sampler(0)]])
{
    float3 rgb = cubeMap.sample(cubeMap_sampler, normalize(in.v_texCoord)).rgb;
    return float4(rgb, autoUniform.czm_a_morphTime);
}


