//
//  SkyRenderer.metal
//  CesiumKit
//
//  Created by 飯塚淳 on 2018/08/08.
//  Copyright © 2018 Test Toast. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct AutomaticUniform
{
    float3x3 czm_a_viewRotation;
    float3x3 czm_a_temeToPseudoFixed;
    float3 czm_a_sunDirectionEC;
    float3 czm_a_sunDirectionWC;
    float3 czm_a_moonDirectionEC;
    float3 czm_a_viewerPositionWC;
    float3 czm_a_sunPositionWC;
    float3 czm_a_sunPositionColumbusView;
    float czm_a_morphTime;
    float czm_a_fogDensity;
    float czm_a_frameNumber;
};
struct FrustumUniform
{
    float4x4 czm_f_viewportOrthographic;
    float4x4 czm_f_viewportTransformation;
    float4x4 czm_f_projection;
    float4x4 czm_f_inverseProjection;
    float4x4 czm_f_view;
    float4x4 czm_f_modelView;
    float4x4 czm_f_modelView3D;
    float4x4 czm_f_inverseModelView;
    float4x4 czm_f_modelViewProjection;
    float4 czm_f_viewport;
    float3x3 czm_f_normal;
    float3x3 czm_f_normal3D;
    float2 czm_f_entireFrustum;
};
struct xlatMtlShaderUniform {
    float u_cameraHeight;
    float u_cameraHeight2;
    float u_outerRadius;
    float u_outerRadius2;
    float u_innerRadius;
    float u_scale;
    float u_scaleDepth;
    float u_scaleOverScaleDepth;
};
struct skyFragmentShaderInput {
    float3 v_rayleighColor;
    float3 v_mieColor;
    float3 v_toCamera;
};
struct skyFragmentShaderOutput {
    half4 gl_FragColor;
};
float czm_luminance(float3 rgb)
{
    // Algorithm from Chapter 10 of Graphics Shaders.
    const float3 W = float3(0.2125, 0.7154, 0.0721);
    return dot(rgb, W);
}
float litIntensity(float3 viewerPositionWC, float3 sunPositionWC) {
    float lightIntensity = dot(normalize(viewerPositionWC), normalize(sunPositionWC));
    if (lightIntensity > 0) {
        // if sun is above horizon, minimum intensity is 0.3
        lightIntensity += 0.3;
    }
    return lightIntensity;
}
fragment float4 sky_fragment (skyFragmentShaderInput in [[stage_in]], constant AutomaticUniform& _auto [[buffer(0)]], constant xlatMtlShaderUniform& _u [[buffer(2)]])
{
    const float g = -0.95;
    const float g2 = g * g;
    const bool dynamicAtmosphereColor = true; // equals to enabledLighting
    // Extra normalize added for Android
    float cosAngle = dot(_auto.czm_a_sunDirectionWC, normalize(in.v_toCamera)) / length(in.v_toCamera);
    float rayleighPhase = 0.75 * (1.0 + cosAngle * cosAngle);
    float miePhase = 1.5 * ((1.0 - g2) / (2.0 + g2)) * (1.0 + cosAngle * cosAngle) / pow(1.0 + g2 - 2.0 * g * cosAngle, 1.5);

    const float exposure = 2.0;

    float3 rgb = rayleighPhase * in.v_rayleighColor + miePhase * in.v_mieColor;
    rgb = float3(1.0) - exp(-exposure * rgb);
    // Compute luminance before color correction to avoid strangely gray night skies
    float l = czm_luminance(rgb);

    #ifdef COLOR_CORRECT
        // Convert rgb color to hsb
        vec3 hsb = czm_RGBToHSB(rgb);
        // Perform hsb shift
        hsb.x += u_hsbShift.x; // hue
        hsb.y = clamp(hsb.y + u_hsbShift.y, 0.0, 1.0); // saturation
        hsb.z = hsb.z > czm_epsilon7 ? hsb.z + u_hsbShift.z : 0.0; // brightness
        // Convert shifted hsb back to rgb
        rgb = czm_HSBToRGB(hsb);

        // Check if correction decreased the luminance to 0
        l = min(l, czm_luminance(rgb));
    #endif

    // Alter alpha based on how close the viewer is to the ground (1.0 = on ground, 0.0 = at edge of atmosphere)
    float atmosphereAlpha = clamp((_u.u_outerRadius - _u.u_cameraHeight) / (_u.u_outerRadius - _u.u_innerRadius), 0.0, 1.0);

    // Alter alpha based on time of day (0.0 = night , 1.0 = day)
    float lightIntensity = litIntensity(_auto.czm_a_viewerPositionWC, _auto.czm_a_sunPositionWC);
    float nightAlpha = (dynamicAtmosphereColor) ? clamp(lightIntensity, 0.0, 1.0) : 1.0;
    atmosphereAlpha *= pow(nightAlpha, 0.5);

    return float4(rgb, mix(rgb.b, 1.0, atmosphereAlpha) * smoothstep(0.0, 1.0, _auto.czm_a_morphTime));
}

struct skyVertexShaderInput {
    float4 position [[attribute(0)]];
};
struct skyVertexShaderOutput {
    float4 gl_Position [[position]];
    float3 v_rayleighColor;
    float3 v_mieColor;
    float3 v_toCamera;
};
float scale(float cosAngle, float scaleDepth)
{
    float x = 1.0 - cosAngle;
    return scaleDepth  * exp(-0.00287 + x*(0.459 + x*(3.83 + x*(-6.80 + x*5.25))));
}
vertex skyVertexShaderOutput sky_vertex (skyVertexShaderInput in [[stage_in]], constant AutomaticUniform& _auto [[buffer(0)]], constant FrustumUniform& _frustum [[buffer(1)]], constant xlatMtlShaderUniform& _u [[buffer(2)]])
{
    const float czm_pi = 3.141592653589793;
    const float rayleighScaleDepth = 0.25;
    const float fSamples = 2.0;
    const int nSamples = 2;
    const float3 InvWavelength = float3(5.60204474633241,  // Red = 1.0 / Math.pow(0.650, 4.0)
                                        9.473284437923038, // Green = 1.0 / Math.pow(0.570, 4.0)
                                        19.643802610477206); // Blue = 1.0 / Math.pow(0.475, 4.0)
    const float Kr = 0.0025;
    const float Kr4PI = Kr * 4.0 * czm_pi;
    const float Km = 0.0015;
    const float Km4PI = Km * 4.0 * czm_pi;
    const float ESun = 15.0;
    const float KmESun = Km * ESun;
    const float KrESun = Kr * ESun;

    // Unpack attributes
    float cameraHeight = _u.u_cameraHeight;
    float outerRadius = _u.u_outerRadius;
    float innerRadius = _u.u_innerRadius;
    bool dynamicAtmosphereColor = false; // equals to enableLighting

    skyVertexShaderOutput out;
    // Get the ray from the camera to the vertex and its length (which is the far point of the ray passing through the atmosphere)
    float3 positionV3 = in.position.xyz;
    float3 ray = positionV3 - _auto.czm_a_viewerPositionWC;
    float far = length(ray);
    ray /= far;
    float atmosphereScale = 1.0 / (outerRadius - innerRadius);

    // SKY_FROM_ATMOSPHERE
    // Calculate the ray's starting position, then calculate its scattering offset
    float3 start = _auto.czm_a_viewerPositionWC;
    float height = length(start);
    float depth = exp((atmosphereScale / rayleighScaleDepth ) * (innerRadius - cameraHeight));
    float startAngle = dot(ray, start) / height;
    float startOffset = depth * scale(startAngle, rayleighScaleDepth);

    // Initialize the scattering loop variables
    float sampleLength = far / fSamples;
    float scaledLength = sampleLength * atmosphereScale;
    float3 sampleRay = ray * sampleLength;
    float3 samplePoint = start + sampleRay * 0.5;

    // Now loop through the sample rays
    float3 frontColor = float3(0.0, 0.0, 0.0);
    float3 lightDir = (dynamicAtmosphereColor) ? _auto.czm_a_sunPositionWC - _auto.czm_a_viewerPositionWC : _auto.czm_a_viewerPositionWC;
    lightDir = normalize(lightDir);

    for(int i=0; i < nSamples; i++)
    {
        float height = length(samplePoint);
        float depth = exp((atmosphereScale / rayleighScaleDepth ) * (innerRadius - height));
        float fLightAngle = dot(lightDir, samplePoint) / height;
        float fCameraAngle = dot(ray, samplePoint) / height;
        float fScatter = (startOffset + depth*(scale(fLightAngle, rayleighScaleDepth) - scale(fCameraAngle, rayleighScaleDepth)));
        float3 attenuate = exp(-fScatter * (InvWavelength * Kr4PI + Km4PI));
        frontColor += attenuate * (depth * scaledLength);
        samplePoint += sampleRay;
    }

    // Finally, scale the Mie and Rayleigh colors and set up the varying variables for the pixel shader
    out.v_mieColor = frontColor * KmESun;
    out.v_rayleighColor = frontColor * (InvWavelength * KrESun);
    out.v_toCamera = _auto.czm_a_viewerPositionWC - positionV3;
    out.gl_Position = _frustum.czm_f_modelViewProjection * in.position;
    return out;
}
fragment skyFragmentShaderOutput sky_fragment_shader (skyFragmentShaderInput _mtl_i [[stage_in]], constant AutomaticUniform& _auto [[buffer(0)]], constant xlatMtlShaderUniform& _mtl_u [[buffer(2)]])
{
    skyFragmentShaderOutput _mtl_o;
    //float tmpvar_2;
    //tmpvar_2 = (dot (_auto.czm_a_sunDirectionWC, normalize(_mtl_i.v_toCamera)) / sqrt(dot (_mtl_i.v_toCamera, _mtl_i.v_toCamera)));
    half tmpvar_2 = (half)(dot (_auto.czm_a_sunDirectionWC, normalize(_mtl_i.v_toCamera)) / sqrt(dot (_mtl_i.v_toCamera, _mtl_i.v_toCamera)));
    //float3 rgb_1;
    //rgb_1 = (float3(1.0, 1.0, 1.0) - exp((-2.0 * (((0.75 * (1.0 + (tmpvar_2 * tmpvar_2)))
    //                                               * _mtl_i.v_rayleighColor) + (((0.05038761 * (1.0 + (tmpvar_2 * tmpvar_2))) /
    //                                                 pow ( (1.9025 - (-1.9 * tmpvar_2)) , 1.5)) * _mtl_i.v_mieColor)))));
    half3 rgb_1 = (half3(1.0, 1.0, 1.0) - exp((-2.0 * (((0.75 * (1.0 + (tmpvar_2 * tmpvar_2)))
                                                   * (half3)_mtl_i.v_rayleighColor) + (((0.05038761 * (1.0 + (tmpvar_2 * tmpvar_2))) /
                                                      pow ( (1.9025 - (-1.9 * tmpvar_2)) , 1.5)) * (half3)_mtl_i.v_mieColor)))));
    //float tmpvar_3;
    //tmpvar_3 = clamp ((dot (rgb_1, float3(0.2125, 0.7154, 0.0721)) / 0.1), 0.0, 1.0);
    half tmpvar_3 = clamp ((dot (rgb_1, half3(0.2125, 0.7154, 0.0721)) / 0.1), 0.0, 1.0);
    //half tmpvar_3 = clamp ((dot((half3)rgb_1, half3(0.2125h, 0.7154h, 0.0721h)) / 0.1h), 0.0h, 1.0h);
    //float tmpvar_4;
    //tmpvar_4 = clamp (_auto.czm_a_morphTime, 0.0, 1.0);
    half tmpvar_4 = clamp((half)_auto.czm_a_morphTime, 0.0h, 1.0h);
    //float4 tmpvar_5;
    half4 tmpvar_5;
    tmpvar_5.xyz = (half3)rgb_1;
    tmpvar_5.w = (min ((tmpvar_3 * (tmpvar_3 * (3.0 - (2.0 * tmpvar_3)))), 1.0) *
                  (tmpvar_4 * (tmpvar_4 * (3.0 - (2.0 * tmpvar_4)))));
    _mtl_o.gl_FragColor = tmpvar_5;
    return _mtl_o;
}

vertex skyVertexShaderOutput sky_vertex_shader (skyVertexShaderInput _mtl_i [[stage_in]], constant AutomaticUniform& _auto [[buffer(0)]], constant FrustumUniform& _frustum [[buffer(1)]], constant xlatMtlShaderUniform& _mtl_u [[buffer(2)]])
{
    skyVertexShaderOutput _mtl_o;
    float3 tmpvar_6;
    tmpvar_6 = (_mtl_i.position.xyz - _auto.czm_a_viewerPositionWC);
    //float3 tmpvar_6 = float3((half3)_mtl_i.position.xyz - (half3)_auto.czm_a_viewerPositionWC);
    float tmpvar_7;
    tmpvar_7 = sqrt(dot (tmpvar_6, tmpvar_6));
    //half tmpvar_7 = half(sqrt(dot (tmpvar_6, tmpvar_6)));
    //float3 v3Ray_5;
    //v3Ray_5 = (tmpvar_6 / tmpvar_7);
    float3 v3Ray_5 = (tmpvar_6 / tmpvar_7);
    float tmpvar_8;
    tmpvar_8 = (1.0 - (dot (v3Ray_5, _auto.czm_a_viewerPositionWC) / sqrt( dot (_auto.czm_a_viewerPositionWC, _auto.czm_a_viewerPositionWC))));
    //half tmpvar_8 = half(1.0 - (dot (v3Ray_5, _auto.czm_a_viewerPositionWC) / sqrt( dot (_auto.czm_a_viewerPositionWC, _auto.czm_a_viewerPositionWC))));
    float fStartOffset_4;
    fStartOffset_4 = (exp((_mtl_u.u_scaleOverScaleDepth *
                           (_mtl_u.u_innerRadius - _mtl_u.u_cameraHeight)
                           )) * (_mtl_u.u_scaleDepth * exp( (-0.00287 + (tmpvar_8 * (0.459 +
                            (tmpvar_8 * (3.83 + (tmpvar_8 * (-6.8 + (tmpvar_8 * 5.25)))))))))));
    //half fStartOffset_4 = half(exp(((half)_mtl_u.u_scaleOverScaleDepth *
    //                       ((half)_mtl_u.u_innerRadius - (half)_mtl_u.u_cameraHeight)
    //                       )) * ((half)_mtl_u.u_scaleDepth * exp( (-0.00287h + (tmpvar_8 * (0.459h +
    //                        (tmpvar_8 * (3.83h + (tmpvar_8 * (-6.8h + (tmpvar_8 * 5.25h)))))))))));
    //float tmpvar_9;
    //tmpvar_9 = (tmpvar_7 / 2.0);
    float tmpvar_9 = (tmpvar_7 / 2.0); // need float
    float u_scaledLength_3;
    u_scaledLength_3 = (tmpvar_9 * _mtl_u.u_scale);
    //half u_scaledLength_3 = half(tmpvar_9 * (half)_mtl_u.u_scale);
    //float3 tmpvar_10;
    //tmpvar_10 = (v3Ray_5 * tmpvar_9);
    float3 tmpvar_10 = (v3Ray_5 * tmpvar_9);
    //float3 v3SamplePoint_2;
    //v3SamplePoint_2 = (_auto.czm_a_viewerPositionWC + (tmpvar_10 * 0.5));
    float3 v3SamplePoint_2 = (_auto.czm_a_viewerPositionWC + (tmpvar_10 * 0.5));
    float tmpvar_11;
    tmpvar_11 = sqrt(dot (v3SamplePoint_2, v3SamplePoint_2));
    //half tmpvar_11 = half(sqrt(dot (v3SamplePoint_2, v3SamplePoint_2)));
    float tmpvar_12;
    tmpvar_12 = exp((_mtl_u.u_scaleOverScaleDepth * (_mtl_u.u_innerRadius - tmpvar_11)));
    //half tmpvar_12 = half(exp((_mtl_u.u_scaleOverScaleDepth * (_mtl_u.u_innerRadius - tmpvar_11))));
    //float3 tmpvar_14;
    //tmpvar_14 = normalize(_auto.czm_a_viewerPositionWC);
    float3 tmpvar_14 = normalize(_auto.czm_a_viewerPositionWC);
    float tmpvar_13;
    tmpvar_13 = (1.0 - (dot (tmpvar_14, v3SamplePoint_2) / tmpvar_11));
    //half tmpvar_13 = half(1.0h - (dot (tmpvar_14, v3SamplePoint_2) / tmpvar_11));
    float tmpvar_15;
    tmpvar_15 = (1.0 - (dot (v3Ray_5, v3SamplePoint_2) / tmpvar_11));
    //half tmpvar_15 = half(1.0h - (dot (v3Ray_5, v3SamplePoint_2) / tmpvar_11));
    float3 v3FrontColor_1;
    v3FrontColor_1 = (exp(( -((fStartOffset_4 + (tmpvar_12 * (
       (_mtl_u.u_scaleDepth * exp((-0.00287 + (tmpvar_13 * (0.459 + (tmpvar_13 * (3.83 + (tmpvar_13 * (-6.8 + (tmpvar_13 * 5.25))))))))))
       -
       (_mtl_u.u_scaleDepth * exp((-0.00287 + (tmpvar_15 * (0.459 + (tmpvar_15 * (3.83 + (tmpvar_15 * (-6.8 + (tmpvar_15 * 5.25))))))))))
                                                             ))))
                           * float3(0.194843, 0.3164616, 0.6359779))) * (tmpvar_12 * u_scaledLength_3));
    //half3 v3FrontColor_1 = half3(exp(( -((fStartOffset_4 + (tmpvar_12 * (
    //   ((half)_mtl_u.u_scaleDepth * exp((-0.00287h + (tmpvar_13 * (0.459h + (tmpvar_13 * (3.83h + (tmpvar_13 * (-6.8h + (tmpvar_13 * 5.25h))))))))))
    //   -
    //   ((half)_mtl_u.u_scaleDepth * exp((-0.00287h + (tmpvar_15 * (0.459h + (tmpvar_15 * (3.83h + (tmpvar_15 * (-6.8h + (tmpvar_15 * 5.25h))))))))))
    //                                                          ))))
    //   * half3(0.194843h, 0.3164616h, 0.6359779h))) * (tmpvar_12 * u_scaledLength_3));
    v3SamplePoint_2 = (v3SamplePoint_2 + tmpvar_10);
    float tmpvar_16;
    tmpvar_16 = sqrt(dot (v3SamplePoint_2, v3SamplePoint_2));
    //half tmpvar_16 = half(sqrt(dot (v3SamplePoint_2, v3SamplePoint_2)));
    float tmpvar_17;
    tmpvar_17 = exp((_mtl_u.u_scaleOverScaleDepth * (_mtl_u.u_innerRadius - tmpvar_16)));
    //half tmpvar_17 = half(exp((_mtl_u.u_scaleOverScaleDepth * (_mtl_u.u_innerRadius - tmpvar_16))));
    float tmpvar_18;
    tmpvar_18 = (1.0 - (dot (tmpvar_14, v3SamplePoint_2) / tmpvar_16));
    //half tmpvar_18 = half(1.0h - (dot (tmpvar_14, v3SamplePoint_2) / tmpvar_16));
    float tmpvar_19;
    tmpvar_19 = (1.0 - (dot (v3Ray_5, v3SamplePoint_2) / tmpvar_16));
    //half tmpvar_19 = half(1.0h - (dot (v3Ray_5, v3SamplePoint_2) / tmpvar_16));
    v3FrontColor_1 = (v3FrontColor_1 + (exp( (-((fStartOffset_4 + (tmpvar_17 *
       ((_mtl_u.u_scaleDepth * exp((-0.00287 + (tmpvar_18 * (0.459 + (tmpvar_18 * (3.83 + (tmpvar_18 * (-6.8 + (tmpvar_18 * 5.25))))))))))
        - (_mtl_u.u_scaleDepth * exp((-0.00287 + (tmpvar_19 * (0.459 + (tmpvar_19 * (3.83 + (tmpvar_19 * (-6.8 + (tmpvar_19 * 5.25)))))))))))
                                                                  ))) *
        float3(0.194843, 0.3164616, 0.6359779))) * (tmpvar_17 * u_scaledLength_3)));
    //v3FrontColor_1 = half3(v3FrontColor_1 + (exp( (-((fStartOffset_4 + (tmpvar_17 *
    //   (((half)_mtl_u.u_scaleDepth * exp((-0.00287h + (tmpvar_18 * (0.459h + (tmpvar_18 * (3.83h + (tmpvar_18 * (-6.8h + (tmpvar_18 * 5.25h))))))))))
    //    - ((half)_mtl_u.u_scaleDepth * exp((-0.00287h + (tmpvar_19 * (0.459h + (tmpvar_19 * (3.83h + (tmpvar_19 * (-6.8h + (tmpvar_19 * 5.25h)))))))))))
    //                                                        ))) *
    //   half3(0.194843h, 0.3164616h, 0.6359779h))) * (tmpvar_17 * u_scaledLength_3)));
    v3SamplePoint_2 = (v3SamplePoint_2 + tmpvar_10);
    _mtl_o.v_mieColor = float3(v3FrontColor_1 * 0.0225h);
    //_mtl_o.v_rayleighColor = float3(v3FrontColor_1 * half3(0.2100767h, 0.3552482h, 0.7366425h));
    _mtl_o.v_rayleighColor = float3(v3FrontColor_1 * float3(0.2100767, 0.3552482, 0.7366425));
    _mtl_o.v_toCamera = (_auto.czm_a_viewerPositionWC - _mtl_i.position.xyz);
    _mtl_o.gl_Position = (_frustum.czm_f_modelViewProjection * _mtl_i.position);
    return _mtl_o;
}

