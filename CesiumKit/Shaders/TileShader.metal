//
//  TileShader.metal
//  CesiumKitTests
//
//  Created by 飯塚淳 on 2018/08/13.
//  Copyright © 2018 Test Toast. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct AutomaticUniform
{
    float3x3 czm_a_viewRotation;
    float3x3 czm_a_temeToPseudoFixed;
    float3 czm_a_sunDirectionEC;
    float3 czm_a_sunDirectionWC;
    float3 czm_a_moonDirectionEC;
    float3 czm_a_viewerPositionWC;
    float3 czm_a_sunPositionWC;
    float3 czm_a_sunPositionColumbusView;
    float czm_a_morphTime;
    float czm_a_fogDensity;
    float czm_a_frameNumber;
};
struct FrustumUniform
{
    float4x4 czm_f_viewportOrthographic;
    float4x4 czm_f_viewportTransformation;
    float4x4 czm_f_projection;
    float4x4 czm_f_inverseProjection;
    float4x4 czm_f_view;
    float4x4 czm_f_modelView;
    float4x4 czm_f_modelView3D;
    float4x4 czm_f_inverseModelView;
    float4x4 czm_f_modelViewProjection;
    float4 czm_f_viewport;
    float3x3 czm_f_normal;
    float3x3 czm_f_normal3D;
    float2 czm_f_entireFrustum;
};
struct xlatMtlShaderUniform {
    float4 u_dayTextureTexCoordsRectangle [31];
    float4 u_dayTextureTranslationAndScale [31];
    float u_dayTextureAlpha [31];
    float u_dayTextureBrightness [31];
    float u_dayTextureContrast [31];
    float u_dayTextureHue [31];
    float u_dayTextureSaturation [31];
    float u_dayTextureOneOverGamma [31];
    float2 u_minMaxHeight;
    float4x4 u_scaleAndBias;
    float4 u_waterMaskTranslationAndScale;
    float4 u_initialColor;
    float4 u_tileRectangle;
    float4x4 u_modifiedModelView;
    float3 u_center3D;
    float2 u_southMercatorYAndOneOverHeight;
    float2 u_southAndNorthLatitude;
    float2 u_lightingFadeDistance;
    float u_zoomedOutOceanSpecularIntensity;
};

struct tileVertexShaderInput {
    float4 compressed [[attribute(0)]];
};
struct tileVertexShaderOutput {
    float4 gl_Position [[position]];
    float3 v_positionMC;
    float3 v_positionEC;
    float2 v_textureCoordinates;
};
vertex tileVertexShaderOutput tile_vertex_shader (tileVertexShaderInput _mtl_i [[stage_in]], constant FrustumUniform& _frustum [[buffer(1)]], constant xlatMtlShaderUniform& _mtl_u [[buffer(2)]])
{
    tileVertexShaderOutput _mtl_o;
    float3 position_1;
    float tmpvar_2;
    tmpvar_2 = floor((_mtl_i.compressed.x / 4096.0h));
    float2 tmpvar_3;
    tmpvar_3.x = (tmpvar_2 / 4095.0h);
    tmpvar_3.y = ((_mtl_i.compressed.x - (tmpvar_2 * 4096.0h)) / 4095.0h);
    float tmpvar_4;
    tmpvar_4 = floor((_mtl_i.compressed.y / 4096.0h));
    float2 tmpvar_5;
    tmpvar_5.x = (tmpvar_4 / 4095.0h);
    tmpvar_5.y = ((_mtl_i.compressed.y - (tmpvar_4 * 4096.0h)) / 4095.0h);
    float3 tmpvar_6;
    tmpvar_6.xy = tmpvar_3;
    tmpvar_6.z = tmpvar_5.x;
    float tmpvar_7;
    tmpvar_7 = floor((_mtl_i.compressed.z / 4096.0h));
    float2 tmpvar_8;
    tmpvar_8.x = (tmpvar_7 / 4095.0h);
    tmpvar_8.y = ((_mtl_i.compressed.z - (tmpvar_7 * 4096.0h)) / 4095.0h);
    float4 tmpvar_9;
    tmpvar_9.w = 1.0h;
    tmpvar_9.xyz = tmpvar_6;
    position_1 = (_mtl_u.u_scaleAndBias * tmpvar_9).xyz;
    float3 tmpvar_10;
    tmpvar_10 = (position_1 + _mtl_u.u_center3D);
    float4 tmpvar_11;
    tmpvar_11.w = 1.0h;
    tmpvar_11.xyz = position_1;
    _mtl_o.gl_Position = (_frustum.czm_f_projection * (_mtl_u.u_modifiedModelView * tmpvar_11));
    _mtl_o.v_textureCoordinates = tmpvar_8;
    float4 tmpvar_12;
    tmpvar_12.w = 1.0h;
    tmpvar_12.xyz = tmpvar_10;
    _mtl_o.v_positionEC = (_frustum.czm_f_modelView3D * tmpvar_12).xyz;
    _mtl_o.v_positionMC = tmpvar_10;
    return _mtl_o;
}



struct tileFragmentShaderInput {
    float3 v_positionMC;
    float2 v_textureCoordinates;
};
struct tileFragmentShaderOutput {
    half4 gl_FragColor;
};
fragment tileFragmentShaderOutput tile_fragment_shader (tileFragmentShaderInput _mtl_i [[stage_in]], constant AutomaticUniform& _auto [[buffer(0)]], constant FrustumUniform& _frustum [[buffer(1)]], constant xlatMtlShaderUniform& _mtl_u [[buffer(2)]]
                                          ,   texture2d<float> u_dayTexture0 [[texture(0)]], sampler _mtlsmp_u_dayTexture0 [[sampler(0)]]
                                          ,   texture2d<float> u_dayTexture1 [[texture(1)]], sampler _mtlsmp_u_dayTexture1 [[sampler(1)]])
{
    tileFragmentShaderOutput _mtl_o;
    half4 tmpvar_1;
    tmpvar_1 = half4(_mtl_u.u_initialColor);
    float4 textureCoordinateRectangle_2;
    textureCoordinateRectangle_2 = _mtl_u.u_dayTextureTexCoordsRectangle[0];
    float4 textureCoordinateTranslationAndScale_3;
    textureCoordinateTranslationAndScale_3 = _mtl_u.u_dayTextureTranslationAndScale[0];
    float textureAlpha_4;
    float2 tmpvar_5;
    tmpvar_5 = float2((_mtl_i.v_textureCoordinates >= textureCoordinateRectangle_2.xy));
    textureAlpha_4 = (tmpvar_5.x * tmpvar_5.y);
    float2 tmpvar_6;
    tmpvar_6 = float2(((textureCoordinateRectangle_2.zw - _mtl_i.v_textureCoordinates) >= float2(0.0, 0.0)));
    textureAlpha_4 = ((textureAlpha_4 * tmpvar_6.x) * tmpvar_6.y);
    float2 textureCoordinates;
    textureCoordinates = ((_mtl_i.v_textureCoordinates * textureCoordinateTranslationAndScale_3.zw) + textureCoordinateTranslationAndScale_3.xy);
    half4 color; // tmpvar_8
    color = half4(u_dayTexture0.sample(_mtlsmp_u_dayTexture0, (float2)(textureCoordinates)));
    half sourceAlpha; // tmpvar_9
    sourceAlpha = ((half)((float)_value.w * textureAlpha_4));
    half outAlpha; // tmpvar_10
    outAlpha = mix (tmpvar_1.w, (half)1.0, sourceAlpha);
    
    float4 textureCoordinateRectangle_11;
    textureCoordinateRectangle_11 = _mtl_u.u_dayTextureTexCoordsRectangle[1];
    float4 textureCoordinateTranslationAndScale_12;
    textureCoordinateTranslationAndScale_12 = _mtl_u.u_dayTextureTranslationAndScale[1];
    float textureAlpha_13;
    float2 tmpvar_14;
    tmpvar_14 = float2((_mtl_i.v_textureCoordinates >= textureCoordinateRectangle_11.xy));
    textureAlpha_13 = (tmpvar_14.x * tmpvar_14.y);
    float2 tmpvar_15;
    tmpvar_15 = float2(((textureCoordinateRectangle_11.zw - _mtl_i.v_textureCoordinates) >= float2(0.0, 0.0)));
    textureAlpha_13 = ((textureAlpha_13 * tmpvar_15.x) * tmpvar_15.y);
    float2 tmpvar_16;
    tmpvar_16 = ((_mtl_i.v_textureCoordinates * textureCoordinateTranslationAndScale_12.zw) + textureCoordinateTranslationAndScale_12.xy);
    half4 tmpvar_17;
    tmpvar_17 = half4(u_dayTexture1.sample(_mtlsmp_u_dayTexture1, (float2)(tmpvar_16)));
    half tmpvar_18;
    tmpvar_18 = ((half)((float)tmpvar_17.w * textureAlpha_13));
    half tmpvar_19;
    tmpvar_19 = mix (outAlpha, (half)1.0, tmpvar_18);
    half3 tmpvar_20; // tmpvar_20
    tmpvar_20 = (mix ((
                       (mix ((tmpvar_1.xyz * tmpvar_1.w), color.xyz, sourceAlpha) / outAlpha) // outColor
                       * outAlpha), tmpvar_17.xyz, tmpvar_18) / tmpvar_19);
    half4 tmpvar_21;
    tmpvar_21.xyz = tmpvar_20;
    tmpvar_21.w = tmpvar_19;
    float4 x_22; // cameraDist = length(x_22)
    x_22 = _frustum.czm_f_view[3];
    float diffuseIntensity;
    diffuseIntensity = mix (1.0, clamp ((
                                  (max (dot (_auto.czm_a_sunDirectionEC, normalize(
                                                                                   (_frustum.czm_f_normal3D * normalize(normalize(_mtl_i.v_positionMC)))
                                                                                   )), 0.0) * 5.0)
                                  + 0.3), 0.0, 1.0), clamp ((
                                                             (sqrt(dot (x_22, x_22)) - _mtl_u.u_lightingFadeDistance.x)
                                                             /
                                                             (_mtl_u.u_lightingFadeDistance.y - _mtl_u.u_lightingFadeDistance.x)
                                                             ), 0.0, 1.0));
    half4 finalColor; // tmpvar_24
    finalColor.xyz = ((half3)((float3)tmpvar_20 * diffuseIntensity));
    finalColor.w = tmpvar_21.w;
    _mtl_o.gl_FragColor = finalColor;
    return _mtl_o;
}


