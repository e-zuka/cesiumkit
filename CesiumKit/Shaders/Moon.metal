//
//  Moon.metal
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/11/28.
//  Copyright © 2019 Test Toast. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct FrustumUniform
{
    float4x4 czm_f_viewportOrthographic;
    float4x4 czm_f_viewportTransformation;
    float4x4 czm_f_projection;
    float4x4 czm_f_inverseProjection;
    float4x4 czm_f_view;
    float4x4 czm_f_modelView;
    float4x4 czm_f_modelView3D;
    float4x4 czm_f_inverseModelView;
    float4x4 czm_f_modelViewProjection;
    float4 czm_f_viewport;
    float3x3 czm_f_normal;
    float3x3 czm_f_normal3D;
    float2 czm_f_entireFrustum;
};

struct TextureUniform {
    float4 color;
    float2 repeat;
};
struct EllipsoidUnirom {
    float3 radii;
    float3 oneOverEllipsoidRadiiSquared;
    float3 sunDirectionEC;
};
struct MoonVertexOut {
    float4 position [[ position ]];
    float3 positionEC;
};

vertex MoonVertexOut moon_vertex(
                          unsigned int vid [[ vertex_id ]],
                          const device packed_float3* verticies [[ buffer(0) ]],
                          constant FrustumUniform& f [[buffer(1)]],
                          constant EllipsoidUnirom & u [[ buffer(2) ]]
                          ) {
    float near = 0.0;
    float far = 1.0;
    
    // In the vertex data, the cube goes from (-1.0, -1.0, -1.0) to (1.0, 1.0, 1.0) in model coordinates.
    // Scale to consider the radii.  We could also do this once on the CPU when using the BoxGeometry,
    // but doing it here allows us to change the radii without rewriting the vertex data, and
    // allows all ellipsoids to reuse the same vertex data.
    float4 p = float4(u.radii * verticies[vid], 1.0);

    MoonVertexOut out;
    out.positionEC = (f.czm_f_modelView * p).xyz;     // position in eye coordinates
    out.position = f.czm_f_modelViewProjection * p;  // position in clip coordinates

    // With multi-frustum, when the ellipsoid primitive is positioned on the intersection of two frustums
    // and close to terrain, the terrain (writes depth) in the closest frustum can overwrite part of the
    // ellipsoid (does not write depth) that was rendered in the farther frustum.
    //
    // Here, we clamp the depth in the vertex shader to avoid being overwritten; however, this creates
    // artifacts since some fragments can be alpha blended twice.  This is solved by only rendering
    // the ellipsoid in the closest frustum to the viewer.
    out.position.z = clamp(out.position.z, near, far);

    return out;
}

struct Ellipsoid
{
    float3 center;
    float3 radii;
    float3 inverseRadii;
    float3 inverseRadiiSquared;
};
struct Ray {
    float3 origin;
    float3 direction;
};
struct RaySegment
{
    float start;
    float stop;
};
Ellipsoid ellipsoidNew(float3 center, float3 radii) {
    float3 inverseRadii = float3(1.0 / radii.x, 1.0 / radii.y, 1.0 / radii.z);
    float3 inverseRadiiSquared = inverseRadii * inverseRadii;
    Ellipsoid temp;
    temp.center = center;
    temp.radii = radii;
    temp.inverseRadii = inverseRadii;
    temp.inverseRadiiSquared = inverseRadiiSquared;

    return temp;
}
RaySegment rayEllipsoidIntersectionInterval(Ray ray, Ellipsoid ellipsoid, FrustumUniform f) {
    RaySegment fullRaySegment;
    fullRaySegment.start = 0.0;
    fullRaySegment.stop = INFINITY;//czm_infinity;
    RaySegment emptyRaySegment;
    emptyRaySegment.start = -INFINITY;
    emptyRaySegment.stop = -INFINITY;
   // ray and ellipsoid center in eye coordinates.  radii in model coordinates.
    float3 q = ellipsoid.inverseRadii * (f.czm_f_inverseModelView * float4(ray.origin, 1.0)).xyz;
    float3 w = ellipsoid.inverseRadii * (f.czm_f_inverseModelView * float4(ray.direction, 0.0)).xyz;
   
    q = q - ellipsoid.inverseRadii * (f.czm_f_inverseModelView * float4(ellipsoid.center, 1.0)).xyz;
    
    float q2 = dot(q, q);
    float qw = dot(q, w);
    
    if (q2 > 1.0) {
        // Outside ellipsoid.
        if (qw >= 0.0) {
            // Looking outward or tangent (0 intersections).
            return emptyRaySegment;
        }
        else {
            // qw < 0.0.
            float qw2 = qw * qw;
            float difference = q2 - 1.0; // Positively valued.
            float w2 = dot(w, w);
            float product = w2 * difference;
            
            if (qw2 < product) {
                // Imaginary roots (0 intersections).
                return emptyRaySegment;
            } else if (qw2 > product) {
                // Distinct roots (2 intersections).
                float discriminant = qw * qw - product;
                float temp = -qw + sqrt(discriminant); // Avoid cancellation.
                float root0 = temp / w2;
                float root1 = difference / temp;
                if (root0 < root1) {
                    RaySegment i;
                    i.start = root0;
                    i.stop = root1;
                    return i;
                } else {
                    RaySegment i;
                    i.start = root1;
                    i.stop = root0;
                    return i;
                }
            } else {
                // qw2 == product.  Repeated roots (2 intersections).
                float root = sqrt(difference / w2);
                RaySegment i;
                i.start = root;
                i.stop = root;
                return i;
            }
        }
    } else if (q2 < 1.0) {
        // Inside ellipsoid (2 intersections).
        float difference = q2 - 1.0; // Negatively valued.
        float w2 = dot(w, w);
        float product = w2 * difference; // Negatively valued.
        float discriminant = qw * qw - product;
        float temp = -qw + sqrt(discriminant); // Positively valued.
        RaySegment i;
        i.start = 0.0;
        i.stop = temp / w2;
        return i;
    } else {
        // q2 == 1.0. On ellipsoid.
        if (qw < 0.0) {
            // Looking inward.
            float w2 = dot(w, w);
            RaySegment i;
            i.start = 0.0;
            i.stop = -qw / w2;
            return i;
        } else {
            // qw >= 0.0.  Looking outward or tangent.
            return emptyRaySegment;
        }
    }
}
bool isEmpty(RaySegment interval) {
    return (interval.stop < 0.0);
}
struct MaterialInput {
    float s;
    float2 st;
    float3 str;
    float3 normalEC;
    float3x3 tangentToEyeMatrix;
    float3 positionToEyeEC;
    float height;
    float slope;
};
struct Material {
    float3 diffuse;
    float specular;
    float shininess;
    float3 normal;
    float3 emission;
    float alpha;
};
Material getDefaultMaterial(MaterialInput materialInput) {
    Material material;
    material.diffuse = float3(0.0);
    material.specular = 0.0;
    material.shininess = 1.0;
    material.normal = materialInput.normalEC;
    material.emission = float3(0.0);
    material.alpha = 1.0;
    return material;
}
/*
Material getMaterial(MaterialInput materialInput, TextureUniform texu) {
    Material material = getDefaultMaterial(materialInput);
    //material.diffuse = czm_gammaCorrect(texture2D(image_0, fract(repeat_1 * materialInput.st)).rgb * color_2.rgb);
    material.diffuse = texture2D(image_0, fract(texu.repeat * materialInput.st)).rgb * texu.color.rgb;
    //material.alpha = czm_gammaCorrect(vec4(vec3(0.0), texture2D(image_0, fract(repeat_1 * materialInput.st)).a * color_2.a)).a;
    material.alpha = vec4(float3(0.0), texture2D(image_0, fract(texu.repeat * materialInput.st)).a * texu.color.a).a;
    return material;
}
 */
float2 ellipsoidWgs84TextureCoordinates(float3 normal) {
    const float oneOverTwoPi = 0.15915494309189535;
    const float oneOverPi = 0.3183098861837907;
    //return float2(atan(normal.y, normal.x) * oneOverTwoPi + 0.5, asin(normal.z) * oneOverPi + 0.5);
    //return float2(atan2(normal.y, normal.x) * oneOverTwoPi + 0.5, asin(normal.z) * oneOverPi + 0.5);
    return float2(atan(normal.y / normal.x) * oneOverTwoPi + 0.5, asin(normal.z) * oneOverPi + 0.5);
}
float3 pointAlongRay(Ray ray, float time) {
    return ray.origin + (time * ray.direction);
}
float3 geodeticSurfaceNormal(float3 positionOnEllipsoid, float3 ellipsoidCenter, float3 oneOverEllipsoidRadiiSquared) {
    return normalize((positionOnEllipsoid - ellipsoidCenter) * oneOverEllipsoidRadiiSquared);
}
float3x3 eastNorthUpToEyeCoordinates(float3 positionMC, float3 normalEC, FrustumUniform f) {
    float3 tangentMC = normalize(float3(-positionMC.y, positionMC.x, 0.0));  // normalized surface tangent in model coordinates
    float3 tangentEC = normalize(f.czm_f_normal3D * tangentMC);                // normalized surface tangent in eye coordiantes
    float3 bitangentEC = normalize(cross(normalEC, tangentEC));            // normalized surface bitangent in eye coordinates

    return float3x3(
        tangentEC.x,   tangentEC.y,   tangentEC.z,
        bitangentEC.x, bitangentEC.y, bitangentEC.z,
        normalEC.x,    normalEC.y,    normalEC.z);
}
float private_getLambertDiffuseOfMaterial(float3 lightDirectionEC, Material material) {
    return max(dot(lightDirectionEC, material.normal), 0.0);
}
float getSpecular(float3 lightDirectionEC, float3 toEyeEC, float3 normalEC, float shininess) {
    float3 toReflectedLight = reflect(-lightDirectionEC, normalEC);
    float specular = max(dot(toReflectedLight, toEyeEC), 0.0);

    const float epsilon2 = 0.01;
    // pow has undefined behavior if both parameters <= 0.
    // Prevent this by making sure shininess is at least czm_epsilon2.
    return pow(specular, max(shininess, epsilon2));
}
float private_getSpecularOfMaterial(float3 lightDirectionEC, float3 toEyeEC, Material material) {
    return getSpecular(lightDirectionEC, toEyeEC, material.normal, material.shininess);
}
float4 private_phong(float3 toEye, Material material, EllipsoidUnirom u)
{
    float diffuse = private_getLambertDiffuseOfMaterial(u.sunDirectionEC, material);
    float specular = private_getSpecularOfMaterial(u.sunDirectionEC, toEye, material);

    float3 ambient = float3(0.0);
    float3 color = ambient + material.emission;
    color += material.diffuse * diffuse;
    color += material.specular * specular;

    return float4(color, material.alpha);
}
MaterialInput materialInputForComputeEllipsoidColor(Ray ray, float intersection, float side, EllipsoidUnirom u, FrustumUniform f) {
    float3 positionEC = pointAlongRay(ray, intersection);
    float3 positionMC = (f.czm_f_inverseModelView * float4(positionEC, 1.0)).xyz;
    float3 geodeticNormal = normalize(geodeticSurfaceNormal(positionMC, float3(0.0), u.oneOverEllipsoidRadiiSquared));
    float3 sphericalNormal = normalize(positionMC / u.radii);
    float3 normalMC = geodeticNormal * side;              // normalized surface normal (always facing the viewer) in model coordinates
    float3 normalEC = normalize(f.czm_f_normal * normalMC);   // normalized surface normal in eye coordiantes

    float2 st = ellipsoidWgs84TextureCoordinates(sphericalNormal);
    float3 positionToEyeEC = -positionEC;

    MaterialInput materialInput;
    materialInput.s = st.x;
    materialInput.st = st;
    materialInput.str = (positionMC + u.radii) / u.radii;
    materialInput.normalEC = normalEC;
    materialInput.tangentToEyeMatrix = eastNorthUpToEyeCoordinates(positionMC, normalEC, f);
    materialInput.positionToEyeEC = positionToEyeEC;

    return materialInput;

}
struct MoonFragmentOut {
    half4 color [[ color(0) ]];
    //float depth [[ depth(any) ]];
};
fragment MoonFragmentOut moon_fragment(
                             MoonVertexOut in [[ stage_in ]],
                             constant FrustumUniform& f [[buffer(0)]],
                             constant EllipsoidUnirom & u [[ buffer(1) ]],
                             constant TextureUniform & texu [[ buffer(2) ]],
                             texture2d<float, access::sample> texture [[ texture(0) ]]
                             ) {
    // Test if the ray intersects a sphere with the ellipsoid's maximum radius.
    // For very oblate ellipsoids, using the ellipsoid's radii for an intersection test
    // may cause false negatives. This will discard fragments before marching the ray forward.
    float maxRadius = max(u.radii.x, max(u.radii.y, u.radii.z)) * 1.5;
    float3 direction = normalize(in.positionEC);
    float3 ellipsoidCenter = f.czm_f_modelView[3].xyz;

    float t1 = -1.0;
    float t2 = -1.0;

    float b = -2.0 * dot(direction, ellipsoidCenter);
    float c = dot(ellipsoidCenter, ellipsoidCenter) - maxRadius * maxRadius;

    float discriminant = b * b - 4.0 * c;
    if (discriminant >= 0.0) {
        t1 = (-b - sqrt(discriminant)) * 0.5;
        t2 = (-b + sqrt(discriminant)) * 0.5;
    }

    if (t1 < 0.0 && t2 < 0.0) {
        discard_fragment();
    }

    float t = min(t1, t2);
    if (t < 0.0) {
        t = 0.0;
    }

    // March ray forward to intersection with larger sphere and find
    // actual intersection point with ellipsoid.
    Ellipsoid ellipsoid = ellipsoidNew(ellipsoidCenter, u.radii);
    Ray ray;
    ray.origin = t * direction;
    ray.direction = direction;
    RaySegment intersection = rayEllipsoidIntersectionInterval(ray, ellipsoid, f);

    if (isEmpty(intersection))
    {
        discard_fragment();
    }

    constexpr sampler s(filter::nearest);
    
    // If the viewer is outside, compute outsideFaceColor, with normals facing outward.
    //float4 outsideFaceColor = (intersection.start != 0.0) ? computeEllipsoidColor(ray, intersection.start, 1.0, u, f) : float4(0.0);
    //material.diffuse = texture2D(image_0, fract(texu.repeat * materialInput.st)).rgb * texu.color.rgb;
    //material.alpha = float4(float3(0.0), texture2D(image_0, fract(texu.repeat * materialInput.st)).a * texu.color.a).a;
    float4 outsideFaceColor = float4(0.0);
    if (intersection.start != 0.0) {
        MaterialInput outsideMaterialInput = materialInputForComputeEllipsoidColor(ray, intersection.start, 1.0, u, f);
        Material outsideMaterial = getDefaultMaterial(outsideMaterialInput);
        outsideMaterial.diffuse = texture.sample(s, fract(texu.repeat * outsideMaterialInput.st)).rgb * texu.color.rgb;
        outsideMaterial.alpha = float4(float3(0.0), texture.sample(s, fract(texu.repeat * outsideMaterialInput.st)).a * texu.color.a).a;
        outsideFaceColor = private_phong(normalize(outsideMaterialInput.positionToEyeEC), outsideMaterial, u);
    }

    // If the viewer either is inside or can see inside, compute insideFaceColor, with normals facing inward.
    //float4 insideFaceColor = (outsideFaceColor.a < 1.0) ? computeEllipsoidColor(ray, intersection.stop, -1.0, u, f) : float4(0.0);
    float4 insideFaceColor = float4(0.0);
    if (outsideFaceColor.a < 1.0) {
        MaterialInput insideMaterialInput = materialInputForComputeEllipsoidColor(ray, intersection.start, 1.0, u, f);
        Material insideMaterial = getDefaultMaterial(insideMaterialInput);
        insideMaterial.diffuse = texture.sample(s, fract(texu.repeat * insideMaterialInput.st)).rgb * texu.color.rgb;
        insideMaterial.alpha = float4(float3(0.0), texture.sample(s, fract(texu.repeat * insideMaterialInput.st)).a * texu.color.a).a;
        insideFaceColor = private_phong(normalize(insideMaterialInput.positionToEyeEC), insideMaterial, u);
    }

    half4 color;
    color = half4(mix(insideFaceColor, outsideFaceColor, outsideFaceColor.a));
    color.a = half(1.0 - (1.0 - insideFaceColor.a) * (1.0 - outsideFaceColor.a));
    
    MoonFragmentOut out;
    out.color = color;
    return out;
}
