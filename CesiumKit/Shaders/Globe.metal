//
//  Globe.metal
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2019/10/29.
//  Copyright © 2019 Test Toast. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;
#include "ShaderType.h"

#define APPLY_FOG_FC_INDEX 0

constant bool fog    [[function_constant(APPLY_FOG_FC_INDEX)]];

typedef struct TileUniform {
    float4 u_dayTextureTexCoordsRectangle[31];
    float4 u_dayTextureTranslationAndScale[31];
    float u_dayTextureAlpha [31];
    float u_dayTextureBrightness [31];
    float u_dayTextureContrast [31];
    float u_dayTextureHue [31];
    float u_dayTextureSaturation [31];
    float u_dayTextureOneOverGamma [31];
    float2 u_minMaxHeight;
    float4x4 u_scaleAndBias;
    float4 u_waterMaskTranslationAndScale;
    float4 u_initialColor;
    float4 u_tileRectangle;
    float4x4 u_modifiedModelView;
    float3 u_center3D;
    float2 u_southMercatorYAndOneOverHeight;
    float2 u_southAndNorthLatitude;
    float2 u_lightingFadeDistance;
    float u_zoomedOutOceanSpecularIntensity;
} TileUniform;

struct AutomaticUniform {
    float3x3    czm_a_viewRotation;
    float3x3    czm_a_temeToPseudoFixed;
    float3 czm_a_sunDirectionEC;
    float3 czm_a_sunDirectionWC;
    float3 czm_a_moonDirectionEC;
    float3 czm_a_viewerPositionWC;
    float3 czm_a_sunPositionWC;
    float3 czm_a_sunPositionColumbusView;
    float czm_a_morphTime;
    float czm_a_fogDensity;
    float czm_a_frameNumber;
};


struct FrustumUniform
{
    float4x4 czm_f_viewportOrthographic;
    float4x4 czm_f_viewportTransformation;
    float4x4 czm_f_projection;
    float4x4 czm_f_inverseProjection;
    float4x4 czm_f_view;
    float4x4 czm_f_modelView;
    float4x4 czm_f_modelView3D;
    float4x4 czm_f_inverseModelView;
    float4x4 czm_f_modelViewProjection;
    float4 czm_f_viewport;
    float3x3 czm_f_normal;
    float3x3 czm_f_normal3D;
    float2 czm_f_entireFrustum;
};

struct AtmosphereColor
{
    float3 mie;
    float3 rayleigh;
};

struct Material {
    texture2d<float> texture_0                  [[ id(CesiumMaterialBufferIndexTexture_0) ]];
    texture2d<float> texture_1                  [[ id(CesiumMaterialBufferIndexTexture_1) ]];
    texture2d<float> texture_2                  [[ id(CesiumMaterialBufferIndexTexture_2) ]];
    sampler sampler_0                           [[ id(CesiumMaterialBufferIndexSampler_0) ]];
    sampler sampler_1                           [[ id(CesiumMaterialBufferIndexSampler_1) ]];
    sampler sampler_2                           [[ id(CesiumMaterialBufferIndexSampler_2) ]];
    uint textureCount                           [[ id(CesiumMaterialBufferIndexTextureCount) ]];
    render_pipeline_state pipeline              [[ id(CesiumMaterialBufferIndexPipelineState) ]];
    texture2d<uint, access::read> random_texture              [[ id(CesiumMaterialBufferIndexRandomTexture) ]];
};

struct TileVertexInput {
  float4 compressed [[attribute(0)]];
};
struct TileVertexRawInput {
  float4 position3DAndHeight [[attribute(0)]];
  float3 textureCoordAndEncodedNormals [[attribute(1)]];
};
struct TileVertexOutput {
    float4 position [[position]];
    float2 v_textureCoordinates;
    float4 prevPosition;
    float3 worldPosition;
    float3 viewPosition;
    float3 normal;
};
struct TileVertexFogOutput {
    float4 position [[position]];
    float2 v_textureCoordinates;
    float v_distance;
    float3 v_mieColor;
    float3 v_rayleighColor;
    float fog_density;
    float4 prevPosition;
    float3 worldPosition;
    float3 viewPosition;
    float3 normal;
};

// utility functions
float2 decompressTextureCoordinates(float encoded)
{
   float xZeroTo4095 = floor(encoded / 4096.0);
   float stx = xZeroTo4095 / 4095.0;
   float sty = (encoded - xZeroTo4095 * 4096.0) / 4095.0;
   return float2(stx, sty);
}
float czm_signNotZero(float value) {
    return value >= 0.0 ? 1.0 : -1.0;
}
float2 czm_signNotZero(float2 value) {
    return float2(czm_signNotZero(value.x), czm_signNotZero(value.y));
}
float3 czm_signNotZero(float3 value) {
    return float3(czm_signNotZero(value.x), czm_signNotZero(value.y), czm_signNotZero(value.z));
}
float4 czm_signNotZero(float4 value) {
    return float4(czm_signNotZero(value.x), czm_signNotZero(value.y), czm_signNotZero(value.z), czm_signNotZero(value.w));
}
float3 czm_octDecode(float2 encoded, float range)
{
    if (encoded.x == 0.0 && encoded.y == 0.0) {
        return float3(0.0, 0.0, 0.0);
    }

   encoded = encoded / range * 2.0 - 1.0;
   float3 v = float3(encoded.x, encoded.y, 1.0 - abs(encoded.x) - abs(encoded.y));
   if (v.z < 0.0)
   {
       v.xy = (1.0 - abs(v.yx)) * czm_signNotZero(v.xy);
   }

   return normalize(v);
}
float3 czm_octDecode(float2 encoded)
{
   return czm_octDecode(encoded, 255.0);
}
float3 czm_octDecode(float encoded)
{
   float temp = encoded / 256.0;
   float x = floor(temp);
   float y = (temp - x) * 256.0;
   return czm_octDecode(float2(x, y));
}
float branchFreeTernary(bool comparison, float a, float b) {
    float useA = float(comparison);
    return a * useA + b * (1.0 - useA);
}
float scale(float fCos)
{
    const float fScaleDepth = 0.25;
    float x = 1.0 - fCos;
    return fScaleDepth * exp(-0.00287 + x*(0.459 + x*(3.83 + x*(-6.80 + x*5.25))));
}
AtmosphereColor computeGroundAtmosphereFromSpace(float3 v3Pos, bool useSunLighting, AutomaticUniform _auto)
{
    const float czm_pi = 3.141592653589793;
    const float fInnerRadius = 6378137.0;
    const float fOuterRadius = 6378137.0 * 1.025;
    const float fOuterRadius2 = fOuterRadius * fOuterRadius;
    const float fScale = 1.0 / (fOuterRadius - fInnerRadius);
    const float fScaleDepth = 0.25;
    const float fScaleOverScaleDepth = fScale / fScaleDepth;
    const int nSamples = 2;
    const float fSamples = 2.0;
    const float Kr = 0.0025;
    const float Km = 0.0015;
    const float ESun = 15.0;
    const float fKrESun = Kr * ESun;
    const float fKmESun = Km * ESun;
    const float fKr4PI = Kr * 4.0 * czm_pi;
    const float fKm4PI = Km * 4.0 * czm_pi;
    
    const float3 czm_viewerPositionWC = _auto.czm_a_viewerPositionWC;
    const float3 czm_sunDirectionWC = _auto.czm_a_sunDirectionWC;
    float3 v3InvWavelength = float3(1.0 / pow(0.650, 4.0), 1.0 / pow(0.570, 4.0), 1.0 / pow(0.475, 4.0));

    // Get the ray from the camera to the vertex and its length (which is the far point of the ray passing through the atmosphere)
    float3 v3Ray = v3Pos - czm_viewerPositionWC;
    float fFar = length(v3Ray);
    v3Ray /= fFar;

    float fCameraHeight = length(czm_viewerPositionWC);
    float fCameraHeight2 = fCameraHeight * fCameraHeight;

    // This next line is an ANGLE workaround. It is equivalent to B = 2.0 * dot(czm_viewerPositionWC, v3Ray),
    // which is what it should be, but there are problems at the poles.
    float B = 2.0 * length(_auto.czm_a_viewerPositionWC) * dot(normalize(_auto.czm_a_viewerPositionWC), v3Ray);
    float C = fCameraHeight2 - fOuterRadius2;
    float fDet = max(0.0, B*B - 4.0 * C);
    float fNear = 0.5 * (-B - sqrt(fDet));

    // Calculate the ray's starting position, then calculate its scattering offset
    float3 v3Start = czm_viewerPositionWC + v3Ray * fNear;
    fFar -= fNear;
    float fDepth = exp((fInnerRadius - fOuterRadius) / fScaleDepth);

    // The light angle based on the sun position would be:
    //    dot(czm_sunDirectionWC, v3Pos) / length(v3Pos);
    // When we want the atmosphere to be uniform over the globe so it is set to 1.0.

    float fLightAngle = branchFreeTernary(useSunLighting, dot(czm_sunDirectionWC, v3Pos) / length(v3Pos), 1.0);
    float fCameraAngle = dot(-v3Ray, v3Pos) / length(v3Pos);
    float fCameraScale = scale(fCameraAngle);
    float fLightScale = scale(fLightAngle);
    float fCameraOffset = fDepth*fCameraScale;
    float fTemp = (fLightScale + fCameraScale);

    // Initialize the scattering loop variables
    float fSampleLength = fFar / fSamples;
    float fScaledLength = fSampleLength * fScale;
    float3 v3SampleRay = v3Ray * fSampleLength;
    float3 v3SamplePoint = v3Start + v3SampleRay * 0.5;

    // Now loop through the sample rays
    float3 v3FrontColor = float3(0.0);
    float3 v3Attenuate = float3(0.0);
    for(int i=0; i<nSamples; i++)
    {
        float fHeight = length(v3SamplePoint);
        float fDepth = exp(fScaleOverScaleDepth * (fInnerRadius - fHeight));
        float fScatter = fDepth*fTemp - fCameraOffset;
        v3Attenuate = exp(-fScatter * (v3InvWavelength * fKr4PI + fKm4PI));
        v3FrontColor += v3Attenuate * (fDepth * fScaledLength);
        v3SamplePoint += v3SampleRay;
    }

    AtmosphereColor color;
    color.mie = v3FrontColor * (v3InvWavelength * fKrESun + fKmESun);
    color.rayleigh = v3Attenuate; // Calculate the attenuation factor for the ground

    return color;
}
half4 sampleAndBlend(
    half4 previousColor,
    texture2d<float, access::sample> textureToSample,
    sampler s,
    float2 tileTextureCoordinates,
    float4 textureCoordinateRectangle,
    float4 textureCoordinateTranslationAndScale//,
/*
    float textureAlpha,
    float textureBrightness,
    float textureContrast,
    float textureHue,
    float textureSaturation,
    float textureOneOverGamma,
    float split*/)
{
    // This crazy step stuff sets the alpha to 0.0 if this following condition is true:
    //    tileTextureCoordinates.s < textureCoordinateRectangle.s ||
    //    tileTextureCoordinates.s > textureCoordinateRectangle.p ||
    //    tileTextureCoordinates.t < textureCoordinateRectangle.t ||
    //    tileTextureCoordinates.t > textureCoordinateRectangle.q
    // In other words, the alpha is zero if the fragment is outside the rectangle
    // covered by this texture.  Would an actual 'if' yield better performance?
    //textureAlpha = textureAlpha * alphaMultiplier.x * alphaMultiplier.y;
    float2 alphaMultiplier = step(textureCoordinateRectangle.xy, tileTextureCoordinates);
    float textureAlpha = alphaMultiplier.x * alphaMultiplier.y;

    alphaMultiplier = step(float2(0.0), textureCoordinateRectangle.zw - tileTextureCoordinates);
    textureAlpha = textureAlpha * alphaMultiplier.x * alphaMultiplier.y;

    float2 translation = textureCoordinateTranslationAndScale.xy;
    float2 scale = textureCoordinateTranslationAndScale.zw;
    float2 textureCoordinates = tileTextureCoordinates * scale + translation;
    half4 value = half4(textureToSample.sample(s, (float2)textureCoordinates));
    half3 color = value.rgb;
    half alpha = value.a;

    half sourceAlpha = (half)((float)alpha * textureAlpha);
    half outAlpha = mix(previousColor.a, 1.0h, sourceAlpha);
    half3 outColor = mix(previousColor.rgb * previousColor.a, color, sourceAlpha) / outAlpha;
    return half4(outColor, outAlpha);
}
float4 computeDayColor(float4 initialColor, float3 textureCoordinates)
{
    float4 color = initialColor;
    return color;
}

vertex TileVertexFogOutput tileQuantizedFogVertex (
                                          TileVertexInput in [[stage_in]],
                                          constant AutomaticUniform& autoUniform [[ buffer(0) ]],
                                          constant FrustumUniform& _frustum [[buffer(1)]],
                                          constant TileUniform& tu [[buffer(2)]],
                                          constant bool & apply_flog [[buffer(5)]],
                                          constant FrustumUniform& prev_frustum [[buffer(6)]],
                                          const device TileUniform* prev_tile_uniform [[buffer(7)]],
                                          constant int& tile_index [[ buffer(8) ]]
) {
    TileVertexFogOutput out;
    //float4 _mtl_i_compressed = float4(in.compressed, 0);
    float4 _mtl_i_compressed = in.compressed;
    float2 xy = decompressTextureCoordinates(_mtl_i_compressed.x);
    float2 zh = decompressTextureCoordinates(_mtl_i_compressed.y);
    float3 position = float3(xy, zh.x);
    position = (tu.u_scaleAndBias * float4(position, 1.0)).xyz;
    //float height = zh.y;
    float encodedNormal = _mtl_i_compressed.w;
    float3 normalMC = czm_octDecode(encodedNormal);
    
    out.v_textureCoordinates = decompressTextureCoordinates(_mtl_i_compressed.z);
    out.position = _frustum.czm_f_projection * (tu.u_modifiedModelView * float4(position, 1.0));
    
    if (tile_index == 1000 || autoUniform.czm_a_frameNumber == 0) {
        out.prevPosition = out.position;
    } else {
        out.prevPosition = prev_frustum.czm_f_projection * (prev_tile_uniform[tile_index].u_modifiedModelView * float4(position, 1.0));
    }
    out.worldPosition = position + tu.u_center3D;
    out.viewPosition = (_frustum.czm_f_view * float4(out.worldPosition, 1.0)).xyz;
    //out.viewPosition = (tu.u_modifiedModelView * float4(position, 1.0)).xyz;
    //out.normal = _frustum.czm_f_normal3D * normalMC;
    out.normal = normalMC;
    //out.normal = float3(0.0, 1.0, 0.0);

    // Fog
    if (apply_flog) {
        float3 position3DWC = position + tu.u_center3D; // position in model coordinates
        AtmosphereColor atmosColor = computeGroundAtmosphereFromSpace(position3DWC, false, autoUniform);
        out.v_mieColor = atmosColor.mie;
        out.v_rayleighColor = atmosColor.rayleigh;
        out.v_distance = length((_frustum.czm_f_modelView3D * float4(position3DWC, 1.0)).xyz);
        out.fog_density = autoUniform.czm_a_fogDensity;
    }
    
    return out;
}
vertex TileVertexFogOutput tileFogVertex (
                                          TileVertexRawInput in [[stage_in]],
                                          constant AutomaticUniform& autoUniform [[ buffer(0) ]],
                                          constant FrustumUniform& _frustum [[buffer(1)]],
                                          constant TileUniform& tu [[buffer(2)]],
                                          constant bool & apply_flog [[buffer(5)]],
                                          constant FrustumUniform& prev_frustum [[buffer(6)]],
                                          const device TileUniform* prev_tile_uniform [[buffer(7)]],
                                          constant int& tile_index [[ buffer(8) ]]
                                          )
{
    TileVertexFogOutput out;
    float3 position = in.position3DAndHeight.xyz;
    //float height = in.position3DAndHeight.w;
    float2 textureCoordinates = in.textureCoordAndEncodedNormals.xy;
    float encodedNormal = in.textureCoordAndEncodedNormals.z;
    float3 normalMC = czm_octDecode(encodedNormal);
    
    out.position = _frustum.czm_f_projection * (tu.u_modifiedModelView * float4(position, 1.0));
    out.v_textureCoordinates = textureCoordinates;
    //out.v_textureCoordinates = float2(0.5);
    
    if (tile_index == 1000 || autoUniform.czm_a_frameNumber == 0) {
        out.prevPosition = out.position;
    } else {
        out.prevPosition = prev_frustum.czm_f_projection * (prev_tile_uniform[tile_index].u_modifiedModelView * float4(position, 1.0));
    }
    out.worldPosition = position + tu.u_center3D;
    out.viewPosition = (_frustum.czm_f_view * float4(out.worldPosition, 1.0)).xyz;
    //out.viewPosition = (tu.u_modifiedModelView * float4(position, 1.0)).xyz;
    //out.normal = _frustum.czm_f_normal3D * normalMC;
    out.normal = normalMC;
    //out.normal = float3(0.0, 1.0, 0.0);

    if (apply_flog) {
        float3 position3DWC = position + tu.u_center3D;
        AtmosphereColor atmosColor = computeGroundAtmosphereFromSpace(position3DWC, false, autoUniform);
        out.v_mieColor = atmosColor.mie;
        out.v_rayleighColor = atmosColor.rayleigh;
        out.v_distance = length((_frustum.czm_f_modelView3D * float4(position3DWC, 1.0)).xyz);
        out.fog_density = autoUniform.czm_a_fogDensity;
    }
    
    return out;
}


// Fragment Shader
half3 fog_color(float distanceToCamera, half3 color, half3 fogColor, float fogDensity)
{
    float scalar = distanceToCamera * fogDensity;
    float fog = 1.0 - exp(-(scalar * scalar));
    return mix(color, fogColor, fog);
}
constant vector_float2 haltonSamples[] = {
    float2(0.5f, 0.333333333333f),
    float2(0.25f, 0.666666666667f),
    float2(0.75f, 0.111111111111f),
    float2(0.125f, 0.444444444444f),
    float2(0.625f, 0.777777777778f),
    float2(0.375f, 0.222222222222f),
    float2(0.875f, 0.555555555556f),
    float2(0.0625f, 0.888888888889f),
    float2(0.5625f, 0.037037037037f),
    float2(0.3125f, 0.37037037037f),
    float2(0.8125f, 0.703703703704f),
    float2(0.1875f, 0.148148148148f),
    float2(0.6875f, 0.481481481481f),
    float2(0.4375f, 0.814814814815f),
    float2(0.9375f, 0.259259259259f),
    float2(0.03125f, 0.592592592593f),
};
// Maps two uniformly random numbers to a uniform distribution within a cone
float3 sampleCone(float2 u, float cosAngle) {
    float phi = 2.0f * M_PI_F * u.x;
    
    float cos_phi;
    float sin_phi = sincos(phi, cos_phi);
    
    float cos_theta = 1.0f - u.y + u.y * cosAngle;
    float sin_theta = sqrt(1.0f - cos_theta * cos_theta);
    
    return float3(sin_theta * cos_phi, cos_theta, sin_theta * sin_phi);
}

// Aligns a direction such that the "up" direction (0, 1, 0) maps to the given
// surface normal direction
float3 alignWithNormal(float3 sample, float3 normal) {
    // Set the "up" vector to the normal
    float3 up = normal;
    
    // Find an arbitrary direction perpendicular to the normal. This will become the
    // "right" vector.
    float3 right = normalize(cross(normal, float3(0.0072f, 1.0f, 0.0034f)));
    
    // Find a third vector perpendicular to the previous two. This will be the
    // "forward" vector.
    float3 forward = cross(right, up);
    
    // Map the direction on the unit hemisphere to the coordinate system aligned
    // with the normal.
    return sample.x * right + sample.y * up + sample.z * forward;
}
float getLambertDiffuse(float3 lightDirectionEC, float3 normalEC)
{
    return max(dot(lightDirectionEC, normalEC), 0.0);
}
float sunlitIntensity(float3 viewerPositionWC, float3 sunPositionWC) {
    float lightIntensity = dot(normalize(viewerPositionWC), normalize(sunPositionWC));
    if (lightIntensity > 0) {
        // if sun is above horizon, minimum intensity is 0.3
        lightIntensity += 0.3;
    }
    return lightIntensity;
}

half4 daylight_shading(
                       half4 color,
                       AutomaticUniform autoUniform,
                       FrustumUniform frustumUniform,
                       TileUniform tu,
                       float3 normal
                       ) {
    float diffuseIntensity = clamp(getLambertDiffuse(autoUniform.czm_a_sunDirectionEC, frustumUniform.czm_f_normal3D * normal) * 5.0 + 0.3, 0.0, 1.0);
    float cameraDist = length(frustumUniform.czm_f_view[3]);
    float fadeOutDist = tu.u_lightingFadeDistance.x;
    float fadeInDist = tu.u_lightingFadeDistance.y;
    float t = clamp((cameraDist - fadeOutDist) / (fadeInDist - fadeOutDist), 0.0, 1.0);
    diffuseIntensity = mix(1.0, diffuseIntensity, t);
    return half4(color.rgb * half(diffuseIntensity), color.a);
}
half4 fog_shading(
                half4 color,
                TileVertexFogOutput in,
                AutomaticUniform autoUniform,
                bool DAYNIGHT_SHADING
                ) {
    const float fExposure = 2.0;
    half3 fogColor = half3(in.v_mieColor) + color.rgb * half3(in.v_rayleighColor);
    fogColor = half3(1.0) - exp(-fExposure * fogColor);
    
    if (DAYNIGHT_SHADING) {
        const float minimumBrightness = 0.03;
        float lightIntensity = sunlitIntensity(autoUniform.czm_a_viewerPositionWC, autoUniform.czm_a_sunPositionWC);
        float darken = clamp(lightIntensity, minimumBrightness, 1.0);
        fogColor *= darken;
    }
    
    half3 foggy = fog_color(in.v_distance, color.rgb, fogColor, in.fog_density);
    return half4(foggy, color.a);
}
float2 compute_motion_vector(
                             TileVertexFogOutput in
                             ) {
    float2 motionVector = 0.0f;
    // TODO: set viewport into uniform or struct
    float width = 1624;
    float height = 750;
    // Map current pixel location to 0..1
    float2 uv = in.position.xy / float2(width, height);
    
    // Unproject the position from the previous frame then transform it from
    // NDC space to 0..1
    float2 prevUV = in.prevPosition.xy / in.prevPosition.w * float2(0.5f, -0.5f) + 0.5f;
    
    motionVector = uv - prevUV;
    if (any(isnan(motionVector))) {
        motionVector = float2(0.0f);
    }
    return motionVector;
}
float4 compute_depth_normal(TileVertexFogOutput in, float3 N) {
    float depth = length(in.viewPosition);
    if (isinf(depth) || depth > 10000) {
        depth = 10000;
    }
    float4 depthNormal = float4(depth, N);
    if (any(isnan(depthNormal))) {
        depthNormal = float4(1000.0, 0.0, 0.0, 0.0);
    }
    return depthNormal;
}
float4 compute_shadow_ray(
                          TileVertexFogOutput in,
                          texture2d<unsigned int, access::read> randomTexture,
                          AutomaticUniform autoUniform
                          ) {
    float3 P = in.worldPosition;
    uint2 pixel = (uint2)in.position.xy;
    unsigned int offset = randomTexture.read(pixel).x;
    // Look up two uniformly random numbers for this thread using a low discrepancy Halton
    // sequence. This sequence will ensure good coverage of the light source.
    uint frameNumber = uint(autoUniform.czm_a_frameNumber);
    float2 r = haltonSamples[(offset + frameNumber) % 16];
    float3 lightDirection = normalize(autoUniform.czm_a_sunDirectionWC);
    bool sunUp = (dot(normalize(autoUniform.czm_a_viewerPositionWC), normalize(autoUniform.czm_a_sunPositionWC)) > 0) ? true : false;
    if (!sunUp) {
        lightDirection = float3(0, 1, 0);
        lightDirection = normalize(P);
    }
    // Choose a shadow ray within a small cone aligned with the light direction.
    // This simulates a light source with some non-zero area such as the sun
    // diffused through the atmosphere or a spherical light source.
    float3 sample = sampleCone(r, cos(5.0f / 180.0f * M_PI_F));
    
    lightDirection = alignWithNormal(sample, lightDirection);
    float sunDistance = INFINITY;
    return float4(lightDirection, sunDistance);
}
struct TileFragmentOut {
    half4 color [[ color(0) ]];
    float4 originMinDistance    [[color(1)]]; // ambient occlusion
    float4 directionMaxDistance [[color(2)]]; // sun light
    float2 motionVector         [[color(3)]];
    float4 depthNormal          [[color(4)]];
};
fragment TileFragmentOut tileFragment (TileVertexFogOutput in [[stage_in]],
                             constant AutomaticUniform& autoUniform [[ buffer(0) ]],
                             constant FrustumUniform& _frustum [[buffer(1)]],
                             constant TileUniform& tu [[buffer(2)]],
                             constant Material& material [[ buffer(3) ]],
                             constant bool & apply_fog [[buffer(4)]]
)
{
    uint textureCount = material.textureCount;
    half4 color = half4(tu.u_initialColor);

    for (uint i = 0; i < textureCount; i++) {
        texture2d<float, access::sample> textureToSample;
        sampler s;
        if (i == 0) {
            textureToSample = material.texture_0;
            s = material.sampler_0;
        } else if (i == 1) {
            textureToSample = material.texture_1;
            s = material.sampler_1;
        } else if (i == 2) {
            textureToSample = material.texture_2;
            s = material.sampler_2;
        }
        color = sampleAndBlend(color, textureToSample, s, in.v_textureCoordinates, tu.u_dayTextureTexCoordsRectangle[i], tu.u_dayTextureTranslationAndScale[i]);
    }
    
    // DAYNIGHT_SHADING
    bool DAYNIGHT_SHADING = true;
    half4 finalColor;
    if (DAYNIGHT_SHADING) {
        finalColor = daylight_shading(color, autoUniform, _frustum, tu, in.normal);
    } else {
        finalColor = color;
    }

    if (apply_fog) {
        finalColor = fog_shading(finalColor, in, autoUniform, DAYNIGHT_SHADING);
    }
    
    TileFragmentOut out;
    out.color = finalColor;
    
    // shadow ray
    // The rasterizer will have interpolated the world space position and normal for the fragment
    float3 P = in.worldPosition;
    float3 N = normalize(in.normal);

    float2 motionVector = 0.0f;
    // Compute motion vectors
    if (autoUniform.czm_a_frameNumber > 0) {
        motionVector = compute_motion_vector(in);
    }
    out.motionVector = motionVector;
    
    out.depthNormal = compute_depth_normal(in, N);
    
    out.originMinDistance = float4(P + N * 1e-3f, 0.0f);
    out.directionMaxDistance = compute_shadow_ray(in, material.random_texture, autoUniform);
    
    return out;
}


struct WireOut {
    float4 position [[ position ]];
    half4 color;
};
vertex WireOut wire_vertex(
                           unsigned int vid [[ vertex_id ]],
                           const device float3* verticies [[ buffer(0) ]],
                           constant FrustumUniform & frustum [[ buffer(1) ]],
                           constant float4x4 & transform [[ buffer(2) ]]
                           ) {
    WireOut out;
    
    float4 positionWC = transform * float4(verticies[vid], 1.0);
    out.position = frustum.czm_f_modelViewProjection * positionWC;
    out.color = half4( half3(1,1,1), 0.1);
    
    return out;
}
fragment half4 wire_fragment(
                             WireOut in [[stage_in]]
                             ) {
    return in.color;
}

fragment TileFragmentOut tileDirectFragment (TileVertexFogOutput in [[stage_in]],
                                             constant AutomaticUniform& autoUniform [[ buffer(0) ]],
                                             constant FrustumUniform& _frustum [[buffer(1)]],
                                             constant TileUniform& tu [[buffer(2)]],
                                             constant uint & textureCount [[ buffer(3) ]],
                                             constant bool & apply_fog [[buffer(4)]],
                                             texture2d<float> material_texture_0 [[texture(0)]], sampler material_sampler_0 [[sampler(0)]],
                                             texture2d<float> material_texture_1 [[texture(1)]],
                                             texture2d<float> material_texture_2 [[texture(2)]],
                                             texture2d<unsigned int, access::read> randomTexture [[ texture(3) ]]
)
{
    half4 color = half4(tu.u_initialColor);
    for (uint i=0; i < textureCount; i++) {
        texture2d<float, access::sample> textureToSample;
        sampler s = material_sampler_0;
        if (i == 0) {
            textureToSample = material_texture_0;
        } else if (i == 1) {
            textureToSample = material_texture_1;
        } else if (i == 2) {
            textureToSample = material_texture_2;
        }
        color = sampleAndBlend(color, textureToSample, s, in.v_textureCoordinates, tu.u_dayTextureTexCoordsRectangle[i], tu.u_dayTextureTranslationAndScale[i]);
    }

    // DAYNIGHT_SHADING
    bool DAYNIGHT_SHADING = true;
    half4 finalColor;
    if (DAYNIGHT_SHADING) {
        finalColor = daylight_shading(color, autoUniform, _frustum, tu, in.normal);
    } else {
        finalColor = color;
    }
    
    if (apply_fog) {
        finalColor = fog_shading(finalColor, in, autoUniform, DAYNIGHT_SHADING);
    }
    
    TileFragmentOut out;
    out.color = finalColor;
    
    // shadow ray
    // The rasterizer will have interpolated the world space position and normal for the fragment
    float3 P = in.worldPosition;
    float3 N = normalize(in.normal);

    float2 motionVector = 0.0f;
    // Compute motion vectors
    if (autoUniform.czm_a_frameNumber > 0) {
        motionVector = compute_motion_vector(in);
    }
    out.motionVector = motionVector;
    
    out.depthNormal = compute_depth_normal(in, N);

    out.originMinDistance = float4(P + N * 1e-3f, 0.0f);
    out.directionMaxDistance = compute_shadow_ray(in, randomTexture, autoUniform);
    return out;
}

fragment TileFragmentOut debug_fragment (TileVertexFogOutput in [[stage_in]],
                             constant AutomaticUniform& autoUniform [[ buffer(0) ]],
                             constant FrustumUniform& _frustum [[buffer(1)]],
                             constant TileUniform& tu [[buffer(2)]],
                             constant Material& material [[ buffer(3) ]],
                             constant bool & apply_fog [[buffer(4)]]
)
{
    uint textureCount = material.textureCount;
    half4 color = half4(tu.u_initialColor);

    for (uint i = 0; i < textureCount; i++) {
        texture2d<float, access::sample> textureToSample;
        sampler s;
        if (i == 0) {
            textureToSample = material.texture_0;
            s = material.sampler_0;
        } else if (i == 1) {
            textureToSample = material.texture_1;
            s = material.sampler_1;
        } else if (i == 2) {
            textureToSample = material.texture_2;
            s = material.sampler_2;
        }
        color = sampleAndBlend(color, textureToSample, s, in.v_textureCoordinates, tu.u_dayTextureTexCoordsRectangle[i], tu.u_dayTextureTranslationAndScale[i]);
    }
    
    // DAYNIGHT_SHADING
    bool DAYNIGHT_SHADING = false;
    half4 finalColor;
    if (DAYNIGHT_SHADING) {
        finalColor = daylight_shading(color, autoUniform, _frustum, tu, in.normal);
    } else {
        finalColor = color;
    }

    if (apply_fog) {
        finalColor = fog_shading(finalColor, in, autoUniform, DAYNIGHT_SHADING);
    }

    TileFragmentOut out;
    out.color = finalColor;
    out.motionVector = float(0.0);
    
    out.depthNormal = float4(0.0);

    out.originMinDistance = float4(0.0);
    out.directionMaxDistance = float4(0.0);
    
    return out;
}
