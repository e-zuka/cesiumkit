//
//  Sun.metal
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2019/10/28.
//  Copyright © 2019 Test Toast. All rights reserved.
//

#include <metal_stdlib>
#import <simd/simd.h>
using namespace metal;

struct AutomaticUniform {
    float3x3    czm_a_viewRotation;
    float3x3    czm_a_temeToPseudoFixed;
    float3 czm_a_sunDirectionEC;
    float3 czm_a_sunDirectionWC;
    float3 czm_a_moonDirectionEC;
    float3 czm_a_viewerPositionWC;
    float3 czm_a_sunPositionWC;
    float3 czm_a_sunPositionColumbusView;
    float czm_a_morphTime;
    float czm_a_fogDensity;
    float czm_a_frameNumber;
};
struct FrustumUniform
{
    float4x4 czm_f_viewportOrthographic;
    float4x4 czm_f_viewportTransformation;
    float4x4 czm_f_projection;
    float4x4 czm_f_inverseProjection;
    float4x4 czm_f_view;
    float4x4 czm_f_modelView;
    float4x4 czm_f_modelView3D;
    float4x4 czm_f_inverseModelView;
    float4x4 czm_f_modelViewProjection;
    float4 czm_f_viewport;
    float3x3 czm_f_normal;
    float3x3 czm_f_normal3D;
    float2 czm_f_entireFrustum;
};


// Sun Compute
//uniform float u_glowLengthTS;

float2 rotate(float2 p, float2 direction)
{
    return float2(p.x * direction.x - p.y * direction.y, p.x * direction.y + p.y * direction.x);
}

float4 addBurst(float2 position, float2 direction)
{
    float2 rotatedPosition = rotate(position, direction) * float2(25.0, 0.75);
    float radius = length(rotatedPosition);
    float burst = 1.0 - smoothstep(0.0, 0.55, radius);

    return burst;
}

kernel void sunTextureCompute(
                               uint2        v_textureCoordinates         [[thread_position_in_grid]],
                               texture2d<float, access::write> outTexture [[texture(0)]],
                               constant float & u_radiusTS               [[ buffer(0) ]],
                               constant float & size                    [[ buffer(1) ]]
                               )
{
    float2 position = float2(v_textureCoordinates) / size - 0.5;
    float radius = length(position);
    float surface = step(radius, u_radiusTS);
    float4 color = float4(1.0, 1.0, surface + 0.2, surface);

    float glow = 1.0 - smoothstep(0.0, 0.55, radius);
    color.ba += mix(float2(0.0), float2(1.0), glow) * 0.75;

    float4 burst = float4(0.0);

    // The following loop has been manually unrolled for speed, to
    // avoid sin() and cos().
    //
    //for (float i = 0.4; i < 3.2; i += 1.047) {
    //    vec2 direction = vec2(sin(i), cos(i));
    //    burst += 0.4 * addBurst(position, direction);
    //
    //    direction = vec2(sin(i - 0.08), cos(i - 0.08));
    //    burst += 0.3 * addBurst(position, direction);
    //}

    burst += 0.4 * addBurst(position, float2(0.38942,  0.92106));  // angle == 0.4
    burst += 0.4 * addBurst(position, float2(0.99235,  0.12348));  // angle == 0.4 + 1.047
    burst += 0.4 * addBurst(position, float2(0.60327, -0.79754));  // angle == 0.4 + 1.047 * 2.0

    burst += 0.3 * addBurst(position, float2(0.31457,  0.94924));  // angle == 0.4 - 0.08
    burst += 0.3 * addBurst(position, float2(0.97931,  0.20239));  // angle == 0.4 + 1.047 - 0.08
    burst += 0.3 * addBurst(position, float2(0.66507, -0.74678));  // angle == 0.4 + 1.047 * 2.0 - 0.08

    // End of manual loop unrolling.

    color += clamp(burst, float4(0.0), float4(1.0)) * 0.15;
    
    outTexture.write(clamp(color, float4(0.0), float4(1.0)), v_textureCoordinates);
}


// Sun Vertex Shader
struct SunVertexIn {
    float2 direction [[ attribute(0) ]];
    float4 position  [[ attribute(1) ]];
};
struct SunVertexOut {
    float4 position [[ position ]];
    float2 v_textureCoordinates;
};

float4 czm_eyeToWindowCoordinates(float4 positionEC, FrustumUniform u)
{
    float4 q = u.czm_f_projection * positionEC;                        // clip coordinates
    q.xyz /= q.w;                                                // normalized device coordinates
    q.xyz = (u.czm_f_viewportTransformation * float4(q.xyz, 1.0)).xyz; // window coordinates
    return q;
}

vertex SunVertexOut sunVertexShader(
                                    SunVertexIn in [[ stage_in ]],
                                    constant float & u_size [[ buffer(0) ]]
                                    // buffer(1) == texCoordBuffer
                                    // buffer(2) == vertexBuffer
                                    )
{
    SunVertexOut out;
    /*
    float4 position;
    if (autoUniform.czm_a_morphTime == 1.0)
    {
        position = float4(autoUniform.czm_a_sunPositionWC, 1.0);
    }
    else
    {
        //position = float4(autoUniform.czm_a_sunPositionColumbusView.zxy, 1.0);
        position = float4(autoUniform.czm_a_sunPositionWC, 1.0);
    }
    
    float4 positionEC = frustumUniform.czm_f_view * position;
    float4 positionWC = czm_eyeToWindowCoordinates(positionEC, frustumUniform);
    
    float2 halfSize = float2(u_size * 0.5);
    halfSize *= ((in.direction * 2.0) - 1.0);
    
    float4 p = frustumUniform.czm_f_viewportOrthographic * float4(positionWC.xy + halfSize, -positionWC.z, 1.0);

    //float4x4 viewportOrtho = float4x4(0.0012315271, 0.0, 0.0, 0.0, 0.0, 0.0026666666, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, -1.0, -1.0, 0.0, 1.0);
    //float4 p = viewportOrtho * float4(positionWC.xy + halfSize, -positionWC.z, 1.0);
    p.z = clamp(p.z, 0.0, 1.0);
    out.position = p;
    //out.position = float4(in.direction.x * 0.08 - 0.04, in.direction.y * 0.18 + 0.74, 0.0, 1.0);

    out.v_textureCoordinates = in.direction;
     */
    
    out.position = in.position;
    out.position.z = clamp(out.position.z, 0.0, 1.0);
    out.v_textureCoordinates = in.direction;
    return out;
}

// Sun Fragment Shader
float3 czm_RGBToXYZ(float3 rgb)
{
    const float3x3 RGB2XYZ = float3x3(0.4124, 0.2126, 0.0193,
                              0.3576, 0.7152, 0.1192,
                              0.1805, 0.0722, 0.9505);
    float3 xyz = RGB2XYZ * rgb;
    float3 Yxy;
    Yxy.r = xyz.g;
    float temp = dot(float3(1.0), xyz);
    Yxy.gb = xyz.rg / temp;
    return Yxy;
}
float3 czm_XYZToRGB(float3 Yxy)
{
    const float3x3 XYZ2RGB = float3x3( 3.2405, -0.9693,  0.0556,
                              -1.5371,  1.8760, -0.2040,
                              -0.4985,  0.0416,  1.0572);
    float3 xyz;
    xyz.r = Yxy.r * Yxy.g / Yxy.b;
    xyz.g = Yxy.r;
    xyz.b = Yxy.r * (1.0 - Yxy.g - Yxy.b) / Yxy.b;
    
    return XYZ2RGB * xyz;
}
float key(float avg)
{
    float guess = 1.5 - (1.5 / (avg * 0.1 + 1.0));
    return max(0.0, guess) + 0.1;
}
fragment float4 sunFragmentShader(
                                 SunVertexOut in [[ stage_in ]],
                                 texture2d<float, access::sample>    texture    [[ texture(0) ]]
                                 )
{
    constexpr sampler s(filter::nearest);
    in.v_textureCoordinates.y = 1.0 - in.v_textureCoordinates.y;
    //return float4(0, 1, 0, 1);
    return texture.sample(s, in.v_textureCoordinates);
}

// bright pass
kernel void sunBrightCompute(
                             texture2d<float, access::read> source [[ texture(0) ]],
                             texture2d<float, access::write> dest [[ texture(1) ]],
                             uint2 gid [[ thread_position_in_grid ]]
                             ) {
    const float avgLuminance = 0.5;
    const float threshold = 0.25;
    const float offset = 0.1;
    float4 source_color = source.read(gid);
    float3 xyz = czm_RGBToXYZ(source_color.rgb);
    float luminance = xyz.r;
    
    float scaledLum = key(avgLuminance) * luminance / avgLuminance;
    float brightLum = max(scaledLum - threshold, 0.0);
    float brightness = brightLum / (offset + brightLum);
    
    xyz.r = brightness;
    dest.write(float4(czm_XYZToRGB(xyz), 1.0), gid);
}

// Sun bloom blend compute
kernel void sunBloomCompute(
  texture2d<float, access::read> source [[ texture(0) ]],
  texture2d<float, access::read> mask [[ texture(1) ]],
  texture2d<float, access::write> dest [[ texture(2) ]],
  uint2 gid [[ thread_position_in_grid ]])
{
  float4 source_color = source.read(gid);
  float4 mask_color = mask.read(gid);
  float4 result_color = source_color + mask_color;

  dest.write(result_color, gid);
}
