//
//  ShaderType.h
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/10/07.
//  Copyright © 2019 Test Toast. All rights reserved.
//
#ifndef ShaderType_h
#define ShaderType_h

#ifdef __METAL_VERSION__
#define NS_ENUM(_type, _name) enum _name : _type _name; enum _name : _type
#define NSInteger metal::int32_t
#else
#import <Foundation/Foundation.h>
#endif

typedef NS_ENUM(NSInteger, CesiumEncoderBufferIndex) {
    CesiumEncoderBufferIndexAutomaticUniform = 0,
    CesiumEncoderBufferIndexFrustumUniform,
    CesiumEncoderBufferIndexTileObject,
    CesiumEncoderBufferIndexMesh,
    CesiumEncoderBufferIndexMaterial,
    CesiumEncoderBufferIndexObject,
    CesiumEncoderBufferIndexICB
};

typedef NS_ENUM(NSInteger, CesiumMaterialBufferIndex) {
    CesiumMaterialBufferIndexTexture_0 = 0,
    CesiumMaterialBufferIndexTexture_1,
    CesiumMaterialBufferIndexTexture_2,
    CesiumMaterialBufferIndexSampler_0,
    CesiumMaterialBufferIndexSampler_1,
    CesiumMaterialBufferIndexSampler_2,
    CesiumMaterialBufferIndexTextureCount,
    CesiumMaterialBufferIndexPipelineState,
    CesiumMaterialBufferIndexRandomTexture
};
typedef NS_ENUM(NSInteger, CesiumMeshBufferIndex) {
    CesiumMeshBufferIndexVertexBuffer = 3,
    CesiumMeshBufferIndexTexCoord,
    CesiumMeshBufferIndexIndexBuffer,
    CesiumMeshBufferIndexIndexCount,
    CesiumMeshBufferIndexTileUniform
};

typedef NS_ENUM(NSInteger, CesiumObjectBufferIndex) {
    CesiumObjectBufferIndexMesh = 0
};

// Buffer index values shared between the compute kernel and C code
typedef NS_ENUM(NSInteger, CesiumKernelBufferIndex)
{
    CesiumKernelBufferIndexAutomaticUniform = 0,
    CesiumKernelBufferIndexFrustumUniform,
    CesiumKernelBufferIndexTileObject,
    CesiumKernelBufferIndexMaterial,
    CesiumKernelBufferIndexCommandBufferContainer,
    CesiumKernelBufferIndexQuantizedMesh,
    CesiumKernelBufferIndexMesh,
    CesiumKernelBufferIndexTileCount,
    CesiumKernelBufferIndexTileOffset,
    CesiumKernelBufferIndexPreviousFrustum,
    CesiumKernelBufferIndexPreviousTileUniform
};

typedef NS_ENUM(NSInteger, CesiumTileBufferIndex) {
    CesiumTileBufferIndexTileUniform = 0,
    CesiumTileBufferIndexVertexAttributeCount,
    CesiumTileBufferIndexFog,
    CesiumTileBufferIndexPreviousIndex
};

// Argument buffer ID for the ICB encoded by the compute kernel
typedef NS_ENUM(NSInteger, CesiumArgumentBufferID)
{
    CesiumArgumentBufferIDCommandBuffer = 0
};

#endif /* ShaderType_h */
