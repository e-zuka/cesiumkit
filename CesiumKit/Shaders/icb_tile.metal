//
//  icb_tile.metal
//  CesiumKitTests
//
//  Created by 飯塚淳 on 2019/10/08.
//  Copyright © 2019 Test Toast. All rights reserved.
//

#include <metal_stdlib>
//#include <metal_command_buffer>
using namespace metal;

#include "ShaderType.h"

typedef struct TileUniform {
    float4 u_dayTextureTexCoordsRectangle[31];
    float4 u_dayTextureTranslationAndScale[31];
    float u_dayTextureAlpha [31];
    float u_dayTextureBrightness [31];
    float u_dayTextureContrast [31];
    float u_dayTextureHue [31];
    float u_dayTextureSaturation [31];
    float u_dayTextureOneOverGamma [31];
    float2 u_minMaxHeight;
    float4x4 u_scaleAndBias;
    float4 u_waterMaskTranslationAndScale;
    float4 u_initialColor;
    float4 u_tileRectangle;
    float4x4 u_modifiedModelView;
    float3 u_center3D;
    float2 u_southMercatorYAndOneOverHeight;
    float2 u_southAndNorthLatitude;
    float2 u_lightingFadeDistance;
    float u_zoomedOutOceanSpecularIntensity;
} TileUniform;

struct AutomaticUniform {
    float3x3    czm_a_viewRotation;
    float3x3    czm_a_temeToPseudoFixed;
    float3 czm_a_sunDirectionEC;
    float3 czm_a_sunDirectionWC;
    float3 czm_a_moonDirectionEC;
    float3 czm_a_viewerPositionWC;
    float3 czm_a_sunPositionWC;
    float3 czm_a_sunPositionColumbusView;
    float czm_a_morphTime;
    float czm_a_fogDensity;
    float czm_a_frameNumber;
};


struct FrustumUniform
{
    float4x4 czm_f_viewportOrthographic;
    float4x4 czm_f_viewportTransformation;
    float4x4 czm_f_projection;
    float4x4 czm_f_inverseProjection;
    float4x4 czm_f_view;
    float4x4 czm_f_modelView;
    float4x4 czm_f_modelView3D;
    float4x4 czm_f_inverseModelView;
    float4x4 czm_f_modelViewProjection;
    float4 czm_f_viewport;
    float3x3 czm_f_normal;
    float3x3 czm_f_normal3D;
    float2 czm_f_entireFrustum;
};


struct TileObject {
    constant TileUniform *tileUniform           [[ id(CesiumTileBufferIndexTileUniform) ]];
    ushort vertexAttributeCount                 [[ id(CesiumTileBufferIndexVertexAttributeCount) ]];
    ushort fog                                  [[ id(CesiumTileBufferIndexFog) ]];
    int previousIndex                           [[ id(CesiumTileBufferIndexPreviousIndex) ]];
};


struct ICBContainer
{
    command_buffer commandBuffer [[ id(CesiumArgumentBufferIDCommandBuffer) ]];
};

struct QuantizedMesh {
    //device packed_float3 *vertexBuffer          [[ id(CesiumMeshBufferIndexVertexBuffer) ]];
    device float4 *vertexBuffer          [[ id(CesiumMeshBufferIndexVertexBuffer) ]];
    device ushort *indexBuffer                  [[ id(CesiumMeshBufferIndexIndexBuffer) ]];
    uint indexCount                             [[ id(CesiumMeshBufferIndexIndexCount) ]];
};
struct Mesh {
    //device float4 *vertexBuffer                 [[ id(CesiumMeshBufferIndexVertexBuffer) ]];
    device float *vertexBuffer                 [[ id(CesiumMeshBufferIndexVertexBuffer) ]];
    device float3 *texCoord                     [[ id(CesiumMeshBufferIndexTexCoord) ]];
    device ushort *indexBuffer                  [[ id(CesiumMeshBufferIndexIndexBuffer) ]];
    uint indexCount                             [[ id(CesiumMeshBufferIndexIndexCount) ]];
};
struct Material {
    texture2d<float> texture_0                  [[ id(CesiumMaterialBufferIndexTexture_0) ]];
    texture2d<float> texture_1                  [[ id(CesiumMaterialBufferIndexTexture_1) ]];
    texture2d<float> texture_2                  [[ id(CesiumMaterialBufferIndexTexture_2) ]];
    sampler sampler_0                           [[ id(CesiumMaterialBufferIndexSampler_0) ]];
    sampler sampler_1                           [[ id(CesiumMaterialBufferIndexSampler_1) ]];
    sampler sampler_2                           [[ id(CesiumMaterialBufferIndexSampler_2) ]];
    uint textureCount                           [[ id(CesiumMaterialBufferIndexTextureCount) ]];
    render_pipeline_state pipeline              [[ id(CesiumMaterialBufferIndexPipelineState) ]];
    texture2d<uint, access::read> random_texture              [[ id(CesiumMaterialBufferIndexRandomTexture) ]];
};

kernel void
icb_tile(uint       tid           [[ thread_position_in_grid ]],
         constant   AutomaticUniform      & automaticUniform    [[ buffer(CesiumKernelBufferIndexAutomaticUniform) ]],
         constant   FrustumUniform        & frustumUniform      [[ buffer(CesiumKernelBufferIndexFrustumUniform) ]],
         const device     TileObject      *tile_object          [[ buffer(CesiumKernelBufferIndexTileObject) ]],
         const device     Material        *material             [[ buffer(CesiumKernelBufferIndexMaterial) ]],
         const device   QuantizedMesh     *quantized_mesh       [[ buffer(CesiumKernelBufferIndexQuantizedMesh) ]],
         const device   Mesh              *mesh                 [[ buffer(CesiumKernelBufferIndexMesh) ]],
         constant     ICBContainer        & icb_container       [[ buffer(CesiumKernelBufferIndexCommandBufferContainer) ]],
         constant  ushort                   & tileCount           [[ buffer(CesiumKernelBufferIndexTileCount) ]],
         constant  ushort                   & tileOffset          [[ buffer(CesiumKernelBufferIndexTileOffset) ]],
         const device   FrustumUniform    *prev_frsutum           [[ buffer(CesiumKernelBufferIndexPreviousFrustum) ]],
         const device   TileUniform       *prev_tile_uniform     [[ buffer(CesiumKernelBufferIndexPreviousTileUniform) ]]
         )
{
    /*
    if (tid >= tileCount) {
        return;
    }
     */
    uint objectIndex = tileOffset + tid;
    const device TileObject &tile = tile_object[objectIndex];
    const device Material &_material = material[objectIndex];
    /*
    constant TileUniform *a = tile.tileUniform;
    float4 b = a->u_modifiedModelView * a->u_initialColor;
    b = b * 3;
     */
    render_command cmd(icb_container.commandBuffer, objectIndex);
    
    cmd.set_render_pipeline_state(_material.pipeline);

    cmd.set_vertex_buffer(&automaticUniform, 0);
    cmd.set_vertex_buffer(&frustumUniform, 1);
    cmd.set_vertex_buffer(tile.tileUniform, 2);
    device ushort * indexBuffer;
    uint indexCount;
    if (tile.vertexAttributeCount <= 1) {
        const device QuantizedMesh &_mesh = quantized_mesh[objectIndex];
        cmd.set_vertex_buffer(_mesh.vertexBuffer, 3);
        indexBuffer = _mesh.indexBuffer;
        indexCount = _mesh.indexCount;
    }
    if (tile.vertexAttributeCount == 2) {
        const device Mesh &_mesh = mesh[objectIndex];
        cmd.set_vertex_buffer(_mesh.vertexBuffer, 3);
        //cmd.set_vertex_buffer(_mesh.vertexBuffer, 3);
        //cmd.set_vertex_buffer(_mesh.texCoord, 4);
        indexBuffer = _mesh.indexBuffer;
        indexCount = _mesh.indexCount;
    }
    cmd.set_vertex_buffer(&tile.fog, 5);
    cmd.set_vertex_buffer(prev_frsutum, 6);
    cmd.set_vertex_buffer(prev_tile_uniform, 7);
    cmd.set_vertex_buffer(&tile.previousIndex, 8);
    
    cmd.set_fragment_buffer(&automaticUniform, 0);
    cmd.set_fragment_buffer(&frustumUniform, 1);
    cmd.set_fragment_buffer(tile.tileUniform, 2);
    cmd.set_fragment_buffer(&_material, 3);
    cmd.set_fragment_buffer(&tile.fog, 4);

    cmd.draw_indexed_primitives(metal::primitive_type::triangle, indexCount, indexBuffer, 1);
}
