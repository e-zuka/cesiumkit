//
//  TileAvailability.swift
//  CesiumKitTests
//
//  Created by 飯塚淳 on 2018/07/04.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation

class TileAvailability {
    var _tilingScheme: TilingScheme
    var _maximumLevel: Int
    var _rootNodes: [QuadtreeNode]
    
    init(tilingScheme: TilingScheme, maximumLevel: Int) {
        self._tilingScheme = tilingScheme
        self._maximumLevel = maximumLevel
        
        self._rootNodes = []
    }
    
    private func findNode(level: Int, x: Int, y: Int, nodes: [QuadtreeNode]) -> Bool {
        for node in nodes {
            if (node.x == x && node.y == y && node.level == level) {
                return true
            }
        }
        return false
    }
    
    private func rectanglesOverlap(recA: Rectangle, recB: Rectangle) -> Bool {
        let west = max(recA.west, recB.west)
        let south = max(recA.south, recB.south)
        let east = min(recA.east, recB.east)
        let north = min(recA.north, recB.north)
        return south < north && west < east
    }
    private func rectangleFullyContainsRectangle(potentialContainer: Rectangle, rectangleToTest: RectangleWithLevel) -> Bool {
        return rectangleToTest.west >= potentialContainer.west &&
            rectangleToTest.east <= potentialContainer.east &&
            rectangleToTest.south >= potentialContainer.south &&
            rectangleToTest.north <= potentialContainer.north
    }
    private func putRectangleInQuadtree(maxDepth: Int, node: QuadtreeNode, rectangle: RectangleWithLevel) {
        var _node = node
        while _node.level < maxDepth {
            if rectangleFullyContainsRectangle(potentialContainer: _node.nw.extent, rectangleToTest: rectangle) {
                _node = _node.nw
            } else if rectangleFullyContainsRectangle(potentialContainer: _node.ne.extent, rectangleToTest: rectangle) {
                _node = _node.ne
            } else if rectangleFullyContainsRectangle(potentialContainer: _node.sw.extent, rectangleToTest: rectangle) {
                _node = _node.sw
            } else if rectangleFullyContainsRectangle(potentialContainer: _node.se.extent, rectangleToTest: rectangle) {
                _node = _node.se
            } else {
                break
            }
        }
        
        if _node.rectangles.count == 0 || _node.rectangles[_node.rectangles.count - 1].level <= rectangle.level {
            _node.rectangles.append(rectangle)
        } else {
            // Maintain ordering by level when inserting.
            var index = _node.rectangles.binarySearch(rectangle, comparator: { a, b -> Int in
                return a.level - b.level
            })
            if index <= 0 {
                index = ~index
            }
            _node.rectangles.insert(rectangle, at: index)
        }
    }
    private func rectangleContainsPosition(potentialContainer: Rectangle, positionToTest: Cartographic) -> Bool {
        return positionToTest.longitude >= potentialContainer.west &&
            positionToTest.longitude <= potentialContainer.east &&
            positionToTest.latitude >= potentialContainer.south &&
            positionToTest.latitude <= potentialContainer.north
    }
    private func findMaxLevelFromNode(stopNode: QuadtreeNode!, node _node: QuadtreeNode, position: Cartographic) -> Int {
        var node: QuadtreeNode! = _node
        var maxLevel = 0
        var found = false
        
        while found == false {
            let nw = (node._nw != nil) && rectangleContainsPosition(potentialContainer: node._nw!.extent, positionToTest: position)
            let ne = (node._ne != nil) && rectangleContainsPosition(potentialContainer: node._ne!.extent, positionToTest: position)
            let sw = (node._sw != nil) && rectangleContainsPosition(potentialContainer: node._sw!.extent, positionToTest: position)
            let se = (node._se != nil) && rectangleContainsPosition(potentialContainer: node._se!.extent, positionToTest: position)
            
            let nw_int = nw == true ? 1 : 0
            let ne_int = ne == true ? 1 : 0
            let sw_int = sw == true ? 1 : 0
            let se_int = se == true ? 1 : 0
            // The common scenario is that the point is in only one quadrant and we can simply
            // iterate down the tree.  But if the point is on a boundary between tiles, it is
            // in multiple tiles and we need to check all of them, so use recursion.
            if nw_int + ne_int + sw_int + se_int > 1 {
                if nw == true {
                    maxLevel = max(maxLevel, findMaxLevelFromNode(stopNode: node, node: node._nw!, position: position))
                }
                if ne == true {
                    maxLevel = max(maxLevel, findMaxLevelFromNode(stopNode: node, node: node._ne!, position: position))
                }
                if sw == true {
                    maxLevel = max(maxLevel, findMaxLevelFromNode(stopNode: node, node: node._sw!, position: position))
                }
                if se == true {
                    maxLevel = max(maxLevel, findMaxLevelFromNode(stopNode: node, node: node._se!, position: position))
                }
                break
            } else if nw == true {
                node = node._nw!
            } else if ne == true {
                node = node._ne!
            } else if sw == true {
                node = node._sw!
            } else if se == true {
                node = node._se!
            } else {
                found = true
            }
        }

        // Work up the tree until we find a rectangle that contains this point.
        while node != stopNode {
            let rectangles = node.rectangles.reversed()
            
            // Rectangles are sorted by level, lowest first.
            for rectangle in rectangles {
                if rectangle.level <= maxLevel {
                    break
                }
                if rectangleContainsPosition(potentialContainer: rectangle.rectangle, positionToTest: position) {
                    maxLevel = rectangle.level
                }
            }
            node = node.parent
        }

        return maxLevel
    }

    func addAvailableTileRange(level: Int, startX: Int, startY: Int, endX: Int, endY: Int) {
        let tilingScheme = _tilingScheme
        
        if level == 0 {
            for y in startY...endY {
                for x in startX...endX {
                    if findNode(level: level, x: x, y: y, nodes: _rootNodes) == false {
                        _rootNodes.append(QuadtreeNode(tilingScheme: tilingScheme, parent: nil, level: 0, x: x, y: y))
                    }
                }
            }
        }
        
        let startRectangle = tilingScheme.tileXYToRectangle(x: startX, y: startY, level: level)
        let west = startRectangle.west
        let north = startRectangle.north
        
        let endRectangle = tilingScheme.tileXYToRectangle(x: endX, y: endY, level: level)
        let east = endRectangle.east
        let south = endRectangle.south
        
        let rectangleWithLevel = RectangleWithLevel(west: west, south: south, east: east, north: north, level: level)
        
        for rootNode in _rootNodes {
            if rectanglesOverlap(recA: rootNode.extent, recB: rectangleWithLevel.rectangle) {
                putRectangleInQuadtree(maxDepth: _maximumLevel, node: rootNode, rectangle: rectangleWithLevel)
            }
        }
    }
    
    func computeMaximumLevelAtPosition(position: Cartographic) -> Int {
        var node: QuadtreeNode? = nil
        for rootNode in self._rootNodes {
            if rectangleContainsPosition(potentialContainer: rootNode.extent, positionToTest: position) {
                node = rootNode
                break
            }
        }
        if node == nil {
            return -1
        }
        return findMaxLevelFromNode(stopNode: nil, node: node!, position: position)
    }
    
    func isTileAvailable(level: Int, x: Int, y: Int) -> Bool {
        let rectangle = _tilingScheme.tileXYToRectangle(x: x, y: y, level: level)
        let center = rectangle.center
        return computeMaximumLevelAtPosition(position: center) >= level
    }
    
    func computeChildMaskForTile(level: Int, x: Int, y: Int) -> Int {
        let childLevel = level + 1
        if childLevel >= _maximumLevel {
            return 0
        }
        
        var mask = 0
        
        mask |= isTileAvailable(level: childLevel, x: 2 * x, y: 2 * y + 1) ? 1 : 0
        mask |= isTileAvailable(level: childLevel, x: 2 * x + 1, y: 2 * y + 1) ? 2 : 0
        mask |= isTileAvailable(level: childLevel, x: 2 * x, y: 2 * y) ? 4 : 0
        mask |= isTileAvailable(level: childLevel, x: 2 * x + 1, y: 2 * y) ? 8 : 0
        
        return mask
    }
}
