//
//  Iau2006XysData.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/11/29.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

struct IAU2006XYS_JSON: Codable {
    var version: String
    var updated: String
    var interpolationOrder: Int
    var xysAlgorithm: String
    var sampleZeroJulianEphemerisDate: Double
    var stepSizeDays: Int
    var startIndex: Int
    var numberOfSamples: Int
    var samples: [Double]
}

struct Iau2006XysSample {
    var x: Double
    var y: Double
    var s: Double
}

class Iau2006XysData {
    private let xysFileUrlTemplate = "IAU2006_XYS"
    private var interpolationOrder = 9
    private let sampleZeroJulianEphemerisDate = 2442396.5
    private let sampleZeroDateTT = JulianDate(julianDayNumber: 2442396.5, secondsOfDay: 0.0, timeStandard: .tai)
    private let stepSizeDays = 1.0
    private let samplesPerXysFile = 1000
    private let totalSamples = 27426
    private var samples = [Iau2006XysSample?](repeating: nil, count: 27426/* * 3*/)
    private var denominators: [Double] = []
    private var xTable: [Double] = []
    private var work: [Double] = []
    private var coef: [Double] = []
    
    init() {
        let order = interpolationOrder
        
        // Compute denominators and X values for interpolation.
        let stepN = pow(stepSizeDays, Double(order))
        for i in 0...order {
            denominators.append(stepN)
            //denominators[i] = stepN
            xTable.append(Double(i) * stepSizeDays)
            //xTable[i] = Double(i) * stepSizeDays
            
            for j in 0...order {
                if j != i {
                    denominators[i] *= Double(i - j)
                }
            }
            
            denominators[i] = 1.0 / denominators[i]
        }
    }
    
    private func getDaysSinceEpoch(dayTT: Double, secondTT: Double) -> Double {
        let dateTT = JulianDate(julianDayNumber: dayTT, secondsOfDay: secondTT, timeStandard: .tai)
        return dateTT.daysDifference(sampleZeroDateTT)
    }
    
    func preload(startDayTT: Double, startSecondTT: Double, stopDayTT: Double, stopSecondTT: Double) {
        let startDaysSinceEpoch = Int(getDaysSinceEpoch(dayTT: startDayTT, secondTT: startSecondTT))
        let stopDaysSinceEpoch = Int(getDaysSinceEpoch(dayTT: stopDayTT, secondTT: stopSecondTT))
        
        var startIndex = (startDaysSinceEpoch / Int(stepSizeDays) - interpolationOrder / 2) | 0
        if startIndex < 0 {
            startIndex = 0
        }
        
        var stopIndex = (stopDaysSinceEpoch / Int(stepSizeDays) - interpolationOrder / 2) | 0 + interpolationOrder
        if stopIndex >= totalSamples {
            stopIndex = totalSamples - 1
        }
        
        let startChunk = (startIndex / samplesPerXysFile) | 0
        let stopChunk = (stopIndex / samplesPerXysFile) | 0
        
        for i in startChunk...stopChunk {
            requestXysChunk(index: i)
            /*
            let fileName = "\(xysFileUrlTemplate)_\(i).json"
            guard let fileURL = fileName.urlForSource() else { continue }
            do {
                let data = try Data(contentsOf: fileURL, options: .mappedIfSafe)
                let json = try Foundation.JSONDecoder().decode(IAU2006XYS_JSON.self, from: data)
                let newSamples = json.samples
                let startIndex = i * samplesPerXysFile// * 3
                for j in stride(from: 0, to: newSamples.count, by: 3) {
                    samples[startIndex + j / 3] = Iau2006XysSample(x: newSamples[j], y: newSamples[j + 1], s: newSamples[j + 2])
                }
            } catch {
                print("failed to fetch and decode json")
            }
            */
        }
    }
    
    private func requestXysChunk(index: Int) {
        let fileName = "\(xysFileUrlTemplate)_\(index).json"
        guard let fileURL = fileName.urlForSource() else { return }
        do {
            let data = try Data(contentsOf: fileURL, options: .mappedIfSafe)
            let json = try Foundation.JSONDecoder().decode(IAU2006XYS_JSON.self, from: data)
            let newSamples = json.samples
            let startIndex = index * samplesPerXysFile// * 3
            for j in stride(from: 0, to: newSamples.count, by: 3) {
                samples[startIndex + j / 3] = Iau2006XysSample(x: newSamples[j], y: newSamples[j + 1], s: newSamples[j + 2])
            }
        } catch {
            print("failed to fetch and decode json")
        }
    }
    
    func computeXysRadians(dayTT: Double, secondTT: Double) -> Iau2006XysSample? {
        let daysSinceEpoch = getDaysSinceEpoch(dayTT: dayTT, secondTT: secondTT)
        if daysSinceEpoch < 0.0 {
            return nil
        }
        
        let centerIndex = Int(daysSinceEpoch / stepSizeDays) | 0
        if centerIndex >= totalSamples {
            return nil
        }
        
        let degree = interpolationOrder
        var firstIndex = centerIndex - (degree / 2 | 0)
        if firstIndex < 0 {
           firstIndex = 0
        }
        
        var lastIndex = firstIndex + degree
        if lastIndex >= totalSamples {
            lastIndex = totalSamples - 1
            firstIndex = lastIndex - degree
            if firstIndex < 0 {
                firstIndex = 0
            }
        }
        
        // Are all the samples we need present?
        // We can assume so if the first and last are present
        //let isDataMissing = false
        if samples[firstIndex] == nil {
            requestXysChunk(index: (firstIndex / samplesPerXysFile) | 0)
            //isDataMissing = true // no missing because of sync operation to load data
        }
        
        if samples[lastIndex] == nil {
            requestXysChunk(index: (lastIndex / samplesPerXysFile) | 0)
            //isDataMissing = true // no missing because of sync operation to load data
        }
        
        /*
        if isDataMissing {
            return nil
        }
        */
        
        var result = Iau2006XysSample(x: 0, y: 0, s: 0)
        
        let x = daysSinceEpoch - Double(firstIndex) * stepSizeDays
        
        work.removeAll()
        coef.removeAll()
        
        for i in 0...degree {
            work.append(x - xTable[i])
            //work[i] = x - xTable[i]
        }
        
        for i in 0...degree {
            coef.append(1.0)
            //coef[i] = 1.0
            
            for j in 0...degree {
                if j != i {
                    coef[i] *= work[j]
                }
            }
            
            coef[i] *= denominators[i]
            
            let sampleIndex = firstIndex + i
            guard let s = samples[sampleIndex] else { continue }
            result.x += coef[i] * s.x
            result.y += coef[i] * s.y
            result.s += coef[i] * s.s
        }
        
        return result
    }
}
