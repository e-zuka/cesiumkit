//
//  Iau2000Orientation.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/11/29.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

struct IauOrientationParameters {
    var rightAscension: Double
    var declination: Double
    var rotation: Double
    var rotationRate: Double
}

class Iau2000Orientation {
    static let TdtMinusTai = 32.184;
    static let J2000d = 2451545.0;

    static let c1 = -0.0529921;
    static let c2 = -0.1059842;
    static let c3 = 13.0120009;
    static let c4 = 13.3407154;
    static let c5 = 0.9856003;
    static let c6 = 26.4057084;
    static let c7 = 13.0649930;
    static let c8 = 0.3287146;
    static let c9 = 1.7484877;
    static let c10 = -0.1589763;
    static let c11 = 0.0036096;
    static let c12 = 0.1643573;
    static let c13 = 12.9590088;

    static func computeMoon(date: JulianDate = .now()) -> IauOrientationParameters {
        let dateTT = date.addSeconds(TdtMinusTai)
        let d = dateTT.totalDays() - J2000d
        let T = d / TimeConstants.DaysPerJulianCentury
        
        let E1 = (125.045 + c1 * d) * Math.RadiansPerDegree
        let E2 = (250.089 + c2 * d) * Math.RadiansPerDegree
        let E3 = (260.008 + c3 * d) * Math.RadiansPerDegree
        let E4 = (176.625 + c4 * d) * Math.RadiansPerDegree
        let E5 = (357.529 + c5 * d) * Math.RadiansPerDegree
        let E6 = (311.589 + c6 * d) * Math.RadiansPerDegree
        let E7 = (134.963 + c7 * d) * Math.RadiansPerDegree
        let E8 = (276.617 + c8 * d) * Math.RadiansPerDegree
        let E9 = (34.226 + c9 * d) * Math.RadiansPerDegree
        let E10 = (15.134 + c10 * d) * Math.RadiansPerDegree
        let E11 = (119.743 + c11 * d) * Math.RadiansPerDegree
        let E12 = (239.961 + c12 * d) * Math.RadiansPerDegree
        let E13 = (25.053 + c13 * d) * Math.RadiansPerDegree
        
        let sinE1 = sin(E1)
        let sinE2 = sin(E2)
        let sinE3 = sin(E3)
        let sinE4 = sin(E4)
        let sinE5 = sin(E5)
        let sinE6 = sin(E6)
        let sinE7 = sin(E7)
        let sinE8 = sin(E8)
        let sinE9 = sin(E9)
        let sinE10 = sin(E10)
        let sinE11 = sin(E11)
        let sinE12 = sin(E12)
        let sinE13 = sin(E13)

        let cosE1 = cos(E1)
        let cosE2 = cos(E2)
        let cosE3 = cos(E3)
        let cosE4 = cos(E4)
        let cosE5 = cos(E5)
        let cosE6 = cos(E6)
        let cosE7 = cos(E7)
        let cosE8 = cos(E8)
        let cosE9 = cos(E9)
        let cosE10 = cos(E10)
        let cosE11 = cos(E11)
        let cosE12 = cos(E12)
        let cosE13 = cos(E13)
        
        let rightAscension = (269.9949 + 0.0031 * T - 3.8787 * sinE1 - 0.1204 * sinE2 +
            0.0700 * sinE3 - 0.0172 * sinE4 + 0.0072 * sinE6 -
            0.0052 * sinE10 + 0.0043 * sinE13) * Math.RadiansPerDegree
        let declination = (66.5392 + 0.013 * T + 1.5419 * cosE1 + 0.0239 * cosE2 -
            0.0278 * cosE3 + 0.0068 * cosE4 - 0.0029 * cosE6 +
            0.0009 * cosE7 + 0.0008 * cosE10 - 0.0009 * cosE13) * Math.RadiansPerDegree
        let rotation = (38.3213 + 13.17635815 * d - 1.4e-12 * d * d + 3.5610 * sinE1 +
            0.1208 * sinE2 - 0.0642 * sinE3 + 0.0158 * sinE4 +
            0.0252 * sinE5 - 0.0066 * sinE6 - 0.0047 * sinE7 -
            0.0046 * sinE8 + 0.0028 * sinE9 + 0.0052 * sinE10 +
            0.004 * sinE11 + 0.0019 * sinE12 - 0.0044 * sinE13) *
            Math.RadiansPerDegree
        let rotationRate = ((13.17635815 - 1.4e-12 * (2.0 * d)) +
            3.5610 * cosE1 * c1 +
            0.1208 * cosE2*c2 - 0.0642 * cosE3*c3 + 0.0158 * cosE4*c4 +
            0.0252 * cosE5*c5 - 0.0066 * cosE6*c6 - 0.0047 * cosE7*c7 -
            0.0046 * cosE8*c8 + 0.0028 * cosE9*c9 + 0.0052 * cosE10*c10 +
            0.004 * cosE11*c11 + 0.0019 * cosE12*c12 - 0.0044 * cosE13*c13) /
            86400.0 * Math.RadiansPerDegree
        
        return IauOrientationParameters(rightAscension: rightAscension, declination: declination, rotation: rotation, rotationRate: rotationRate)
    }
}
