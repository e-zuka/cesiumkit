//
//  QuantizedMeshUpsampler.swift
//  CesiumKit
//
//  Created by Ryan Walklin on 30/03/2016.
//  Copyright © 2016 Test Toast. All rights reserved.
//

import Foundation
import os.signpost

private let upsampleLog = OSLog(subsystem: "com.ai.FS", category: "UpsampleDetail")

private let halfMaxShort = Int((Int16.max / 2) | 0)
private let maxShort = Int(Int16.max)
private let doubleMaxShort = Double(Int16.max)

class QMUpsampler {
/*
 
 var maxShort = 32767;
 var halfMaxShort = (maxShort / 2) | 0;
 
 var clipScratch = [];
 var clipScratch2 = [];
 var verticesScratch = [];
 var cartographicScratch = new Cartographic();
 var cartesian3Scratch = new Cartesian3();
 var uScratch = [];
 var vScratch = [];
 var heightScratch = [];
 var indicesScratch = [];
 var normalsScratch = [];
 var horizonOcclusionPointScratch = new Cartesian3();
 var boundingSphereScratch = new BoundingSphere();
 var orientedBoundingBoxScratch = new OrientedBoundingBox();
 */
    class func upsampleQuantizedTerrainMesh(
        vertices parentVertices: [Float],
        vertexCountWithoutSkirts quantizedVertexCount: Int,
        indices _parentIndices: [Int],
        minimumHeight parentMinimumHeight: Double,
        maximumHeight parentMaximumHeight: Double,
        isEastChild: Bool,
        isNorthChild: Bool,
        childRectangle: Rectangle,
        ellipsoid: Ellipsoid,
        skirtIndex: Int,
        encoding: TerrainEncodingProtocol,
        exaggeration: Double
        ) ->
        (
        vertices: [UInt16],
        encodedNormals: [UInt8]?,
        indices: [Int],
        minimumHeight: Double,
        maximumHeight: Double,
        westIndices: [Int],
        southIndices: [Int],
        eastIndices: [Int],
        northIndices: [Int],
        boundingSphere: BoundingSphere,
        orientedBoundingBox: OrientedBoundingBox,
        horizonOcclusionPoint: Cartesian3,
        cartesianVertices: [Float]
        )
    {
        let spid = OSSignpostID(log: upsampleLog, object: parentVertices as AnyObject)

        let minU = isEastChild ? halfMaxShort : 0
        let maxU = isEastChild ? maxShort : halfMaxShort
        let minV = isNorthChild ? halfMaxShort : 0
        let maxV = isNorthChild ? maxShort : halfMaxShort
        var uBuffer = [Int]()
        var vBuffer = [Int]()
        var heightBuffer = [Double]()
        var normalBuffer = [UInt8]()
        
        var indices = [Int]()
        var vertexMap: [String: Int] = [:]
        
        let parentIndices = _parentIndices[0..<skirtIndex]
        
        let hasVertexNormals = encoding.hasVertexNormals
        
        var vertexCount = 0
        //let quantizedVertexCount = vertexCountWithoutSkirts
        var parentUBuffer: [Int] = Array(repeating: 0, count: quantizedVertexCount)
        var parentVBuffer: [Int] = Array(repeating: 0, count: quantizedVertexCount)
        var parentHeightBuffer: [Double] = Array(repeating: 0, count: quantizedVertexCount)
        var parentNormalBuffer: [UInt8]? = hasVertexNormals ? Array(repeating: 0, count: quantizedVertexCount * 2) : nil
        
        let threshold = 20
        var height: Double
        
        var n = 0
        for i in 0..<quantizedVertexCount {
            let texCoords = encoding.decodeTextureCoordinates(parentVertices, index: i)
            height = encoding.decodeHeight(parentVertices, index: i) / exaggeration

            //parentUBuffer[i] = Int(Math.clamp(Double(Int(texCoords.x * doubleMaxShort)), min: 0, max: doubleMaxShort))
            parentUBuffer[i] = Math.clamp(Int(texCoords.x * doubleMaxShort) | 0, min: 0, max: maxShort)
            
            //parentVBuffer[i] = Int(Math.clamp(Double(Int(texCoords.y * doubleMaxShort)), min: 0, max: doubleMaxShort))
            parentVBuffer[i] = Math.clamp(Int(texCoords.y * doubleMaxShort) | 0, min: 0, max: maxShort)
            
            let pHeightRange = (parentMaximumHeight == parentMinimumHeight) ? Double.leastNonzeroMagnitude : parentMaximumHeight - parentMinimumHeight
            //parentHeightBuffer[i] = Math.clamp(((height - parentMinimumHeight) / pHeightRange) * doubleMaxShort) | 0, min: 0, max: doubleMaxShort)
            
            //let pHeightRange = parentMaximumHeight - parentMinimumHeight
            if !pHeightRange.isZero {
                parentHeightBuffer[i] = Math.clamp(((height - parentMinimumHeight) / pHeightRange * doubleMaxShort), min: 0, max: doubleMaxShort)
            }
            //parentHeightBuffer[i] = Double(Math.clamp(Int(((height! - parentMinimumHeight) / (parentMaximumHeight - parentMinimumHeight)) * Double(maxShort)) | 0, min: 0, max: maxShort))

            if parentUBuffer[i] < threshold {
                parentUBuffer[i] = 0
            }
            
            if parentVBuffer[i] < threshold {
                parentVBuffer[i] = 0
            }
            
            if (maxShort - parentUBuffer[i]) < threshold {
                parentUBuffer[i] = maxShort
            }
            
            if (maxShort - parentVBuffer[i]) < threshold {
                parentVBuffer[i] = maxShort
            }
            
            if hasVertexNormals {
                let encodedNormal = encoding.octEncodedNormal(parentVertices, index: i)
                parentNormalBuffer![n] = UInt8(encodedNormal.x)
                parentNormalBuffer![n + 1] = UInt8(encodedNormal.y)
            }
            n += 2
        }

        n = 0
        for i in 0..<quantizedVertexCount {
            let u = parentUBuffer[i]
            let v = parentVBuffer[i]
            if (isEastChild && u >= halfMaxShort || !isEastChild && u <= halfMaxShort) &&
               (isNorthChild && v >= halfMaxShort || !isNorthChild && v <= halfMaxShort) {
                vertexMap["\(i)"] = vertexCount
                uBuffer.append(u)
                vBuffer.append(v)
                heightBuffer.append(parentHeightBuffer[i])
                if hasVertexNormals {
                    normalBuffer.append(parentNormalBuffer![n])
                    normalBuffer.append(parentNormalBuffer![n + 1])
                }
                
                vertexCount += 1
                n += 2
            }
        }

        var triangleVertices = [Vertex(), Vertex(), Vertex()]
        
        var clippedTriangleVertices = [Vertex(), Vertex(), Vertex()]
        
        var clippedIndex: Int
        var clipped2: [Double]

        os_signpost(.begin, log: upsampleLog, name: "heavy_loop", signpostID: spid)
        for i in stride(from: 0, to: parentIndices.count, by: 3) {
            //os_signpost(.begin, log: upsampleLog, name: "heavy_loop prepare", signpostID: spid)
            let i0 = parentIndices[i]
            let i1 = parentIndices[i + 1]
            let i2 = parentIndices[i + 2]
            
            let u0 = parentUBuffer[i0]
            let u1 = parentUBuffer[i1]
            let u2 = parentUBuffer[i2]
            
            triangleVertices[0].initializeIndexed(
                parentUBuffer,
                vBuffer: parentVBuffer,
                heightBuffer: parentHeightBuffer,
                normalBuffer: parentNormalBuffer,
                index: i0
            )
            triangleVertices[1].initializeIndexed(
                parentUBuffer,
                vBuffer: parentVBuffer,
                heightBuffer: parentHeightBuffer,
                normalBuffer: parentNormalBuffer,
                index: i1
            )
            triangleVertices[2].initializeIndexed(
                parentUBuffer,
                vBuffer: parentVBuffer,
                heightBuffer: parentHeightBuffer,
                normalBuffer: parentNormalBuffer,
                index: i2
            )
            //os_signpost(.end, log: upsampleLog, name: "heavy_loop prepare", signpostID: spid)
            
            //os_signpost(.begin, log: upsampleLog, name: "heavy_loop clip", signpostID: spid)
            // Clip triangle on the east-west boundary.
            let clipped = Intersections2D.clipTriangleAtAxisAlignedThreshold(threshold: halfMaxShort, keepAbove: isEastChild, u0: u0, u1: u1, u2: u2)
            
            // Get the first clipped triangle, if any.
            clippedIndex = 0
            
            if clippedIndex >= clipped.count {
                continue
            }
            clippedIndex = clippedTriangleVertices[0].initializeFromClipResult(clipped, index: clippedIndex, vertices: triangleVertices)
            
            if clippedIndex >= clipped.count {
                continue
            }
            clippedIndex = clippedTriangleVertices[1].initializeFromClipResult(clipped, index: clippedIndex, vertices: triangleVertices)
            
            if clippedIndex >= clipped.count {
                continue
            }
            clippedIndex = clippedTriangleVertices[2].initializeFromClipResult(clipped, index: clippedIndex, vertices: triangleVertices)
            
            // Clip the triangle against the North-south boundary.
            clipped2 = Intersections2D.clipTriangleAtAxisAlignedThreshold(threshold: halfMaxShort, keepAbove: isNorthChild, u0: clippedTriangleVertices[0].getV(), u1: clippedTriangleVertices[1].getV(), u2: clippedTriangleVertices[2].getV())
            //os_signpost(.end, log: upsampleLog, name: "heavy_loop clip", signpostID: spid)
            
            //os_signpost(.begin, log: upsampleLog, name: "heavy_loop 1st addPolygon", signpostID: spid)
            addClippedPolygon(&uBuffer, vBuffer: &vBuffer, heightBuffer: &heightBuffer, normalBuffer: &normalBuffer, indices: &indices, vertexMap: &vertexMap, clipped: clipped2, triangleVertices: clippedTriangleVertices, hasVertexNormals: hasVertexNormals)
            //os_signpost(.end, log: upsampleLog, name: "heavy_loop 1st addPolygon", signpostID: spid)
            
            // If there's another vertex in the original clipped result,
            // it forms a second triangle.  Clip it as well.
            //os_signpost(.begin, log: upsampleLog, name: "heavy_loop 2nd addPolygon", signpostID: spid)
            if clippedIndex < clipped.count {
                //let _ = clippedTriangleVertices[2].clone(clippedTriangleVertices[1])
                clippedTriangleVertices[1] = clippedTriangleVertices[2].clone(nil)
                //let _ = clippedTriangleVertices[2].initializeFromClipResult(clipped, index: clippedIndex, vertices: triangleVertices)
                let _ = clippedTriangleVertices[2].initializeFromClipResult(clipped, index: clippedIndex, vertices: triangleVertices)
                
                clipped2 = Intersections2D.clipTriangleAtAxisAlignedThreshold(threshold: halfMaxShort, keepAbove: isNorthChild, u0: clippedTriangleVertices[0].getV(), u1: clippedTriangleVertices[1].getV(), u2: clippedTriangleVertices[2].getV())
                addClippedPolygon(&uBuffer, vBuffer: &vBuffer, heightBuffer: &heightBuffer, normalBuffer: &normalBuffer, indices: &indices, vertexMap: &vertexMap, clipped: clipped2, triangleVertices: clippedTriangleVertices, hasVertexNormals: hasVertexNormals)
            }
            //os_signpost(.end, log: upsampleLog, name: "heavy_loop 2nd addPolygon", signpostID: spid)
        }
        os_signpost(.end, log: upsampleLog, name: "heavy_loop", signpostID: spid)
        
        let uOffset = isEastChild ? -maxShort : 0
        let vOffset = isNorthChild ? -maxShort : 0
        
        var westIndices = [Int]()
        var southIndices = [Int]()
        var eastIndices = [Int]()
        var northIndices = [Int]()
        
        var minimumHeight = Double(Int.max)
        var maximumHeight = -minimumHeight
        
        var cartesianVertices = [Float]()
        
        let north = childRectangle.north
        let south = childRectangle.south
        var east = childRectangle.east
        let west = childRectangle.west
        
        if east < west {
            east += Math.TwoPi
        }
        
        var u, v: Int
        
        for i in 0..<uBuffer.count {
            u = uBuffer[i]
            if u <= minU {
                westIndices.append(i)
                u = 0
            } else if u >= maxU {
                eastIndices.append(i)
                u = maxShort
            } else {
                u = u * 2 + uOffset
            }
            
            uBuffer[i] = u
            
            v = vBuffer[i]
            if v <= minV {
                southIndices.append(i)
                v = 0
            } else if (v >= maxV) {
                northIndices.append(i)
                v = maxShort
            } else {
                v = v * 2 + vOffset
            }
            
            vBuffer[i] = v
            
            let height = Math.lerp(p: parentMinimumHeight, q: parentMaximumHeight, time: heightBuffer[i] / doubleMaxShort)
            if height < minimumHeight {
                minimumHeight = height
            }
            if height > maximumHeight {
                maximumHeight = height
            }
            
            heightBuffer[i] = height
            
            let cartesian = ellipsoid.cartographicToCartesian(
                Cartographic(
                    longitude: Math.lerp(p: west, q: east, time: Double(u / maxShort)),
                    latitude:  Math.lerp(p: south, q: north, time: Double(v / maxShort)),
                    height: height
                )
            )
            cartesianVertices.append(contentsOf: (0..<3).map { cartesian.floatRepresentation[$0] })
        }

        let boundingSphere = BoundingSphere.fromVertices(cartesianVertices, center: Cartesian3.zero, stride: 3)
        let orientedBoundingBox = OrientedBoundingBox(fromRectangle: childRectangle, minimumHeight: minimumHeight, maximumHeight: maximumHeight, ellipsoid: ellipsoid)
        
        let occluder = EllipsoidalOccluder(ellipsoid: ellipsoid)
        let horizonOcclusionPoint = occluder.computeHorizonCullingPointFromVertices(directionToPoint: boundingSphere.center, vertices: cartesianVertices, stride: 3, center: boundingSphere.center)!
        
        let heightRange = maximumHeight - minimumHeight
        //heightRange = heightRange.isZero ? Double.leastNonzeroMagnitude : heightRange
        
        var vertices = uBuffer.map { UInt16 ($0) }
        vertices.append(contentsOf: vBuffer.map { UInt16($0) })
        if heightRange.isZero {
            vertices.append(contentsOf: heightBuffer.map { UInt16($0 * 0) })
        } else {
            vertices.append(contentsOf: heightBuffer.map { UInt16(doubleMaxShort * ($0 - minimumHeight) / heightRange) } )
        }
        

        return (
            vertices : vertices,
            //encodedNormals : encodedNormals,
            encodedNormals : (hasVertexNormals) ? normalBuffer : nil,
            indices : indices,
            minimumHeight : minimumHeight,
            maximumHeight : maximumHeight,
            westIndices : westIndices,
            southIndices : southIndices,
            eastIndices : eastIndices,
            northIndices : northIndices,
            boundingSphere : boundingSphere,
            orientedBoundingBox : orientedBoundingBox,
            horizonOcclusionPoint : horizonOcclusionPoint,
            cartesianVertices: cartesianVertices
        );
    }
    
    fileprivate class func addClippedPolygon (
        _ uBuffer: inout [Int],
        vBuffer: inout [Int],
        heightBuffer: inout [Double],
        normalBuffer: inout [UInt8],
        indices: inout [Int],
        vertexMap: inout [String: Int],
        clipped: [Double],
        triangleVertices: [Vertex],
        hasVertexNormals: Bool)
    {
        var polygonVertices = [Vertex(), Vertex(), Vertex(), Vertex()]

        if clipped.count == 0 {
            return
        }
        
        var numVertices = 0
        var clippedIndex = 0
        while clippedIndex < clipped.count {
            clippedIndex = polygonVertices[numVertices].initializeFromClipResult(clipped, index: clippedIndex, vertices: triangleVertices)
            numVertices += 1
        }
        
        for i in 0..<numVertices {
            let polygonVertex = polygonVertices[i]

            if !polygonVertex.isIndexed {
                let key = polygonVertex.getKey()
                if let newIndex = vertexMap[key] {
                    polygonVertex.newIndex = newIndex
                } else {
                    let newIndex = uBuffer.count
                    uBuffer.append(polygonVertex.getU())
                    vBuffer.append(polygonVertex.getV())
                    heightBuffer.append(polygonVertex.getH())
                    if hasVertexNormals {
                        normalBuffer.append(polygonVertex.getNormalX())
                        normalBuffer.append(polygonVertex.getNormalY())
                    }
                    polygonVertex.newIndex = newIndex
                    vertexMap[key] = newIndex
                }
            } else {
                polygonVertex.newIndex = vertexMap["\(polygonVertex.index!)"]
                polygonVertex.uBuffer = uBuffer
                polygonVertex.vBuffer = vBuffer
                polygonVertex.heightBuffer = heightBuffer
                if hasVertexNormals {
                    polygonVertex.normalBuffer = normalBuffer
                }
            }
        }
        
        if numVertices == 3 {
            // A triangle.
            indices.append(polygonVertices[0].newIndex!)
            indices.append(polygonVertices[1].newIndex!)
            indices.append(polygonVertices[2].newIndex!)
        } else if numVertices == 4 {
            // A quad - two triangles.
            indices.append(polygonVertices[0].newIndex!)
            indices.append(polygonVertices[1].newIndex!)
            indices.append(polygonVertices[2].newIndex!)
            
            indices.append(polygonVertices[0].newIndex!)
            indices.append(polygonVertices[2].newIndex!)
            indices.append(polygonVertices[3].newIndex!)
        }
    }

 
}

private class Vertex {
    //var vertexBuffer: [Float32]? = nil
    var uBuffer: [Int]
    var vBuffer: [Int]
    var heightBuffer: [Double]
    var normalBuffer: [UInt8]!
    var index: Int? = nil
    var newIndex: Int? = nil
    var first: Vertex? = nil
    var second: Vertex? = nil
    var ratio: Double? = nil
    
    init() {
        uBuffer = []
        vBuffer = []
        heightBuffer = []
        normalBuffer = nil
    }
 
    func clone (_ result: Vertex?) -> Vertex {
        
        let result = result ?? Vertex()

        result.uBuffer = self.uBuffer
        result.vBuffer = self.vBuffer
        result.heightBuffer = self.heightBuffer
        result.normalBuffer = self.normalBuffer
        result.index = self.index
        result.first = self.first
        result.second = self.second
        result.ratio = self.ratio
 
        return result
    }

    func initializeIndexed (_ uBuffer: [Int], vBuffer: [Int], heightBuffer: [Double], normalBuffer: [UInt8]?, index: Int) {
        self.uBuffer = uBuffer
        self.vBuffer = vBuffer
        self.heightBuffer = heightBuffer
        self.normalBuffer = normalBuffer
        self.index = index
        self.first = nil
        self.second = nil
        self.ratio = nil
    }
 /*
 Vertex.prototype.initializeInterpolated = function(first, second, ratio) {
 this.vertexBuffer = undefined;
 this.index = undefined;
 this.newIndex = undefined;
 this.first = first;
 this.second = second;
 this.ratio = ratio;
 };
     */
    func initializeFromClipResult (_ clipResult: [Double], index: Int, vertices: [Vertex]) -> Int {
        var nextIndex = index + 1
        
        if (clipResult[index] != -1) {
            _ = vertices[Int(clipResult[index])].clone(self)
        } else {
            //vertexBuffer = nil
            self.index = nil
            first = vertices[Int(clipResult[nextIndex])]
            nextIndex += 1
            second = vertices[Int(clipResult[nextIndex])]
            nextIndex += 1
            ratio = clipResult[nextIndex]
            nextIndex += 1
        }
        
        return nextIndex
    }
 
    func getKey () -> String {
        if isIndexed {
            return "\(index!)"
        }
        return "first:\(first!.getKey()),second:\(second!.getKey()),ratio:\(ratio!)"
    }
 
    var isIndexed: Bool {
        return index != nil
    }

    func getH () -> Double {
        if let index = index {
            return heightBuffer[index]
        }
        return Math.lerp(p: first!.getH(), q: second!.getH(), time: Double(ratio!))
    }
    
    func getU () -> Int {
        if let index = index {
            return uBuffer[index]
        }
        return Int(Math.lerp(p: Double(first!.getU()), q: Double(second!.getU()), time: Double(ratio!)))
    }
 
    func getV () -> Int {
        if let index = index {
            return vBuffer[index]
        }
        return Int(Math.lerp(p: Double(first!.getV()), q: Double(second!.getV()), time: Double(ratio!)))
    }
 
    fileprivate var encodedScratch = Cartesian2()
    // An upsampled triangle may be clipped twice before it is assigned an index
    // In this case, we need a buffer to handle the recursion of getNormalX() and getNormalY().
    fileprivate var depth = -1
    fileprivate var cartesianScratch1 = [Cartesian3(), Cartesian3()]
    fileprivate var cartesianScratch2 = [Cartesian3(), Cartesian3()]
    
    func lerpOctEncodedNormal(_ vertex: Vertex) -> Cartesian2 {
        depth += 1
        // what about scratch variables
        var first = cartesianScratch1[depth]
        var second = cartesianScratch2[depth]
        
        first = AttributeCompression.octDecode(x: vertex.first!.getNormalX(), y: vertex.first!.getNormalY())
        second = AttributeCompression.octDecode(x: vertex.second!.getNormalX(), y: vertex.second!.getNormalY())
        
        depth -= 1
        return AttributeCompression.octEncode(first.lerp(second, t: Double(vertex.ratio!)).normalize())
    }
 
    func getNormalX () -> UInt8 {
        if let index = index {
            return normalBuffer![index * 2]
        }
        return UInt8(lerpOctEncodedNormal(self).x)
    }
    
    func getNormalY () -> UInt8 {
        if let index = index {
            return normalBuffer![index * 2 + 1]
        }
        return UInt8(lerpOctEncodedNormal(self).y)
    }

}
