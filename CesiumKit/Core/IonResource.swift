//
//  IonResource.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2018/07/02.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation
import Hydra

public class IonResource : Resource {
    var _ionEndpoint: JSONObject
    var _ionEndpointResource: Resource
    var _ionRoot: IonResource?
    typealias pendingPromise = () -> Void
    var _pendingPromise: pendingPromise?
    var _credits: [Credit]?
    var _isExternal: Bool
    
    var credits: [Credit]? {
        if _ionRoot != nil {
            return _ionRoot!.credits
        }
        if _credits != nil {
            return _credits!
        }
        _credits = IonResource.getCreditsFromEndpoint(endpoint: _ionEndpoint, endpointResource: _ionEndpointResource)
        return _credits
    }
    
    init?(endpoint: JSONObject, endpointResource: Resource) {
        let externalType = endpoint["externalType"]?.string
        let isExternal = (externalType != nil) ? true : false

        _ionEndpoint = endpoint
        _ionEndpointResource = endpointResource
        _ionRoot = nil
        _pendingPromise = nil
        _credits = nil
        _isExternal = isExternal
        
        if isExternal == false, let url = endpoint["url"]?.string {
            super.init(url: url)
            retryAttempts = 1
            //retryCallback = {() in }
        } else if let type = externalType, (type == "3DTILES" || type == "STK_TERRAIN_SERVER") {
            guard let url = endpoint["options"]?.object?["url"]?.string else { return nil }
            super.init(url: url)
        } else {
            assertionFailure("Ion.createResource does not support external imagery assets; use IonImageryProvider instead.")
            return nil
        }
    }
    
    class func fromAssetId(_ assetId: Int, server: Resource? = nil, accessToken: String? = nil) -> IonResource? {
        let endpointResource = IonResource._createEndpointResource(assetId: assetId, server: server, accessToken: accessToken)
        do {
            let endpointJson = try await(in: .background, endpointResource.fetchJSON())
            guard let endpoint = endpointJson.object else {
                throw CesiumResourceError.invalidState
            }
            return IonResource(endpoint: endpoint, endpointResource: endpointResource)
        } catch {
            logPrint(.error, error.localizedDescription)
            return nil
        }
    }
    class func getCreditsFromEndpoint(endpoint: JSONObject, endpointResource: Resource) -> [Credit]? {
        // var credits = endpoint.attributions.map(Credit.getIonCredit);
        guard let accessToken = endpointResource.queryParameters["access_token"] else { return nil }
        guard let defaultTokenCredit = Ion.getDefaultTokenCredit(providedKey: accessToken) else { return nil }
        /*
        if (defined(defaultTokenCredit)) {
            credits.push(defaultTokenCredit);
        }
        */
        return [defaultTokenCredit]
    }
    
    override func clone() -> Resource {
        let ionRoot = (_ionRoot == nil) ? self : _ionRoot!
        
        let result = IonResource(endpoint: ionRoot._ionEndpoint, endpointResource: ionRoot._ionEndpointResource)!
        result._ionRoot = ionRoot
        result._isExternal = self._isExternal
        return result
    }

    class func _createEndpointResource(assetId: Int, server: Resource? = nil, accessToken: String? = nil) -> Resource {
        var _server = (server == nil) ? Ion.defaultServer : server!
        let accessToken = (accessToken == nil) ? Ion.defaultAccessToken : accessToken!
        _server = Resource.createIfNeeded(resource: _server)!
        
        let queryItem = URLQueryItem(name: "access_token", value: accessToken)
        return _server.getDerivedResource(url: "v1/assets/\(assetId)/endpoint", queryParameters: [queryItem], headers: _server.headers, templateValues: _server._templateValues)
    }
    
    override func _makeRequest(url: String, options: [String : String], completion: @escaping (Data, String?) -> Void) {
        if _isExternal == true {
            super._makeRequest(url: url, options: options, completion: completion)
            return
        }

        let acceptToken = "*/*;access_token=\(_ionEndpoint["accessToken"]?.string ?? "")"
        var existingAccept = acceptToken
        let oldHeaders = headers
        if oldHeaders.count > 0 && oldHeaders["Accept"] != nil {
            existingAccept = "\(oldHeaders["Accept"]!),\(acceptToken)"
        }
        var resourceHeaders = options
        if let acceptHeader = resourceHeaders["Accept"] {
            resourceHeaders["Accept"] = "\(acceptHeader),\(acceptToken)"
        } else {
            resourceHeaders["Accept"] = existingAccept
        }
        super._makeRequest(url: url, options: resourceHeaders, completion: completion)
    }
}
