//
//  EarthOrientationParameters.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/11/29.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

struct EarthOrientationParametersSample {
    var xPoleWander: Double
    var yPoleWander: Double
    var xPoleOffset: Double
    var yPoleOffset: Double
    var ut1MinusUtc: Double
}

enum EarthOrientationParameters {
    case none
    
    func compute() -> EarthOrientationParametersSample {
        switch self {
        case .none:
            return EarthOrientationParametersSample(xPoleWander: 0.0, yPoleWander: 0.0, xPoleOffset: 0.0, yPoleOffset: 0.0, ut1MinusUtc: 0.0)
            
        }
    }
}
