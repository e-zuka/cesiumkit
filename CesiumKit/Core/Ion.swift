//
//  Ion.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2018/07/02.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation

class Ion {
    static var defaultTokenCredit: Credit? = nil
    static let _defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0NDViM2NkNi0xYTE2LTRlZTUtODBlNy05M2Q4ODg4M2NmMTQiLCJpZCI6MjU5LCJpYXQiOjE1MTgxOTc4MDh9.sld5jPORDf_lWavMEsugh6vHPnjR6j3qd1aBkQTswNM"

    static var defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJlMWVjZWI5Ny1iOTA0LTRjYWMtYmI1YS1iOThhOTY1ZDJlNDUiLCJpZCI6Mjg2LCJpYXQiOjE1MjUyMTkwNDB9._eLbngYzgeRowdyl49cuWZ4YZryukERwzkBX1Sos0-U"
    static let defaultServer = Resource(url: "https://api.cesium.com/")!

    class func getDefaultTokenCredit(providedKey: String) -> Credit? {
        if providedKey != _defaultAccessToken {
            return nil
        }
        if defaultTokenCredit == nil {
            let defaultTokenMessage = """
            This application is using Cesium's default ion access token. Please assign Cesium.Ion.defaultAccessToken
            with an access token from your ion account before making any Cesium API calls.
            You can sign up for a free ion account at https://cesium.com.
            """
            defaultTokenCredit = Credit(text: defaultTokenMessage)
        }
        return defaultTokenCredit
    }
}
