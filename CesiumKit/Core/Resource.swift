//
//  Resource.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2018/07/01.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation
import Hydra
import Alamofire

public class Resource {
    private func parseQuery(uri: URLComponents?, resource: Resource, merge: Bool = false, preserveQueryParameters: Bool = false) {
        guard let uri = uri else { return }
        if merge == true {
             resource._queryParameters = combineQueryParameters(base: uri.queryItems, target: resource._queryParameters, preserveQueryParameters: preserveQueryParameters)
        } else {
            resource._queryParameters = uri.queryItems ?? []
        }
    }
    private func parseQuery(uri: URLComponents?, merge: Bool = false, preserveQueryParameters: Bool = false) {
        guard let uri = uri else { return }
        if merge == true {
            _queryParameters = combineQueryParameters(base: uri.queryItems, target: nil, preserveQueryParameters: preserveQueryParameters)
        } else {
            _queryParameters = uri.queryItems ?? []
        }
    }
    private func stringifyQuery(uri: inout URLComponents, resource: Resource) {
        uri.query = resource.request.url?.query
    }
    private func combineQueryParameters(base: [URLQueryItem]?, target: [URLQueryItem]?, preserveQueryParameters: Bool) -> [URLQueryItem] {
        return (base ?? []) + (target ?? [])
        /*
        if preserveQueryParameters == false {
            return (base ?? []) + (target ?? [])
        }
        if base == nil {
            return target ?? []
        }
        if target == nil {
            return base ?? []
        }
        */
    }
    
    var _url: String? = nil
    var _templateValues: [String: String] = [:]
    var _queryParameters: [URLQueryItem] = []
    var queryParameters: [String: String] {
        var params: [String: String] = [:]
        for item in _queryParameters {
            params[item.name] = item.value ?? ""
        }
        return params
    }
    var headers: [String: String] = [:]
    var request: URLRequest!
    var url: String {
        get {
            return getUrlComponent(query: true, proxy: true)
        }
        set {
            guard var urlComponents = URLComponents(string: url) else { return }
            parseQuery(uri: urlComponents, resource: self, merge: false)
            urlComponents.fragment = nil
            _url = urlComponents.string
        }
    }
    /*
    var _extension: String {
        get {
            return getExtensionFromUri(uri: _url)
        }
    }
    */
    var isDataUri: Bool {
        get {
            //guard let regex = try? NSRegularExpression(pattern: "/^data:", options: .caseInsensitive) else { return false }
            guard let url = _url else { return false }
            return url.range(of: "^data:", options: String.CompareOptions.regularExpression, range: nil, locale: nil) != nil
            //return isDataUri(_url);
        }
    }
    /*
    var isBlobUri: Bool {
        get {
            return isBlobUri(_url)
        }
    }
    var isCrossOriginUrl: Bool {
        get {
            return isCrossOriginUrl(_url)
        }
    }
    */
    var hasHeaders: Bool {
        get {
            return headers.count > 0
        }
    }
        
    //var retryCallback: () -> Void
    var retryAttempts = 0
    var _retryCount = 0
    
    init?(url: String, headers: [String: String]? = nil, request: URLRequest? = nil, templateValues: [String]? = nil, queryParameters: [URLQueryItem]? = nil) {
        guard let _url = URL(string: url), let urlComponents = URLComponents(url: _url, resolvingAgainstBaseURL: false) else { return nil }
        if let req = request {
            self.request = req
        } else {
            self.request = URLRequest(url: _url)
        }
        parseQuery(uri: urlComponents, merge: true, preserveQueryParameters: true)
        
        if let _headers = headers {
            self.headers = _headers
        }
        self._url = url
    }
    
    class func createIfNeeded(resource: Any) -> Resource? {
        if let _resource = resource as? Resource {
            return _resource.getDerivedResource(request: _resource.request)
        }
        if let resourceString = resource as? String {
            return Resource(url: resourceString)
        }
        return nil
    }
    
    func getUrlComponent(query: Bool, proxy: Bool) -> String {
        if isDataUri == true {
            return _url!
        }
        guard let url = _url, var uri = URLComponents(string: url) else {
            assertionFailure("url invalid")
            return ""
        }
        if query == true {
            //stringifyQuery(uri: &uri, resource: self)
            uri.queryItems = uri.queryItems ?? [] +  _queryParameters
        }
        
        if let urlString = uri.string {
            var url = urlString.replace("%7B", "{").replace("%7D", "}")
            for (key, value) in _templateValues {
                url = url.replace("{\(key)}", value)
            }
            /*
            if (proxy && defined(this.proxy)) {
                url = this.proxy.getURL(url);
            }
            */
            
            return url
        } else {
            assertionFailure("url invalid")
            return ""
        }
    }
    
    func setQueryParameters(params: [URLQueryItem], useAsDefault: Bool) {
        if useAsDefault == true {
            _queryParameters = combineQueryParameters(base: _queryParameters, target: params, preserveQueryParameters: false)
        } else {
            _queryParameters = combineQueryParameters(base: params, target: _queryParameters, preserveQueryParameters: false)
        }
    }
    func appendQueryParameters(params: [URLQueryItem]) {
        _queryParameters = combineQueryParameters(base: params, target: _queryParameters, preserveQueryParameters: true)
    }
    func setTemplateValues(template: [String: String], useAsDefault: Bool) {
        if useAsDefault == true {
            _templateValues = _templateValues.merging(template, uniquingKeysWith: {(first, _) in first })
        } else {
            _templateValues = _templateValues.merging(template, uniquingKeysWith: {(_, last) in last })
        }
    }
    
    func getDerivedResource(request: URLRequest) -> Resource {
        let resource = self.clone()
        resource.request = request
        return resource
    }
    func getDerivedResource(url: String, queryParameters: [URLQueryItem], headers: [String: String], templateValues: [String: String]) -> Resource {
        let resource = self.clone()
        let urlComponents = URLComponents(string: url)
        parseQuery(uri: urlComponents, resource: resource)
        let encodedURL = url.replace("{", "%7B").replace("}", "%7D")
        if let thisURL = self._url, let baseURL = URL(string: thisURL), let resolveURL = URL(string: encodedURL, relativeTo: baseURL) {
            resource._url = resolveURL.absoluteString
        } else {
            resource._url = url
        }
        resource._queryParameters = queryParameters + resource._queryParameters
        resource.headers = resource.headers.merging(headers, uniquingKeysWith: { _, last in last })
        resource._templateValues = resource._templateValues.merging(templateValues, uniquingKeysWith: { _, last in last })
        return resource
    }
    func clone() -> Resource {
        let result = Resource(url: _url!)!
        result._url = _url
        result._queryParameters = _queryParameters
        result._templateValues = _templateValues
        result.headers = headers
        //result.proxy = this.proxy;
        //result.retryCallback = retryCallback
        result.retryAttempts = retryAttempts
        result._retryCount = 0
        result.request = request
        return result
    }
    
    func getBaseUri(includeQuery: Bool = false) -> String? {
        guard let _url = _url, let url = URL(string: _url) else { return nil }
        if let baseURL = url.baseURL?.absoluteString {
            if includeQuery == true, let query = url.query {
                return "\(baseURL)?\(query)"
            }
            return baseURL
        }
        return nil
    }
    func appendForwardSlash() {
        if let _url = _url {
            if _url.count == 0 {
                if let char = _url.last, char != "/" {
                    self._url = "\(_url)/"
                }
            }
        }
    }
    
    
    func fetchJSON() -> Promise<JSON> {
        return Promise<JSON>(in: .background, { resolve, reject, _ in
            guard let jsonURL = self._url else {
                reject(CesiumResourceError.invalidState)
                return
            }
            let resourceHeaders = ["Accept": "application/json,*/*;q=0.01"]
            
            self._makeRequest(url: jsonURL, options: resourceHeaders, completion: { data, error in
                do {
                    let resourceData = try JSON.decode(data, options: JSONOptions(strict: true))
                    if error != nil {
                        if let rescue = resourceData.bool, rescue == true {
                            reject(CesiumResourceError.preMetadata)
                            return
                        }
                        reject(CesiumResourceError.invalidState)
                        return
                    }
                    resolve(resourceData)
                    //completion(resourceData, nil)
                } catch {
                    logPrint(.error, "metadata decode failed - invalid JSON")
                    reject(CesiumResourceError.invalidJSON(key: "root"))
                    //completion(JSON(booleanLiteral: false), "decode error")
                }
            })
        })
        //_makeRequest(url: jsonURL, options: resourceHeaders, completion: completion)
    }
    
    func fetchArrayBuffer() -> Promise<Data> {
        return Promise<Data>(in: .background, { resolve, reject, _ in
            var params: [String: String] = [:]
            for item in self._queryParameters {
                params[item.name] = item.value ?? ""
            }
            self._makeRequest(url: self.url, options: params, completion: { data, error in
                if error != nil {
                    logPrint(.error, error.debugDescription)
                    reject(CesiumResourceError.general)
                    return
                }
                resolve(data)
            })
            
            /*
            let resourceFailure = { (error: String, data: Data) -> () in
                logPrint(.error, error)
                reject(CesiumResourceError.general)
                /*metadataError = TileProviderError.handleError(metadataError, that, that._errorEvent, message, undefined, undefined, undefined, requestMetadata);*/
            }
            
            let resourceOperation = NetworkOperation(url: self.url, headers: self.headers, parameters: params)
            resourceOperation.completionBlock = {
                if let error = resourceOperation.error {
                    resourceFailure("An error occurred while accessing \(self.url): \(error)", resourceOperation.data)
                    return
                }
                resolve(resourceOperation.data)
            }
            resourceOperation.enqueue()
            */
        })
    }
    
    func fetchImage() -> Promise<CGImage> {
        return Promise<CGImage>(in: .background, { resolve, reject, _ in
            var params: [String: String] = [:]
            for item in self._queryParameters {
                params[item.name] = item.value ?? ""
            }
            self._makeRequest(url: self.url, options: params, completion: { data, error in
                if error != nil {
                    logPrint(.error, error.debugDescription)
                    reject(CesiumResourceError.general)
                    return
                }
                if let image = CGImage.from(data) {
                    resolve(image)
                } else {
                    logPrint(.error, "fetchImage with not image data")
                    reject(CesiumResourceError.general)
                }
            })
        })
    }
    
    func _makeLegacyRequest(url: String, options: [String: String], completion: @escaping (Data, String?) -> Void) {
        let resourceSuccess = { (data: Data) -> () in
            completion(data, nil)
            /*
            do {
                let resourceData = try JSON.decode(data, options: JSONOptions(strict: true))
                completion(resourceData, nil)
            } catch {
                logPrint(.error, "metadata decode failed - invalid JSON")
                completion(JSON(booleanLiteral: false), "decode error")
            }
            */
        }
        
        let resourceFailure = { (error: String, data: Data) -> () in
            logPrint(.error, error)
            //completion(JSON(booleanLiteral: true), error)
            completion(data, error)
            /*metadataError = TileProviderError.handleError(metadataError, that, that._errorEvent, message, undefined, undefined, undefined, requestMetadata);*/
        }
        
        let headerParams: [String: String] = options.merging(self.headers, uniquingKeysWith: { first, _ in first})

        var params: [String: String] = [:]
        for item in _queryParameters {
            params[item.name] = item.value ?? ""
        }
        let resourceOperation = NetworkOperation(url: url, headers: headerParams, parameters: params)
        resourceOperation.completionBlock = {
            if let error = resourceOperation.error {
                resourceFailure("An error occurred while accessing \(url): \(error)", resourceOperation.data)
                return
            }
            resourceSuccess(resourceOperation.data)
        }
        resourceOperation.enqueue()
        //resourceOperation.start()
    }
    
    func _makeRequest(url: String, options: [String: String], completion: @escaping (Data, String?) -> Void) {
        let headerParams: [String: String] = options.merging(self.headers, uniquingKeysWith: { first, _ in first})
        var params: [String: String] = [:]
        for item in _queryParameters {
            params[item.name] = item.value ?? ""
        }
        
        Alamofire.request(url, parameters: params, encoding: URLEncoding(destination: .methodDependent), headers: headerParams).validate().responseData(queue: DispatchQueue.global(qos: .background)) { response in
        //Alamofire.request(url, parameters: params, headers: headerParams).response { response in
            switch response.result {
            case .success(let data):
                completion(data, nil)
            case .failure(let error):
                completion(Data(), error.localizedDescription)
            }
        }
    }
}
