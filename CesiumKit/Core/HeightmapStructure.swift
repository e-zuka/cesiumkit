//
//  HeightmapStructure.swift
//  CesiumKit
//
//  Created by Ryan Walklin on 26/12/2015.
//  Copyright © 2015 Test Toast. All rights reserved.
//

import Foundation

/**
 * The default structure of a heightmap, as given to {@link HeightmapTessellator.computeVertices}.
 *
 * @constant
 */
struct HeightmapStructure {
    var heightScale = 1.0
    var heightOffset = 0.0
    var elementsPerHeight = 1
    var stride = 1
    var elementMultiplier = 256.0
    var isBigEndian = false
    
    var lowestEncodedHeight = 0
    var highestEncodedHeight = 256 * 256 - 1
    
    init(heightScale: Double = 1, heightOffset: Double = 0, elementsPerHeight: Int = 1, stride: Int = 1, elementMultiplier: Double = 256, isBigEndian: Bool = false, lowestEncodedHeight: Int = 0, highestEncodedHeight: Int = 0) {
        self.heightScale = heightScale
        self.heightOffset = heightOffset
        self.elementsPerHeight = elementsPerHeight
        self.stride = stride
        self.elementMultiplier = elementMultiplier
        self.isBigEndian = isBigEndian
        self.lowestEncodedHeight = lowestEncodedHeight
        self.highestEncodedHeight = highestEncodedHeight
    }
}
