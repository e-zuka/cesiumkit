//
//  IauOrientationAxes.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/11/29.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

class IauOrientationAxes {
    init () {
    }
    
    private func computeRotationMatrix(alpha: Double, delta: Double) -> Matrix3 {
        var xAxis = Cartesian3()
        xAxis.x = cos(alpha + .pi / 2)
        xAxis.y = sin(alpha + .pi / 2)
        xAxis.z = 0.0

        let cosDec = cos(delta)

        var zAxis = Cartesian3()
        zAxis.x = cosDec * cos(alpha)
        zAxis.y = cosDec * sin(alpha)
        zAxis.z = sin(delta)

        let yAxis = zAxis.cross(xAxis)
        
        let matrix = Matrix3()
        var grid = matrix.toArray()
        grid[0] = xAxis.x
        grid[1] = yAxis.x
        grid[2] = zAxis.x
        grid[3] = xAxis.y
        grid[4] = yAxis.y
        grid[5] = zAxis.y
        grid[6] = xAxis.z
        grid[7] = yAxis.z
        grid[8] = zAxis.z
        
        return Matrix3(array: grid)
    }
    
    func evaluate(date: JulianDate = .now()) -> Matrix3 {
        let alphaDeltaW = Iau2000Orientation.computeMoon(date: date)
        let precMtx = computeRotationMatrix(alpha: alphaDeltaW.rightAscension, delta: alphaDeltaW.declination)
        
        let rot = Math.zeroToTwoPi(alphaDeltaW.rotation)
        let quat = Quaternion(axis: Cartesian3.unitZ, angle: rot)
        let rotMtx = Matrix3(quaternion: quat.conjugate())
        
        return rotMtx.multiply(precMtx)
    }
}
