//
//  CesiumTerrainProvider.swift
//  CesiumKit
//
//  Created by Ryan Walklin on 21/12/2015.
//  Copyright © 2015 Test Toast. All rights reserved.
//

import Foundation
import simd
import Hydra
import os.signpost

private let requestGeometryLog = OSLog(subsystem: "com.ai.FS", category: "RequestGeometry")
private let terrainInitLog = OSLog(subsystem: "com.ai.FS", category: "CesiumTerrainInit")

struct TileRange {
    var startX: Int
    var startY: Int
    var endX: Int
    var endY: Int
    
    init(json: JSON) {
        startX = json["startX"]?.int ?? 0
        startY = json["startY"]?.int ?? 0
        endX = json["endX"]?.int ?? 0
        endY = json["endY"]?.int ?? 0
    }
    init(startX: Int, startY: Int, endX: Int, endY: Int) {
        self.startX = startX
        self.startY = startY
        self.endX = endX
        self.endY = endY
    }
}

struct LayerJSON: Codable {
    let attribution: String
    let available: [[TileRangee]]
    let bounds: [Int]
    let description: String
    let extensions: [String]
    let format: String
    let maxzoom: Int
    let minzoom: Int
    let name: String
    let projection: String
    let scheme: String
    let tiles: [String]
    let version: String
}
struct TileRangee: Codable {
    let startX: Int
    let startY: Int
    let endX: Int
    let endY: Int
}


/**
 * A {@link TerrainProvider} that access terrain data in a Cesium terrain format.
 * The format is described on the
 * {@link https://github.com/AnalyticalGraphicsInc/cesium/wiki/Cesium-Terrain-Server|Cesium wiki}.
 *
 * @alias CesiumTerrainProvider
 * @constructor
 *
 * @param {Object} options Object with the following properties:
 * @param {String} options.url The URL of the Cesium terrain server.
 * @param {Proxy} [options.proxy] A proxy to use for requests. This object is expected to have a getURL function which returns the proxied URL, if needed.
 * @param {Boolean} [options.requestVertexNormals=false] Flag that indicates if the client should request additional lighting information from the server, in the form of per vertex normals if available.
 * @param {Boolean} [options.requestWaterMask=false] Flag that indicates if the client should request per tile water masks from the server,  if available.
 * @param {Ellipsoid} [options.ellipsoid] The ellipsoid.  If not specified, the WGS84 ellipsoid is used.
 * @param {Credit|String} [options.credit] A credit for the data source, which is displayed on the canvas.
 *
 * @see TerrainProvider
 *
 * @example
 * // Construct a terrain provider that uses per vertex normals for lighting
 * // to add shading detail to an imagery provider.
 * var terrainProvider = new Cesium.CesiumTerrainProvider({
 *     url : '//assets.agi.com/stk-terrain/world',
 *     requestVertexNormals : true
 * });
 *
 * // Terrain geometry near the surface of the globe is difficult to view when using NaturalEarthII imagery,
 * // unless the TerrainProvider provides additional lighting information to shade the terrain (as shown above).
 * var imageryProvider = new Cesium.TileMapServiceImageryProvider({
 *        url : 'http://localhost:8080/Source/Assets/Textures/NaturalEarthII',
 *        fileExtension : 'jpg'
 *    });
 *
 * var viewer = new Cesium.Viewer('cesiumContainer', {
 *     imageryProvider : imageryProvider,
 *     baseLayerPicker : false,
 *     terrainProvider : terrainProvider
 * });
 *
 * // The globe must enable lighting to make use of the terrain's vertex normals
 * viewer.scene.globe.enableLighting = true;
 */
class CesiumTerrainProvider: TerrainProvider {
    
    let url: String

    //private var _proxy: Proxy
    
    /**
    * Gets an event that is raised when the terrain provider encounters an asynchronous error.  By subscribing
    * to the event, you will be notified of the error and can potentially recover from it.  Event listeners
    * are passed an instance of {@link TileProviderError}.
    * @memberof EllipsoidTerrainProvider.prototype
    * @type {Event}
    */
    var errorEvent = Event()

    /**
    * Gets the tiling scheme used by the provider.  This function should
    * not be called before {@link TerrainProvider#ready} returns true.
    * @memberof TerrainProvider.prototype
    * @type {TilingScheme}
    */
    let tilingScheme: TilingScheme
    
    /**
     * Gets the ellipsoid used by the provider. Default is WGS84.
     */
    let ellipsoid: Ellipsoid
    
    /**
     * Gets the credit to display when this terrain provider is active.  Typically this is used to credit
     * the source of the terrain. This function should
     * not be called before {@link TerrainProvider#ready} returns true.
     * @memberof TerrainProvider.prototype
     * @type {Credit}
     */
    fileprivate (set) var credit: Credit? = nil
    
    /**
     * Gets a value indicating whether or not the provider is ready for use.
     * @memberof TerrainProvider.prototype
     * @type {Boolean}
     */
    fileprivate (set) var ready = false
    
    fileprivate let _levelZeroMaximumGeometricError: Double
    
    var heightmapTerrainQuality = 0.25
    
    fileprivate let _heightmapWidth = 65
    
    fileprivate var _heightmapStructure: HeightmapStructure? = nil
    
    fileprivate var _tileUrlTemplates = [String]()
    
    fileprivate var _availableTiles: [JSON]? = nil
    
    /**
     * Gets a value indicating whether or not the requested tiles include vertex normals.
     * This function should not be called before {@link CesiumTerrainProvider#ready} returns true.
     * @memberof CesiumTerrainProvider.prototype
     * @type {Boolean}
     * @exception {DeveloperError} This property must not be called before {@link CesiumTerrainProvider#ready}
     */
    var hasVertexNormals: Bool {
        assert(ready, "hasVertexNormals must not be called before the terrain provider is ready.")
        // returns true if we can request vertex normals from the server
        return _hasVertexNormals && requestVertexNormals
    }
    
    /**
     * Boolean flag that indicates if the Terrain Server can provide vertex normals.
     * @type {Boolean}
     * @default false
     * @private
     */
    fileprivate var _hasVertexNormals = false

    /**
     * Boolean flag that indicates if the client should request vertex normals from the server.
     * Vertex normals data is appended to the standard tile mesh data only if the client requests the vertex normals and
     * if the server provides vertex normals.
     * @memberof CesiumTerrainProvider.prototype
     * @type {Boolean}
     */
    fileprivate (set) var requestVertexNormals: Bool
    
    /**
     * Gets a value indicating whether or not the provider includes a water mask.  The water mask
     * indicates which areas of the globe are water rather than land, so they can be rendered
     * as a reflective surface with animated waves.  This function should not be
     * called before {@link CesiumTerrainProvider#ready} returns true.
     * @memberof CesiumTerrainProvider.prototype
     * @type {Boolean}
     * @exception {DeveloperError} This property must not be called before {@link CesiumTerrainProvider#ready}
     */
    var hasWaterMask: Bool {
        assert(ready, "hasWaterMask must not be called before the terrain provider is ready.")
        return _hasWaterMask && _requestWaterMask
    }
    
    fileprivate var _hasWaterMask = false
    
    /**
     * Boolean flag that indicates if the client should request tile watermasks from the server.
     * @type {Boolean}
     * @default false
     * @private
     */
    fileprivate var _requestWaterMask: Bool
    
    fileprivate var _littleEndianExtensionSize = true
    
    /** v1.46
     */
    var _ready: Bool = false
    //var _readyPromise: Promise<Bool>
    var _tileCredits: [Credit]? = nil
    var _availability: TileAvailability? = nil
    var _layers: [LayerInformation] = []
    struct LayerInformation {
        var resource: Resource
        var version: String
        var isHeightmap: Bool
        var tileUrlTemplates: JSONArray
        var availability: TileAvailability?
        var hasVertexNormals: Bool
        var hasWaterMask: Bool
        var littleEndianExtensionSize: Bool
    }
    
    var attribution: String = ""
    var rootResource: Resource? = nil
    
    init(resourceURL: String, ellipsoid: Ellipsoid = Ellipsoid.wgs84(), tilingScheme: TilingScheme = GeographicTilingScheme(), requestVertexNormals: Bool = false, requestWaterMask: Bool = false) {
        self.url = resourceURL
        self.ellipsoid = ellipsoid
        self.tilingScheme = tilingScheme
        self.requestVertexNormals = requestVertexNormals
        _requestWaterMask = requestWaterMask
        
        _levelZeroMaximumGeometricError = CesiumTerrainProvider.estimatedLevelZeroGeometricErrorForAHeightmap(ellipsoid: self.ellipsoid, tileImageWidth: _heightmapWidth, numberOfTilesAtLevelZero: tilingScheme.numberOfXTilesAt(level: 0))
    }
    
    func parseMetadata(data: LayerJSON, resource: Resource) -> [Int: [TileRange]]? {
        var lastResource = resource
        var metadataError: Error? = nil
        
        let format = data.format
        let tiles = data.tiles
        if tiles.count < 1 {
            metadataError = CesiumResourceError.invalidJSON(key: "tiles")
            //reject(CesiumResourceError.invalidJSON(key: "tiles"))
            return nil
        }
        
        var overallAvailability: [Int: [TileRange]] = [:]
        
        var hasVertexNormals = false
        var hasWaterMask = false
        var littleEndianExtensionSize = true
        var isHeightmap = false
        
        if format == "heightmap-1.0" {
            isHeightmap = true
            if self._heightmapStructure == nil {
                self._heightmapStructure = HeightmapStructure(
                    heightScale: 1.0 / 5.0,
                    heightOffset: -1000.0,
                    elementsPerHeight: 1,
                    stride: 1,
                    elementMultiplier: 256.0,
                    isBigEndian: false,
                    lowestEncodedHeight: 0,
                    highestEncodedHeight: 256 * 256 - 1
                )
            }
            hasWaterMask = true
            self._requestWaterMask = true
        } else if format.hasPrefix("quantized-mesh-1.") == false {
            metadataError = CesiumResourceError.invalidTileFormat(format: format)
            //reject(CesiumResourceError.invalidTileFormat(format: format))
            return nil
        }
        
        let tileUrlTemplates = tiles
        let availableTiles = data.available// data["available"]?.array
        let availability = TileAvailability(tilingScheme: self.tilingScheme, maximumLevel: availableTiles.count)
        for (level, availableTile) in availableTiles.enumerated() {
            //let rangesAtLevel: [TileRange] = try! data["available"]!.mapArray(level, TileRange.init(json:))
            let rangesAtLevel: [TileRangee] = availableTile
            let yTiles = self.tilingScheme.numberOfYTilesAt(level: level)
            if overallAvailability[level] == nil {
                overallAvailability[level] = []
            }

            for range in rangesAtLevel {
                let yStart = yTiles - range.endY - 1
                let yEnd = yTiles - range.startY - 1
                let tileRange = TileRange(startX: range.startX, startY: yStart, endX: range.endX, endY: yEnd)
                overallAvailability[level]!.append(tileRange)
                availability.addAvailableTileRange(level: level, startX: tileRange.startX, startY: tileRange.startY, endX: tileRange.endX, endY: tileRange.endY)
            }
        }

        // The vertex normals defined in the 'octvertexnormals' extension is identical to the original
        // contents of the original 'vertexnormals' extension.  'vertexnormals' extension is now
        // deprecated, as the extensionLength for this extension was incorrectly using big endian.
        // We maintain backwards compatibility with the legacy 'vertexnormal' implementation
        // by setting the _littleEndianExtensionSize to false. Always prefer 'octvertexnormals'
        // over 'vertexnormals' if both extensions are supported by the server.
        let extensions = data.extensions
        if extensions.contains("octvertexnormals") {
            hasVertexNormals = true
        } else if extensions.contains("vertexnormals") {
            hasVertexNormals = true
            littleEndianExtensionSize = false
        }
        if extensions.contains("watermask") {
            hasWaterMask = true
        }

        self._hasWaterMask = self._hasWaterMask || hasWaterMask
        self._hasVertexNormals = self._hasVertexNormals || hasVertexNormals
        
        if data.attribution.count > 0 {
            if self.attribution.count > 0 {
                self.attribution += " "
            }
            self.attribution += data.attribution
        }

        self._layers.append(LayerInformation(
            resource: lastResource,
            version: data.version,
            isHeightmap: isHeightmap,
            tileUrlTemplates: JSON(tileUrlTemplates.map { JSON($0)}).array ?? [],
            availability: availability,
            hasVertexNormals: hasVertexNormals,
            hasWaterMask: hasWaterMask,
            littleEndianExtensionSize: littleEndianExtensionSize
        ))
        
        /*
        if let parentUrl = data["parentUrl"]?.string {
            if availability == nil {
                logPrint(.info, "A layer.json can\'t have a parentUrl if it does\'t have an available array.")
                return overallAvailability
            }
            lastResource = lastResource.getDerivedResource(url: parentUrl, queryParameters: [], headers: [:], templateValues: [:])
            lastResource.appendForwardSlash()
            let metadataResource = lastResource.getDerivedResource(url: "layer.json", queryParameters: [], headers: [:], templateValues: [:])
            do {
                let json = try await(metadataResource.fetchJSON())
                return self.parseMetadata(data: json, resource: metadataResource)
            } catch {
                return nil
            }
        }
        */
        return overallAvailability
    }
    
    func _parseMetadata(data _data: JSON, resource: Resource) -> [Int: [TileRange]]? {
        var lastResource = resource
        var metadataError: Error? = nil
        
        guard let data = _data.object else {
            metadataError = CesiumResourceError.invalidJSON(key: "root")
            //reject(CesiumResourceError.invalidJSON(key: "root"))
            return nil
        }
        guard let format = data["format"]?.string else {
            metadataError = CesiumResourceError.invalidJSON(key: "format")
            //reject(CesiumResourceError.invalidJSON(key: "format"))
            return nil
        }
        guard let tiles = data["tiles"]?.array, tiles.count > 0 else {
            metadataError = CesiumResourceError.invalidJSON(key: "tiles")
            //reject(CesiumResourceError.invalidJSON(key: "tiles"))
            return nil
        }
        
        var overallAvailability: [Int: [TileRange]] = [:]
        
        var hasVertexNormals = false
        var hasWaterMask = false
        var littleEndianExtensionSize = true
        var isHeightmap = false
        
        if format == "heightmap-1.0" {
            isHeightmap = true
            if self._heightmapStructure == nil {
                self._heightmapStructure = HeightmapStructure(
                    heightScale: 1.0 / 5.0,
                    heightOffset: -1000.0,
                    elementsPerHeight: 1,
                    stride: 1,
                    elementMultiplier: 256.0,
                    isBigEndian: false,
                    lowestEncodedHeight: 0,
                    highestEncodedHeight: 256 * 256 - 1
                )
            }
            hasWaterMask = true
            self._requestWaterMask = true
        } else if format.hasPrefix("quantized-mesh-1.") == false {
            metadataError = CesiumResourceError.invalidTileFormat(format: format)
            //reject(CesiumResourceError.invalidTileFormat(format: format))
            return nil
        }
        
        let tileUrlTemplates = tiles
        let _availableTiles = data["available"]?.array
        var availability: TileAvailability? = nil
        if let availableTiles = _availableTiles {
            availability = TileAvailability(tilingScheme: self.tilingScheme, maximumLevel: availableTiles.count)
            for (level, availableTile) in availableTiles.enumerated() {
                let rangesAtLevel: [TileRange] = try! data["available"]!.mapArray(level, TileRange.init(json:))
                let yTiles = self.tilingScheme.numberOfYTilesAt(level: level)
                if overallAvailability[level] == nil {
                    overallAvailability[level] = []
                }
                
                for range in rangesAtLevel {
                    let yStart = yTiles - range.endY - 1
                    let yEnd = yTiles - range.startY - 1
                    let tileRange = TileRange(startX: range.startX, startY: yStart, endX: range.endX, endY: yEnd)
                    overallAvailability[level]!.append(tileRange)
                    availability!.addAvailableTileRange(level: level, startX: tileRange.startX, startY: tileRange.startY, endX: tileRange.endX, endY: tileRange.endY)
                }
            }
        }
        
        // The vertex normals defined in the 'octvertexnormals' extension is identical to the original
        // contents of the original 'vertexnormals' extension.  'vertexnormals' extension is now
        // deprecated, as the extensionLength for this extension was incorrectly using big endian.
        // We maintain backwards compatibility with the legacy 'vertexnormal' implementation
        // by setting the _littleEndianExtensionSize to false. Always prefer 'octvertexnormals'
        // over 'vertexnormals' if both extensions are supported by the server.
        if let extensions = data["extensions"]?.string {
            if extensions.contains("octvertexnormals") {
                hasVertexNormals = true
            } else if extensions.contains("vertexnormals") {
                hasVertexNormals = true
                littleEndianExtensionSize = false
            }
            if extensions.contains("watermask") {
                hasWaterMask = true
            }
        }
        
        self._hasWaterMask = self._hasWaterMask || hasWaterMask
        self._hasVertexNormals = self._hasVertexNormals || hasVertexNormals
        
        if let _attribution = data["attribution"]?.string {
            if self.attribution.count > 0 {
                self.attribution += " "
            }
            self.attribution += _attribution
        }
        
        self._layers.append(LayerInformation(
            resource: lastResource,
            version: data["version"]?.string ?? "0",
            isHeightmap: isHeightmap,
            tileUrlTemplates: tileUrlTemplates,
            availability: availability,
            hasVertexNormals: hasVertexNormals,
            hasWaterMask: hasWaterMask,
            littleEndianExtensionSize: littleEndianExtensionSize
        ))
        
        if let parentUrl = data["parentUrl"]?.string {
            if availability == nil {
                logPrint(.info, "A layer.json can\'t have a parentUrl if it does\'t have an available array.")
                return overallAvailability
            }
            lastResource = lastResource.getDerivedResource(url: parentUrl, queryParameters: [], headers: [:], templateValues: [:])
            lastResource.appendForwardSlash()
            let metadataResource = lastResource.getDerivedResource(url: "layer.json", queryParameters: [], headers: [:], templateValues: [:])
            do {
                let json = try await(metadataResource.fetchJSON())
                return self._parseMetadata(data: json, resource: metadataResource)
            } catch {
                return nil
            }
        }
        return overallAvailability
    }
    
    func setMetadata(overallAvailability: [Int: [TileRange]]) -> Bool {
        let length = overallAvailability.count
        if length > 0 {
            self._availability = TileAvailability(tilingScheme: self.tilingScheme, maximumLevel: length)
            for level in 0..<length {
                guard let levelRanges = overallAvailability[level] else { continue }
                for range in levelRanges {
                    self._availability!.addAvailableTileRange(level: level, startX: range.startX, startY: range.startY, endX: range.endX, endY: range.endY)
                }
            }
        }
        
        let layerJsonCredit = Credit(text: self.attribution)
        if self._tileCredits != nil {
            self._tileCredits!.append(layerJsonCredit)
        } else {
            self._tileCredits = [layerJsonCredit]
        }
        self._ready = true
        self.ready = true
        return true
    }
    
    func loadMetadata(with resource: Resource) -> Promise<Bool> {
        guard let _resource = Resource.createIfNeeded(resource: resource) else {
            assertionFailure("failed to set resourse")
            return Promise<Bool>() { _, reject, _ in reject(CesiumResourceError.general)}
        }
        _resource.appendForwardSlash()
        let lastResource = _resource
        let metadataResource = lastResource.getDerivedResource(url: "layer.json", queryParameters: [], headers: [:], templateValues: [:])
        
        func filePath() -> URL? {
            guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
            let fileURL = documentDirectoryUrl.appendingPathComponent("layer.json")
            return fileURL
        }

        return Promise<Bool>() { resolve, reject, _ in
            do {
                let data = try await(metadataResource.fetchArrayBuffer())
                let json = try Foundation.JSONDecoder().decode(LayerJSON.self, from: data)
                guard let overallAvailability = self.parseMetadata(data: json, resource: lastResource) else {
                    reject(CesiumResourceError.general)
                    return
                }
                let success = self.setMetadata(overallAvailability: overallAvailability)
                resolve(success)
            } catch {
                reject(CesiumResourceError.general)
            }
        }
    }
    
    init (url: String, /*proxy: Proxy,*/ ellipsoid: Ellipsoid = Ellipsoid.wgs84(), tilingScheme: TilingScheme = GeographicTilingScheme(), requestVertexNormals: Bool = false, requestWaterMask: Bool = false) {
        
        self.url = url
        self.ellipsoid = ellipsoid
        self.tilingScheme = tilingScheme
        self.requestVertexNormals = requestVertexNormals
        _requestWaterMask = requestWaterMask
        
        _levelZeroMaximumGeometricError = CesiumTerrainProvider.estimatedLevelZeroGeometricErrorForAHeightmap(ellipsoid: self.ellipsoid, tileImageWidth: _heightmapWidth, numberOfTilesAtLevelZero: tilingScheme.numberOfXTilesAt(level: 0))
        
        
        //this._readyPromise = when.defer();
        
        let metadataUrl = url + "/layer.json"
        /*if (defined(this._proxy)) {
            metadataUrl = this._proxy.getURL(metadataUrl);
        }*/
        //var metadataError: NSError? = nil
        
        let metadataSuccess = { (data: Data) in
            
            do {
                //let metadata = try JSON.decode(String(data: data, encoding: .utf8)!, strict: true)
                let metadata = try JSON.decode(data, options: JSONOptions(strict: true))
                
                guard let tiles = try metadata.getArrayOrNil("tiles") else {
                    logPrint(.error, "The layer.json file does not specify any tile URL templates.")
                    //metadataError = TileProviderError.handleError(metadataError, that, that._errorEvent, message, undefined, undefined, undefined, requestMetadata);
                    return
                }

                guard let format = try metadata.getStringOrNil("format") else {
                    logPrint(.error, "The tile format is not specified in the layer.json file.")
                    //metadataError = TileProviderError.handleError(metadataError, that, that._errorEvent, message, undefined, undefined, undefined, requestMetadata);
                    return
                }
                
                if format == "heightmap-1.0" {
                    self._heightmapStructure = HeightmapStructure(
                        heightScale: 1.0 / 5.0,
                        heightOffset: -1000.0,
                        elementsPerHeight: 1,
                        stride: 1,
                        elementMultiplier: 256.0,
                        isBigEndian: false
                    )
                    self._hasWaterMask = true
                    self._requestWaterMask = true
                } else if !format.hasPrefix("quantized-mesh-1.") {
                    logPrint(.warning, "The tile format '" + format + "' is invalid or not supported.")
                    //metadataError = TileProviderError.handleError(metadataError, that, that._errorEvent, message, undefined, undefined, undefined, requestMetadata);
                    return
                }
                let version = try metadata.getString("version")
                
                var baseURL: URLComponents = URLComponents(string: self.url)!
                self._tileUrlTemplates = tiles.map {
                    var templateString = $0.string!
                    let template = URLComponents(string: templateString)
                    if template?.host != nil && baseURL.host == nil {
                        baseURL.host = template!.host
                        baseURL.user = template!.user
                        baseURL.password = template!.password
                        baseURL.scheme = template!.scheme
                    }
                    if let string = template?.string {
                        templateString = string
                    }
                    guard let url = baseURL.url?.absoluteString else {
                        return ""
                    }
                    
                    let path = templateString.replace("{version}", version)
                    return url + "/" + path
                }
                
                self._availableTiles = try metadata.getArray("available").map { $0 }
                
                if let attribution = try metadata.getStringOrNil("attribution") {
                    self.credit = Credit(text: attribution)
                }
                
                // The vertex normals defined in the 'octvertexnormals' extension is identical to the original
                // contents of the original 'vertexnormals' extension.  'vertexnormals' extension is now
                // deprecated, as the extensionLength for this extension was incorrectly using big endian.
                // We maintain backwards compatibility with the legacy 'vertexnormal' implementation
                // by setting the _littleEndianExtensionSize to false. Always prefer 'octvertexnormals'
                // over 'vertexnormals' if both extensions are supported by the server.
                if let extensions = try metadata.getArrayOrNil("extensions") {
                    if extensions.contains("octvertexnormals") {
                        self._hasVertexNormals = true
                    } else if extensions.contains("vertexnormals") {
                        self._hasVertexNormals = true
                        self._littleEndianExtensionSize = false
                    }
                    if extensions.contains("watermask") {
                        self._hasWaterMask = true
                    }
                }
                DispatchQueue.main.async(execute: {
                    self.ready = true
                })
                //that._readyPromise.resolve(true);
            } catch {
                logPrint(.error, "invalid JSON from terrain provider")
                return
            }
        }
     
        let metadataFailure = { (data: Data) in
            // If the metadata is not found, assume this is a pre-metadata heightmap tileset.
            /*if (defined(data) && data.statusCode === 404) {
                metadataSuccess({
                    tilejson: '2.1.0',
                    format : 'heightmap-1.0',
                    version : '1.0.0',
                    scheme : 'tms',
                    tiles : [
                    '{z}/{x}/{y}.terrain?v={version}'
                    ]
                });
                return;
            }
            var message = 'An error occurred while accessing ' + metadataUrl + '.';
            metadataError = TileProviderError.handleError(metadataError, that, that._errorEvent, message, undefined, undefined, undefined, requestMetadata);*/
        }
        
        let metadataHeaders = ["Accept": "application/json"]
        
        let metadataOperation = NetworkOperation(url: metadataUrl, headers: metadataHeaders)
        
        metadataOperation.completionBlock = {
            if let _ = metadataOperation.error {
                metadataFailure(metadataOperation.data)
                return
            }
            metadataSuccess(metadataOperation.data)
        }
        metadataOperation.enqueue()
    }

/**
* When using the Quantized-Mesh format, a tile may be returned that includes additional extensions, such as PerVertexNormals, watermask, etc.
* This enumeration defines the unique identifiers for each type of extension data that has been appended to the standard mesh data.
*
* @namespace
* @alias QuantizedMeshExtensionIds
* @see CesiumTerrainProvider
* @private
*/
    enum QuantizedMeshExtensionIds: UInt8 {
    /**
    * Oct-Encoded Per-Vertex Normals are included as an extension to the tile mesh
    *
    * @type {Number}
    * @constant
    * @default 1
    */
    case octVertexNormals = 1
    /**
    * A watermask is included as an extension to the tile mesh
    *
    * @type {Number}
    * @constant
    * @default 2
    */
    case waterMask
}
    
    fileprivate func getRequestHeader(_ extensionsList: [String]?) -> [String: String] {
        if extensionsList == nil || extensionsList!.count == 0 {
            return ["Accept": "application/vnd.quantized-mesh,application/octet-stream;q=0.9,*/*;q=0.01"]
        } else {
            let extensions = extensionsList!.joined(separator: "-")
            return ["Accept" : "application/vnd.quantized-mesh;extensions=" + extensions + ",application/octet-stream;q=0.9,*/*;q=0.01"]
        }
    }

    /*
     function createHeightmapTerrainData(provider, buffer, level, x, y, tmsY) {
     var heightBuffer = new Uint16Array(buffer, 0, provider._heightmapWidth * provider._heightmapWidth);
     return new HeightmapTerrainData({
     buffer : heightBuffer,
     childTileMask : new Uint8Array(buffer, heightBuffer.byteLength, 1)[0],
     waterMask : new Uint8Array(buffer, heightBuffer.byteLength + 1, buffer.byteLength - heightBuffer.byteLength - 1),
     width : provider._heightmapWidth,
     height : provider._heightmapWidth,
     structure : provider._heightmapStructure
     });
     }
     */
    
    func createQuantizedMeshTerrainData(_ data: Data, level: Int, x: Int, y: Int, tmsY: Int, littleEndianExtensionSize: Bool = true) -> TerrainData {
        var pos = 0
        let cartesian3Elements = 3
        let boundingSphereElements = cartesian3Elements + 1
        let cartesian3Length = MemoryLayout<Double>.size * cartesian3Elements
        let boundingSphereLength = MemoryLayout<Double>.size * boundingSphereElements
        let encodedVertexElements = 3
        let encodedVertexLength = MemoryLayout<UInt16>.size * encodedVertexElements
        let triangleElements = 3
        var bytesPerIndex = MemoryLayout<UInt16>.size
        var triangleLength = bytesPerIndex * triangleElements
        
        let center = Cartesian3(
            x: data.getFloat64(pos),
            y: data.getFloat64(pos + 8),
            z: data.getFloat64(pos + 16)
        )
        pos += cartesian3Length
        
        let minimumHeight = Double(data.getFloat32(pos))
        pos += MemoryLayout<Float>.size
        let maximumHeight = Double(data.getFloat32(pos))
        pos += MemoryLayout<Float>.size
        
        let boundingSphere = BoundingSphere(
            center: Cartesian3(
                x: data.getFloat64(pos),
                y: data.getFloat64(pos + 8),
                z: data.getFloat64(pos + 16)),
            radius: data.getFloat64(pos + cartesian3Length)
        )
        pos += boundingSphereLength
        
        let horizonOcclusionPoint = Cartesian3(
            x: data.getFloat64(pos),
            y: data.getFloat64(pos + 8),
            z: data.getFloat64(pos + 16)
        )
        pos += cartesian3Length
        
        //let vertexCount = Int(data.getUInt32(pos))
        let vertexCount = data.getUInt(pos)
        pos += MemoryLayout<UInt32>.size
        
        var encodedVertexBuffer = data.getUInt16Array(pos, elementCount: vertexCount * encodedVertexElements)
        pos += vertexCount * encodedVertexLength
        
        if vertexCount > Math.SixtyFourKilobytes {
            // More than 64k vertices, so indices are 32-bit.
            bytesPerIndex = MemoryLayout<UInt32>.size
            triangleLength = bytesPerIndex * triangleElements
        }
        
        func zigZagDecode(_ value: UInt16) -> Int16 {
            let int32Value = Int32(value)
            return Int16((int32Value >> 1) ^ (-(int32Value & 1)))
        }
        
        var u: UInt16 = 0
        var v: UInt16 = 0
        var height: UInt16 = 0
        
        // Decode the vertex buffer.
        let uBuffer = encodedVertexBuffer[0..<vertexCount].map { (value) -> UInt16 in
            u = u &+ UInt16(bitPattern: zigZagDecode(value))
            return u
        }
        let vBuffer = encodedVertexBuffer[vertexCount..<(vertexCount * 2)].map { (value) -> UInt16 in
            v = v &+ UInt16(bitPattern: zigZagDecode(value))
            return v
        }
        let heightBuffer = encodedVertexBuffer[(vertexCount * 2)..<(vertexCount * 3)].map { (value) -> UInt16 in
            height = height &+ UInt16(bitPattern: zigZagDecode(value))
            return height
        }
        encodedVertexBuffer = uBuffer + vBuffer + heightBuffer
        // skip over any additional padding that was added for 2/4 byte alignment
        if pos % bytesPerIndex != 0 {
            pos += (bytesPerIndex - (pos % bytesPerIndex))
        }
        
        //let triangleCount = Int(data.getUInt32(pos))
        let triangleCount = data.getUInt(pos)
        pos += MemoryLayout<UInt32>.stride
        
        // High water mark decoding based on decompressIndices_ in webgl-loader's loader.js.
        // https://code.google.com/p/webgl-loader/source/browse/trunk/samples/loader.js?r=99#55
        // Copyright 2012 Google Inc., Apache 2.0 license.
        var highest = 0
        
        let indices = IndexDatatype
            .createIntegerIndexArrayFromData(data, numberOfVertices: vertexCount, byteOffset: pos, length: triangleCount * triangleElements)
            .map { (value: Int) -> Int in
                let result = highest - Int(value)
                if value == 0 {
                    highest += 1
                }
                return result
        }
        
        pos += triangleCount * triangleLength
                
        //let westVertexCount = Int(data.getUInt32(pos))
        let westVertexCount = data.getUInt(pos)
        pos += MemoryLayout<UInt32>.size
        let westIndices = IndexDatatype.createIntegerIndexArrayFromData(data, numberOfVertices: vertexCount, byteOffset: pos, length: westVertexCount)
        pos += westVertexCount * bytesPerIndex
        
        //let southVertexCount = Int(data.getUInt32(pos))
        let southVertexCount = data.getUInt(pos)
        pos += MemoryLayout<UInt32>.size
        let southIndices = IndexDatatype.createIntegerIndexArrayFromData(data, numberOfVertices: vertexCount, byteOffset: pos, length: southVertexCount)
        pos += southVertexCount * bytesPerIndex
        
        //let eastVertexCount = Int(data.getUInt32(pos))
        let eastVertexCount = data.getUInt(pos)
        pos += MemoryLayout<UInt32>.size
        let eastIndices = IndexDatatype.createIntegerIndexArrayFromData(data, numberOfVertices: vertexCount, byteOffset: pos, length: eastVertexCount)
        pos += eastVertexCount * bytesPerIndex
        
        //let northVertexCount = Int(data.getUInt32(pos))
        let northVertexCount = data.getUInt(pos)
        pos += MemoryLayout<UInt32>.size
        let northIndices = IndexDatatype.createIntegerIndexArrayFromData(data, numberOfVertices: vertexCount, byteOffset: pos, length: northVertexCount)
        pos += northVertexCount * bytesPerIndex
        
        var encodedNormalBuffer: [UInt8]? = nil
        var waterMaskBuffer: [UInt8]? = nil
        while pos < data.count {
            let extensionId = QuantizedMeshExtensionIds(rawValue: data.getUInt8(pos))
            pos += MemoryLayout<UInt8>.size
            //let extensionLength = Int(data.getUInt32(pos, littleEndian: _littleEndianExtensionSize))
            let extensionLength = data.getUInt(pos, littleEndian: _littleEndianExtensionSize)
            pos += MemoryLayout<UInt32>.size
            
            if extensionId == .octVertexNormals && requestVertexNormals {
                encodedNormalBuffer = data.getUInt8Array(pos, elementCount: vertexCount * 2)
            } else if extensionId == .waterMask && _requestWaterMask {
                waterMaskBuffer = data.getUInt8Array(pos, elementCount: extensionLength)
            }
            pos += extensionLength
        }
        
        let skirtHeight = levelMaximumGeometricError(level) * 5.0
        
        let rectangle = tilingScheme.tileXYToRectangle(x: x, y: y, level: level)
        let orientedBoundingBox: OrientedBoundingBox?
        if (rectangle.width < .pi/2 + Math.Epsilon5) {
            // Here, rectangle.width < pi/2, and rectangle.height < pi
            // (though it would still work with rectangle.width up to pi)
            
            // The skirt is not included in the OBB computation. If this ever
            // causes any rendering artifacts (cracks), they are expected to be
            // minor and in the corners of the screen. It's possible that this
            // might need to be changed - just change to `minimumHeight - skirtHeight`
            // A similar change might also be needed in `upsampleQuantizedTerrainMesh.js`.
            orientedBoundingBox = OrientedBoundingBox(
                fromRectangle: rectangle,
                minimumHeight: minimumHeight,
                maximumHeight: maximumHeight,
                ellipsoid: tilingScheme.ellipsoid
            )
        } else {
            orientedBoundingBox = nil
        }
       let terrainData = QuantizedMeshTerrainData(
            quantizedVertices: encodedVertexBuffer,
            indices: indices,
            encodedNormals: encodedNormalBuffer,
            minimumHeight: minimumHeight,
            maximumHeight: maximumHeight,
            boundingSphere: boundingSphere,
            orientedBoundingBox: orientedBoundingBox,
            horizonOcclusionPoint: horizonOcclusionPoint,
            westIndices: westIndices,
            southIndices: southIndices,
            eastIndices: eastIndices,
            northIndices: northIndices,
            westSkirtHeight: skirtHeight,
            southSkirtHeight: skirtHeight,
            eastSkirtHeight: skirtHeight,
            northSkirtHeight: skirtHeight,
            childTileMask: getChildMaskForTile(level, x: x, y: tmsY),
            waterMask: waterMaskBuffer
        )
        return terrainData
    }
 
    
    /**
     * Requests the geometry for a given tile.  This function should not be called before
     * {@link CesiumTerrainProvider#ready} returns true.  The result must include terrain data and
     * may optionally include a water mask and an indication of which child tiles are available.
     *
     * @param {Number} x The X coordinate of the tile for which to request geometry.
     * @param {Number} y The Y coordinate of the tile for which to request geometry.
     * @param {Number} level The level of the tile for which to request geometry.
     * @param {Boolean} [throttleRequests=true] True if the number of simultaneous requests should be limited,
     *                  or false if the request should be initiated regardless of the number of requests
     *                  already in progress.
     * @returns {Promise.<TerrainData>|undefined} A promise for the requested geometry.  If this method
     *          returns undefined instead of a promise, it is an indication that too many requests are already
     *          pending and the request will be retried later.
     *
     * @exception {DeveloperError} This function must not be called before {@link CesiumTerrainProvider#ready}
     *            returns true.
     */
    func resourceForTileGeometry(x: Int, y: Int, level: Int) -> Resource? {
        assert(ready, "requestTileGeometry must not be called before the terrain provider is ready.")
        /*
        if level > 13 {
            return nil
        }
        */
        
        var layerToUse: LayerInformation?
        let layerCount = _layers.count
        
        if layerCount == 1 { // Optimized path for single layers
            layerToUse = _layers[0]
        } else {
            for layer in _layers {
                if layer.availability == nil || layer.availability!.isTileAvailable(level: level, x: x, y: y) {
                    layerToUse = layer
                    break
                }
            }
        }
        
        if layerToUse == nil {
            return nil
        }
        
        let urlTemplates = layerToUse!.tileUrlTemplates
        if urlTemplates.count == 0 {
            return nil
        }
        
        let yTiles = tilingScheme.numberOfYTilesAt(level: level)
        
        let tmsY = (yTiles - y - 1)
        
        var extensionList: [String] = []
        if requestVertexNormals == true && layerToUse!.hasVertexNormals {
            extensionList.append((layerToUse!.littleEndianExtensionSize) ? "octvertexnormals" : "vertexnormals")
        }
        if _requestWaterMask && layerToUse!.hasWaterMask {
            extensionList.append("watermask")
        }
        
        var headers: [String: String] = [:]
        var query: [URLQueryItem] = []
        guard let url = urlTemplates[(x + tmsY + level) % urlTemplates.count].string else {
            assertionFailure("invalid urlTemplates: \(urlTemplates.debugDescription)")
            return nil
        }
        let resource = layerToUse!.resource
        if let ionResource = resource as? IonResource {
            if let _ = ionResource._ionEndpoint["externalType"]?.string {
                query.append(URLQueryItem(name: "extensions", value: extensionList.joined(separator: "-")))
                headers = getRequestHeader(nil)
            } else {
                headers = getRequestHeader(extensionList)
            }
        }
        
        let templateValues: [String: String] = ["version": layerToUse!.version, "z": String(level), "x": String(x), "y": String(tmsY)]
        let derivedResource = resource.getDerivedResource(url: url, queryParameters: query, headers: headers, templateValues: templateValues)
        //print("url: \(derivedResource.url)")

        return derivedResource
    }
    func requestTileGeometry(x: Int, y: Int, level: Int) -> Promise<TerrainData>? {
        assert(ready, "requestTileGeometry must not be called before the terrain provider is ready.")
        
        var layerToUse: LayerInformation?
        let layerCount = _layers.count
        
        if layerCount == 1 { // Optimized path for single layers
            layerToUse = _layers[0]
        } else {
            for layer in _layers {
                if layer.availability == nil || layer.availability!.isTileAvailable(level: level, x: x, y: y) {
                    layerToUse = layer
                    break
                }
            }
        }
        if layerToUse == nil {
            return nil
        }
        
        let urlTemplates = layerToUse!.tileUrlTemplates
        if urlTemplates.count == 0 {
            return nil
        }
        
        let yTiles = tilingScheme.numberOfYTilesAt(level: level)
        
        let tmsY = (yTiles - y - 1)
        
        os_signpost(.begin, log: requestGeometryLog, name: "geometryRequest", "x:%d y:%d l:%d", x, y, level)
        guard let derivedResource = resourceForTileGeometry(x: x, y: y, level: level) else { return nil }
        return Promise<TerrainData>({ resolve, reject, _ in
            derivedResource.fetchArrayBuffer().then({ data in
                os_signpost(.event, log: requestGeometryLog, name: "geometryRequest", "got array buffer")
                let terrainData = self.createQuantizedMeshTerrainData(data, level: level, x: x, y: y, tmsY: tmsY, littleEndianExtensionSize: layerToUse!.littleEndianExtensionSize)
                os_signpost(.end, log: requestGeometryLog, name: "geometryRequest", "x:%d y:%d l:%d", x, y, level)
                resolve(terrainData)
            }).catch({ error in
                os_signpost(.event, log: requestGeometryLog, name: "geometryRequest", "fetchArrayBuffer error")
                os_signpost(.end, log: requestGeometryLog, name: "geometryRequest", "x:%d y:%d l:%d", x, y, level)
                reject(error)
            })
        })
    }
    func requestTileGeometry(x: Int, y: Int, level: Int, throttleRequests: Bool = true, completionBlock: @escaping (TerrainData?) -> ()) -> NetworkOperation? {
        assert(ready, "requestTileGeometry must not be called before the terrain provider is ready.")
        
        var layerToUse: LayerInformation?
        let layerCount = _layers.count
        
        if layerCount == 1 { // Optimized path for single layers
            layerToUse = _layers[0]
        } else {
            for layer in _layers {
                if layer.availability == nil || layer.availability!.isTileAvailable(level: level, x: x, y: y) {
                    layerToUse = layer
                    break
                }
            }
        }

        if layerToUse == nil {
            completionBlock(nil)
            return nil
        }
        
        let urlTemplates = layerToUse!.tileUrlTemplates
        if urlTemplates.count == 0 {
            completionBlock(nil)
            return nil
        }
        
        let yTiles = tilingScheme.numberOfYTilesAt(level: level)
        
        let tmsY = (yTiles - y - 1)
        /*
        
        var extensionList: [String] = []
        if requestVertexNormals == true && layerToUse!.hasVertexNormals {
            extensionList.append((layerToUse!.littleEndianExtensionSize) ? "octvertexnormals" : "vertexnormals")
        }
        if _requestWaterMask && layerToUse!.hasWaterMask {
            extensionList.append("watermask")
        }
        
        var headers: [String: String] = [:]
        var query: [URLQueryItem] = []
        guard let url = urlTemplates[(x + tmsY + level) % urlTemplates.count].string else {
            assertionFailure("invalid urlTemplates: \(urlTemplates.debugDescription)")
            completionBlock(nil)
            return nil
        }
        let resource = layerToUse!.resource
        if let ionResource = resource as? IonResource {
            if let _ = ionResource._ionEndpoint["externalType"]?.string {
                query.append(URLQueryItem(name: "extensions", value: extensionList.joined(separator: "-")))
                headers = getRequestHeader(nil)
            } else {
                headers = getRequestHeader(extensionList)
            }
        }
        
        let templateValues: [String: String] = ["version": layerToUse!.version, "z": String(level), "x": String(x), "y": String(tmsY)]
        let derivedResource = resource.getDerivedResource(url: url, queryParameters: query, headers: headers, templateValues: templateValues)
        */
        guard let derivedResource = resourceForTileGeometry(x: x, y: y, level: level) else {
            completionBlock(nil)
            return nil
        }

        derivedResource.fetchArrayBuffer().then({ data in
            let terrainData = self.createQuantizedMeshTerrainData(data, level: level, x: x, y: y, tmsY: tmsY, littleEndianExtensionSize: layerToUse!.littleEndianExtensionSize)
            completionBlock(terrainData)
        }).catch({ error in
            completionBlock(nil)
            logPrint(.error, error.localizedDescription)
        })
        
        /*
         do {
         let data = try await(derivedResource.fetchArrayBuffer())
         if self._heightmapStructure != nil {
         assertionFailure("heigt map structure not implemented")
         completionBlock(nil)
         //return createHeightmapTerrainData(that, buffer, level, x, y, tmsY);
         } else {
         let terrainData = self.createQuantizedMeshTerrainData(data, level: level, x: x, y: y, tmsY: tmsY, littleEndianExtensionSize: layerToUse!.littleEndianExtensionSize)
         //, completionBlock: { terrainData in
         //})
         completionBlock(terrainData)
         }
         } catch {
         logPrint(.error, "fetchArrayBuffer() error: \(error.localizedDescription)")
         }
         */
        return nil
    }

    
        /*
        defineProperties(CesiumTerrainProvider.prototype, {
        /**
        * Gets an event that is raised when the terrain provider encounters an asynchronous error.  By subscribing
        * to the event, you will be notified of the error and can potentially recover from it.  Event listeners
        * are passed an instance of {@link TileProviderError}.
        * @memberof CesiumTerrainProvider.prototype
        * @type {Event}
        */
        errorEvent : {
        get : function() {
        return this._errorEvent;
        }
        },
        
        /**
        * Gets the credit to display when this terrain provider is active.  Typically this is used to credit
        * the source of the terrain.  This function should not be called before {@link CesiumTerrainProvider#ready} returns true.
        * @memberof CesiumTerrainProvider.prototype
        * @type {Credit}
        */
        credit : {
        get : function() {
        //>>includeStart('debug', pragmas.debug)
        if (!this._ready) {
        throw new DeveloperError('credit must not be called before the terrain provider is ready.');
        }
        //>>includeEnd('debug');
        
        return this._credit;
        }
        },
        
        /**
        * Gets the tiling scheme used by this provider.  This function should
        * not be called before {@link CesiumTerrainProvider#ready} returns true.
        * @memberof CesiumTerrainProvider.prototype
        * @type {GeographicTilingScheme}
        */
        tilingScheme : {
        get : function() {
        //>>includeStart('debug', pragmas.debug)
        if (!this._ready) {
        throw new DeveloperError('tilingScheme must not be called before the terrain provider is ready.');
        }
        //>>includeEnd('debug');
        
        return this._tilingScheme;
        }
        },
        
        /**
        * Gets a value indicating whether or not the provider is ready for use.
        * @memberof CesiumTerrainProvider.prototype
        * @type {Boolean}
        */
        ready : {
        get : function() {
        return this._ready;
        }
        },
        
        /**
        * Gets a promise that resolves to true when the provider is ready for use.
        * @memberof CesiumTerrainProvider.prototype
        * @type {Promise.<Boolean>}
        * @readonly
        */
        readyPromise : {
        get : function() {
        return this._readyPromise.promise;
        }
        },
        */
/*
        /**
        * Boolean flag that indicates if the client should request a watermask from the server.
        * Watermask data is appended to the standard tile mesh data only if the client requests the watermask and
        * if the server provides a watermask.
        * @memberof CesiumTerrainProvider.prototype
        * @type {Boolean}
        */
        requestWaterMask : {
        get : function() {
        return this._requestWaterMask;
        }
        }
        });
        */
    
    /**
     * Gets the maximum geometric error allowed in a tile at a given level.
     *
     * @param {Number} level The tile level for which to get the maximum geometric error.
     * @returns {Number} The maximum geometric error.
     */
    func levelMaximumGeometricError (_ level: Int) -> Double {
        return _levelZeroMaximumGeometricError / Double(1 << level)
    }
    
    func calculateTileCount () {
        var tileCount = 0
        do {
            guard let availableTiles = _availableTiles else {
                return
            }
            for i in 0...15 {
                var thisLevelCount = 0
                guard let tileArray = availableTiles[i].array else { continue }
                do {
                    for tileBlock in tileArray {
                        let blockStartX = try tileBlock.getInt("startX")
                        let blockStartY = try tileBlock.getInt("startY")
                        let blockEndX = try tileBlock.getInt("endX")
                        let blockEndY = try tileBlock.getInt("endY")
                        let blockWidth = blockEndX - blockStartX + 1
                        let blockHeight = blockEndY - blockStartY + 1
                        let blockCount = blockWidth * blockHeight
                        thisLevelCount += blockCount
                    }
                }
                tileCount += thisLevelCount
                print("\(thisLevelCount) tiles at level \(i), \(tileCount) total")
                
            }
        } catch {
            return
        }
    }
    
    func getChildMaskForTile(_ level: Int, x: Int, y: Int) -> Int {
        guard let availableTiles = _availableTiles else {
            return 15
        }
        
        if availableTiles.count == 0 {
            return 15
        }
        
        let childLevel = level + 1
        if childLevel >= availableTiles.count {
            return 0
        }

        guard let levelAvailable = availableTiles[childLevel].array else {
            return 0
        }
        
        var mask = 0
        mask |= isTileInRange(levelAvailable, x: 2 * x, y: 2 * y) ? 1 : 0
        mask |= isTileInRange(levelAvailable, x: 2 * x + 1, y: 2 * y) ? 2 : 0
        mask |= isTileInRange(levelAvailable, x: 2 * x, y: 2 * y + 1) ? 4 : 0
        mask |= isTileInRange(levelAvailable, x: 2 * x + 1, y: 2 * y + 1) ? 8 : 0
        
        return mask
    }
    
    func isTileInRange(_ levelAvailable: JSONArray, x: Int, y: Int) -> Bool {
        for range in levelAvailable {
            do {
                let startX = try range.getInt("startX")
                let endX = try range.getInt("endX")
                let startY = try range.getInt("startY")
                let endY = try range.getInt("endY")
                
                if x >= startX && x <= endX && y >= startY && y <= endY {
                    return true
                }
                
            } catch {
                return false
            }

        }
        return false
    }
    /*
        /**
        * Determines whether data for a tile is available to be loaded.
        *
        * @param {Number} x The X coordinate of the tile for which to request geometry.
        * @param {Number} y The Y coordinate of the tile for which to request geometry.
        * @param {Number} level The level of the tile for which to request geometry.
        * @returns {Boolean} Undefined if not supported, otherwise true or false.
        */
        CesiumTerrainProvider.prototype.getTileDataAvailable = function(x, y, level) {
        var available = this._availableTiles;
        
        if (!available || available.length === 0) {
        return undefined;
        } else {
        if (level >= available.length) {
        return false;
        }
        var levelAvailable = available[level];
        var yTiles = this._tilingScheme.getNumberOfYTilesAtLevel(level);
        var tmsY = (yTiles - y - 1);
        return isTileInRange(levelAvailable, x, tmsY);
        }
        };
        
        return CesiumTerrainProvider;
        });

    */
    static func estimatedLevelZeroGeometricErrorForAHeightmap(
        ellipsoid: Ellipsoid,
        tileImageWidth: Int,
        numberOfTilesAtLevelZero: Int) -> Double {
            return ellipsoid.maximumRadius * Math.TwoPi * 0.25/*heightmapTerrainQuality*/ / Double(tileImageWidth * numberOfTilesAtLevelZero)
    }
    
    func getTileDataAvailable(x: Int, y: Int, level: Int) -> Bool? {
        guard let availability = self._availability else { return nil }
        if level > availability._maximumLevel {
            return false
        }
        if availability.isTileAvailable(level: level, x: x, y: y) {
            return true
        }
        
        return false
    }
}
