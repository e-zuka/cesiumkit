//
//  Cartographic.swift
//  CesiumKit
//
//  Created by Ryan Walklin on 9/06/14.
//  Copyright (c) 2014 Test Toast. All rights reserved.
//

import Foundation

/**
* A position defined by longitude, latitude, and height.
* @alias Cartographic
* @constructor
*
* @param {Number} [longitude=0.0] The longitude, in radians.
* @param {Number} [latitude=0.0] The latitude, in radians.
* @param {Number} [height=0.0] The height, in meters, above the ellipsoid.
*
* @see Ellipsoid
*/

public struct Cartographic: Equatable {
    /**
    * The longitude, in radians.
    * @type {Number}
    * @default 0.0
    */
    public var longitude: Double = 0.0
    /**
    * The latitude, in radians.
    * @type {Number}
    * @default 0.0
    */
    public var latitude: Double = 0.0
    
    /**
    * The height, in meters, above the ellipsoid.
    * @type {Number}
    * @default 0.0
    */
    public var height: Double = 0.0
    
    public init(longitude: Double = 0.0, latitude: Double = 0.0, height: Double = 0.0) {
        self.longitude = longitude
        self.latitude = latitude
        self.height = height
    }

    /**
    * Creates a new Cartographic instance from longitude and latitude
    * specified in radians.
    *
    * @param {Number} longitude The longitude, in radians.
    * @param {Number} latitude The latitude, in radians.
    * @param {Number} [height=0.0] The height, in meters, above the ellipsoid.
    * @param {Cartographic} [result] The object onto which to store the result.
    * @returns {Cartographic} The modified result parameter or a new Cartographic instance if one was not provided.
    */
    static public func fromRadians(_ longitude: Double, latitude: Double, height: Double = 0.0) -> Cartographic {
        
        return Cartographic(longitude: longitude, latitude: latitude, height: height)
    }
    
    /**
    * Creates a new Cartographic instance from longitude and latitude
    * specified in degrees.  The values in the resulting object will
    * be in radians.
    *
    * @param {Number} longitude The longitude, in degrees.
    * @param {Number} latitude The latitude, in degrees.
    * @param {Number} [height=0.0] The height, in meters, above the ellipsoid.
    * @param {Cartographic} [result] The object onto which to store the result.
    * @returns {Cartographic} The modified result parameter or a new Cartographic instance if one was not provided.
    */
    static public func fromDegrees(_ longitude: Double, latitude: Double, height: Double = 0.0) -> Cartographic {
        let lon = Math.toRadians(longitude)
        let lat = Math.toRadians(latitude)
        
        return Cartographic.fromRadians(lon, latitude: lat, height: height);
    }
    static public func fromDegrees(_ longitude: Float, latitude: Float, height: Float = 0.0) -> Cartographic {
        let lon = Math.toRadians(Double(longitude))
        let lat = Math.toRadians(Double(latitude))
        
        return Cartographic.fromRadians(lon, latitude: lat, height: Double(height));
    }
    
    /*
     +    var cartesianToCartographicN = new Cartesian3();
     +    var cartesianToCartographicP = new Cartesian3();
     +    var cartesianToCartographicH = new Cartesian3();
     +    var wgs84OneOverRadii = new Cartesian3(1.0 / 6378137.0, 1.0 / 6378137.0, 1.0 / 6356752.3142451793);
     +    var wgs84OneOverRadiiSquared = new Cartesian3(1.0 / (6378137.0 * 6378137.0), 1.0 / (6378137.0 * 6378137.0), 1.0 / (6356752.3142451793 * 6356752.3142451793));
     +    var wgs84CenterToleranceSquared = CesiumMath.EPSILON1;
     +
     +    /**
     +     * Creates a new Cartographic instance from a Cartesian position. The values in the
     +     * resulting object will be in radians.
     +     *
     +     * @param {Cartesian3} cartesian The Cartesian position to convert to cartographic representation.
     +     * @param {Ellipsoid} [ellipsoid=Ellipsoid.WGS84] The ellipsoid on which the position lies.
     +     * @param {Cartographic} [result] The object onto which to store the result.
     +     * @returns {Cartographic} The modified result parameter, new Cartographic instance if none was provided, or undefined if the cartesian is at the center of the ellipsoid.
     +     */
     +    Cartographic.fromCartesian = function(cartesian, ellipsoid, result) {
     +        var oneOverRadii = defined(ellipsoid) ? ellipsoid.oneOverRadii : wgs84OneOverRadii;
     +        var oneOverRadiiSquared = defined(ellipsoid) ? ellipsoid.oneOverRadiiSquared : wgs84OneOverRadiiSquared;
     +        var centerToleranceSquared = defined(ellipsoid) ? ellipsoid._centerToleranceSquared : wgs84CenterToleranceSquared;
     +
     +        //`cartesian is required.` is thrown from scaleToGeodeticSurface
     +        var p = scaleToGeodeticSurface(cartesian, oneOverRadii, oneOverRadiiSquared, centerToleranceSquared, cartesianToCartographicP);
     +
     +        if (!defined(p)) {
     +            return undefined;
     +        }
     +
     +        var n = Cartesian3.multiplyComponents(cartesian, oneOverRadiiSquared, cartesianToCartographicN);
     +        n = Cartesian3.normalize(n, n);
     +
     +        var h = Cartesian3.subtract(cartesian, p, cartesianToCartographicH);
     +
     +        var longitude = Math.atan2(n.y, n.x);
     +        var latitude = Math.asin(n.z);
     +        var height = CesiumMath.sign(Cartesian3.dot(h, cartesian)) * Cartesian3.magnitude(h);
     +
     +        if (!defined(result)) {
     +            return new Cartographic(longitude, latitude, height);
     +        }
     +        result.longitude = longitude;
     +        result.latitude = latitude;
     +        result.height = height;
     +        return result;
     +    };
     +  
    */
    static let oneOverRadii = Cartesian3(x: 1.0 / 6378137.0, y: 1.0 / 6378137.0, z: 1.0 / 6356752.3142451793)
    static let oneOverRadiiSquared = Cartesian3(x: 1.0 / (6378137.0 * 6378137.0), y: 1.0 / (6378137.0 * 6378137.0), z: 1.0 / (6356752.3142451793 * 6356752.3142451793))
    static let centerToleranceSquared = Math.Epsilon1
    static public func fromCartesian(_ cartesian: Cartesian3) -> Cartographic? {
        guard let p = scaleToGeodeticSurface(cartesian: cartesian) else {
            return nil
        }
        let n = cartesian.multiplyComponents(oneOverRadiiSquared).normalize()
        
        let h = cartesian.subtract(p)
        
        let longitude = atan2(n.y, n.x)
        let latitude = asin(n.z)
        let _h = h.dot(cartesian) * h.magnitude
        let height: Double = (_h == 0) ? 0 : (_h > 0) ? 1 : -1
        
        return Cartographic(longitude: longitude, latitude: latitude, height: height)
    }
    
    /**
    * Compares the provided cartographics componentwise and returns
    * <code>true</code> if they are equal, <code>false</code> otherwise.
    *
    * @param {Cartographic} [left] The first cartographic.
    * @param {Cartographic} [right] The second cartographic.
    * @returns {Boolean} <code>true</code> if left and right are equal, <code>false</code> otherwise.
    */
    /* @infix func == (left: Cartographic, right: Cartographic) -> Bool {
    
    return (left.longitude == right.longitude) &&
    (left.latitude == right.latitude) &&
    (left.height == right.height))
    }*/
    
    /**
    * Compares the provided cartographics componentwise and returns
    * <code>true</code> if they are within the provided epsilon,
    * <code>false</code> otherwise.
    *
    * @param {Cartographic} [left] The first cartographic.
    * @param {Cartographic} [right] The second cartographic.
    * @param {Number} epsilon The epsilon to use for equality testing.
    * @returns {Boolean} <code>true</code> if left and right are within the provided epsilon, <code>false</code> otherwise.
    */
    func equalsEpsilon(_ other: Cartographic, epsilon: Double) -> Bool {
        return (abs(self.longitude - other.longitude) <= epsilon) &&
            (abs(self.latitude - other.latitude) <= epsilon) &&
            (abs(self.height - other.height) <= epsilon)
    }
    
    /**
    * Creates a string representing the provided cartographic in the format '(longitude, latitude, height)'.
    *
    * @param {Cartographic} cartographic The cartographic to stringify.
    * @returns {String} A string representing the provided cartographic in the format '(longitude, latitude, height)'.
    */
    func toString() -> String {
        return "(\(longitude)), (\(latitude)), (\(height))";
    }
    
    /**
    * An immutable Cartographic instance initialized to (0.0, 0.0, 0.0).
    *
    * @type {Cartographic}
    * @constant
    */
    static func zero() -> Cartographic {
        return Cartographic(longitude: 0.0, latitude: 0.0, height: 0.0)
    }
    
    static private func scaleToGeodeticSurface(cartesian: Cartesian3) -> Cartesian3? {
        let positionX = cartesian.x
        let positionY = cartesian.y
        let positionZ = cartesian.z
        
        let oneOverRadiiX = oneOverRadii.x
        let oneOverRadiiY = oneOverRadii.y
        let oneOverRadiiZ = oneOverRadii.z
        
        let x2 = positionX * positionX * oneOverRadiiX * oneOverRadiiX
        let y2 = positionY * positionY * oneOverRadiiY * oneOverRadiiY
        let z2 = positionZ * positionZ * oneOverRadiiZ * oneOverRadiiZ
        
        // Compute the squared ellipsoid norm.
        let squaredNorm = x2 + y2 + z2
        let ratio = sqrt(1.0 / squaredNorm)
        
        // As an initial approximation, assume that the radial intersection is the projection point.
        let intersection = cartesian.multiplyBy(scalar: ratio)
        
        // If the position is near the center, the iteration will not converge.
        if (squaredNorm < centerToleranceSquared) {
            return !ratio.isFinite ? nil : intersection
        }
        
        let oneOverRadiiSquaredX = oneOverRadiiSquared.x;
        let oneOverRadiiSquaredY = oneOverRadiiSquared.y;
        let oneOverRadiiSquaredZ = oneOverRadiiSquared.z;
        
        // Use the gradient at the intersection point in place of the true unit normal.
        // The difference in magnitude will be absorbed in the multiplier.
        var gradient = Cartesian3()
        gradient.x = intersection.x * oneOverRadiiSquaredX * 2.0
        gradient.y = intersection.y * oneOverRadiiSquaredY * 2.0
        gradient.z = intersection.z * oneOverRadiiSquaredZ * 2.0
        
        // Compute the initial guess at the normal vector multiplier, lambda.
        var lambda = (1.0 - ratio) * cartesian.magnitude / (0.5 * gradient.magnitude)
        var correction = 0.0
        
        var _func: Double = 0
        var denominator: Double = 0
        var xMultiplier: Double = 0
        var yMultiplier: Double = 0
        var zMultiplier: Double = 0
        var xMultiplier2: Double = 0
        var yMultiplier2: Double = 0
        var zMultiplier2: Double = 0
        var xMultiplier3: Double = 0
        var yMultiplier3: Double = 0
        var zMultiplier3: Double = 0
        
        repeat {
            lambda -= correction
            
            xMultiplier = 1.0 / (1.0 + lambda * oneOverRadiiSquaredX)
            yMultiplier = 1.0 / (1.0 + lambda * oneOverRadiiSquaredY)
            zMultiplier = 1.0 / (1.0 + lambda * oneOverRadiiSquaredZ)
            
            xMultiplier2 = xMultiplier * xMultiplier
            yMultiplier2 = yMultiplier * yMultiplier
            zMultiplier2 = zMultiplier * zMultiplier
            
            xMultiplier3 = xMultiplier2 * xMultiplier
            yMultiplier3 = yMultiplier2 * yMultiplier
            zMultiplier3 = zMultiplier2 * zMultiplier
            
            _func = x2 * xMultiplier2 + y2 * yMultiplier2 + z2 * zMultiplier2 - 1.0;
            
            // "denominator" here refers to the use of this expression in the velocity and acceleration
            // computations in the sections to follow.
            denominator = x2 * xMultiplier3 * oneOverRadiiSquaredX + y2 * yMultiplier3 * oneOverRadiiSquaredY + z2 * zMultiplier3 * oneOverRadiiSquaredZ;
            
            let derivative = -2.0 * denominator;
            
            correction = _func / derivative;
        } while abs(_func) > Math.Epsilon12
        
        return Cartesian3(x: positionX * xMultiplier, y: positionY * yMultiplier, z: positionZ * zMultiplier)
    }
}
