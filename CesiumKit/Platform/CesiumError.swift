//
//  Errors.swift
//  CesiumKit
//
//  Created by Ryan Walklin on 18/06/2016.
//  Copyright © 2016 Test Toast. All rights reserved.
//

import Foundation

enum CesiumError: Error, CustomDebugStringConvertible {
    
    case invalidProjectionInput
    
    var debugDescription: String {
        switch self {
        case .invalidProjectionInput:
            return "Invalid Cartesian projection point"
        }
    }
    
}

enum CesiumResourceError: Error, CustomDebugStringConvertible {
    case general
    case invalidJSON(key: String)
    case preMetadata
    case invalidState
    case invalidTileFormat(format: String)
    
    var debugDescription: String {
        switch self {
        case .general:
            return "Cesium Resource Error"
        case .invalidJSON(let key):
            return "Cesium Resource JSON Error with key: \(key)"
        case .preMetadata:
            return "Cesium Resource preMetadata"
        case .invalidState:
            return "Cesium Resource invalid state"
        case .invalidTileFormat(let format):
            return "Cesium Resource invalid tile format: \(format)"
        }
    }
}
