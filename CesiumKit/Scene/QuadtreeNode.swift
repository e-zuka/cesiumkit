//
//  QuadtreeNode.swift
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2018/07/04.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation

class QuadtreeNode {
    let tilingScheme: TilingScheme
    let parent: QuadtreeNode?
    public var level: Int
    public var x: Int
    public var y: Int
    public var extent: Rectangle
    
    var rectangles: [RectangleWithLevel] = []
    var _sw: QuadtreeNode? = nil
    var _se: QuadtreeNode? = nil
    var _nw: QuadtreeNode? = nil
    var _ne: QuadtreeNode? = nil
    
    var nw: QuadtreeNode {
        if _nw == nil {
            _nw = QuadtreeNode(tilingScheme: tilingScheme, parent: self, level: level + 1, x: x * 2, y: y * 2)
        }
        return _nw!
    }
    var ne: QuadtreeNode {
        if _ne == nil {
            _ne = QuadtreeNode(tilingScheme: tilingScheme, parent: self, level: level + 1, x: x * 2 + 1, y: y * 2)
        }
        return _ne!
    }
    var sw: QuadtreeNode {
        if _sw == nil {
            _sw = QuadtreeNode(tilingScheme: tilingScheme, parent: self, level: level + 1, x: x * 2, y: y * 2 + 1)
        }
        return _sw!
    }
    var se: QuadtreeNode {
        if _se == nil {
            _se = QuadtreeNode(tilingScheme: tilingScheme, parent: self, level: level + 1, x: x * 2 + 1, y: y * 2 + 1)
        }
        return _se!
    }
    
    init(tilingScheme: TilingScheme, parent: QuadtreeNode?, level: Int, x: Int, y: Int) {
        self.tilingScheme = tilingScheme
        self.parent = parent
        self.level = level
        self.x = x
        self.y = y
        self.extent = tilingScheme.tileXYToRectangle(x: x, y: y, level: level)
    }
}

extension QuadtreeNode : Equatable {}
func == (lhs: QuadtreeNode, rhs: QuadtreeNode) -> Bool {
    return lhs.x == rhs.x && lhs.y == rhs.y && lhs.level == rhs.level && lhs.extent.equals(rhs.extent)
}
