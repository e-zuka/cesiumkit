//
//  IonImageryProvider.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2018/07/02.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation
import Hydra

class IonImageryProvider : ImageryProvider {
    var delegate: ImageryProviderDelegate? = nil

    var defaultAlpha: Float = 1
    
    var defaultBrightness: Float = 1
    
    var defaultContrast: Float = 1
    
    var defaultHue: Float = 0
    
    var defaultSaturation: Float = 1
    
    var defaultGamma: Float = 1
    
    func tileCredits(x: Int, y: Int, level: Int) -> [Credit] {
        if _ready == false {
            assertionFailure("tileCredits must not be called before the imagery provider is ready.")
        }
        let innerCredits = _imageryProvider!.tileCredits(x: x, y: y, level: level)
        if innerCredits.count < 1 {
            return _tileCredits ?? []
        }
        return (_tileCredits ?? []) + innerCredits
    }
    
    func requestImage(x: Int, y: Int, level: Int, completionBlock: @escaping ((CGImage?) -> Void)) {
        if _ready == false {
            assertionFailure("requestImage must not be called before the imagery provider is ready.")
        }
        delegate?.imageryProvider(_imageryProvider!, providing: level)
        return _imageryProvider!.requestImage(x: x, y: y, level: level, completionBlock: completionBlock)
    }
    
    enum ImageryProviderMapping: String {
        //case ARCGIS_MAPSERVER
        case BING
        //case GOOGLE_EARTH
        //case MAPBOX
        //case SINGLE_TILE
        //case TMS
        //case URL_TEMPLATE
        //case WMS
        //case WMTS
    }
    var ready: Bool {
        return _ready
    }
    var readyPromise: fetchCompletionBlock {
        return _readyPromise
    }
    var rectangle: Rectangle {
        if _ready == false {
            assertionFailure("rectangle must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.rectangle
    }
    var tileWidth: Int {
        if _ready == false {
            assertionFailure("tileWidth must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.tileWidth
    }
    var tileHeight: Int {
        if _ready == false {
            assertionFailure("tileHeight must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.tileHeight
    }
    var maximumLevel: Int {
        if _ready == false {
            assertionFailure("maximumLevel must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.maximumLevel
    }
    var minimumLevel: Int? {
        if _ready == false {
            assertionFailure("minimumLevel must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.minimumLevel
    }
    var tilingScheme: TilingScheme {
        if _ready == false {
            assertionFailure("tilingScheme must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.tilingScheme
    }
    var tileDiscardPolicy: TileDiscardPolicy? {
        if _ready == false {
            assertionFailure("tileDiscardPolicy must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.tileDiscardPolicy
    }
    var errorEvent: Event {
        return _errorEvent
    }
    var credit: Credit? {
        if _ready == false {
            assertionFailure("credit must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.credit
    }
    var hasAlphaChannel: Bool {
        if _ready == false {
            assertionFailure("hasAlphaChannel must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.hasAlphaChannel
    }
    
    var _ready: Bool = false
    var _tileCredits: [Credit]?
    var _errorEvent: Event!
    var _imageryProvider: ImageryProvider? = nil {
        didSet {
            _ready = _imageryProvider != nil
        }
    }

    typealias fetchCompletionBlock = (_ endpoint: JSON) -> Void
    var _readyPromise: fetchCompletionBlock = {(_) -> Void in }
    
    //var defaultMinificationFilter: TextureMinificationFilter?
    //var defaultMagnificationFilter: TextureMagnificationFilter?
    
    init?(assetId: Int) {
        let endpointResource = IonResource._createEndpointResource(assetId: assetId/*, options: [String: Any]?*/)

        _errorEvent = Event()
        _readyPromise = {(_ endpointJSON: JSON)  in
        }
        //endpointResource.fetchJSON(completion: _readyPromise)
        do {
            let endpointJson = try await(endpointResource.fetchJSON())
            guard let endpoint = endpointJson.object else {
                assertionFailure("endpoint JSON is not JSONObject")
                return nil
            }
            assert((endpoint["type"]?.string ?? "") == "IMAGERY", "Cesium ion asset \(assetId) is not an imagery asset.")
            if let externalType = endpoint["externalType"]?.string {
                guard let _imageryProviderType = ImageryProviderMapping(rawValue: externalType) else {
                    assertionFailure("Unrecognized Cesium ion imagery type: \(externalType)")
                    return nil
                }
                switch _imageryProviderType {
                case .BING:
                    guard let key = endpoint["options"]?.object?["key"]?.string else {
                        assertionFailure("no BING KEY found")
                        return nil
                    }
                    self._imageryProvider = BingMapsImageryProvider(key: key)
                    self._ready = true
                }
                /*
                 } else {
                 imageryProvider = createTileMapServiceImageryProvider(url: IonResource(endpoint: endpoint, endpointResource: endpointResource))
                 */
            }
            self._tileCredits = IonResource.getCreditsFromEndpoint(endpoint: endpoint, endpointResource: endpointResource)
        } catch {
            return nil
        }
    }
   
    func pickFeatures(x: Int, y: Int, level: Int, longitude: Double, latitude: Double) -> [ImageryLayerFeatureInfo]? {
        if _ready == false {
            assertionFailure("pickFeatures must not be called before the imagery provider is ready.")
        }
        return _imageryProvider!.pickFeatures(x: x, y: y, level: level, longitude: longitude, latitude: latitude)
    }
}
