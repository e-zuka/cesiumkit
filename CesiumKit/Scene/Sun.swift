//
//  Sun.swift
//  CesiumKitTests
//
//  Created by 飯塚淳 on 2019/10/26.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation
import MetalKit
import MetalPerformanceShaders

/**
 * Draws a sun billboard.
 * <p>This is only supported in 3D and Columbus view.</p>
 *
 * @alias Sun
 * @constructor
 *
 *
 * @example
 * scene.sun = new Cesium.Sun();
 *
 * @see Scene#sun
 */
struct ComputeAndDrawCommand {
    var drawCommand: DrawCommand? = nil
    var computeCommand: ComputeCommand? = nil
}
class Sun {
    var show: Bool = true
    var sunReady: Bool = false
    
    let SOLAR_RADIUS = 6.955e8
    
    let commandQueue: MTLCommandQueue
    
    private var _drawCommand = DrawCommand(boundingVolume: BoundingSphere(), primitiveType: .triangle)
    var drawCommand: DrawCommand {
        _drawCommand
    }
    private var _commands = ComputeAndDrawCommand()
    private var _boundingVolume = BoundingSphere()
    private var _boundingVolume2D = BoundingSphere()
    
    private var _texture: MTLTexture? = nil
    private var _drawingBufferWidth: Double? = nil
    private var _drawingBufferHeight: Double? = nil
    private var _radiusTS: Double! = nil
    private var _size: Double? = nil
    
    var glowFactor: Double {
        get {
            _glowFactor
        }
        set {
            let factor = max(newValue, 0.0)
            _glowFactor = factor
            _glowFactorDirty = true
        }
    }
    private var _glowFactor: Double = 1.0
    private var _glowFactorDirty: Bool = false
    private var _glowLengthTS: Double! = nil
    
    private var _useHDR: Bool? = nil
    
    private var _uniformMap: [String: Any?] {
        ["u_texture": _texture, "u_size": _size]
    }
    
    private var computePipeline: MTLComputePipelineState? = nil
    
    private var renderPipeline: MTLRenderPipelineState? = nil
    private var indexBuffer: Buffer! = nil
    private var vertexBuffer: Buffer! = nil
    
    var renderPassDescriptor = MTLRenderPassDescriptor()
    var blurCompute: MPSImageGaussianBlur? = nil
    var bloomTexturePool: [MTLTexture] = []
    private var blendComputePipeline: MTLComputePipelineState? = nil
    private var brightComputePipeline: MTLComputePipelineState? = nil
    
    private var addMPSCompute: MPSImageAdd? = nil
    private var inPlaceCopy: MPSCopyAllocator? = nil
    /*
    var scratchPositionWC = new Cartesian2();
    var scratchLimbWC = new Cartesian2();
    var scratchPositionEC = new Cartesian4();
    var scratchCartesian4 = new Cartesian4();
    */
    required init(device: MTLDevice, commandQueue: MTLCommandQueue, view: MTKView) {
        self.commandQueue = commandQueue
        _commands.drawCommand = _drawCommand
        //_drawCommand.owner = self

        guard let bundle = Bundle(identifier: "com.testtoast.CesiumKit") else {
            logPrint(.error, "Could not find CesiumKit bundle in executable")
            return
        }
        guard let libraryPath = bundle.url(forResource: "default", withExtension: "metallib")?.path else {
            logPrint(.error, "Could not find shader source from bundle")
            return
        }
        let shaderLibrary: MTLLibrary
        do {
            shaderLibrary = try device.makeLibrary(filepath: libraryPath)
        } catch let error as NSError {
            logPrint(.error, "Could not generate library from compiled shader lib: \(error.localizedDescription)")
            return
        }
        
        guard let computeFunction = shaderLibrary.makeFunction(name: "sunTextureCompute") else { return }
        guard let blendFunction = shaderLibrary.makeFunction(name: "sunBloomCompute") else { return }
        guard let brightFunction = shaderLibrary.makeFunction(name: "sunBrightCompute") else { return }
        do {
            computePipeline = try device.makeComputePipelineState(function: computeFunction)
            blendComputePipeline = try device.makeComputePipelineState(function: blendFunction)
            brightComputePipeline = try device.makeComputePipelineState(function: brightFunction)
        } catch {
            assertionFailure("failed to create Sun compute pipeline")
        }
        
        guard let vertexFunction = shaderLibrary.makeFunction(name: "sunVertexShader"), let fragmentFunction = shaderLibrary.makeFunction(name: "sunFragmentShader") else { return }
        let renderPipelineDescriptor = MTLRenderPipelineDescriptor()
        renderPipelineDescriptor.vertexFunction = vertexFunction
        renderPipelineDescriptor.fragmentFunction = fragmentFunction
        
        // alpha blending
        let blendingState = BlendingState.AlphaBlend()
        func setBlendState(color: MTLRenderPipelineColorAttachmentDescriptor?) {
            color?.isBlendingEnabled = true
            color?.rgbBlendOperation = blendingState.equationRgb.toMetal()
            color?.sourceRGBBlendFactor = blendingState.functionSourceRgb.toMetal()
            color?.destinationRGBBlendFactor = blendingState.functionDestinationRgb.toMetal()
            
            color?.alphaBlendOperation = blendingState.equationAlpha.toMetal()
            color?.sourceAlphaBlendFactor = blendingState.functionSourceAlpha.toMetal()
            color?.destinationAlphaBlendFactor = blendingState.functionDestinationAlpha.toMetal()
        }
        let color = renderPipelineDescriptor.colorAttachments[0]
        color?.pixelFormat = view.colorPixelFormat
        setBlendState(color: color)
        /*
        color?.isBlendingEnabled = true
        color?.rgbBlendOperation = blendingState.equationRgb.toMetal()
        color?.sourceRGBBlendFactor = blendingState.functionSourceRgb.toMetal()
        color?.destinationRGBBlendFactor = blendingState.functionDestinationRgb.toMetal()

        color?.alphaBlendOperation = blendingState.equationAlpha.toMetal()
        color?.sourceAlphaBlendFactor = blendingState.functionSourceAlpha.toMetal()
        color?.destinationAlphaBlendFactor = blendingState.functionDestinationAlpha.toMetal()
        */
        
        /*
        let color2 = renderPipelineDescriptor.colorAttachments[1]
        color2?.pixelFormat = .bgra8Unorm
        setBlendState(color: color2)
        */
        
        renderPipelineDescriptor.depthAttachmentPixelFormat = .depth32Float_stencil8
        renderPipelineDescriptor.stencilAttachmentPixelFormat = .depth32Float_stencil8
        
        // vertex setup
        let texAttribute = VertexAttributes(buffer: nil, bufferIndex: 1, index: 0, format: .float2, offset: 0, size: 8, normalize: true)
        let posAttribute = VertexAttributes(buffer: nil, bufferIndex: 2, index: 1, format: .float4, offset: 0, size: 16, normalize: true)
        let vertexDescriptor = VertexDescriptor(attributes: [texAttribute, posAttribute])
        renderPipelineDescriptor.vertexDescriptor = vertexDescriptor.metalDescriptor
        do {
            renderPipeline = try device.makeRenderPipelineState(descriptor: renderPipelineDescriptor)
        } catch {
            assertionFailure("failed to create Sun render pipeline")
        }
        
        let indicesShort: [UInt16] = [0, 1, 2, 0, 2, 3]
        indexBuffer = Buffer(
            device: device,
            array: indicesShort,
            componentDatatype: ComponentDatatype.unsignedShort,
            sizeInBytes: indicesShort.sizeInBytes,
            label: "SunIndexBuffer"
        )
        let vertecies: [Float32] = [
            0.0, 0.0,
            1.0, 0.0,
            1.0, 1.0,
            0.0, 1.0
        ]
        vertexBuffer = Buffer(device: device, array: vertecies, componentDatatype: .float32, sizeInBytes: vertecies.sizeInBytes, label: "SunVertexBuffer")
        vertexBuffer.metalBuffer.setPurgeableState(.nonVolatile)
        
        blurCompute = MPSImageGaussianBlur(device: device, sigma: 2.0)
        blurCompute?.label = "sun blur"
        blurCompute?.options = .allowReducedPrecision
        /*
        addMPSCompute = MPSImageAdd(device: device)
        addMPSCompute?.label = "sun add"
        
        inPlaceCopy = {(kernel: MPSKernel, commandBuffer: MTLCommandBuffer, sourceTexture: MTLTexture) -> MTLTexture in
            let descriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: sourceTexture.pixelFormat, width: sourceTexture.width, height: sourceTexture.height, mipmapped: false)
            descriptor.storageMode = sourceTexture.storageMode
            descriptor.usage = sourceTexture.usage
            return device.makeTexture(descriptor: descriptor)!
        }
        */
    }
    
    func createTexture(device: MTLDevice) {
        bloomTexturePool.removeAll()
        let bloomDesc = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .bgra8Unorm, width: Int(_drawingBufferWidth!), height: Int(_drawingBufferHeight!), mipmapped: false)
        bloomDesc.usage = [.shaderRead, .shaderWrite, .renderTarget]
        bloomDesc.storageMode = .private
        for _ in 0..<3 {
            bloomTexturePool.append(device.makeTexture(descriptor: bloomDesc)!)
        }
    }
    
    func makeRenderPass(context: Context, passState: PassState) {
        let target = passState.framebuffer?.colorTextures![0] ?? context.defaultFramebuffer.colorTextures![0]
        renderPassDescriptor.colorAttachments[0].texture = target.metalTexture
        renderPassDescriptor.colorAttachments[0].loadAction = .load
        renderPassDescriptor.colorAttachments[0].storeAction = .store
        
        /*
        renderPassDescriptor.colorAttachments[1].texture = bloomTexturePool[context.bufferSyncState.rawValue]
        renderPassDescriptor.colorAttachments[1].loadAction = .clear
        renderPassDescriptor.colorAttachments[1].storeAction = .store
        renderPassDescriptor.colorAttachments[1].clearColor = MTLClearColor(red: 0, green: 0, blue: 0, alpha: 0)
        */
        
        renderPassDescriptor.depthAttachment.texture = passState.framebuffer.depthTexture?.metalTexture
        renderPassDescriptor.stencilAttachment.texture = passState.framebuffer.stencilTexture?.metalTexture
    }
    
    func renderAndBloom(context: Context, passState: PassState, offscreen: Bool = true) {
        let target = passState.framebuffer?.colorTextures![0] ?? context.defaultFramebuffer.colorTextures![0]
        let commandBuffer = offscreen ? context.offscreenCommandBuffer : context.currentCommandBuffer()
        guard let blurCompute = blurCompute, let blendComputePipeline = blendComputePipeline, let brightCompute = brightComputePipeline else { return }
        var bloomTexture = bloomTexturePool[context.bufferSyncState.rawValue]
        if bloomTexture.width != Int(passState.viewport?.width ?? 0) || bloomTexture.height != Int(passState.viewport?.height ?? 0) {
            return
        }
        
        commandBuffer.pushDebugGroup("sun bloom")
        // execute render write to colorAttachments[0] = normal, colorAttatchments[1] = bright
        makeRenderPass(context: context, passState: passState)
        let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
        renderEncoder.label = "sun"
        execute(encoder: renderEncoder, context: context, passState: passState)
        renderEncoder.endEncoding()
        
        let threadgroupSize = MTLSizeMake(16, 16, 1)
        var threadgroupCount = MTLSize()
        threadgroupCount.width = (Int(_drawingBufferWidth!)  + threadgroupSize.width -  1) / threadgroupSize.width
        threadgroupCount.height = (Int(_drawingBufferHeight!) + threadgroupSize.height - 1) / threadgroupSize.height
        threadgroupCount.depth = 1
        
        let brightEncoder = commandBuffer.makeComputeCommandEncoder()!
        brightEncoder.label = "sun bright"
        brightEncoder.setComputePipelineState(brightCompute)
        brightEncoder.setTexture(target.metalTexture, index: 0)
        brightEncoder.setTexture(bloomTexture, index: 1)
        brightEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadgroupSize)
        brightEncoder.endEncoding()

        blurCompute.encode(commandBuffer: commandBuffer, inPlaceTexture: &bloomTexture, fallbackCopyAllocator: nil)
        
        if let addMPSCompute = addMPSCompute, let inPlaceCopy = inPlaceCopy {
            addMPSCompute.encode(commandBuffer: commandBuffer, inPlacePrimaryTexture: &target.metalTexture, secondaryTexture: bloomTexture, fallbackCopyAllocator: inPlaceCopy)
        } else {
            let computeEncoder = commandBuffer.makeComputeCommandEncoder()!
            computeEncoder.label = "sun add"
            computeEncoder.setComputePipelineState(blendComputePipeline)
            computeEncoder.setTexture(target.metalTexture, index: 0)
            computeEncoder.setTexture(bloomTexture, index: 1)
            computeEncoder.setTexture(bloomTexture, index: 2)
            computeEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadgroupSize)
            computeEncoder.endEncoding()
    
            let origin = MTLOriginMake(0, 0, 0)
            let size = MTLSizeMake(Int(_drawingBufferWidth!), Int(_drawingBufferHeight!), 1)
            let blitEncoder = commandBuffer.makeBlitCommandEncoder()!
            blitEncoder.label = "sun blit"
            blitEncoder.copy(from: bloomTexture,
                             sourceSlice: 0,
                             sourceLevel: 0,
                             sourceOrigin: origin,
                             sourceSize: size,
                             to: target.metalTexture,
                             destinationSlice: 0,
                             destinationLevel: 0,
                             destinationOrigin: origin)
            blitEncoder.endEncoding()
        }
        commandBuffer.popDebugGroup()
    }

    func update(frameState: FrameState, passState: PassState, useHDR: Bool) {
        /*
        if !show {
            return nil
        }
        if frameState.mode == .scene2D || frameState.mode == .morphing {
            return nil
        }
        if !frameState.passes.render {
            return nil
        }
        */
        guard show && (frameState.mode != .scene2D && frameState.mode != .morphing) && frameState.passes.render else {
            return
        }
        guard let drawingBufferWidth = passState.viewport?.width, let drawingBufferHeight = passState.viewport?.height else {
            return
        }
        if _texture == nil || drawingBufferWidth != _drawingBufferWidth || drawingBufferHeight != _drawingBufferHeight || _glowFactorDirty || useHDR != _useHDR {
            sunReady = false
            
            _texture = nil
            _drawingBufferWidth = drawingBufferWidth
            _drawingBufferHeight = drawingBufferHeight
            _glowFactorDirty = false
            _useHDR = useHDR
            
            createTexture(device: frameState.context.device)
            
            var size = max(drawingBufferWidth, drawingBufferHeight)
            size = pow(2.0, ceil(log(size) / log(2.0)) - 2.0)
            
            // The size computed above can be less than 1.0 if size < 4.0. This will probably
            // never happen in practice, but does in the tests. Clamp to 1.0 to prevent WebGL
            // errors in the tests.
            size = max(1.0, size)

            //var pixelDatatype = useHdr ? (context.halfFloatingPointTexture ? PixelDatatype.HALF_FLOAT : PixelDatatype.FLOAT) : PixelDatatype.UNSIGNED_BYTE;
            let texDescriptor = MTLTextureDescriptor()
            texDescriptor.width = Int(size)
            texDescriptor.height = Int(size)
            texDescriptor.usage = [.shaderRead, .shaderWrite]
            texDescriptor.storageMode = .private
            texDescriptor.pixelFormat = .rgba8Unorm
            _texture = frameState.context.device.makeTexture(descriptor: texDescriptor)

            _glowLengthTS = _glowFactor * 5.0
            _radiusTS = (1.0 / (1.0 + 2.0 * _glowLengthTS!)) * 0.5
            
            guard let commandBuffer = commandQueue.makeCommandBuffer(), let computePipeline = computePipeline else { return }
            commandBuffer.addCompletedHandler{[unowned self] _ in
                self.sunReady = true
            }
            let computeEncoder = commandBuffer.makeComputeCommandEncoder()!
            computeEncoder.label = "Sun Compute"
            computeEncoder.setComputePipelineState(computePipeline)
            computeEncoder.setTexture(_texture, index: 0)
            var radiusTS = Float(_radiusTS)
            computeEncoder.setBytes(&radiusTS, length: 4, index: 0)
            var _size = Float(size)
            computeEncoder.setBytes(&_size, length: 4, index: 1)
            let threadgroupSize = MTLSizeMake(16, 16, 1)
            var threadgroupCount = MTLSize()
            threadgroupCount.width = (Int(size)  + threadgroupSize.width -  1) / threadgroupSize.width
            threadgroupCount.height = (Int(size) + threadgroupSize.height - 1) / threadgroupSize.height
            threadgroupCount.depth = 1
            computeEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadgroupSize)
            computeEncoder.endEncoding()
            
            commandBuffer.commit()
            
            /*
            let uniformMap: SunComputeUniformMap = SunComputeUniformMap()
            uniformMap.uniformBufferProvider = UniformBufferProvider(device: frameState.context.device, bufferSize: MemoryLayout<SunComputeUniformMap>.stride, deallocationBlock: nil, label: "SunComputeUniform")
            uniformMap.radiusTS = Float(_radiusTS)
            let sunComputeCommand = ComputeCommand(
                fragmentShaderSource: ShaderSource(),
                uniformMap: uniformMap,
                outputTexture: _texture!,
                postExecute: { [unowned self] outputTexture in
                    self._commands.computeCommand = nil
                },
                persists: false,
                owner: self
            )
            _commands.computeCommand = sunComputeCommand
            */
        }

        let sunPosition = frameState.context.uniformState.sunPositionWC
        let sunPositionCV = frameState.context.uniformState.sunPositionColumbusView

        var boundingVolume = _boundingVolume
        var boundingVolume2D = _boundingVolume2D

        boundingVolume.center = sunPosition
        boundingVolume2D.center.x = sunPositionCV.z
        boundingVolume2D.center.y = sunPositionCV.x
        boundingVolume2D.center.z = sunPositionCV.y

        boundingVolume.radius = SOLAR_RADIUS + SOLAR_RADIUS * _glowLengthTS
        boundingVolume2D.radius = boundingVolume.radius

        if frameState.mode == .scene3D {
            drawCommand.boundingVolume = boundingVolume
        } else if frameState.mode == .columbusView {
            drawCommand.boundingVolume = boundingVolume2D
        }

        guard let position = SceneTransforms.computeActualWgs84Position(frameState, position: sunPosition), let camera = frameState.camera, let viewport = passState.viewport else { return }

        let dist = position.subtract(camera.position).magnitude
        let projMatrix = frameState.context.uniformState.projection

        var positionEC = Cartesian4(x: 0, y: 0, z: -dist, w: 1)

        let positionCC = projMatrix.multiplyByVector(positionEC)
        let positionWC = SceneTransforms.clipToGLWindowCoordinates(viewport, position: positionCC)

        positionEC.x = SOLAR_RADIUS
        let limbCC = projMatrix.multiplyByVector(positionEC)
        let limbWC = SceneTransforms.clipToGLWindowCoordinates(viewport, position: limbCC)

        _size = ceil(limbWC.subtract(positionWC).magnitude())
        let _glTS = 1.0 + 2.0 * _glowLengthTS
        _size = 2.0 * _size! * _glTS
    }
    
    func execute(encoder: MTLRenderCommandEncoder, context: Context, passState: PassState) {
        guard let renderPipeline = renderPipeline, let size = _size else { return }
        // set render state same across all tile (in other words, can not set with ICB)
        let renderState = context.defaultRenderState()
        //context.applyRenderState(renderPass, renderState: command.renderState ?? context.defaultRenderState(), passState: renderPass.passState)
        encoder.setRenderPipelineState(renderPipeline)
        encoder.setFrontFacing(renderState.windingOrder.toMetal())
        encoder.setCullMode(renderState.cullFace.toMetal())
        if renderState.depthTest.enabled {
            encoder.setDepthStencilState(renderState.depthStencilState())
        }
        // TODO: get rid of renderState
        renderState.applyViewport(encoder, passState: passState)
        if renderState.wireFrame {
            encoder.setTriangleFillMode(.lines)
        } else {
            encoder.setTriangleFillMode(.fill)
        }
        //print("draw sun")

        var u_size = Float(size)
        encoder.setVertexBytes(&u_size, length: 4, index: 0)
        encoder.setVertexBuffer(vertexBuffer.metalBuffer, offset: 0, index: 1)
        let posBuffer = calculateSunVertexPosition(uniformState: context.uniformState)
        encoder.setVertexBytes(posBuffer, length: 16 * 4, index: 2)

        encoder.setFragmentTexture(_texture, index: 0)

        encoder.drawIndexedPrimitives(type: .triangle, indexCount: 6, indexType: .uint16, indexBuffer: indexBuffer.metalBuffer, indexBufferOffset: 0)
    }
}

extension Sun {
    func calculateSunVertexPosition(uniformState: UniformState) -> [SIMD4<Float>] {
        guard let size = _size else { return [SIMD4<Float>(0, 0, 0, 0)]}
        let direction: [Cartesian2] = [Cartesian2(x: 0, y: 0), Cartesian2(x: 1, y: 0), Cartesian2(x: 1, y: 1), Cartesian2(x: 0, y: 1)]
        func eyeToWindowCoordinates(pEC: Cartesian4) -> Cartesian4 {
            let q = uniformState.projection.multiplyByVector(pEC)
            let p3 = Cartesian3(x: q.x, y: q.y, z: q.z).divideBy(scalar: q.w)
            let p4 = Cartesian4(x: p3.x, y: p3.y, z: p3.z, w: 1.0)
            let trans = uniformState.viewportTransformation.multiplyByVector(p4)
            return Cartesian4(x: trans.x, y: trans.y, z: trans.z, w: q.w)
        }
        let position = uniformState.sunPositionWC
        let positionEC = uniformState.view.multiplyByVector(Cartesian4(x: position.x, y: position.y, z: position.z, w: 1.0))
        let positionWC = eyeToWindowCoordinates(pEC: positionEC)
        
        var vertecies: [SIMD4<Float>] = []
        for d in direction {
            let _d = d.multiplyBy(scalar: 2.0).subtract(Cartesian2(x: 1.0, y: 1.0))
            let halfSize = Cartesian2(x: size * 0.5, y: size * 0.5).multiplyComponents(_d).add(Cartesian2(x: positionWC.x, y: positionWC.y))
    
            let p = uniformState.viewportOrthographic.multiplyByVector(Cartesian4(x: halfSize.x, y: halfSize.y, z: -positionWC.z, w: 1.0))
            vertecies.append(p.floatRepresentation)
        }
        return vertecies
    }
}

struct SunComputeUniformStruct: UniformStruct {
    var radiusTS = Float(0.0)
}
class SunComputeUniformMap: NativeUniformMap {
    private var _uniformStruct = SunComputeUniformStruct()
    
    let uniformDescriptors: [UniformDescriptor] = [
        UniformDescriptor(name:  "u_radiusTS", type: .floatVec1, count: 1)
    ]
    var uniformBufferProvider: UniformBufferProvider! = nil
    lazy var uniformUpdateBlock: UniformUpdateBlock = { [unowned self] buffer in
        buffer.write(from: &self._uniformStruct, length: MemoryLayout<SunComputeUniformStruct>.size)
        return []
    }
    var radiusTS: Float {
        get {
            _uniformStruct.radiusTS
        }
        set {
            _uniformStruct.radiusTS = newValue
        }
    }
}
