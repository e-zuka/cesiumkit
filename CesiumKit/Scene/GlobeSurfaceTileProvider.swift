//
//  GlobeSurfaceTileProvider.swift
//  CesiumKit
//
//  Created by Ryan Walklin on 16/08/14.
//  Copyright (c) 2014 Test Toast. All rights reserved.
//

import Foundation
import simd
import os.signpost

/**
* Provides quadtree tiles representing the surface of the globe.  This type is intended to be used
* with {@link QuadtreePrimitive}.
*
* @alias GlobeSurfaceTileProvider
* @constructor
*
* @param {TerrainProvider} options.terrainProvider The terrain provider that describes the surface geometry.
* @param {ImageryLayerCollection} option.imageryLayers The collection of imagery layers describing the shading of the surface.
* @param {GlobeSurfaceShaderSet} options.surfaceShaderSet The set of shaders used to render the surface.
*
* @private
*/
class GlobeSurfaceTileProvider/*: QuadtreeTileProvider*/ {
    
    /**
    * Gets or sets the {@link QuadtreePrimitive} for which this provider is
    * providing tiles.
    * @memberof QuadtreeTileProvider.prototype
    * @type {QuadtreePrimitive}
    */
    weak var quadtree: QuadtreePrimitive? = nil
    
    /**
    * Gets a value indicating whether or not the provider is ready for use.
    * @memberof QuadtreeTileProvider.prototype
    * @type {Boolean}
    */
    
    var ready: Bool {
        get {
            return terrainProvider.ready && (imageryLayers.count == 0 || imageryLayers[0]!.imageryProvider.ready)
        }
    }
    
    let imageryLayers: ImageryLayerCollection
    
    /**
    * Gets the tiling scheme used by the provider.  This property should
    * not be accessed before {@link QuadtreeTileProvider#ready} returns true.
    * @memberof QuadtreeTileProvider.prototype
    * @type {TilingScheme}
    */
    var tilingScheme: TilingScheme {
        get {
            return terrainProvider.tilingScheme
        }
    }
    
    /**
    * Gets or sets the terrain provider that describes the surface geometry.
    * @memberof GlobeSurfaceTileProvider.prototype
    * @type {TerrainProvider}
    */
    var terrainProvider: TerrainProvider
    
    var surfaceShaderSet: GlobeSurfaceShaderSet
    
    /**
    * Stores Metal renderer pipeline. Updated if/when shaders changed (add/remove tile provider etc)
    */
    fileprivate var _pipeline: RenderPipeline!
    
    /**
    * Gets an event that is raised when the geometry provider encounters an asynchronous error.  By subscribing
    * to the event, you will be notified of the error and can potentially recover from it.  Event listeners
    * are passed an instance of {@link TileProviderError}.
    * @memberof QuadtreeTileProvider.prototype
    * @type {Event}
    */
    let errorEvent = Event()
    
    /**
    * The distance where everything becomes lit. This only takes effect
    * when <code>enableLighting</code> is <code>true</code>.
    *
    * @type {Number}
    * @default 6500000.0
    */
    //var lightingFadeOutDistance: Float = 6500000 // GlobeSurfaceTileNG
    
    /**
    * The distance where lighting resumes. This only takes effect
    * when <code>enableLighting</code> is <code>true</code>.
    *
    * @type {Number}
    * @default 9000000.0
    */
    //var lightingFadeInDistance: Float = 9000000 // GlobeSurfaceTileNG
    
    var hasWaterMask = false
     
    var oceanNormalMap: Texture? = nil
    
    var zoomedOutOceanSpecularIntensity: Float = 0.5
    
    var enableLighting = false
    
    fileprivate var _renderState: RenderState? = nil
    
    fileprivate var _blendRenderState: RenderState? = nil
    
    fileprivate var _pickRenderState: RenderState? = nil
    
    fileprivate var _layerOrderChanged = false
    
    fileprivate var _tilesToRenderByTextureCount = [Int: Array<QuadtreeTile>]() // Dictionary of arrays of QuadtreeTiles

    fileprivate var _drawCommands = [DrawCommand]()
    
    fileprivate var _manualUniformBufferProviderPool = [UniformBufferProvider]()
    
    fileprivate var _pickCommands = [DrawCommand]()
    
    fileprivate var _usedDrawCommands = 0
    
    fileprivate var _usedPickCommands = 0
    
    fileprivate var _debug: (wireframe: Bool, boundingSphereTile: QuadtreeTile?, tilesRendered : Int, texturesRendered: Int) = (false, nil, 0, 0)
    
    var baseColor: Cartesian4 {
        get {
            return _baseColor
        }
        set (value) {
            _baseColor = value
            _firstPassInitialColor = value
        }
    }
    
    fileprivate var _baseColor: Cartesian4
    
    fileprivate var _firstPassInitialColor: Cartesian4
    
    var _vertexArraysToDestroy: [VertexArray] = []
    
    var hasLoadedTilesThisFrame: Bool = false
    var hasFillTilesThisFrame: Bool = false
    var cartographicLimitRectangle = Rectangle.maxValue()
    var clippingPlanes: ClippingPlaneCollection? = nil
    
    var showGroundAtmosphere: Bool = false
    var lightingFadeOutDistance: Double = 6500000
    var lightingFadeInDistance: Double = 9000000
    var nightFadeOutDistance: Double? = nil
    var nightFadeInDistance: Double? = nil

    required init (terrainProvider: TerrainProvider, imageryLayers: ImageryLayerCollection, surfaceShaderSet: GlobeSurfaceShaderSet) {
        
        self.terrainProvider = terrainProvider
        self.imageryLayers = imageryLayers
        self.surfaceShaderSet = surfaceShaderSet
        
        // FIXME: events
        /*
        this._imageryLayers.layerAdded.addEventListener(GlobeSurfaceTileProvider.prototype._onLayerAdded, this);
        this._imageryLayers.layerRemoved.addEventListener(GlobeSurfaceTileProvider.prototype._onLayerRemoved, this);
        this._imageryLayers.layerMoved.addEventListener(GlobeSurfaceTileProvider.prototype._onLayerMoved, this);
        this._imageryLayers.layerShownOrHidden.addEventListener(GlobeSurfaceTileProvider.prototype._onLayerShownOrHidden, this);
        */
        if quadtree != nil {
            quadtree!.invalidateAllTiles()
        }
        _baseColor = Cartesian4()
        _firstPassInitialColor = Cartesian4()
        baseColor = Cartesian4(red: 0.0, green: 0.0, blue: 0.5, alpha: 1.0)
        //baseColor = Cartesian4(fromRed: 0.0, green: 0.8434, blue: 0.2665, alpha: 1.0)
    }
    
    func computeDefaultLevelZeroMaximumGeometricError() -> Double {
        return tilingScheme.ellipsoid.maximumRadius * Math.TwoPi * 0.25 / (65.0 * Double(tilingScheme.numberOfXTilesAt(level: 0)))
    }
    
    fileprivate func sortTileImageryByLayerIndex (_ a: TileImagery, b: TileImagery) -> Bool {
        let aImagery = a.loadingImagery ?? a.readyImagery!
        let bImagery = b.loadingImagery ?? b.readyImagery!
        
        return aImagery.imageryLayer.layerIndex < bImagery.imageryLayer.layerIndex
    }
    
    func update() {
        imageryLayers.update()
    }
    /**
    * Called at the beginning of each render frame, before {@link QuadtreeTileProvider#showTileThisFrame}
    * or any other functions.
    *
    * @param {FrameState} frameState The frame state.
    */
    func initialize (_ frameState: inout FrameState) {
        
        //imageryLayers.update() // move to update with GlobeSurfaceTileNG
        // update each layer for texture reprojection.
        imageryLayers.queueReprojectionCommands(&frameState)
        
        if _layerOrderChanged {
            _layerOrderChanged = false
            quadtree?.forEachLoadedTile({ (tile) -> () in
                tile.data?.imagery.sort(by: self.sortTileImageryByLayerIndex)
            })
        }
        
        // Add credits for terrain and imagery providers.
        // FIXME: Credits
        let creditDisplay = frameState.creditDisplay
        
        if let credit = terrainProvider.credit , terrainProvider.ready {
            creditDisplay.addCredit(credit)
        }
        for i in 0..<imageryLayers.count {
            if let imageryProvider = imageryLayers[i]?.imageryProvider {
                if let credit = imageryProvider.credit , imageryProvider.ready {
                    creditDisplay.addCredit(credit)
                }
            }
        }
        _vertexArraysToDestroy.removeAll()
        /*
        var vertexArraysToDestroy = this._vertexArraysToDestroy;
        var length = vertexArraysToDestroy.length;
        for (var j = 0; j < length; ++j) {
            freeVertexArray(vertexArraysToDestroy[j]);
        }
        vertexArraysToDestroy.length = 0;
        */
    }
    
    /**
     * Called at the beginning of the update cycle for each render frame, before {@link QuadtreeTileProvider#showTileThisFrame}
     * or any other functions.
     *
     * @param {FrameState} frameState The frame state.
     */
    func beginUpdate (_ frameState: FrameState) {
        _tilesToRenderByTextureCount.removeAll()
        // update clipping planes
        if let clippingPlanes = clippingPlanes, clippingPlanes.enabled {
            // TODO: impl clippingPlanes
            //clippingPlanes.update(frameState)
        }
        _usedDrawCommands = 0
        
        hasFillTilesThisFrame = false
        hasLoadedTilesThisFrame = false
    }
    
    /**
    * Called at the end of the update cycle for each render frame, after {@link QuadtreeTileProvider#showTileThisFrame}
    * and any other functions.
    *
    * @param {FrameState} frameState The frame state.
    */
    func endUpdate (_ frameState: inout FrameState) {

        let context = frameState.context
        
        if _renderState == nil {
            _renderState = RenderState(
                device: context!.device,
                cullFace: .back,
                depthTest: RenderState.DepthTest(enabled: true, function: .less)
            )
        }
        
        if _blendRenderState == nil {
            
            _blendRenderState = RenderState(
                device: context!.device,
                cullFace: .back,
                depthTest: RenderState.DepthTest(enabled: true, function: .lessOrEqual),
                blending: BlendingState.AlphaBlend(Cartesian4())
            )
        }
        
        // If this frame has a mix of loaded and fill tiles, we need to propagate
        // loaded heights to the fill tiles.
        if hasFillTilesThisFrame && hasLoadedTilesThisFrame {
            TerrainFillMesh.updateFillTiles(tileProvider: self, renderedTiles: quadtree?.tilesToRender ?? [], frameState: &frameState, vertexArraysToDestroy: _vertexArraysToDestroy)
        }
        
        // Add the tile render commands to the command list, sorted by texture count.
        let textureCountList = _tilesToRenderByTextureCount.keys
        for textureCount in textureCountList {
            guard textureCount != 0, let tilesToRender = _tilesToRenderByTextureCount[textureCount] else { continue }
            for tile in tilesToRender {
                addTileModelToRender(tile, frameState: &frameState)
            }
        }
        /* GlobeSurfaceTileNG
        // And the tile render commands to the command list, sorted by texture count.
        for tilesToRender in _tilesToRenderByTextureCount.values {
            for tile in tilesToRender {
                //addDrawCommandsForTile(tile, frameState: &frameState)
                addTileModelToRender(tile, frameState: &frameState)
            }
        }
        */
    }
    
    /**
         * Adds draw commands for tiles rendered in the previous frame for a pick pass.
         *
         * @param {Context} context The rendering context.
         * @param {FrameState} frameState The frame state.
         * @param {DrawCommand[]} commandList An array of rendering commands.  This method may push
         *        commands into this array.
         */
    func updateForPick (_ context: Context, frameState: FrameState, commandList: inout [Command]) {
        if _pickRenderState == nil {
            _pickRenderState = RenderState(
                device: context.device,
                depthTest: RenderState.DepthTest(
                    enabled : true,
                    function: .less
                )
            )
        }
        _usedPickCommands = 0
        
        // Add the tile pick commands from the tiles drawn last frame.
        for i in 0..<_usedDrawCommands {
            addPickCommandsForTile(_drawCommands[i], context: context, frameState: frameState, commandList: &commandList)
        }
    }
    
    /**
     * Cancels any imagery re-projections in the queue.
     */
    func cancelReprojections () {
        imageryLayers.cancelReprojections()
    }
 
    /**
    * Gets the maximum geometric error allowed in a tile at a given level, in meters.  This function should not be
    * called before {@link GlobeSurfaceTileProvider#ready} returns true.
    *
    * @param {Number} level The tile level for which to get the maximum geometric error.
    * @returns {Number} The maximum geometric error in meters.
    */
    func levelMaximumGeometricError(_ level: Int) -> Double {
        return terrainProvider.levelMaximumGeometricError(level)
    }
    
    /**
    * Loads, or continues loading, a given tile.  This function will continue to be called
    * until {@link QuadtreeTile#state} is no longer {@link QuadtreeTileLoadState#LOADING}.  This function should
    * not be called before {@link GlobeSurfaceTileProvider#ready} returns true.
    *
    * @param {Context} context The rendering context.
    * @param {FrameState} frameState The frame state.
    * @param {QuadtreeTile} tile The tile to load.
    *
    * @exception {DeveloperError} <code>loadTile</code> must not be called before the tile provider is ready.
    */
    func loadTile (_ tile: QuadtreeTile, frameState: inout FrameState) {
        // We don't want to load imagery until we're certain that the terrain tiles are actually visible.
        // So if our bounding volume isn't accurate because it came from another tile, load terrain only
        // initially. If we load some terrain and suddenly have a more accurate bounding volume and the
        // tile is _still_ visible, give the tile a chance to load imagery immediately rather than
        // waiting for next frame.
        var terrainOnly = true
        var terrainStateBefore: TerrainState? = nil
        if let surfaceTile = tile.data {
            terrainOnly = (surfaceTile.boundingVolumeSourceTile != nil && surfaceTile.boundingVolumeSourceTile! != tile) || tile.lastSelectionResult == .culled_but_needed
            terrainStateBefore = surfaceTile.terrainState
        }
        
        GlobeSurfaceTile.processStateMachine(tile, frameState: &frameState, terrainProvider: terrainProvider, imageryLayerCollection: imageryLayers, vertexArraysToDestroy: _vertexArraysToDestroy, terrainOnly: terrainOnly)
        
        guard let surfaceTile = tile.data else { return }
        if terrainOnly && terrainStateBefore != nil  && terrainStateBefore! != surfaceTile.terrainState {
            // Terrain state changed. If:
            // a) The tile is visible, and
            // b) The bounding volume is accurate (updated as a side effect of computing visibility)
            // Then we'll load imagery, too.
            if computeTileVisibility(tile, frameState: &frameState, occluders: quadtree?.occluders) != .none && surfaceTile.boundingVolumeSourceTile != nil && surfaceTile.boundingVolumeSourceTile! == tile {
                terrainOnly = false
                GlobeSurfaceTile.processStateMachine(tile, frameState: &frameState, terrainProvider: terrainProvider, imageryLayerCollection: imageryLayers, vertexArraysToDestroy: _vertexArraysToDestroy, terrainOnly: terrainOnly)
            }
        }
    }

    /**
    * Determines the priority for loading this tile. Lower priority values load sooner.
    * @param {QuadtreeTile} tile The tile.
    * @param {FrameState} frameState The frame state.
    * @returns {Number} The load priority value.
    */
    func computeTileLoadPriority(_ tile: QuadtreeTile, frameState: FrameState) -> Double {
        guard let surfaceTile = tile.data, let obb = surfaceTile.orientedBoundingBox else { return 0.0 }
        let cameraPosition = frameState.camera!.positionWC
        let cameraDirection = frameState.camera!.directionWC
        var tileDirection = obb.center.subtract(cameraPosition)
        let magnitude = tileDirection.magnitude
        if magnitude < Math.Epsilon5 {
            return 0.0
        }
        tileDirection = tileDirection.divideBy(scalar: magnitude)
        return (1.0 - tileDirection.dot(cameraDirection)) * tile.distance
    }
    
    /**
    * Determines the visibility of a given tile.  The tile may be fully visible, partially visible, or not
    * visible at all.  Tiles that are renderable and are at least partially visible will be shown by a call
    * to {@link GlobeSurfaceTileProvider#showTileThisFrame}.
    *
    * @param {QuadtreeTile} tile The tile instance.
    * @param {FrameState} frameState The state information about the current frame.
    * @param {QuadtreeOccluders} occluders The objects that may occlude this tile.
    *
    * @returns {Visibility} The visibility of the tile.
    */
    func computeTileVisibility (_ tile: QuadtreeTile, frameState: inout FrameState, occluders: QuadtreeOccluders?) -> Visibility {
        //let distance = computeDistanceToTile(tile, frameState: &frameState)
        let distance = computeDistanceToTile(tile, frameState: frameState) // GlobeSurfaceTileNG
        tile.distance = distance
        
        if frameState.fog.enabled {
            if Math.fog(distance, density: frameState.fog.density) >= 1.0 {
                // Tile is completely in fog so return that it is not visible.
                return .none
            }
        }
        
        let surfaceTile = tile.data!
        let tileBoundingRegion = surfaceTile.tileBoundingRegion

        if surfaceTile.boundingVolumeSourceTile == nil {
            // We have no idea where this tile is, so let's just call it partially visible.
            return .partial
        }
        
        let cullingVolume = frameState.cullingVolume!
        var boundingVolume: BoundingVolume? = surfaceTile.orientedBoundingBox// ?? surfaceTile.boundingSphere3D
        if boundingVolume == nil && surfaceTile.renderedMesh != nil {
            boundingVolume = surfaceTile.renderedMesh!.boundingSphere3D
        }
        
        // Check if the tile is outside the limit area in cartographic space
        surfaceTile.clippedByBoundaries = false
        let clippedCartographicLimitRectangle = clipRectangleAntimeridian(tileRectangle: tile.rectangle, cartographicLimitRectangle: cartographicLimitRectangle)
        let areaLimitIntersection = clippedCartographicLimitRectangle.simpleIntersection(tile.rectangle)
        //let areaLimitIntersection = Rectangle.simpleIntersection(clippedCartographicLimitRectangle, tile.rectangle, rectangleIntersectionScratch);
        if areaLimitIntersection == nil {
            return .none
        }
        //if (!Rectangle.equals(areaLimitIntersection, tile.rectangle)) {
        if areaLimitIntersection != tile.rectangle {
            surfaceTile.clippedByBoundaries = true
        }
        
        /*
        if frameState.mode != .scene3D {
            var boundingSphere = BoundingSphere(
                fromRectangleWithHeights2D: tile.rectangle,
                projection: frameState.mapProjection,
                minimumHeight: surfaceTile.minimumHeight,
                maximumHeight: surfaceTile.maximumHeight)
            boundingSphere.center = Cartesian3(
                x: boundingVolume.center.z,
                y: boundingVolume.center.x,
                z: boundingVolume.center.y)
            boundingVolume = boundingSphere
            
            if frameState.mode == .morphing {
                boundingVolume = surfaceTile.boundingSphere3D.union(boundingVolume as! BoundingSphere)
            }
        }
        */
        guard let bv = boundingVolume else {
            return Visibility(rawValue: Intersect.intersecting.rawValue)!
        }
        
        if let clippingPlanes = clippingPlanes, clippingPlanes.enabled {
            let planeIntersection = clippingPlanes.computeIntersectionWithBoundingVolume(tileBoundingVolume: bv)
            //tile.isClipped = planeIntersection != .inside
            if planeIntersection == .outside {
                return .none
            }
        }

        let intersection = cullingVolume.visibility(bv)
        if intersection == .outside {
            return .none
        }
        
        if frameState.mode == .scene3D, let occluders = occluders {
            let occludeePointInScaledSpace = surfaceTile.occludeePointInScaledSpace
            if occludeePointInScaledSpace == nil {
                return Visibility(rawValue: intersection.rawValue)!
            }
            
            if occluders.ellipsoid.isScaledSpacePointVisible(occludeeScaledSpacePosition: occludeePointInScaledSpace!) {
                return Visibility(rawValue: intersection.rawValue)!
            }
            return Visibility.none
        }
        return Visibility(rawValue: intersection.rawValue)!
    }
    
    /**
    * Shows a specified tile in this frame.  The provider can cause the tile to be shown by adding
    * render commands to the commandList, or use any other method as appropriate.  The tile is not
    * expected to be visible next frame as well, unless this method is called next frame, too.
    *
    * @param {Object} tile The tile instance.
    * @param {Context} context The rendering context.
    * @param {FrameState} frameState The state information of the current rendering frame.
    * @param {DrawCommand[]} commandList The list of rendering commands.  This method may add additional commands to this list.
    */
    func showTileThisFrame (_ tile: QuadtreeTile, frameState: inout FrameState) {
        
        var readyTextureCount = 0
        
        for tileImagery in tile.data!.imagery {
            if let readyImagery = tileImagery.readyImagery, readyImagery.imageryLayer.alpha() != 0.0 {
                readyTextureCount += 1
            }
        }
        
        if _tilesToRenderByTextureCount[readyTextureCount] == nil {
            _tilesToRenderByTextureCount[readyTextureCount] = [tile]
        } else {
            _tilesToRenderByTextureCount[readyTextureCount]!.append(tile)
        }
        /*
        var tileSet = _tilesToRenderByTextureCount[readyTextureCount]
        if tileSet == nil {
            tileSet = [QuadtreeTile]()
        }
        tileSet!.append(tile)
        _tilesToRenderByTextureCount[readyTextureCount] = tileSet
        */
        
        let surfaceTile = tile.data!
        if surfaceTile.vertexArray == nil {
            hasFillTilesThisFrame = true
        } else {
            hasLoadedTilesThisFrame = true
        }
        
        _debug.tilesRendered += 1
        _debug.texturesRendered += readyTextureCount
    }
    
    /**
    * Gets the distance from the camera to the closest point on the tile.  This is used for level-of-detail selection.
    *
    * @param {QuadtreeTile} tile The tile instance.
    * @param {FrameState} frameState The state information of the current rendering frame.
    * @param {Cartesian3} cameraCartesianPosition The position of the camera in world coordinates.
    * @param {Cartographic} cameraCartographicPosition The position of the camera in cartographic / geodetic coordinates.
    *
    * @returns {Number} The distance from the camera to the closest point on the tile, in meters.
    */
    /*
    func computeDistanceToTile (_ tile: QuadtreeTile, frameState: inout FrameState) -> Double {
        let surfaceTile = tile.data!
        return surfaceTile.tileBoundingRegion!.distanceToCamera(&frameState)
    }
    */
    func computeDistanceToTile(_ tile: QuadtreeTile, frameState: FrameState) -> Double {
        // The distance should be:
        // 1. the actual distance to the tight-fitting bounding volume, or
        // 2. a distance that is equal to or greater than the actual distance to the tight-fitting bounding volume.
        //
        // When we don't know the min/max heights for a tile, but we do know the min/max of an ancestor tile, we can
        // build a tight-fitting bounding volume horizontally, but not vertically. The min/max heights from the
        // ancestor will likely form a volume that is much bigger than it needs to be. This means that the volume may
        // be deemed to be much closer to the camera than it really is, causing us to select tiles that are too detailed.
        // Loading too-detailed tiles is super expensive, so we don't want to do that. We don't know where the child
        // tile really lies within the parent range of heights, but we _do_ know the child tile can't be any closer than
        // the ancestor height surface (min or max) that is _farthest away_ from the camera. So if we compute distance
        // based that conservative metric, we may end up loading tiles that are not detailed enough, but that's much
        // better (faster) than loading tiles that are too detailed.
        
        let heightSource = updateTileBoundingRegion(tile: tile, terrainProvider: terrainProvider, frameState: frameState)
        let surfaceTile = tile.data!
        let tileBoundingRegion = surfaceTile.tileBoundingRegion!

        if heightSource == nil {
            // Can't find any min/max heights anywhere? Ok, let's just say the
            // tile is really far away so we'll load and render it rather than refining.
            return 9999999999.0
        } else if surfaceTile.boundingVolumeSourceTile !== heightSource {
            // Heights are from a new source tile, so update the bounding volume.
            surfaceTile.boundingVolumeSourceTile = heightSource;

            let rectangle = tile.rectangle
            if rectangle.width < .pi / 2 + Math.Epsilon5 {
            //if (defined(rectangle) && rectangle.width < CesiumMath.PI_OVER_TWO + CesiumMath.EPSILON5)
                surfaceTile.orientedBoundingBox = OrientedBoundingBox.init(fromRectangle: tile.rectangle, minimumHeight: tileBoundingRegion.minimumHeight, maximumHeight: tileBoundingRegion.maximumHeight, ellipsoid: tile.tilingScheme.ellipsoid)

                surfaceTile.occludeePointInScaledSpace = computeOccludeePoint(tileProvider: self, center: surfaceTile.orientedBoundingBox!.center, rectangle: tile.rectangle, height: tileBoundingRegion.maximumHeight)
            }
        }

        let min = tileBoundingRegion.minimumHeight
        let max = tileBoundingRegion.maximumHeight

        if surfaceTile.boundingVolumeSourceTile != tile {
            let cameraHeight = frameState.camera!.positionCartographic.height
            let distanceToMin = abs(cameraHeight - min)
            let distanceToMax = abs(cameraHeight - max)
            if distanceToMin > distanceToMax {
                surfaceTile.tileBoundingRegion!.minimumHeight = min
                surfaceTile.tileBoundingRegion!.maximumHeight = min
            } else {
                surfaceTile.tileBoundingRegion!.minimumHeight = max
                surfaceTile.tileBoundingRegion!.maximumHeight = max
            }
        }

        let result = tileBoundingRegion.distanceToCamera(frameState)

        surfaceTile.tileBoundingRegion!.minimumHeight = min
        surfaceTile.tileBoundingRegion!.maximumHeight = max

        return result
    }
    
    private func updateTileBoundingRegion(tile: QuadtreeTile, terrainProvider: TerrainProvider, frameState: FrameState) -> QuadtreeTile? {
        let surfaceTile: GlobeSurfaceTile
        if tile.data == nil {
            tile.data = GlobeSurfaceTile()
            surfaceTile = tile.data!
        } else {
            surfaceTile = tile.data!
        }

        if surfaceTile.tileBoundingRegion == nil {
            surfaceTile.tileBoundingRegion = TileBoundingRegion(
                rectangle: tile.rectangle,
                ellipsoid: tile.tilingScheme.ellipsoid,
                minimumHeight: 0.0,
                maximumHeight: 0.0,
                computeBoundingVolumes: false
            )
        }

        //let mesh = surfaceTile.mesh
        //let tileBoundingRegion = surfaceTile.tileBoundingRegion!

        if let mesh = surfaceTile.mesh {
        //if mesh != nil && mesh.minimumHeight != nil && mesh.maximumHeight != nil
            // We have tight-fitting min/max heights from the mesh.
            surfaceTile.tileBoundingRegion!.minimumHeight = mesh.minimumHeight
            surfaceTile.tileBoundingRegion!.maximumHeight = mesh.maximumHeight
            return tile
        }

        //let terrainData = surfaceTile.terrainData
        if let terrainData = surfaceTile.terrainData as? QuantizedMeshTerrainData {
        //if (terrainData !== undefined && terrainData._minimumHeight !== undefined && terrainData._maximumHeight !== undefined)
            // We have tight-fitting min/max heights from the terrain data.
            surfaceTile.tileBoundingRegion!.minimumHeight = terrainData._minimumHeight * frameState.terrainExaggeration
            surfaceTile.tileBoundingRegion!.maximumHeight = terrainData._maximumHeight * frameState.terrainExaggeration
            return tile
        }

        // No accurate min/max heights available, so we're stuck with min/max heights from an ancestor tile.
        surfaceTile.tileBoundingRegion!.minimumHeight = .nan
        surfaceTile.tileBoundingRegion!.maximumHeight = .nan

        var ancestor = tile.parent
        while ancestor != nil {
            if let ancestorSurfaceTile = ancestor!.data {
            //if ancestorSurfaceTile != nil
                if let ancestorMesh = ancestorSurfaceTile.mesh {
                //if (ancestorMesh !== undefined && ancestorMesh.minimumHeight !== undefined && ancestorMesh.maximumHeight !== undefined)
                    surfaceTile.tileBoundingRegion!.minimumHeight = ancestorMesh.minimumHeight
                    surfaceTile.tileBoundingRegion!.maximumHeight = ancestorMesh.maximumHeight
                    return ancestor!
                }

                if let ancestorTerrainData = ancestorSurfaceTile.terrainData as? QuantizedMeshTerrainData {
                //if (ancestorTerrainData !== undefined && ancestorTerrainData._minimumHeight !== undefined && ancestorTerrainData._maximumHeight !== undefined) {
                    surfaceTile.tileBoundingRegion!.minimumHeight = ancestorTerrainData._minimumHeight * frameState.terrainExaggeration
                    surfaceTile.tileBoundingRegion!.maximumHeight = ancestorTerrainData._maximumHeight * frameState.terrainExaggeration
                    return ancestor!
                }
            }
            ancestor = ancestor!.parent
        }

        return nil
    }
    
    private func computeOccludeePoint(tileProvider: GlobeSurfaceTileProvider, center: Cartesian3, rectangle: Rectangle, height: Double) -> Cartesian3? {
        
        guard let ellipsoidalOccluder = tileProvider.quadtree?.occluders.ellipsoid else { return nil }
        let ellipsoid = ellipsoidalOccluder.ellipsoid

        var cornerPositions: [Cartesian3] = []
        cornerPositions.append(Cartesian3.fromRadians(longitude: rectangle.west, latitude: rectangle.south, height: height, ellipsoid: ellipsoid))
        cornerPositions.append(Cartesian3.fromRadians(longitude: rectangle.east, latitude: rectangle.south, height: height, ellipsoid: ellipsoid))
        cornerPositions.append(Cartesian3.fromRadians(longitude: rectangle.west, latitude: rectangle.north, height: height, ellipsoid: ellipsoid))
        cornerPositions.append(Cartesian3.fromRadians(longitude: rectangle.east, latitude: rectangle.north, height: height, ellipsoid: ellipsoid))

        return ellipsoidalOccluder.computeHorizonCullingPoint(directionToPoint: center, positions: cornerPositions)
    }
    /*
    GlobeSurfaceTileProvider.prototype._onLayerAdded = function(layer, index) {
    if (layer.show) {
    var terrainProvider = this._terrainProvider;
    
    // create TileImagerys for this layer for all previously loaded tiles
    this._quadtree.forEachLoadedTile(function(tile) {
    if (layer._createTileImagerySkeletons(tile, terrainProvider)) {
    tile.state = QuadtreeTileLoadState.LOADING;
    }
    });
    
    this._layerOrderChanged = true;
    }
    };
    
    GlobeSurfaceTileProvider.prototype._onLayerRemoved = function(layer, index) {
    // destroy TileImagerys for this layer for all previously loaded tiles
    this._quadtree.forEachLoadedTile(function(tile) {
    var tileImageryCollection = tile.data.imagery;
    
    var startIndex = -1;
    var numDestroyed = 0;
    for ( var i = 0, len = tileImageryCollection.length; i < len; ++i) {
    var tileImagery = tileImageryCollection[i];
    var imagery = tileImagery.loadingImagery;
    if (!defined(imagery)) {
    imagery = tileImagery.readyImagery;
    }
    if (imagery.imageryLayer === layer) {
    if (startIndex === -1) {
    startIndex = i;
    }
    
    tileImagery.freeResources();
    ++numDestroyed;
    } else if (startIndex !== -1) {
    // iterated past the section of TileImagerys belonging to this layer, no need to continue.
    break;
    }
    }
    
    if (startIndex !== -1) {
    tileImageryCollection.splice(startIndex, numDestroyed);
    }
    });
    };
    
    GlobeSurfaceTileProvider.prototype._onLayerMoved = function(layer, newIndex, oldIndex) {
    this._layerOrderChanged = true;
    };
    
    GlobeSurfaceTileProvider.prototype._onLayerShownOrHidden = function(layer, index, show) {
    if (show) {
    this._onLayerAdded(layer, index);
    } else {
    this._onLayerRemoved(layer, index);
    }
    };
    */
    
    fileprivate func createTileUniformMap(_ maxTextureCount: Int) -> TileUniformMap {
        
        return TileUniformMap(maxTextureCount: maxTextureCount)
    }
    
    
    fileprivate func getManualUniformBufferProvider (_ context: Context, size: Int, deallocationBlock: UniformMapDeallocBlock?) -> UniformBufferProvider {
        if _manualUniformBufferProviderPool.count < 10 {
            QueueManager.sharedInstance.resourceLoadQueue.async(execute: {
                let newProviders = (0..<10).map { _ in return UniformBufferProvider(device: context.device, bufferSize: size, deallocationBlock: deallocationBlock, label: "ManualUniform")
                }
                DispatchQueue.main.async(execute: {
                    self._manualUniformBufferProviderPool.append(contentsOf: newProviders)
                })
            })
        }
        if _manualUniformBufferProviderPool.isEmpty {
            return UniformBufferProvider(device: context.device, bufferSize: size, deallocationBlock: deallocationBlock, label: "ManualUniform")
        }
        return _manualUniformBufferProviderPool.removeLast()
    }
    
    func returnManualUniformBufferProvider (_ provider: UniformBufferProvider) {
        _manualUniformBufferProviderPool.append(provider)
    }
    
    
    fileprivate var _dayTextureTranslationAndScale = [SIMD4<Float>](repeating: SIMD4<Float>(), count: 31)
    fileprivate var _dayTextureTexCoordsRectangle = [SIMD4<Float>](repeating: SIMD4<Float>(), count: 31)

    func addTileModelToRender(_ qtile: QuadtreeTile, frameState: inout FrameState) {
        /* GlobeSurfaceTileNG
        if qtile.invalidateCommandCache {
            qtile._cachedModels.removeAll()
            qtile._cachedCommands.removeAll()
            qtile._cachedTextureArrays.removeAll()
            qtile._cachedCredits.removeAll()
        }
        guard qtile._cachedModels.isEmpty else {
            let surfaceTile = qtile.data!
            
            let viewMatrix = frameState.context.uniformState.view
            //let rtc = surfaceTile.center
            let rtc = surfaceTile.renderedMesh!.center // GlobeSurfaceTileNG
            let centerEye = viewMatrix.multiplyByPoint(rtc)
            let modifiedModelView = viewMatrix.setTranslation(centerEye)
            
            for m in qtile._cachedModels {
                m.uniform.modifiedModelView = modifiedModelView.floatRepresentation
            }
            frameState.tileList.append(contentsOf: qtile._cachedModels)
            return
        }
        */
        
        let surfaceTile = qtile.data!
        if surfaceTile.vertexArray == nil {
            if surfaceTile.fill == nil {
                // No fill was created for this tile, probably because this tile is not connected to
                // any renderable tiles. So create a simple tile in the middle of the tile's possible
                // height range.
                surfaceTile.fill = TerrainFillMesh(tile: qtile)
            }
            surfaceTile.fill!.update(tileProvider: self, frameState: &frameState)
        }
        
        let tileImageryCollection = surfaceTile.imagery
        var imageryIndex = 0
        let imageryLen = tileImageryCollection.count
        let maxTextures = 31
        
        guard let mesh = surfaceTile.renderedMesh else { return } // GlobeSurfaceTileNG
        let rtc = mesh.center
        let encoding = mesh.encoding
        //let rtc = surfaceTile.center
        let viewMatrix = frameState.context.uniformState.view
        let centerEye = viewMatrix.multiplyByPoint(rtc)
        let modifiedModelView = viewMatrix.setTranslation(centerEye)
        
        repeat {
            var numberOfDayTextures = 0
            
            let tileUniformMap = createTileUniformMap(maxTextures)
                tileUniformMap.initialColor = _firstPassInitialColor.floatRepresentation
            tileUniformMap.oceanNormalMap = oceanNormalMap
            tileUniformMap.lightingFadeDistance = SIMD2<Float>(Float(lightingFadeOutDistance), Float(lightingFadeInDistance))
            //tileUniformMap.nightFadeDistance = SIMD2<Float>(Float(nightFadeOutDistance ?? 0), Float(nightFadeInDistance ?? 0))
    
            tileUniformMap.zoomedOutOceanSpecularIntensity = zoomedOutOceanSpecularIntensity
            /*
            var highlightFillTile = !defined(surfaceTile.vertexArray) && defined(tileProvider.fillHighlightColor) && tileProvider.fillHighlightColor.alpha > 0.0;
            if (highlightFillTile) {
                Color.clone(tileProvider.fillHighlightColor, uniformMapProperties.fillHighlightColor);
            }
            */
    
            tileUniformMap.center3D = rtc.floatRepresentation
    
    
            let applyFog = frameState.fog.enabled && Math.fog(qtile.distance, density: frameState.fog.density) > Math.Epsilon3
            
            while numberOfDayTextures < maxTextures && imageryIndex < imageryLen {
                
                let tileImagery = tileImageryCollection[imageryIndex]
                imageryIndex += 1
                
                guard let imagery = tileImagery.readyImagery else {
                    continue
                }
                if imagery.state != .ready || imagery.imageryLayer.alpha() == 0.0 {
                    continue
                }
                if imagery.texture == nil {
                    assertionFailure("readyImagery is not actually ready!")
                }
                let imageryLayer = imagery.imageryLayer
                
                if tileImagery.textureTranslationAndScale == nil {
                    tileImagery.textureTranslationAndScale = imageryLayer.calculateTextureTranslationAndScale(qtile, tileImagery: tileImagery)
                }
                tileUniformMap.dayTextures.append(imagery.texture!)
                
                _dayTextureTranslationAndScale[numberOfDayTextures] = tileImagery.textureTranslationAndScale!.floatRepresentation
                _dayTextureTexCoordsRectangle[numberOfDayTextures] = tileImagery.textureCoordinateRectangle!.floatRepresentation
                
                numberOfDayTextures += 1
            }
            if tileUniformMap.dayTextures.count < 1 {
                continue
            }
            
            tileUniformMap.dayTextureTranslationAndScale = _dayTextureTranslationAndScale
            tileUniformMap.dayTextureTexCoordsRectangle = _dayTextureTexCoordsRectangle
            
            //let encoding = surfaceTile.pickTerrain!.mesh!.encoding
            let quantized = encoding.quantization == .bits12 ? true : false
            
            tileUniformMap.minMaxHeight = Cartesian2(x: encoding.minimumHeight, y: encoding.maximumHeight).floatRepresentation
            
            tileUniformMap.scaleAndBias = encoding.matrix.floatRepresentation
            
            tileUniformMap.modifiedModelView = modifiedModelView.floatRepresentation
            
            //let tile = TileModel(id: "\(qtile.x)_\(qtile.y)_\(qtile.level)", uniform: tileUniformMap.tileUniform, fog: applyFog, textures: tileUniformMap.dayTextures, vertexArray: surfaceTile.vertexArray!, boundingVolume: surfaceTile.boundingSphere3D, orientedBoundingBox: surfaceTile.orientedBoundingBox, quantized: quantized)
            let tile = TileModel(id: "\(qtile.x)_\(qtile.y)_\(qtile.level)", uniform: tileUniformMap.tileUniform, fog: applyFog, textures: tileUniformMap.dayTextures, vertexArray: surfaceTile.vertexArray ?? surfaceTile.fill!.vertexArray!, boundingVolume: mesh.boundingSphere3D, orientedBoundingBox: surfaceTile.orientedBoundingBox, quantized: quantized) // GlobeSurfaceTileNG
            
            frameState.tileList.append(tile)
            //qtile._cachedModels.append(tile) // GlobeSurfaceTileNG
            
            qtile.invalidateCommandCache = false
        } while (imageryIndex < imageryLen)
    }

    func addDrawCommandsForTile(_ tile: QuadtreeTile, frameState: inout FrameState) {
        /* GlobeSurfaceTileNG
        if tile.invalidateCommandCache {
            tile._cachedCommands.removeAll()
            tile._cachedTextureArrays.removeAll()
            tile._cachedCredits.removeAll()
        }
        
        if !tile._cachedCommands.isEmpty {
            updateRTCPosition(forTile: tile, frameState: frameState)
            frameState.commandList.append(contentsOf: tile._cachedCommands.map { $0 as Command })
            //_ = tile._cachedCredits.map { frameState.creditDisplay.addCredit($0) }
            return
        }
        */
        
        guard let context = frameState.context else {
            return
        }

        let otherPassesInitialColor = Cartesian4(x: 0.0, y: 0.0, z: 0.0, w: 0.0)

        let surfaceTile = tile.data!
        
        if surfaceTile.vertexArray == nil {
            if surfaceTile.fill == nil {
                // No fill was created for this tile, probably because this tile is not connected to
                // any renderable tiles. So create a simple tile in the middle of the tile's possible
                // height range.
                surfaceTile.fill = TerrainFillMesh(tile: tile)
            }
            surfaceTile.fill!.update(tileProvider: self, frameState: &frameState)
        }

        let waterMaskTexture = surfaceTile.waterMaskTexture
        /*
        var waterMaskTranslationAndScale = surfaceTile.waterMaskTranslationAndScale
        if (!defined(waterMaskTexture) && defined(surfaceTile.fill)) {
            waterMaskTexture = surfaceTile.fill.waterMaskTexture;
            waterMaskTranslationAndScale = surfaceTile.fill.waterMaskTranslationAndScale;
        }
        */
        
        let showReflectiveOcean = hasWaterMask && waterMaskTexture != nil
        let showOceanWaves = showReflectiveOcean && oceanNormalMap != nil
        let hasVertexNormals = terrainProvider.ready && terrainProvider.hasVertexNormals
        let enableFog = frameState.fog.enabled
        /*
        var castShadows = ShadowMode.castShadows(tileProvider.shadows);
        var receiveShadows = ShadowMode.receiveShadows(tileProvider.shadows);

        var hueShift = tileProvider.hueShift;
        var saturationShift = tileProvider.saturationShift;
        var brightnessShift = tileProvider.brightnessShift;

        var colorCorrect = !(CesiumMath.equalsEpsilon(hueShift, 0.0, CesiumMath.EPSILON7) &&
                             CesiumMath.equalsEpsilon(saturationShift, 0.0, CesiumMath.EPSILON7) &&
                             CesiumMath.equalsEpsilon(brightnessShift, 0.0, CesiumMath.EPSILON7));
        */
        var perFragmentGroundAtmosphere = false
        if showGroundAtmosphere, let nightFadeOutDistance = nightFadeOutDistance {
            let mode = frameState.mode
            let camera = frameState.camera!
            let cameraDistance: Double
            if mode == .scene2D || mode == .columbusView {
                cameraDistance = camera.positionCartographic.height
            } else {
                cameraDistance = camera.positionWC.magnitude
            }
            var fadeOutDistance = nightFadeOutDistance
            if mode != .scene3D {
                fadeOutDistance -= frameState.mapProjection.ellipsoid.maximumRadius
            }
            perFragmentGroundAtmosphere = cameraDistance > fadeOutDistance
        }
        
        var maxTextures = context.limits.maximumTextureImageUnits
        
        if showReflectiveOcean {
            maxTextures -= 1
        }
        if showOceanWaves {
            maxTextures -= 1
        }
        
        /*
        if (defined(frameState.shadowState) && frameState.shadowState.shadowsEnabled) {
            --maxTextures;
        }
        if (defined(tileProvider.clippingPlanes) && tileProvider.clippingPlanes.enabled) {
            --maxTextures;
        }
        */
        
        guard let mesh = surfaceTile.renderedMesh else { return }
        let rtc = mesh.center
        let encoding = mesh.encoding
        //var rtc = surfaceTile.center
        
        var scratchArray = [Float32](repeating: 0.0, count: 1)
        
        // Not used in 3D.
        var tileRectangle = Cartesian4()
        
        // Only used for Mercator projections.
        var southLatitude = 0.0
        var northLatitude = 0.0
        var southMercatorYHigh = 0.0
        var southMercatorYLow = 0.0
        var oneOverMercatorHeight = 0.0
        
        var useWebMercatorProjection = false

        if frameState.mode != .scene3D {
            let projection = frameState.mapProjection
            let southwest = projection.project(tile.rectangle.southwest)
            let northeast = projection.project(tile.rectangle.northeast)
            
            tileRectangle.x = southwest.x
            tileRectangle.y = southwest.y
            tileRectangle.z = northeast.x
            tileRectangle.w = northeast.y
            
            // In 2D and Columbus View, use the center of the tile for RTC rendering.
            /*if frameState.mode != .Morphing {
                rtc = Cartesian3(x: 0.0, y: (tileRectangle.z + tileRectangle.x) * 0.5, z: (tileRectangle.w + tileRectangle.y) * 0.5)
                tileRectangle.x -= rtc.y
                tileRectangle.y -= rtc.z
                tileRectangle.z -= rtc.y
                tileRectangle.w -= rtc.z
            }*/
            
            /*
             if (frameState.mode === SceneMode.SCENE2D && encoding.quantization === TerrainQuantization.BITS12) {
             // In 2D, the texture coordinates of the tile are interpolated over the rectangle to get the position in the vertex shader.
             // When the texture coordinates are quantized, error is introduced. This can be seen through the 1px wide cracking
             // between the quantized tiles in 2D. To compensate for the error, move the expand the rectangle in each direction by
             // half the error amount.
             var epsilon = (1.0 / (Math.pow(2.0, 12.0) - 1.0)) * 0.5;
             var widthEpsilon = (tileRectangle.z - tileRectangle.x) * epsilon;
             var heightEpsilon = (tileRectangle.w - tileRectangle.y) * epsilon;
             tileRectangle.x -= widthEpsilon;
             tileRectangle.y -= heightEpsilon;
             tileRectangle.z += widthEpsilon;
             tileRectangle.w += heightEpsilon;
             }
            */
            
            if projection is WebMercatorProjection {
                southLatitude = tile.rectangle.south
                northLatitude = tile.rectangle.north
                
                let southMercatorY = WebMercatorProjection.geodeticLatitudeToMercatorAngle(southLatitude)
                let northMercatorY = WebMercatorProjection.geodeticLatitudeToMercatorAngle(northLatitude)
                
                scratchArray[0] = Float32(southMercatorY)
                southMercatorYHigh = Double(scratchArray[0])
                southMercatorYLow = southMercatorY - Double(scratchArray[0])
                
                oneOverMercatorHeight = 1.0 / (northMercatorY - southMercatorY)
                
                useWebMercatorProjection = true
            }
        }
        
        let tileImageryCollection = surfaceTile.imagery
        var imageryIndex = 0
        let imageryLen = tileImageryCollection.count
        
        let firstPassRenderState = _renderState
        let otherPassesRenderState = _blendRenderState
        var renderState = firstPassRenderState
        
        var initialColor = _firstPassInitialColor
        
        if _debug.boundingSphereTile == nil {
            //debugDestroyPrimitive()
        }

        repeat {
            var numberOfDayTextures = 0
            
            let command = DrawCommand()
            command.cull = false
            command.boundingVolume = BoundingSphere()
            command.orientedBoundingBox = nil
            
            let tileUniformMap = createTileUniformMap(maxTextures)
            
            _usedDrawCommands += 1

            /*if (tile === tileProvider._debug.boundingSphereTile) {
                // If a debug primitive already exists for this tile, it will not be
                // re-created, to avoid allocation every frame. If it were possible
                // to have more than one selected tile, this would have to change.
                if (defined(surfaceTile.orientedBoundingBox)) {
                    getDebugOrientedBoundingBox(surfaceTile.orientedBoundingBox, Color.RED).update(context, frameState, commandList);
                } else if (defined(surfaceTile.boundingSphere3D)) {
                    getDebugBoundingSphere(surfaceTile.boundingSphere3D, Color.RED).update(context, frameState, commandList);
                }
            }*/
            
            tileUniformMap.initialColor = initialColor.floatRepresentation
            tileUniformMap.oceanNormalMap = oceanNormalMap
            tileUniformMap.lightingFadeDistance = SIMD2<Float>(Float(lightingFadeOutDistance), Float(lightingFadeInDistance))

            tileUniformMap.zoomedOutOceanSpecularIntensity = zoomedOutOceanSpecularIntensity
            
            //tileUniformMap.center3D = surfaceTile.center.floatRepresentation
            tileUniformMap.center3D = rtc.floatRepresentation // GlobeSurfaceTileNG
            
            tileUniformMap.tileRectangle = tileRectangle.floatRepresentation
            
            tileUniformMap.southAndNorthLatitude = Cartesian2(x: southLatitude, y: northLatitude).floatRepresentation
            tileUniformMap.southMercatorYAndOneOverHeight = SIMD2<Float>(x: Float(southMercatorYHigh), y: Float(oneOverMercatorHeight))
            
            // For performance, use fog in the shader only when the tile is in fog.
            let applyFog = enableFog && Math.fog(tile.distance, density: frameState.fog.density) > Math.Epsilon3

            let applyBrightness = false
            let applyContrast = false
            let applyHue = false
            let applySaturation = false
            let applyGamma = false
            let applyAlpha = false
            
            command.label = "tile_\(tile.x)_\(tile.y)_\(tile.level)"
            if applyFog {
                command.label! += "_fog"
            }
            
            tileUniformMap.dayTextures.removeAll()
            
            while (numberOfDayTextures < maxTextures && imageryIndex < imageryLen) {
                
                let tileImagery = tileImageryCollection[imageryIndex]
                imageryIndex += 1
                
                guard let imagery = tileImagery.readyImagery else {
                    continue
                }
                
                if imagery.state != .ready || imagery.imageryLayer.alpha() == 0.0 {
                    continue
                }
                if imagery.texture == nil {
                    assertionFailure("readyImagery is not actually ready!")
                }
                
                let imageryLayer = imagery.imageryLayer
                
                if tileImagery.textureTranslationAndScale == nil {
                    tileImagery.textureTranslationAndScale = imageryLayer.calculateTextureTranslationAndScale(tile, tileImagery: tileImagery)
                }
                tileUniformMap.dayTextures.append(imagery.texture!)
                
                _dayTextureTranslationAndScale[numberOfDayTextures] = tileImagery.textureTranslationAndScale!.floatRepresentation
                _dayTextureTexCoordsRectangle[numberOfDayTextures] = tileImagery.textureCoordinateRectangle!.floatRepresentation
                
                //let alpha = imageryLayer.alpha()
                
                //uniformStruct.dayTextureAlpha[numberOfDayTextures] = imageryLayer.alpha()
                //applyAlpha = applyAlpha || uniformStruct.dayTextureAlpha[numberOfDayTextures] != 1.0
                
                //uniformStruct.dayTextureBrightness[numberOfDayTextures] = imageryLayer.brightness()
                //applyBrightness = applyBrightness || (uniformStruct.dayTextureBrightness[numberOfDayTextures] != imageryLayer.defaultBrightness)
                
                //uniformStruct.dayTextureContrast[numberOfDayTextures] = imageryLayer.contrast()
                //applyContrast = applyContrast || uniformStruct.dayTextureContrast[numberOfDayTextures] != imageryLayer.defaultContrast
                
                //uniformStruct.dayTextureHue[numberOfDayTextures] = imageryLayer.hue()
                //applyHue = applyHue || uniformStruct.dayTextureHue[numberOfDayTextures] != imageryLayer.defaultHue
                
                //uniformStruct.dayTextureSaturation[numberOfDayTextures] = imageryLayer.saturation()
                //applySaturation = applySaturation || uniformStruct.dayTextureSaturation[numberOfDayTextures] != imageryLayer.defaultSaturation
                
                //uniformStruct.dayTextureOneOverGamma[numberOfDayTextures] = 1.0 / imageryLayer.gamma()
                //applyGamma = applyGamma || uniformStruct.dayTextureOneOverGamma[numberOfDayTextures] != 1.0 / imageryLayer.defaultGamma
                
                tile._cachedCredits.append(contentsOf: imagery.credits)
                
                numberOfDayTextures += 1
            }
            tileUniformMap.dayTextureTranslationAndScale = _dayTextureTranslationAndScale
            tileUniformMap.dayTextureTexCoordsRectangle = _dayTextureTexCoordsRectangle
            
            if let waterMaskTexture = waterMaskTexture {
                tileUniformMap.waterMask = waterMaskTexture
            }
            if let oceanNormalMap = oceanNormalMap {
                tileUniformMap.oceanNormalMap = oceanNormalMap
            }
            tileUniformMap.waterMaskTranslationAndScale = surfaceTile.waterMaskTranslationAndScale.floatRepresentation
            
            //let encoding = surfaceTile.pickTerrain!.mesh!.encoding // GlobeSurfaceTileNG
            command.quantization = encoding.quantization

            tileUniformMap.minMaxHeight = Cartesian2(x: encoding.minimumHeight, y: encoding.maximumHeight).floatRepresentation

            tileUniformMap.scaleAndBias = encoding.matrix.floatRepresentation
            
            // update clipping planes
            /*
            var clippingPlanes = tileProvider._clippingPlanes;
            var clippingPlanesEnabled = defined(clippingPlanes) && clippingPlanes.enabled && tile.isClipped;
            if (clippingPlanesEnabled) {
                uniformMapProperties.clippingPlanesEdgeColor = Color.clone(clippingPlanes.edgeColor, uniformMapProperties.clippingPlanesEdgeColor);
                uniformMapProperties.clippingPlanesEdgeWidth = clippingPlanes.edgeWidth;
            }
            */

            command.pipeline = surfaceShaderSet.getRenderPipeline(
                frameState: frameState,
                surfaceTile: surfaceTile,
                numberOfDayTextures: numberOfDayTextures,
                applyBrightness: applyBrightness,
                applyContrast: applyContrast,
                applyHue: applyHue,
                applySaturation: applySaturation,
                applyGamma: applyGamma,
                applyAlpha: applyAlpha,
                showReflectiveOcean: showReflectiveOcean,
                showOceanWaves: showOceanWaves,
                enableLighting: enableLighting,
                hasVertexNormals: hasVertexNormals,
                useWebMercatorProjection: useWebMercatorProjection,
                enableFog: applyFog
            )
            
            command.renderState = renderState
            command.primitiveType = .triangle
            command.vertexArray = surfaceTile.vertexArray
            command.uniformMap = tileUniformMap
            
            command.uniformMap!.uniformBufferProvider = getManualUniformBufferProvider(context, size: MemoryLayout<TileUniformStruct>.stride, deallocationBlock: { provider in
                    self.returnManualUniformBufferProvider(provider)
                }
            )
                        
            command.pass = .globe
            
            command.renderState!.wireFrame = _debug.wireframe
            
            var boundingSphere: BoundingSphere
            
            if frameState.mode != .scene3D {
                let tileBoundingRegion = surfaceTile.tileBoundingRegion!
                boundingSphere = BoundingSphere(
                    fromRectangleWithHeights2D: tile.rectangle,
                    projection: frameState.mapProjection,
                    //minimumHeight: surfaceTile.minimumHeight,
                    minimumHeight: tileBoundingRegion.minimumHeight, // GlobeSurfaceTileNG
                    //maximumHeight: surfaceTile.maximumHeight)
                    maximumHeight: tileBoundingRegion.maximumHeight) // GlobeSurfaceTileNG
                
                boundingSphere.center = Cartesian3(
                    x: boundingSphere.center.z,
                    y: boundingSphere.center.x,
                    z: boundingSphere.center.y)
                
                if (frameState.mode == .morphing) {
                    //boundingSphere = surfaceTile.boundingSphere3D.union(boundingSphere)
                    boundingSphere = mesh.boundingSphere3D.union(boundingSphere) // GlobeSurfaceTileNG
                }
            } else {
                //boundingSphere = surfaceTile.boundingSphere3D
                boundingSphere = mesh.boundingSphere3D // GlobeSurfaceTileNG
            }
            
            command.boundingVolume = boundingSphere
            command.orientedBoundingBox = surfaceTile.orientedBoundingBox
            
            frameState.commandList.append(command)
            //tile._cachedCommands.append(command) // GlobeSurfaceTileNG
            
            tile.invalidateCommandCache = false
            
            renderState = otherPassesRenderState
            initialColor = otherPassesInitialColor
        } while (imageryIndex < imageryLen)
        
        //tile._cachedCredits.map { frameState.creditDisplay.addCredit($0) }
        for credit in tile._cachedCredits {
            frameState.creditDisplay.addCredit(credit)
        }
        updateRTCPosition(forTile: tile, frameState: frameState)
    }
    
    func updateRTCPosition(forTile tile: QuadtreeTile, frameState: FrameState) {
        //let viewMatrix = frameState.camera!._viewMatrix
        let viewMatrix = frameState.context.uniformState.view
        //let rtc = tile.data!.center
        let rtc = tile.data!.renderedMesh!.center
        let centerEye = viewMatrix.multiplyByPoint(rtc)
        let modifiedModelView = viewMatrix.setTranslation(centerEye)

        for command in tile._cachedCommands {
            (command.uniformMap! as! TileUniformMap).modifiedModelView = modifiedModelView.floatRepresentation
        }
    }

    func addPickCommandsForTile(_ drawCommand: DrawCommand, context: Context, frameState: FrameState, commandList: inout [Command]) {
        let pickCommand: DrawCommand
        if (_pickCommands.count <= _usedPickCommands) {
            pickCommand = DrawCommand(cull: false)
            pickCommand.cull = false
            pickCommand.label = "pick"
            
            _pickCommands.append(pickCommand)
        } else {
            pickCommand = _pickCommands[_usedPickCommands]
        }
        
        _usedPickCommands += 1
        
        //let useWebMercatorProjection = frameState.mapProjection is WebMercatorProjection
        // FIXME: pickCommand
        //pickCommand.pipeline = surfaceShaderSet.getPickRenderPipeline(context: context, sceneMode: frameState.mode, useWebMercatorProjection: useWebMercatorProjection)
        pickCommand.renderState = _pickRenderState
        
        pickCommand.owner = drawCommand.owner
        pickCommand.primitiveType = drawCommand.primitiveType
        pickCommand.vertexArray = drawCommand.vertexArray
        pickCommand.uniformMap = drawCommand.uniformMap
        pickCommand.boundingVolume = drawCommand.boundingVolume
        pickCommand.orientedBoundingBox = drawCommand.orientedBoundingBox
        pickCommand.pass = drawCommand.pass
        
        commandList.append(pickCommand)
    }
    
    func canRenderWithoutLosingDetail(_ tile: QuadtreeTile) -> Bool {
        let surfaceTile = tile.data

        var readyImagery = [Bool](repeating: false, count: imageryLayers.count)

        var terrainReady = false
        var initialImageryState = false
        var imagery: [TileImagery] = []

        if surfaceTile != nil {
            // We can render even with non-ready terrain as long as all our rendered descendants
            // are missing terrain geometry too. i.e. if we rendered fills for more detailed tiles
            // last frame, it's ok to render a fill for this tile this frame.
            terrainReady = surfaceTile!.terrainState == .ready

            // Initially assume all imagery layers are ready, unless imagery hasn't been initialized at all.
            initialImageryState = true

            imagery = surfaceTile!.imagery
        }

        readyImagery = readyImagery.map{ _ in initialImageryState }

        for tileImagery in imagery {
            let loadingImagery = tileImagery.loadingImagery
            let isReady = loadingImagery == nil || loadingImagery!.state == .failed || loadingImagery!.state == .invalid
            let layerIndex = (tileImagery.loadingImagery ?? tileImagery.readyImagery!).imageryLayer.layerIndex
            // For a layer to be ready, all tiles belonging to that layer must be ready.
            readyImagery[layerIndex] = isReady && readyImagery[layerIndex]
        }

        let lastFrame = quadtree?.lastSelectionFrameNumber ?? 0

        // Traverse the descendants looking for one with terrain or imagery that is not loaded on this tile.
        var stack: [QuadtreeTile] = []
        stack.append(contentsOf: [tile.southwestChild, tile.southeastChild, tile.northwestChild, tile.northeastChild])

        while stack.count > 0 {
            let descendant = stack.removeLast()
            let lastFrameSelectionResult = descendant.lastSelectionResultFrame == lastFrame ? descendant.lastSelectionResult : .none

            if lastFrameSelectionResult == .rendered {
                // Descendant has no data, so it can't block rendering.
                guard let descendantSurface = descendant.data else { continue }

                if !terrainReady && descendantSurface.terrainState == .ready {
                    // Rendered descendant has real terrain, but we don't. Rendering is blocked.
                    return false
                }

                let descendantImagery = descendantSurface.imagery
                for descendantTileImagery in descendantImagery {
                    let descendantLoadingImagery = descendantTileImagery.loadingImagery
                    let descendantIsReady = descendantLoadingImagery == nil || descendantLoadingImagery!.state == .failed || descendantLoadingImagery!.state == .invalid
                    let descendantLayerIndex = (descendantTileImagery.loadingImagery ?? descendantTileImagery.readyImagery!).imageryLayer.layerIndex

                    // If this imagery tile of a descendant is ready but the layer isn't ready in this tile,
                    // then rendering is blocked.
                    if descendantIsReady && !readyImagery[descendantLayerIndex] {
                        return false
                    }
                }
            } else if lastFrameSelectionResult == .refined {
                stack.append(contentsOf: [descendant.southwestChild, descendant.southeastChild, descendant.northwestChild, descendant.northeastChild])
            }
        }
        return true
    }
    
    func canRefine(_ tile: QuadtreeTile) -> Bool {
        // Only allow refinement it we know whether or not the children of this tile exist.
        // For a tileset with `availability`, we'll always be able to refine.
        // We can ask for availability of _any_ child tile because we only need to confirm
        // that we get a yes or no answer, it doesn't matter what the answer is.
        if tile.data?.terrainData != nil {
            return true
        }
        let childAvailable = terrainProvider.getTileDataAvailable(x: tile.x * 2, y: tile.y * 2, level: tile.level + 1)
        return childAvailable ?? false
        //return childAvailable != nil
    }
}

extension GlobeSurfaceTileProvider {
    // cartographicLimitRectangle may span the IDL, but tiles never will.
    private func clipRectangleAntimeridian(tileRectangle: Rectangle, cartographicLimitRectangle: Rectangle) -> Rectangle {
        if cartographicLimitRectangle.west < cartographicLimitRectangle.east {
            return cartographicLimitRectangle
        }
        var splitRectangle = cartographicLimitRectangle
        let tileCenter = tileRectangle.center
        if tileCenter.longitude > 0.0 {
            splitRectangle.east = .pi
        } else {
            splitRectangle.west = -.pi
        }
        return splitRectangle
    }
}
