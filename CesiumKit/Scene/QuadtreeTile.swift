//
//  QuadtreeTile.swift
//  CesiumKit
//
//  Created by Ryan Walklin on 16/08/14.
//  Copyright (c) 2014 Test Toast. All rights reserved.
//

enum TileSelectionResult: Int {
    case none = 0
    case culled = 1
    case rendered = 2
    case refined = 3
    case kicked = 4
    case rendered_and_kicked = 6
    case refined_and_kicked = 7
    case culled_but_needed = 9
    
    var original: TileSelectionResult {
        TileSelectionResult(rawValue: (self.rawValue & 3))!
    }
    var kick: TileSelectionResult {
        TileSelectionResult(rawValue: (self.rawValue | 4))!
    }
    var wasKicked: Bool {
        self.rawValue >= TileSelectionResult.rendered_and_kicked.rawValue
    }
}

enum TileEdge: Int {
    case east = 0
    case west = 1
    case north = 2
    case south = 3
    case northwest = 4
    case northeast
    case southwest
    case southeast
}

/**
* A single tile in a {@link QuadtreePrimitive}.
*
* @alias QuadtreeTile
* @constructor
* @private
*
* - parameter level: The level of the tile in the quadtree.
* @param x The X coordinate of the tile in the quadtree.  0 is the westernmost tile.
* @param y The Y coordinate of the tile in the quadtree.  0 is the northernmost tile.
* @param tilingScheme The tiling scheme in which this tile exists.
* @param  This tile's parent, or undefined if this is a root tile.
*/
class QuadtreeTile: Equatable {
    
    let level: Int
    let x: Int
    let y: Int
    let tilingScheme: TilingScheme
    let parent: QuadtreeTile?
    
    /**
    * Gets the cartographic rectangle of the tile, with north, south, east and
    * west properties in radians.
    * @memberof QuadtreeTile.prototype
    * @type {Rectangle}
    */
    let rectangle: Rectangle
    
    /**
    * Gets or sets the current state of the tile in the tile load pipeline.
    * @type {QuadtreeTileLoadState}
    * @default {@link QuadtreeTileLoadState.START}
    */
    var state: TileLoadState = .start
    
    /**
    * Gets or sets a value indicating whether or not the tile is currently renderable.
    * @type {Boolean}
    * @default false
    */
    var renderable = false
    
    /**
    * tile imagery or terrain has changed, so draw commands need updating
    */
    var invalidateCommandCache = true
    
    // QuadtreeTileReplacementQueue gets/sets these private properties.
    var replacementPrevious: QuadtreeTile? = nil
    var replacementNext: QuadtreeTile? = nil
    
    // The distance from the camera to this tile, updated when the tile is selected
    // for rendering.  We can get rid of this if we have a better way to sort by
    // distance - for example, by using the natural ordering of a quadtree.
    // QuadtreePrimitive gets/sets this private property.
    var distance = 0.0
    var loadPriority = 0.0

    /**
     * An array of objects associated with this tile.
     * @memberof QuadtreeTile.prototype
     * @type {Array}
     */
    var customData = [CallbackObject]()
    
    //fileprivate (set) var frameUpdated = Int.max
    fileprivate (set) var frameUpdated: Int? = nil
    //var frameRendered = Int.max
    var frameRendered: Int? = nil
    var _loadedCallbacks: [String: Any] = [:]
    
    /**
    * Gets or set a value indicating whether or not the tile was entire upsampled from its
    * parent tile.  If all four children of a parent tile were upsampled from the parent,
    * we will render the parent instead of the children even if the LOD indicates that
    * the children would be preferable.
    * @type {Boolean}
    * @default false
    */
    var upsampledFromParent = false
    
    /**
    * Gets a value indicating whether or not this tile needs further loading.
    * This property will return true if the {@link QuadtreeTile#state} is
    * <code>START</code> or <code>LOADING</code>.
    * @memberof QuadtreeTile.prototype
    * @type {Boolean}
    */
    var needsLoading: Bool {
        get {
            return state == .start || state == .loading
        }
    }
    
    /**
    * Gets a value indicating whether or not this tile is eligible to be unloaded.
    * Typically, a tile is ineligible to be unloaded while an asynchronous operation,
    * such as a request for data, is in progress on it.  A tile will never be
    * unloaded while it is needed for rendering, regardless of the value of this
    * property.  If {@link QuadtreeTile#data} is defined and has an
    * <code>eligibleForUnloading</code> property, the value of that property is returned.
    * Otherwise, this property returns true.
    * @memberof QuadtreeTile.prototype
    * @type {Boolean}
    */
    var eligibleForUnloading: Bool {
        get {
            if data != nil {
                return data!.eligibleForUnloading()
            }
            return true
        }
    }


    /**
    * An array of tiles that is at the next level of the tile tree.
    * @memberof QuadtreeTile.prototype
    * @type {QuadtreeTile[]}
    */
    var children: [QuadtreeTile] {
        /*
        get {
            if _children == nil {
                let nextlevel = level + 1
                let nextX = x * 2
                let nextY = y * 2
                _children = [
                    QuadtreeTile(level: nextlevel, x: nextX, y: nextY, tilingScheme: tilingScheme, parent: self),
                    QuadtreeTile(level: nextlevel, x: nextX + 1, y: nextY, tilingScheme: tilingScheme, parent: self),
                    QuadtreeTile(level: nextlevel, x: nextX, y: nextY + 1, tilingScheme: tilingScheme, parent: self),
                    QuadtreeTile(level: nextlevel, x: nextX + 1, y: nextY + 1, tilingScheme: tilingScheme, parent: self)
                ]
            }
            return _children!
        }
        */
        return [northwestChild, northeastChild, southwestChild, southeastChild]
    }
    
    //var _children: [QuadtreeTile]?
    
    /**
    * Gets or sets the additional data associated with this tile.  The exact content is specific to the
    * {@link QuadtreeTileProvider}.
    * @type {Object: QuadTreeTileData}
    * @default undefined
    */
    var data: GlobeSurfaceTile? = nil
    
    var _cachedCommands = [DrawCommand]()
    var _cachedCredits = [Credit]()
    var _cachedTextureArrays = [Array<Texture>]()
    
    var _cachedModels: [TileModel] = []
    /**
     ver 1.46
     */
    var _southwestChild: QuadtreeTile? = nil
    var southwestChild: QuadtreeTile {
        if _southwestChild == nil {
            _southwestChild = QuadtreeTile(level: level + 1, x: x * 2, y: y * 2 + 1, tilingScheme: tilingScheme, parent: self)
        }
        return _southwestChild!
    }
    var _southeastChild: QuadtreeTile? = nil
    var southeastChild: QuadtreeTile {
        if _southeastChild == nil {
            _southeastChild = QuadtreeTile(level: level + 1, x: x * 2 + 1, y: y * 2 + 1, tilingScheme: tilingScheme, parent: self)
        }
        return _southeastChild!
    }
    var _northwestChild: QuadtreeTile? = nil
    var northwestChild: QuadtreeTile {
        if _northwestChild == nil {
            _northwestChild = QuadtreeTile(level: level + 1, x: x * 2, y: y * 2, tilingScheme: tilingScheme, parent: self)
        }
        return _northwestChild!
    }
    var _northeastChild: QuadtreeTile? = nil
    var northeastChild: QuadtreeTile {
        if _northeastChild == nil {
            _northeastChild = QuadtreeTile(level: level + 1, x: x * 2 + 1, y: y * 2, tilingScheme: tilingScheme, parent: self)
        }
        return _northeastChild!
    }

    typealias priorityFunctionType = () -> Double?
    var priorityFunction: priorityFunctionType?
    
    var lastSelectionResult: TileSelectionResult = .none
    var lastSelectionResultFrame: Int? = nil
    
    init(level: Int, x: Int, y: Int, tilingScheme: TilingScheme, parent: QuadtreeTile?) {
        
        assert(x >= 0 && y >= 0, "x and y must be greater than or equal to zero")
        assert(level >= 0, "level must be greater than or equal to zero.")
        self.tilingScheme = tilingScheme
        self.x = x
        self.y = y
        self.level = level
        self.parent = parent
        self.rectangle = tilingScheme.tileXYToRectangle(x: x, y: y, level: level)
    }
    
    /**
    * Creates a rectangular set of tiles for level of detail zero, the coarsest, least detailed level.
    *
    * @memberof QuadtreeTile
    *
    * @param {TilingScheme} tilingScheme The tiling scheme for which the tiles are to be created.
    * @returns {QuadtreeTile[]} An array containing the tiles at level of detail zero, starting with the
    * tile in the northwest corner and followed by the tile (if any) to its east.
    */
    class func createLevelZeroTiles (_ tilingScheme: TilingScheme) -> [QuadtreeTile] {
        
        let numberOfLevelZeroTilesX = tilingScheme.numberOfXTilesAt(level: 0)
        let numberOfLevelZeroTilesY = tilingScheme.numberOfYTilesAt(level: 0)
        
        var result = [QuadtreeTile]()
        
        for y in 0..<numberOfLevelZeroTilesY {
            for x in 0..<numberOfLevelZeroTilesX {
                result.append(QuadtreeTile(level: 0, x: x, y: y, tilingScheme: tilingScheme, parent: nil))
            }
        }
        return result
    }
    
    func updateCustomData (_ frameNumber: Int, added: [CallbackObject]? = nil, removed: [CallbackObject]? = nil) {
        
        
        if added != nil && removed != nil {
            // level zero tile
            
            //for (i, data) in removed!.enumerated() {
            for data in removed! {
                
                for j in 0..<customData.count {
                    if (customData[j] === data) {
                        customData.remove(at: j)
                        break
                    }
                }
            }
            
            //for (i, data) in added!.enumerated() {
            for data in added! {
                if rectangle.contains(data.positionCartographic) {
                    customData.append(data)
                }
            }
            
            frameUpdated = frameNumber
        } else {
            // interior or leaf tile, update from parent
            guard let parent = self.parent else { return }
            if frameUpdated != parent.frameUpdated {
                customData.removeAll()
                
                //var parentCustomData = parent!.customData
                for data in parent.customData {
                    if rectangle.contains(data.positionCartographic) {
                        customData.append(data)
                    }
                }
                
                frameUpdated = parent.frameUpdated
            }
        }
    
    }
    
    /**
    * Frees the resources assocated with this tile and returns it to the <code>START</code>
    * {@link QuadtreeTileLoadState}.  If the {@link QuadtreeTile#data} property is defined and it
    * has a <code>freeResources</code> method, the method will be invoked.
    *
    * @memberof QuadtreeTile
    */
    func freeResources () {
        state = .start
        renderable = false
        upsampledFromParent = false

        data?.freeResources()
        
        freeTile(_southwestChild)
        _southwestChild = nil
        freeTile(_southeastChild)
        _southeastChild = nil
        freeTile(_northwestChild)
        _northwestChild = nil
        freeTile(_northeastChild)
        _northeastChild = nil
        /*
        if _children != nil {
            for tile in _children! {
                tile.freeResources(provider)
            }
            _children = nil
        }
        */
        _cachedCommands.removeAll()
        _cachedModels.removeAll()
        _cachedTextureArrays.removeAll()
        _cachedCredits.removeAll()
    }
    
    func freeTile(_ tile: QuadtreeTile?) {
        tile?.freeResources()
    }
}

func ==(lhs: QuadtreeTile, rhs: QuadtreeTile) -> Bool {
    return lhs.level == rhs.level && lhs.x == rhs.x && lhs.y == rhs.y
}

class CustomData {
    var level: Int = 0
    var positionCartographic: Cartographic = Cartographic()
}

protocol QuadTreeTileData: class {
    var eligibleForUnloading: Bool { get }
}

extension QuadTreeTileData {
    var eligibleForUnloading: Bool {
        return true
    }
    
}

extension QuadtreeTile {
    func findLevelZeroTile(levelZeroTiles: [QuadtreeTile], x: Int, y: Int) -> QuadtreeTile? {
        let xTiles = tilingScheme.numberOfXTilesAt(level: 0)
        var x = x
        if x < 0 {
            x += xTiles
        } else if x >= xTiles {
            x -= xTiles
        }

        if y < 0 || y >= tilingScheme.numberOfYTilesAt(level: 0) {
            return nil
        }
        
        let filteredLevelZero = levelZeroTiles.filter { $0.x == x && $0.y == y }
        return filteredLevelZero[0]
    }
    
    func findTileToWest(levelZeroTiles: [QuadtreeTile]) -> QuadtreeTile? {
        guard let parent = parent else {
            return findLevelZeroTile(levelZeroTiles: levelZeroTiles, x: x - 1, y: y)
        }

        if parent.southeastChild == self {
            return parent.southwestChild
        } else if parent.northeastChild == self {
            return parent.northwestChild
        }

        guard let westOfParent = parent.findTileToWest(levelZeroTiles: levelZeroTiles) else { return nil }
        if parent.southwestChild == self {
            return westOfParent.southeastChild
        }
        return westOfParent.northeastChild
    }
    
    func findTileToEast(levelZeroTiles: [QuadtreeTile]) -> QuadtreeTile? {
        guard let parent = parent else {
            return findLevelZeroTile(levelZeroTiles: levelZeroTiles, x: x + 1, y: y)
        }

        if parent.southwestChild == self {
            return parent.southeastChild
        } else if parent.northwestChild == self {
            return parent.northeastChild
        }

        guard let eastOfParent = parent.findTileToEast(levelZeroTiles: levelZeroTiles) else {
            return nil
        }
        if parent.southeastChild == self {
            return eastOfParent.southwestChild
        }
        return eastOfParent.northwestChild
    }
    
    func findTileToSouth(levelZeroTiles: [QuadtreeTile]) -> QuadtreeTile? {
        guard let parent = parent else {
            return findLevelZeroTile(levelZeroTiles: levelZeroTiles, x: x, y: y + 1)
        }

        if parent.northwestChild == self {
            return parent.southwestChild
        } else if parent.northeastChild == self {
            return parent.southeastChild
        }

        guard let southOfParent = parent.findTileToSouth(levelZeroTiles: levelZeroTiles) else {
            return nil
        }
        if parent.southwestChild == self {
            return southOfParent.northwestChild
        }
        return southOfParent.northeastChild
    }
    
    func findTileToNorth(levelZeroTiles: [QuadtreeTile]) -> QuadtreeTile? {
        guard let parent = parent else {
            return findLevelZeroTile(levelZeroTiles: levelZeroTiles, x: x, y: y - 1)
        }

        if parent.southwestChild == self {
            return parent.northwestChild
        } else if parent.southeastChild == self {
            return parent.northeastChild
        }

        guard let northOfParent = parent.findTileToNorth(levelZeroTiles: levelZeroTiles) else {
            return nil
        }
        if parent.northwestChild == self {
            return northOfParent.southwestChild
        }
        return northOfParent.southeastChild
    }
}

