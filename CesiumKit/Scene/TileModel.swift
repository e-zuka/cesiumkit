//
//  TileModel.swift
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2019/11/26.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

class TileModel {
    let identifier: String
    let boundingVolume: BoundingSphere
    let orientedBoundingBox: OrientedBoundingBox?
    var uniform: TileUniformStruct
    let applyFog: Bool
    let dayTextures: [Texture]
    let quantized: Bool
    var vertexArray: VertexArray

    required init(id: String, uniform: TileUniformStruct, fog: Bool, textures: [Texture], vertexArray: VertexArray, boundingVolume: BoundingSphere, orientedBoundingBox: OrientedBoundingBox?, quantized: Bool) {
        self.identifier = id
        self.uniform = uniform
        self.applyFog = fog
        self.dayTextures = textures
        self.vertexArray = vertexArray
        self.boundingVolume = boundingVolume
        self.orientedBoundingBox = orientedBoundingBox
        self.quantized = quantized
    }
    
    deinit {
        //self.vertexArray = nil
        //self.uniform = nil
    }
}
