//
//  deleteTerrainData.swift
//  CesiumKitTests
//
//  Created by 飯塚淳 on 2018/09/14.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation

extension Scene {
    public func deleteCachedTerrainData(_ navIdentifier: String?) -> Bool {
        return context.coreDataController.deleteData(of: navIdentifier)
    }
    public func deleteCachedTerrainData(_ navIdentifier: String?, callback: @escaping (_ success: Bool) -> Void) {
        context.coreDataController.deleteData(of: navIdentifier, callback: callback)
    }
}
