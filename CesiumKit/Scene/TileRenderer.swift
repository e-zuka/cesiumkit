//
//  TileRenderer.swift
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2019/11/26.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

enum TileBufferType: String {
    case tileUniform
    case prevTileUniform
    case prevAutoUniform
    case prevFrustumUniform
}
enum ArgumentBufferType: String {
    case quantizedMesh
    case mesh
    case material
    case tile
    case icb
}
enum ArgumentEncoderType: String {
    case compute
    case tile
    case quantizedMesh
    case mesh
    case material
}
enum TilePipelineState: CaseIterable {
    case rawDirect
    case rawDirectShadow
    case quantizedDirect
    case quantizedDirectShadow
    case rawICB
    case rawICBShadow
    case quantizedICB
    case quantizedICBShadow
}

class TileRenderer {
    var pipelineStateCollection: [TilePipelineState: MTLRenderPipelineState] = [:]

    let depthStencilState: MTLDepthStencilState
    
    var width: Int
    var height: Int
    let frameState: FrameState
    let enableICB: Bool
    let shadow: Shadow
    
    var castShadow: Bool = true
    
    var bufferSyncState: BufferSyncState = .zero {
        willSet {
            if bufferSyncState != newValue {
                tileOffset = 0
            }
        }
        didSet {
            if bufferSyncState != oldValue {
                tileIndexes = [:]
            }
        }
    }
    var autoUnifromBuffer: MTLBuffer? = nil
    var frustumUniformBuffer: MTLBuffer? = nil
    let maxTile = 10_000
    
    var resourceList: [MTLResource] = []
    var textureList: [MTLTexture] = []
    
    typealias BufferDict = [TileBufferType: MTLBuffer]
    var bufferPool: [BufferDict] = []
    var currentBuffer: BufferDict {
        bufferPool[bufferSyncState.rawValue]
    }
    var previousBuffer: BufferDict {
        let prevInflight = bufferSyncState.rawValue - 1 < 0 ? 2 : bufferSyncState.rawValue - 1
        return bufferPool[prevInflight]
    }
    
    var argumentEncoder: [ArgumentEncoderType: MTLArgumentEncoder] = [:]
    typealias ArgumentBufferDict = [ArgumentBufferType: MTLBuffer]
    var argumentBufferPool: [ArgumentBufferDict] = []
    var currentArgumentBuffer: ArgumentBufferDict {
        argumentBufferPool[bufferSyncState.rawValue]
    }
    var icbPool: [MTLIndirectCommandBuffer] = []
    var icb: MTLIndirectCommandBuffer {
        icbPool[bufferSyncState.rawValue]
    }
    var computePipelineState: MTLComputePipelineState? = nil
    
    var singleTileUniformBufferPool: [[MTLBuffer]] = []
    var singleTileUniformBuffer: [MTLBuffer] {
        singleTileUniformBufferPool[bufferSyncState.rawValue]
    }
    
    var tileOffset: Int = 0
    typealias TileIndexMap = [String: Int] // tile_x_y_level: 0
    var _tileIndexes: [TileIndexMap] = [[:], [:], [:]]
    var tileIndexes: TileIndexMap {
        get {
            _tileIndexes[bufferSyncState.rawValue]
        }
        set {
            _tileIndexes[bufferSyncState.rawValue] = newValue
        }
    }
    var prevTileIndexes: TileIndexMap {
        let prevInflight = bufferSyncState.rawValue - 1 < 0 ? 2 : bufferSyncState.rawValue - 1
        return _tileIndexes[prevInflight]
    }

    required init(device: MTLDevice, viewport: (width: Int, height: Int), shadow: Shadow, frameState: FrameState, enableICB: Bool = true) {
        self.width = viewport.width
        self.height = viewport.height
        self.frameState = frameState
        self.shadow = shadow
        self.enableICB = enableICB
        
        let depthStencilDescriptor = MTLDepthStencilDescriptor()

        depthStencilDescriptor.depthCompareFunction = .less
        depthStencilDescriptor.isDepthWriteEnabled = true
        depthStencilState = device.makeDepthStencilState(descriptor: depthStencilDescriptor)!
        
        guard let bundle = Bundle(identifier: "com.testtoast.CesiumKit") else {
            logPrint(.error, "Could not find CesiumKit bundle in executable")
            return
        }
        guard let libraryPath = bundle.url(forResource: "default", withExtension: "metallib")?.path else {
            logPrint(.error, "Could not find shader source from bundle")
            return
        }
        let shaderLibrary: MTLLibrary
        do {
            shaderLibrary = try device.makeLibrary(filepath: libraryPath)
        } catch let error as NSError {
            logPrint(.error, "Could not generate library from compiled shader lib: \(error.localizedDescription)")
            return
        }
        
        let quantizedVertexDescriptor = MTLVertexDescriptor()
        quantizedVertexDescriptor.layouts[3].stepFunction = .perVertex
        quantizedVertexDescriptor.layouts[3].stepRate = 1
        quantizedVertexDescriptor.layouts[3].stride = MemoryLayout<SIMD4<Float>>.stride
        quantizedVertexDescriptor.attributes[0].bufferIndex = 3
        quantizedVertexDescriptor.attributes[0].format = .float4
        quantizedVertexDescriptor.attributes[0].offset = 0
        
        let rawVertexDescriptor = MTLVertexDescriptor()
        rawVertexDescriptor.layouts[3].stepFunction = .perVertex
        rawVertexDescriptor.layouts[3].stepRate = 1
        rawVertexDescriptor.layouts[3].stride = 28//MemoryLayout<SIMD4<Float>>.stride * 2
        rawVertexDescriptor.attributes[0].bufferIndex = 3
        rawVertexDescriptor.attributes[0].format = .float4
        rawVertexDescriptor.attributes[0].offset = 0
        rawVertexDescriptor.attributes[1].bufferIndex = 3
        rawVertexDescriptor.attributes[1].format = .float3
        rawVertexDescriptor.attributes[1].offset = MemoryLayout<SIMD4<Float>>.size

        let vertexFunction = shaderLibrary.makeFunction(name: "tileFogVertex")
        let quantizedVertexFunction = shaderLibrary.makeFunction(name: "tileQuantizedFogVertex")

        //let fragmentFunction = shaderLibrary.makeFunction(name: "tileFragment")
        let fragmentFunction = shaderLibrary.makeFunction(name: "debug_fragment")
        let directFragmentFunction = shaderLibrary.makeFunction(name: "tileDirectFragment")
        
        func createPipelineState(_ pipelineMode: TilePipelineState) {
            let pipelineDescriptor = MTLRenderPipelineDescriptor()
            pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
            pipelineDescriptor.depthAttachmentPixelFormat = .depth32Float_stencil8
            pipelineDescriptor.stencilAttachmentPixelFormat = .depth32Float_stencil8
            
            func setAdditionalColor() {
                pipelineDescriptor.colorAttachments[1].pixelFormat = .rgba32Float
                pipelineDescriptor.colorAttachments[2].pixelFormat = .rgba32Float
                pipelineDescriptor.colorAttachments[3].pixelFormat = .rg16Float
                pipelineDescriptor.colorAttachments[4].pixelFormat = .rgba16Float
            }
            
            switch pipelineMode {
            case .rawDirect:
                pipelineDescriptor.vertexFunction = vertexFunction
                pipelineDescriptor.fragmentFunction = directFragmentFunction
                pipelineDescriptor.vertexDescriptor = rawVertexDescriptor
                pipelineDescriptor.label = "DirectRawPSO"
            case .quantizedDirect:
                pipelineDescriptor.vertexFunction = quantizedVertexFunction
                pipelineDescriptor.fragmentFunction = directFragmentFunction
                pipelineDescriptor.vertexDescriptor = quantizedVertexDescriptor
                pipelineDescriptor.label = "DirectQuantizedPSO"
            case .rawICB:
                pipelineDescriptor.vertexFunction = vertexFunction
                pipelineDescriptor.fragmentFunction = fragmentFunction
                pipelineDescriptor.vertexDescriptor = rawVertexDescriptor
                pipelineDescriptor.supportIndirectCommandBuffers = true
                pipelineDescriptor.label = "ICBRawPSO"
            case .quantizedICB:
                pipelineDescriptor.vertexFunction = quantizedVertexFunction
                pipelineDescriptor.fragmentFunction = fragmentFunction
                pipelineDescriptor.vertexDescriptor = quantizedVertexDescriptor
                pipelineDescriptor.supportIndirectCommandBuffers = true
                pipelineDescriptor.label = "ICBQuantizedPSO"
            case .rawDirectShadow:
                setAdditionalColor()
                pipelineDescriptor.vertexFunction = vertexFunction
                pipelineDescriptor.fragmentFunction = directFragmentFunction
                pipelineDescriptor.vertexDescriptor = rawVertexDescriptor
                pipelineDescriptor.label = "DirectRawShadowPSO"
            case .quantizedDirectShadow:
                setAdditionalColor()
                pipelineDescriptor.vertexFunction = quantizedVertexFunction
                pipelineDescriptor.fragmentFunction = directFragmentFunction
                pipelineDescriptor.vertexDescriptor = quantizedVertexDescriptor
                pipelineDescriptor.label = "DirectQuantizedShadowPSO"
            case .rawICBShadow:
                setAdditionalColor()
                pipelineDescriptor.vertexFunction = vertexFunction
                pipelineDescriptor.fragmentFunction = fragmentFunction
                pipelineDescriptor.vertexDescriptor = rawVertexDescriptor
                pipelineDescriptor.supportIndirectCommandBuffers = true
                pipelineDescriptor.label = "ICBRawShadowPSO"
            case .quantizedICBShadow:
                setAdditionalColor()
                pipelineDescriptor.vertexFunction = quantizedVertexFunction
                pipelineDescriptor.fragmentFunction = fragmentFunction
                pipelineDescriptor.vertexDescriptor = quantizedVertexDescriptor
                pipelineDescriptor.supportIndirectCommandBuffers = true
                pipelineDescriptor.label = "ICBQuantizedShadowPSO"
            }
            do {
                pipelineStateCollection[pipelineMode] = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
            } catch {
                assertionFailure("failed to create pipeline of \(pipelineMode)")
            }
        }
        for tileState in TilePipelineState.allCases {
            createPipelineState(tileState)
        }
        
        prepareBuffer(device: device)
        
        if enableICB {
            prepareArgumentEncoder(device: device, library: shaderLibrary)
            prepareArgumentBuffer(device: device)
        }
    }
    private func prepareBuffer(device: MTLDevice) {
        for _ in 0..<3 {
            var bufferDict: BufferDict = [:]
            let tileUniformBuffer = device.makeBuffer(length: MemoryLayout<TileUniformStruct>.stride * maxTile, options: .storageModeShared)!
            let prevTileUniformBuffer = device.makeBuffer(length: MemoryLayout<TileUniformStruct>.stride * maxTile, options: .storageModeShared)!
            bufferDict[.tileUniform] = tileUniformBuffer
            bufferDict[.prevTileUniform] = prevTileUniformBuffer
            
            let prevAutoUniformBuffer = device.makeBuffer(length: MemoryLayout<AutomaticUniformBufferLayout>.size, options: .storageModeShared)!
            bufferDict[.prevAutoUniform] = prevAutoUniformBuffer
            
            let prevFrustumUniformBuffer = device.makeBuffer(length: MemoryLayout<FrustumUniformBufferLayout>.stride * 5, options: .storageModeShared)!
            bufferDict[.prevFrustumUniform] = prevFrustumUniformBuffer
            
            bufferPool.append(bufferDict)
            
            var singleBufferPool: [MTLBuffer] = []
            for _ in 0..<maxTile {
                let singleBuffer = device.makeBuffer(length: MemoryLayout<TileUniformStruct>.stride, options: .storageModeShared)!
                singleBufferPool.append(singleBuffer)
            }
            singleTileUniformBufferPool.append(singleBufferPool)
        }
    }
    private func prepareArgumentEncoder(device: MTLDevice, library: MTLLibrary) {
        let mainComputeFunction = library.makeFunction(name: "icb_tile")!
        argumentEncoder[.compute] = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.commandBufferContainer.rawValue)
        argumentEncoder[.tile] = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.tileObject.rawValue)
        argumentEncoder[.quantizedMesh] = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.quantizedMesh.rawValue)
        argumentEncoder[.mesh] = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.mesh.rawValue)
        argumentEncoder[.material] = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.material.rawValue)

        do {
            computePipelineState = try device.makeComputePipelineState(function: mainComputeFunction)
        } catch {
            assertionFailure("failed to make ICB compute PSO")
        }
    }
    private func prepareArgumentBuffer(device: MTLDevice) {
        let icbDescriptor = MTLIndirectCommandBufferDescriptor()
        icbDescriptor.commandTypes = .drawIndexed
        icbDescriptor.inheritPipelineState = false
        icbDescriptor.inheritBuffers = false
        icbDescriptor.maxVertexBufferBindCount = 9
        icbDescriptor.maxFragmentBufferBindCount = 4
        
        for _ in 0..<3 {
            var bufferDict: ArgumentBufferDict = [:]
            bufferDict[.tile] = device.makeBuffer(length: argumentEncoder[.tile]!.encodedLength * maxTile, options: .storageModeShared)!
            bufferDict[.quantizedMesh] = device.makeBuffer(length: argumentEncoder[.quantizedMesh]!.encodedLength * maxTile, options: .storageModeShared)!
            bufferDict[.mesh] = device.makeBuffer(length: argumentEncoder[.mesh]!.encodedLength * maxTile, options: .storageModeShared)!
            bufferDict[.material] = device.makeBuffer(length: argumentEncoder[.material]!.encodedLength * maxTile, options: .storageModeShared)!
            bufferDict[.icb] = device.makeBuffer(length: argumentEncoder[.compute]!.encodedLength, options: .storageModeShared)!
            argumentBufferPool.append(bufferDict)
            
            guard let icb = device.makeIndirectCommandBuffer(descriptor: icbDescriptor, maxCommandCount: maxTile, options: .storageModeShared) else {
                assertionFailure("Error making icb")
                continue
            }
            icbPool.append(icb)
        }
    }
    func resizeViewport(width: Int, height: Int) {
        self.width = width
        self.height = height
    }
    func pipelineState(of tile: TileModel) -> MTLRenderPipelineState {
        let pipelineMode: TilePipelineState
        if tile.quantized {
            if enableICB {
                if castShadow {
                    pipelineMode = .quantizedICBShadow
                } else {
                    pipelineMode = .quantizedICB
                }
            } else {
                if castShadow {
                    pipelineMode = .quantizedDirectShadow
                } else {
                    pipelineMode = .quantizedDirect
                }
            }
        } else {
            if enableICB {
                if castShadow {
                    pipelineMode = .rawICBShadow
                } else {
                    pipelineMode = .rawICB
                }
            } else {
                if castShadow {
                    pipelineMode = .rawDirectShadow
                } else {
                    pipelineMode = .rawDirect
                }
            }
        }
        return pipelineStateCollection[pipelineMode]!
    }
    
    func renderTile(_ tiles: [TileModel], passDescriptor: MTLRenderPassDescriptor, with commandBuffer: MTLCommandBuffer, bufferSyncState: BufferSyncState, frustumIndex: Int) {
        guard let _ = autoUnifromBuffer, let _ = frustumUniformBuffer else { return }
        defer {
            autoUnifromBuffer = nil
            frustumUniformBuffer = nil
        }
        self.bufferSyncState = bufferSyncState
        
        if enableICB {
            encodeICB(tiles, commandBuffer: commandBuffer, frustumIndex: frustumIndex)
        }
        
        let encoder: MTLRenderCommandEncoder
        if castShadow {
            encoder = shadow.shadowRayRenderEncoder(with: commandBuffer, passDescriptor: passDescriptor, clear: frustumIndex == 0)
        } else {
            encoder = commandBuffer.makeRenderCommandEncoder(descriptor: passDescriptor)!
        }
        encoder.setCullMode(.back)
        encoder.setDepthStencilState(depthStencilState)
        encoder.setFrontFacing(.counterClockwise)
        encoder.setViewport(MTLViewport(originX: 0, originY: 0, width: Double(width), height: Double(height), znear: 0.0, zfar: 1.0))
        
        if enableICB {
            encoder.pushDebugGroup("TileRendererICB\(frustumIndex)")
            renderTileICB(tileCount: tiles.count, with: encoder)
        } else {
            encoder.pushDebugGroup("TileRendererNoAB\(frustumIndex)")
            renderTileDirect(tiles, with: encoder, frustumIndex: frustumIndex)
        }
    }
    
    func encodeICB(_ tiles: [TileModel], commandBuffer: MTLCommandBuffer, frustumIndex: Int) {
        let tileCount = tiles.count
        func resetICB() {
            // reset icb
            guard let resetBlitEncoder = commandBuffer.makeBlitCommandEncoder() else { return }
            resetBlitEncoder.label = "Reset ICB Blit Encoder"
            resetBlitEncoder.resetCommandsInBuffer(icb, range: tileOffset..<(tileOffset + tileCount))
            resetBlitEncoder.endEncoding()
        }
        
        guard let computePipelineState = computePipelineState else { return }
        guard let autoUniform = autoUnifromBuffer, let frustumUniform = frustumUniformBuffer else { return }
        let singleBufferArray = singleTileUniformBuffer
        
        resetICB()
        
        // compute encoder
        guard let computeEncoder = commandBuffer.makeComputeCommandEncoder() else { return }
        computeEncoder.label = "Render command encoder"
        
        let tileArgumentBuffer = currentArgumentBuffer[.tile]!
        let qMeshArgumentBuffer = currentArgumentBuffer[.quantizedMesh]!
        let meshArgmentBuffer = currentArgumentBuffer[.mesh]!
        let materialArgumentBuffer = currentArgumentBuffer[.material]!
        let icbArgumentBuffer = currentArgumentBuffer[.icb]!

        // tile uniform buffer for next frame
        let tileUniformBuffer = currentBuffer[.tileUniform]!
        
        // previous buffer for motion vector
        let prevFrustumUniformBuffer = previousBuffer[.prevFrustumUniform]!
        let prevTileUniformBuffer = previousBuffer[.prevTileUniform]!
        
        // set current frustumUniform to use at next frame
        let frustumUniformBufferForNextFrame = currentBuffer[.prevFrustumUniform]!
        frustumUniformBufferForNextFrame.copy(from: frustumUniform, length: MemoryLayout<FrustumUniformBufferLayout>.size, sourceOffset: 0, targetOffset: MemoryLayout<FrustumUniformBufferLayout>.stride * frustumIndex)
        
        resourceList = [tileArgumentBuffer, qMeshArgumentBuffer, meshArgmentBuffer, materialArgumentBuffer, icbArgumentBuffer, autoUniform, frustumUniform, prevFrustumUniformBuffer, prevTileUniformBuffer]
        
        for (index, tile) in tiles.enumerated() {
            // assert vertex data
            let vertexArray = tile.vertexArray
            guard let _ = vertexArray.attributes[0].buffer?.metalBuffer,
                let indexBuffer = vertexArray.indexBuffer?.metalBuffer
                else {
                assertionFailure("invalid command")
                continue
            }
            // set current tile uniform for next frame use
            let tileUniformBufferOffset = MemoryLayout<TileUniformStruct>.stride * (tileOffset + index)
            tileUniformBuffer.write(from: &tile.uniform, length: MemoryLayout<TileUniformStruct>.size, offset: tileUniformBufferOffset)
            
            let singleBuffer = singleBufferArray[tileOffset + index]
            singleBuffer.write(from: &tile.uniform, length: MemoryLayout<TileUniformStruct>.size)
            
            // tileIndex for motion vector
            tileIndexes[tile.identifier] = tileOffset + index
            
            resourceList.append(singleBuffer)
            resourceList.append(indexBuffer)
            textureList.append(contentsOf: tile.dayTextures.map{ $0.metalTexture })

            argumentEncoder[.tile]!.setArgumentBuffer(tileArgumentBuffer, startOffset: 0, arrayElement: tileOffset + index)
            argumentEncoder[.quantizedMesh]!.setArgumentBuffer(qMeshArgumentBuffer, startOffset: 0, arrayElement: tileOffset + index)
            argumentEncoder[.mesh]!.setArgumentBuffer(meshArgmentBuffer, startOffset: 0, arrayElement: tileOffset + index)
            argumentEncoder[.material]!.setArgumentBuffer(materialArgumentBuffer, startOffset: 0, arrayElement: tileOffset + index)
            
            argumentEncoder[.tile]!.setBuffer(singleBuffer, offset: 0, index: CesiumTileBufferIndex.tileUniform.rawValue)
            /*
            var attributeCount: UInt16 = 0
            for attribute in vertexArray.attributes {
                if let _ = attribute.buffer {
                    attributeCount += 1
                }
            }
            */
            let attributeCount: UInt16 = tile.quantized ? 1 : 2
            argumentEncoder[.tile]!.constantData(at: CesiumTileBufferIndex.vertexAttributeCount.rawValue).storeBytes(of: UInt16(attributeCount), as: UInt16.self)
            argumentEncoder[.tile]!.constantData(at: CesiumTileBufferIndex.fog.rawValue).storeBytes(of: tile.applyFog, as: Bool.self)
            let previousTileIndex = prevTileIndex(of: tile.identifier)
            argumentEncoder[.tile]!.constantData(at: CesiumTileBufferIndex.previousIndex.rawValue).storeBytes(of: previousTileIndex, as: Int32.self)

            let _meshArgumentEncoder = attributeCount == 1 ? argumentEncoder[.quantizedMesh]! : argumentEncoder[.mesh]!
            for attribute in vertexArray.attributes {
                if let buffer = attribute.buffer {
                    resourceList.append(buffer.metalBuffer)
                    _meshArgumentEncoder.setBuffer(buffer.metalBuffer, offset: 0, index: attribute.bufferIndex)
                }
            }
            _meshArgumentEncoder.setBuffer(indexBuffer, offset: 0, index: CesiumMeshBufferIndex.indexBuffer.rawValue)
            _meshArgumentEncoder.constantData(at: CesiumMeshBufferIndex.indexCount.rawValue).storeBytes(of: UInt16(vertexArray.indexCount!), as: UInt16.self)
            
            argumentEncoder[.material]!.constantData(at: CesiumMaterialBufferIndex.textureCount.rawValue).storeBytes(of: UInt16(tile.dayTextures.count), as: UInt16.self)
            for (j, texture) in tile.dayTextures.enumerated() {
                argumentEncoder[.material]!.setTexture(texture.metalTexture, index: CesiumMaterialBufferIndex.texture_0.rawValue + j)
                argumentEncoder[.material]!.setSamplerState(texture.sampler.state, index: CesiumMaterialBufferIndex.sampler_0.rawValue + j)
            }
            if castShadow {
                //textureList.append(shadow.randomTexture)
                argumentEncoder[.material]!.setTexture(shadow.randomTexture, index: CesiumMaterialBufferIndex.randomTexture.rawValue)
            }
            argumentEncoder[.material]!.setRenderPipelineState(pipelineState(of: tile), index: CesiumMaterialBufferIndex.pipelineState.rawValue)
        }
        // set tileUniform to use at next frame
        let tileUniformBufferForNextFrame = currentBuffer[.prevTileUniform]!
        let tileUniformStride = MemoryLayout<TileUniformStruct>.stride
        tileUniformBufferForNextFrame.copy(from: tileUniformBuffer, length: tileUniformStride * tiles.count, sourceOffset: tileUniformStride * tileOffset, targetOffset: tileUniformStride * tileOffset)
        
        argumentEncoder[.compute]!.setArgumentBuffer(icbArgumentBuffer, offset: 0)
        argumentEncoder[.compute]!.setIndirectCommandBuffer(icb, index: CesiumArgumentBufferID.commandBuffer.rawValue)

        computeEncoder.setComputePipelineState(computePipelineState)
        
        computeEncoder.setBuffer(autoUniform, offset: 0, index: CesiumKernelBufferIndex.automaticUniform.rawValue)
        computeEncoder.setBuffer(frustumUniform, offset: 0, index: CesiumKernelBufferIndex.frustumUniform.rawValue)
        computeEncoder.setBuffer(tileArgumentBuffer, offset: 0, index: CesiumKernelBufferIndex.tileObject.rawValue)
        computeEncoder.setBuffer(icbArgumentBuffer, offset: 0, index: CesiumKernelBufferIndex.commandBufferContainer.rawValue)
        var _tileCount = UInt16(tileCount)
        computeEncoder.setBytes(&_tileCount, length: 2, index: CesiumKernelBufferIndex.tileCount.rawValue)
        computeEncoder.setBuffer(qMeshArgumentBuffer, offset: 0, index: CesiumKernelBufferIndex.quantizedMesh.rawValue)
        computeEncoder.setBuffer(meshArgmentBuffer, offset: 0, index: CesiumKernelBufferIndex.mesh.rawValue)
        computeEncoder.setBuffer(materialArgumentBuffer, offset: 0, index: CesiumKernelBufferIndex.material.rawValue)
        var _tileOffset = UInt16(tileOffset)
        computeEncoder.setBytes(&_tileOffset, length: 2, index: CesiumKernelBufferIndex.tileOffset.rawValue)
        computeEncoder.setBuffer(prevFrustumUniformBuffer, offset: MemoryLayout<FrustumUniformBufferLayout>.stride * frustumIndex, index: CesiumKernelBufferIndex.previousFrustum.rawValue)
        computeEncoder.setBuffer(prevTileUniformBuffer, offset: 0, index: CesiumKernelBufferIndex.previousTileUniform.rawValue)

        for res in resourceList {
            computeEncoder.useResource(res, usage: .read)
        }
        for tex in textureList {
            computeEncoder.useResource(tex, usage: .read)
        }
        computeEncoder.useResource(icb, usage: .write)
        let w = computePipelineState.threadExecutionWidth
        computeEncoder.dispatchThreads(MTLSize(width: tileCount, height: 1, depth: 1), threadsPerThreadgroup: MTLSize(width: w, height: 1, depth: 1))
        computeEncoder.endEncoding()
    }
    
    func renderTileICB(tileCount: Int, with encoder: MTLRenderCommandEncoder) {
        guard let autoUniform = autoUnifromBuffer, let frustumUniform = frustumUniformBuffer else { return }

        encoder.label = "Main Render Encoder"
        encoder.pushDebugGroup("useResource")
        encoder.useResource(autoUniform, usage: .read)
        encoder.useResource(frustumUniform, usage: .read)
        for res in resourceList {
            encoder.useResource(res, usage: .read)
        }
        for tex in textureList {
            encoder.useResource(tex, usage: .sample)
        }
        if castShadow {
            encoder.useResource(shadow.randomTexture, usage: .read)
        }
        encoder.popDebugGroup()
        // Draw everything in the indirect command buffer
        encoder.executeCommandsInBuffer(icb, range: tileOffset..<(tileOffset + tileCount))
        
        encoder.endEncoding()
        
        tileOffset += tileCount
        
        // reset resource list
        resourceList.removeAll()
        textureList.removeAll()
    }
    
    func renderTileDirect(_ tiles: [TileModel], with encoder: MTLRenderCommandEncoder, frustumIndex: Int) {
        guard let autoUniform = autoUnifromBuffer, let frustumUniform = frustumUniformBuffer else { return }
        let tileUniformBuffer = currentBuffer[.tileUniform]!
        
        let singleBufferArray = singleTileUniformBuffer
        
        // previous buffer for motion vector
        let prevFrustumUniformBuffer = previousBuffer[.prevFrustumUniform]!
        let prevTileUniformBuffer = previousBuffer[.prevTileUniform]!
        
        // set current frustumUniform to use at next frame
        let frustumUniformBufferForNextFrame = currentBuffer[.prevFrustumUniform]!
        frustumUniformBufferForNextFrame.copy(from: frustumUniform, length: MemoryLayout<FrustumUniformBufferLayout>.size, sourceOffset: 0, targetOffset: MemoryLayout<FrustumUniformBufferLayout>.stride * frustumIndex)

        for (index, tile) in tiles.enumerated() {

            // assert vertex data
            let vertexArray = tile.vertexArray
            guard let _ = vertexArray.attributes[0].buffer?.metalBuffer,
                let indexBuffer = vertexArray.indexBuffer?.metalBuffer,
                let indexType = vertexArray.indexBuffer?.componentDatatype.toMTLIndexType(),
                let indexCount = vertexArray.indexCount
                else {
                assertionFailure("invalid command")
                continue
            }
            // uniform setup
            var tileUniform = tile.uniform
            let tileUniformBufferOffset = MemoryLayout<TileUniformStruct>.stride * (tileOffset + index)
            tileUniformBuffer.write(from: &tileUniform, length: MemoryLayout<TileUniformStruct>.size, offset: tileUniformBufferOffset)
            
            let singleBuffer = singleBufferArray[tileOffset + index]
            singleBuffer.write(from: &tileUniform, length: MemoryLayout<TileUniformStruct>.size)

            // tileIndex for motion vector
            tileIndexes[tile.identifier] = tileOffset + index

            // set pipeline state
            encoder.pushDebugGroup("\(tile.identifier)")
            encoder.setRenderPipelineState(pipelineState(of: tile))
            
            // vertex buffer
            encoder.setVertexBuffer(autoUniform, offset: 0, index: 0)
            encoder.setVertexBuffer(frustumUniform, offset: 0, index: 1)
            //encoder.setVertexBuffer(tileUniformBuffer, offset: tileUniformBufferOffset, index: 2)
            encoder.setVertexBuffer(singleBuffer, offset: 0, index: 2)
            for attribute in vertexArray.attributes {
                if let buffer = attribute.buffer {
                    encoder.setVertexBuffer(buffer.metalBuffer, offset: 0, index: attribute.bufferIndex)
                }
            }
            var applyFog = tile.applyFog
            encoder.setVertexBytes(&applyFog, length: 1, index: 5)
            
            //if frameState.frameNumber > 0 { // skip for first frame
                encoder.setVertexBuffer(prevFrustumUniformBuffer, offset: MemoryLayout<FrustumUniformBufferLayout>.stride * frustumIndex, index: 6)
                encoder.setVertexBuffer(prevTileUniformBuffer, offset: 0, index: 7)
            //}
            var previousTileIndex = prevTileIndex(of: tile.identifier)
            encoder.setVertexBytes(&previousTileIndex, length: 4, index: 8)

            
            // fragment buffer
            encoder.setFragmentBuffer(autoUniform, offset: 0, index: 0)
            encoder.setFragmentBuffer(frustumUniform, offset: 0, index: 1)
            //encoder.setFragmentBuffer(tileUniformBuffer, offset: tileUniformBufferOffset, index: 2)
            encoder.setFragmentBuffer(singleBuffer, offset: 0, index: 2)
            var textureCount = UInt(tile.dayTextures.count)
            encoder.setFragmentBytes(&textureCount, length: 4, index: 3)
            encoder.setFragmentBytes(&applyFog, length: 1, index: 4)
            
            for (j, tex) in tile.dayTextures.enumerated() {
                encoder.setFragmentTexture(tex.metalTexture, index: j)
                encoder.setFragmentSamplerState(tex.sampler.state, index: j)
            }
            if castShadow {
                encoder.setFragmentTexture(shadow.randomTexture, index: 3)
            }
            encoder.drawIndexedPrimitives(type: .triangle, indexCount: indexCount, indexType: indexType, indexBuffer: indexBuffer, indexBufferOffset: 0, instanceCount: 1, baseVertex: 0, baseInstance: 0)
            encoder.popDebugGroup()
        }
        encoder.popDebugGroup()
        encoder.endEncoding()
        
        // set tileUniform to use at next frame
        let tileUniformBufferForNextFrame = currentBuffer[.prevTileUniform]!
        let tileUniformStride = MemoryLayout<TileUniformStruct>.stride
        tileUniformBufferForNextFrame.copy(from: tileUniformBuffer, length: tileUniformStride * tiles.count, sourceOffset: tileUniformStride * tileOffset, targetOffset: tileUniformStride * tileOffset)
        
        tileOffset += tiles.count
    }
    
    // MARK: - Tile Indexing
    func prevTileIndex(of identifier: String) -> Int32 {
        let tileMap = prevTileIndexes
        return Int32(tileMap[identifier] ?? 1000)
    }
}
