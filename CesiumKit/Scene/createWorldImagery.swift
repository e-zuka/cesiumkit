//
//  createWorldImagery.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2018/07/02.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation
import Hydra

enum IonWorldImageryStyle : Int {
    case AERIAL = 2
    case AERIAL_WITH_LABELS = 3
    case ROAD = 4
}

extension Scene {
    func createWorldImagery(options: [String: Int]) -> IonImageryProvider {
        let _options: [String: Int] = (options.count > 0) ? options : [:]
        let style = (options["style"] != nil) ? _options["style"]! : IonWorldImageryStyle.AERIAL.rawValue
        return IonImageryProvider(assetId: style)!
    }
}
