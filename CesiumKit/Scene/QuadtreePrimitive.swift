//
//  QuadTreePrimitive.swift
//  CesiumKit
//
//  Created by Ryan Walklin on 16/08/14.
//  Copyright (c) 2014 Test Toast. All rights reserved.
//

import Foundation

/**
* Renders massive sets of data by utilizing level-of-detail and culling.  The globe surface is divided into
* a quadtree of tiles with large, low-detail tiles at the root and small, high-detail tiles at the leaves.
* The set of tiles to render is selected by projecting an estimate of the geometric error in a tile onto
* the screen to estimate screen-space error, in pixels, which must be below a user-specified threshold.
* The actual content of the tiles is arbitrary and is specified using a {@link QuadtreeTileProvider}.
*
* @alias QuadtreePrimitive
* @constructor
* @private
*
* @param {QuadtreeTileProvider} options.tileProvider The tile provider that loads, renders, and estimates
*        the distance to individual tiles.
* @param {Number} [options.maximumScreenSpaceError=2] The maximum screen-space error, in pixels, that is allowed.
*        A higher maximum error will render fewer tiles and improve performance, while a lower
*        value will improve visual quality.
* @param {Number} [options.tileCacheSize=100] The maximum number of tiles that will be retained in the tile cache.
*        Note that tiles will never be unloaded if they were used for rendering the last
*        frame, so the actual number of resident tiles may be higher.  The value of
*        this property will not affect visual quality.
*/

class QuadtreePrimitive {

    let tileProvider: GlobeSurfaceTileProvider//QuadtreeTileProvider
    
    fileprivate var _debug = (
        enableDebugOutput : true,
        
        maxDepth: 0,
        tilesVisited: 0,
        tilesCulled: 0,
        tilesRendered: 0,
        tilesWaitingForChildren: 0,
        
        lastMaxDepth: -1,
        lastTilesVisited: -1,
        lastTilesCulled: -1,
        lastTilesRendered: -1,
        lastTilesWaitingForChildren: -1,
        
        suspendLodUpdate: false
    )
    
    var tilesToRender: [QuadtreeTile] {
        get {
            return _tilesToRender
        }
    }
    
    fileprivate var _tilesToRender = [QuadtreeTile]()
    
    fileprivate var _tileTraversalQueue = Queue<QuadtreeTile>()
    
    //fileprivate var _tileLoadQueue = [QuadtreeTile]()
    var _tileLoadQueueHigh: [QuadtreeTile] = []
    var _tileLoadQueueMedium: [QuadtreeTile] = []
    var _tileLoadQueueLow: [QuadtreeTile] = []
    
    fileprivate var _tileReplacementQueue: TileReplacementQueue
    
    var _levelZeroTiles: [QuadtreeTile]? = nil
    fileprivate var _levelZeroTilesReady = false
    //fileprivate var _loadQueueTimeSlice = 0.04 // 5ms
    fileprivate var _loadQueueTimeSlice = 0.02 // 5ms
    fileprivate var _tilesInvalidated = false
    
    fileprivate var _addHeightCallbacks = [CallbackObject]()
    fileprivate var _removeHeightCallbacks = [CallbackObject]()
    
    fileprivate var _tilesToUpdateHeights = [QuadtreeTile]()
    fileprivate var _lastTileIndex = 0
    fileprivate var _updateHeightsTimeSlice = 0.02

    
    /**
    * Gets or sets the maximum screen-space error, in pixels, that is allowed.
    * A higher maximum error will render fewer tiles and improve performance, while a lower
    * value will improve visual quality.
    * @type {Number}
    * @default 2
    */
    var maximumScreenSpaceError: Double = 2.0
    
    /**
    * Gets or sets the maximum number of tiles that will be retained in the tile cache.
    * Note that tiles will never be unloaded if they were used for rendering the last
    * frame, so the actual number of resident tiles may be higher.  The value of
    * this property will not affect visual quality.
    * @type {Number}
    * @default 100
    */
    var tileCacheSize: Int
    
    fileprivate var _occluders: QuadtreeOccluders
    var occluders: QuadtreeOccluders {
        _occluders
    }
    
    let tileLoadProgressEvent = Event()
    
    fileprivate var _lastTileLoadQueueLength = 0
    
    //private var lastSelectedFrameNumber: Int = 0
    var tileSelectInterval: Int = 20
    
    var rootTraversalDetails: [TraversalDetails] = []
    var traversalQuadsByLevel = [TraversalQuadDetails](repeating: TraversalQuadDetails(), count: 30)
    var cameraPositionCartographic: Cartographic? = nil
    var cameraReferenceFrameOriginCartographic: Cartographic? = nil
    var lastSelectionFrameNumber: Int = 0
    /**
    * Gets or sets the number of loading descendant tiles that is considered "too many".
    * If a tile has too many loading descendants, that tile will be loaded and rendered before any of
    * its descendants are loaded and rendered. This means more feedback for the user that something
    * is happening at the cost of a longer overall load time. Setting this to 0 will cause each
    * tile level to be loaded successively, significantly increasing load time. Setting it to a large
    * number (e.g. 1000) will minimize the number of tiles that are loaded but tend to make
    * detail appear all at once after a long wait.
    * @type {Number}
    * @default 20
    */
    let loadingDescendantLimit = 20
    /**
    * Gets or sets a value indicating whether the ancestors of rendered tiles should be preloaded.
    * Setting this to true optimizes the zoom-out experience and provides more detail in
    * newly-exposed areas when panning. The down side is that it requires loading more tiles.
    * @type {Boolean}
    * @default true
    */
    let preloadAncestors = true
    /**
    * Gets or sets a value indicating whether the siblings of rendered tiles should be preloaded.
    * Setting this to true causes tiles with the same parent as a rendered tile to be loaded, even
    * if they are culled. Setting this to true may provide a better panning experience at the
    * cost of loading more tiles.
    * @type {Boolean}
    * @default false
    */
    let preloadSiblings = false
    var lastTileIndex = 0

    init (tileProvider: /*QuadtreeTileProvider*/GlobeSurfaceTileProvider, maximumScreenSpaceError: Double = 2.0, tileCacheSize: Int = 100) {
        
        self.maximumScreenSpaceError = maximumScreenSpaceError
        self.tileCacheSize = tileCacheSize
        
        assert(tileProvider.quadtree == nil, "A QuadtreeTileProvider can only be used with a single QuadtreePrimitive")
        
        self.tileProvider = tileProvider
        _tileReplacementQueue = TileReplacementQueue(tileProvider: self.tileProvider)
        
        let tilingScheme = tileProvider.tilingScheme
        let ellipsoid = tilingScheme.ellipsoid
        
        _occluders = QuadtreeOccluders(ellipsoid : ellipsoid)
        
        self.tileProvider.quadtree = self
    }
    
    /**
    * Invalidates and frees all the tiles in the quadtree.  The tiles must be reloaded
    * before they can be displayed.
    *
    * @memberof QuadtreePrimitive
    */
    func invalidateAllTiles() {
        _tilesInvalidated = true
    }
    func invalidateAllTiles(_ primitive: QuadtreePrimitive) {
        // Clear the replacement queue
        let replacementQueue = primitive._tileReplacementQueue
        
        replacementQueue.head = nil
        replacementQueue.tail = nil
        replacementQueue.count = 0
        
        clearTileLoadQueue()
        
        // Free and recreate the level zero tiles.
        if let levelZeroTiles = primitive._levelZeroTiles {
            for tile in levelZeroTiles {
                let customData = tile.customData
                for data in customData {
                    data.level = 0
                    // FIXME: addHeightCallbacks
                    primitive._addHeightCallbacks.append(data)
                }
                tile.freeResources()
            }
        }
        primitive._levelZeroTiles = nil
        primitive.tileProvider.cancelReprojections()
    }
    
    private func clearTileLoadQueue() {
        _debug.maxDepth = 0
        _debug.tilesVisited = 0
        _debug.tilesCulled = 0
        _debug.tilesRendered = 0
        _debug.tilesWaitingForChildren = 0
        
        _tileLoadQueueHigh = []
        _tileLoadQueueMedium = []
        _tileLoadQueueLow = []
    }
    /**
    * Invokes a specified function for each {@link QuadtreeTile} that is partially
    * or completely loaded.
    *
    * @param {Function} tileFunction The function to invoke for each loaded tile.  The
    *        function is passed a reference to the tile as its only parameter.
    */
    
    func forEachLoadedTile (_ tileFunction: (QuadtreeTile) -> ()) {
        var tile = _tileReplacementQueue.head
        while tile != nil {
            if tile!.state != .start {
                tileFunction(tile!)
            }
            tile = tile!.replacementNext
        }
    }
    
    /**
    * Invokes a specified function for each {@link QuadtreeTile} that was rendered
    * in the most recent frame.
    *
    * @param {Function} tileFunction The function to invoke for each rendered tile.  The
    *        function is passed a reference to the tile as its only parameter.
    */
    /*QuadtreePrimitive.prototype.forEachRenderedTile = function(tileFunction) {
    var tilesRendered = this._tilesToRender;
    for (var i = 0, len = tilesRendered.length; i < len; ++i) {
    tileFunction(tilesRendered[i]);
    }
    }*/
     

    /**
     * Calls the callback when a new tile is rendered that contains the given cartographic. The only parameter
     * is the cartesian position on the tile.
     *
     * @param {Cartographic} cartographic The cartographic position.
     * @param {Function} callback The function to be called when a new tile is loaded containing cartographic.
     * @returns {Function} The function to remove this callback from the quadtree.
     */
    func updateHeight (_ cartographic: Cartographic, callback: @escaping (Cartesian3) -> ()) -> (() -> ()) {
        let object = CallbackObject(callback: callback)
        
        object.removeFunc = {
            let addedCallbacks = self._addHeightCallbacks
            for (i, addedCallback) in addedCallbacks.enumerated() {
                if addedCallback == object {
                    self._addHeightCallbacks.remove(at: i)
                    break
                }
            }
            self._removeHeightCallbacks.append(object)
        }
        
        _addHeightCallbacks.append(object)
        return object.removeFunc
    }
    
    func update (_ frameState: inout FrameState) {
        tileProvider.update()
    }
    
    /**
    * Initializes values for a new render frame and prepare the tile load queue.
    * @private
    */
    func beginFrame (_ frameState: inout FrameState) {
        if !frameState.passes.render {
            return
        }
        
        if _tilesInvalidated {
            invalidateAllTiles(self)
            _tilesInvalidated = false
        }
    
        // Gets commands for any texture re-projections
        tileProvider.initialize(&frameState)
        
        clearTileLoadQueue()
        
        if _debug.suspendLodUpdate {
            return
        }
        
        _tileReplacementQueue.markStartOfRenderFrame()

        /* GlobeSurfaceTileNG
        if lastSelectedFrameNumber + tileSelectInterval < frameState.frameNumber {
            clearTileLoadQueue()
            _tileReplacementQueue.markStartOfRenderFrame()
        }
        */
    }

    func render(_ frameState: inout FrameState) {
        if frameState.passes.render {
            tileProvider.beginUpdate(frameState)
            
            /* GlobeSurfaceTileNG
            let interval = (UserDefaults.standard.bool(forKey: "maxTerrainCompute")) ? 0 : tileSelectInterval
            if lastSelectionFrameNumber == 0 || lastSelectionFrameNumber + interval < frameState.frameNumber {
                selectTilesForRendering(&frameState)
                lastSelectionFrameNumber = frameState.frameNumber
            }
            */
            selectTilesForRendering(&frameState)
            createRenderCommandsForSelectedTiles(&frameState)
            
            tileProvider.endUpdate(&frameState)
        }
        if frameState.passes.pick && _tilesToRender.count > 0 {
            // FIXME: pick
            //tileProvider.updateForPick(frameState)
        }
    }
    
    fileprivate (set) var debugDisplayString: String? = nil
    
    /**
    * Checks if the load queue length has changed since the last time we raised a queue change event - if so, raises
    * a new change event at the end of the render cycle.
    */
    private func updateTileLoadProgress(frameState: inout FrameState) {
        let currentLoadQueueLength = _tileLoadQueueHigh.count + _tileLoadQueueMedium.count + _tileLoadQueueLow.count
        
        if currentLoadQueueLength != _lastTileLoadQueueLength || _tilesInvalidated == true {
            frameState.afterRender.append({ self.tileLoadProgressEvent.raiseEvent() })
            _lastTileLoadQueueLength = currentLoadQueueLength
        }
        
        /* no debug processing
        if (_debug.suspendLodUpdate) {
            return
        }
        
        if _debug.enableDebugOutput {
            if _debug.tilesVisited != _debug.lastTilesVisited ||
                _debug.tilesRendered != _debug.lastTilesRendered ||
                _debug.tilesCulled != _debug.lastTilesCulled ||
                _debug.maxDepth != _debug.lastMaxDepth ||
                _debug.tilesWaitingForChildren != _debug.lastTilesWaitingForChildren {
                
                debugDisplayString = "Visited \(_debug.tilesVisited), Rendered: \(_debug.tilesRendered), Culled: \(_debug.tilesCulled), Max Depth: \(_debug.maxDepth), Waiting for children: \(_debug.tilesWaitingForChildren)"
                _debug.lastTilesVisited = _debug.tilesVisited
                _debug.lastTilesRendered = _debug.tilesRendered
                _debug.lastTilesCulled = _debug.tilesCulled
                _debug.lastMaxDepth = _debug.maxDepth
                _debug.lastTilesWaitingForChildren = _debug.tilesWaitingForChildren
            }
        }
        */
    }
    
    /**
    * Updates terrain heights.
    * @private
    */
    func endFrame (_ frameState: inout FrameState) {
        if !frameState.passes.render || frameState.mode == .morphing {
            // Only process the load queue for a single pass.
            // Don't process the load queue or update heights during the morph flights.
            return
        }
        
        /* moves to beginFrame
        if _tilesInvalidated == true {
            invalidateAllTiles(self)
        }
        */
    
        // Load/create resources for terrain and imagery. Prepare texture re-projections for the next frame.
        processTileLoadQueue(&frameState)
        //updateHeights(&frameState)
        updateHeights(frameState: frameState) // GlobeSufaceTileNG
        updateTileLoadProgress(frameState: &frameState)
        
        // _tilesInvalidated = false // moves to beginFrame
    }
    
    func selectTilesForRendering(_ frameState: inout FrameState) {
        if _debug.suspendLodUpdate {
            return
        }
        
        // Clear the render list.
        _tilesToRender.removeAll()

        // We can't render anything before the level zero tiles exist.
        if _levelZeroTiles == nil {
            if tileProvider.ready == true {
                let tilingScheme = tileProvider.tilingScheme
                _levelZeroTiles = QuadtreeTile.createLevelZeroTiles(tilingScheme)
                let numberOfRootTiles = _levelZeroTiles!.count
                if rootTraversalDetails.count < numberOfRootTiles {
                    rootTraversalDetails = [TraversalDetails](repeating: TraversalDetails(), count: numberOfRootTiles)
                }
            } else {
                // Nothing to do until the provider is ready.
                return
            }
        }
        
        _occluders.ellipsoid.cameraPosition = frameState.camera!._positionWC
        let occluders = _levelZeroTiles!.count > 1 ? _occluders : nil
        
        // Sort the level zero tiles by the distance from the center to the camera.
        // The level zero tiles aren't necessarily a nice neat quad, so we can't use the
        // quadtree ordering we use elsewhere in the tree
        let comparisonPoint = frameState.camera!.positionCartographic
        func compareDistanceToPoint(a: QuadtreeTile, b: QuadtreeTile) -> Bool {
            let centerA = a.rectangle.center
            let alon = centerA.longitude - comparisonPoint.longitude
            let alat = centerA.latitude - comparisonPoint.latitude
            
            let centerB = b.rectangle.center
            let blon = centerB.longitude - comparisonPoint.longitude
            let blat = centerB.latitude - comparisonPoint.latitude
            
            let aDist = pow(alon, 2) + pow(alat, 2)
            let bDist = pow(blon, 2) + pow(blat, 2)
            return (aDist - bDist) <= 0 //? true : false
            //return (alon * alon + alat * alat) - (blon * blon + blat * blat)
        }
        let levelZeroTiles = _levelZeroTiles!.sorted(by: compareDistanceToPoint(a:b:))
        
        var customDataAdded = _addHeightCallbacks
        var customDataRemoved = _removeHeightCallbacks
        let frameNumber = frameState.frameNumber
        if customDataAdded.count > 0 || customDataRemoved.count > 0 {
            for i in 0..<levelZeroTiles.count {
                let tile = levelZeroTiles[i]
                tile.updateCustomData(frameNumber, added: customDataAdded, removed: customDataRemoved)
            }
            customDataAdded.removeAll()
            _addHeightCallbacks.removeAll()
            customDataRemoved.removeAll()
            _removeHeightCallbacks.removeAll()
        }

        cameraPositionCartographic = frameState.camera!.positionCartographic
        let cameraFrameOrigin = frameState.camera!.transform.translation
        cameraReferenceFrameOriginCartographic = tileProvider.tilingScheme.ellipsoid.cartesianToCartographic(cameraFrameOrigin)
        
        // Traverse in depth-first, near-to-far order.
        for (i, tile) in levelZeroTiles.enumerated() {
            _tileReplacementQueue.markTileRendered(tile)
            if !tile.renderable {
                queueTileLoad(queue: &_tileLoadQueueHigh, tile: tile, frameState: frameState)
            } else {
                var ancestorMeetsSse: Bool = false
                visitIfVisible(tile: tile, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &rootTraversalDetails[i])
            }
        }
        
        lastSelectionFrameNumber = frameNumber
    }

    private func queueTileLoad(queue: inout [QuadtreeTile], tile: QuadtreeTile, frameState: FrameState) {
        if !tile.needsLoading {
            return
        }
        tile.loadPriority = tileProvider.computeTileLoadPriority(tile, frameState: frameState)
        queue.append(tile)
    }

    /**
    * Visits a tile for possible rendering. When we call this function with a tile:
    *
    *    * the tile has been determined to be visible (possibly based on a bounding volume that is not very tight-fitting)
    *    * its parent tile does _not_ meet the SSE (unless ancestorMeetsSse=true, see comments below)
    *    * the tile may or may not be renderable
    *
    * @private
    *
    * @param {Primitive} primitive The QuadtreePrimitive.
    * @param {FrameState} frameState The frame state.
    * @param {QuadtreeTile} tile The tile to visit
    * @param {Boolean} ancestorMeetsSse True if a tile higher in the tile tree already met the SSE and we're refining further only
    *                  to maintain detail while that higher tile loads.
    * @param {TraversalDetails} traveralDetails On return, populated with details of how the traversal of this tile went.
    */
    private func visitTile(frameState: inout FrameState, tile: QuadtreeTile, ancestorMeetsSse: inout Bool, traversalDetails: inout TraversalDetails) {
        _debug.tilesVisited += 1
        _tileReplacementQueue.markTileRendered(tile)
        tile.updateCustomData(frameState.frameNumber)

        if tile.level > _debug.maxDepth {
            _debug.maxDepth = tile.level
        }

        let meetsSse = screenSpaceError(&frameState, tile: tile) < maximumScreenSpaceError

        let southwestChild = tile.southwestChild
        let southeastChild = tile.southeastChild
        let northwestChild = tile.northwestChild
        let northeastChild = tile.northeastChild

        let lastFrame = lastSelectionFrameNumber
        let lastFrameSelectionResult: TileSelectionResult = tile.lastSelectionResultFrame == lastFrame ? tile.lastSelectionResult : .none

        if meetsSse || ancestorMeetsSse {
            // This tile (or an ancestor) is the one we want to render this frame, but we'll do different things depending
            // on the state of this tile and on what we did _last_ frame.

            // We can render it if _any_ of the following are true:
            // 1. We rendered it (or kicked it) last frame.
            // 2. This tile was culled last frame, or it wasn't even visited because an ancestor was culled.
            // 3. The tile is completely done loading.
            // 4. a) Terrain is ready, and
            //    b) All necessary imagery is ready. Necessary imagery is imagery that was rendered with this tile
            //       or any descendants last frame. Such imagery is required because rendering this tile without
            //       it would cause detail to disappear.
            //
            // Determining condition 4 is more expensive, so we check the others first.
            //
            // Note that even if we decide to render a tile here, it may later get "kicked" in favor of an ancestor.

            let oneRenderedLastFrame = lastFrameSelectionResult.original == .rendered
            let twoCulledOrNotVisited = lastFrameSelectionResult.original == .culled || lastFrameSelectionResult == .none
            let threeCompletelyLoaded = tile.state == .done

            var renderable = oneRenderedLastFrame || twoCulledOrNotVisited || threeCompletelyLoaded

            if !renderable {
                // Check the more expensive condition 4 above. This requires details of the thing
                // we're rendering (e.g. the globe surface), so delegate it to the tile provider.
                renderable = tileProvider.canRenderWithoutLosingDetail(tile)
            }

            if renderable {
                // Only load this tile if it (not just an ancestor) meets the SSE.
                if meetsSse {
                    queueTileLoad(queue: &_tileLoadQueueMedium, tile: tile, frameState: frameState)
                }
                addTileToRenderList(tile)

                traversalDetails.allAreRenderable = tile.renderable
                traversalDetails.anyWereRenderedLastFrame = lastFrameSelectionResult == .rendered
                traversalDetails.notYetRenderableCount = tile.renderable ? 0 : 1

                tile.lastSelectionResultFrame = frameState.frameNumber
                tile.lastSelectionResult = .rendered

                if !traversalDetails.anyWereRenderedLastFrame {
                    // Tile is newly-rendered this frame, so update its heights.
                    _tilesToUpdateHeights.append(tile)
                }

                return
            }

            // Otherwise, we can't render this tile (or its fill) because doing so would cause detail to disappear
            // that was visible last frame. Instead, keep rendering any still-visible descendants that were rendered
            // last frame and render fills for newly-visible descendants. E.g. if we were rendering level 15 last
            // frame but this frame we want level 14 and the closest renderable level <= 14 is 0, rendering level
            // zero would be pretty jarring so instead we keep rendering level 15 even though its SSE is better
            // than required. So fall through to continue traversal...
            ancestorMeetsSse = true

            // Load this blocker tile with high priority, but only if this tile (not just an ancestor) meets the SSE.
            if meetsSse {
                queueTileLoad(queue: &_tileLoadQueueHigh, tile: tile, frameState: frameState)
            }
        }
        
        
        if tileProvider.canRefine(tile) {
            let allAreUpsampled = southwestChild.upsampledFromParent && southeastChild.upsampledFromParent &&
                                  northwestChild.upsampledFromParent && northeastChild.upsampledFromParent

            if allAreUpsampled {
                // No point in rendering the children because they're all upsampled.  Render this tile instead.
                addTileToRenderList(tile)

                // Rendered tile that's not waiting on children loads with medium priority.
                queueTileLoad(queue: &_tileLoadQueueMedium, tile: tile, frameState: frameState)

                // Make sure we don't unload the children and forget they're upsampled.
                _tileReplacementQueue.markTileRendered(southwestChild)
                _tileReplacementQueue.markTileRendered(southeastChild)
                _tileReplacementQueue.markTileRendered(northwestChild)
                _tileReplacementQueue.markTileRendered(northeastChild)

                traversalDetails.allAreRenderable = tile.renderable
                traversalDetails.anyWereRenderedLastFrame = lastFrameSelectionResult == .rendered
                traversalDetails.notYetRenderableCount = tile.renderable ? 0 : 1

                tile.lastSelectionResultFrame = frameState.frameNumber
                tile.lastSelectionResult = .rendered

                if !traversalDetails.anyWereRenderedLastFrame {
                    // Tile is newly-rendered this frame, so update its heights.
                    _tilesToUpdateHeights.append(tile)
                }

                return
            }

            // SSE is not good enough, so refine.
            tile.lastSelectionResultFrame = frameState.frameNumber
            tile.lastSelectionResult = .refined

            let firstRenderedDescendantIndex = tilesToRender.count
            let loadIndexLow = _tileLoadQueueLow.count
            let loadIndexMedium = _tileLoadQueueMedium.count
            let loadIndexHigh = _tileLoadQueueHigh.count
            let tilesToUpdateHeightsIndex = _tilesToUpdateHeights.count

            // No need to add the children to the load queue because they'll be added (if necessary) when they're visited.
            visitVisibleChildrenNearToFar(southwest: southwestChild, southeast: southeastChild, northwest: northwestChild, northeast: northeastChild, frameState: &frameState, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &traversalDetails)

            // If no descendant tiles were added to the render list by the function above, it means they were all
            // culled even though this tile was deemed visible. That's pretty common.

            if firstRenderedDescendantIndex != tilesToRender.count {
                // At least one descendant tile was added to the render list.
                // The traversalDetails tell us what happened while visiting the children.

                let allAreRenderable = traversalDetails.allAreRenderable
                let anyWereRenderedLastFrame = traversalDetails.anyWereRenderedLastFrame
                let notYetRenderableCount = traversalDetails.notYetRenderableCount
                var queuedForLoad = false

                if !allAreRenderable && !anyWereRenderedLastFrame {
                    // Some of our descendants aren't ready to render yet, and none were rendered last frame,
                    // so kick them all out of the render list and render this tile instead. Continue to load them though!

                    // Mark the rendered descendants and their ancestors - up to this tile - as kicked.
                    let renderList = tilesToRender
                    for i in firstRenderedDescendantIndex..<renderList.count {
                        var workTile: QuadtreeTile? = renderList[i]
                        while workTile != nil && workTile!.lastSelectionResult != .kicked && workTile != tile {
                            workTile!.lastSelectionResult = workTile!.lastSelectionResult.kick
                            workTile = workTile!.parent
                            _tilesToRender[i] = workTile!
                        }
                    }

                    // Remove all descendants from the render list and add this tile.
                    _tilesToRender = Array(_tilesToRender[0..<firstRenderedDescendantIndex])
                    _tilesToUpdateHeights = Array(_tilesToUpdateHeights[0..<tilesToUpdateHeightsIndex])
                    addTileToRenderList(tile)

                    tile.lastSelectionResult = .rendered

                    // If we're waiting on heaps of descendants, the above will take too long. So in that case,
                    // load this tile INSTEAD of loading any of the descendants, and tell the up-level we're only waiting
                    // on this tile. Keep doing this until we actually manage to render this tile.
                    let wasRenderedLastFrame = lastFrameSelectionResult == .rendered
                    if !wasRenderedLastFrame && notYetRenderableCount > loadingDescendantLimit {
                        // Remove all descendants from the load queues.
                        _tileLoadQueueLow = Array(_tileLoadQueueLow[0..<loadIndexLow])
                        _tileLoadQueueMedium = Array(_tileLoadQueueMedium[0..<loadIndexMedium])
                        _tileLoadQueueHigh = Array(_tileLoadQueueHigh[0..<loadIndexHigh])
                        queueTileLoad(queue: &_tileLoadQueueMedium, tile: tile, frameState: frameState)
                        traversalDetails.notYetRenderableCount = tile.renderable ? 0 : 1
                        queuedForLoad = true
                    }

                    traversalDetails.allAreRenderable = tile.renderable
                    traversalDetails.anyWereRenderedLastFrame = wasRenderedLastFrame

                    if !wasRenderedLastFrame {
                        // Tile is newly-rendered this frame, so update its heights.
                        _tilesToUpdateHeights.append(tile)
                    }
                    _debug.tilesWaitingForChildren += 1
                }

                if preloadAncestors && !queuedForLoad {
                    queueTileLoad(queue: &_tileLoadQueueLow, tile: tile, frameState: frameState)
                }
            }
            return
        }

        tile.lastSelectionResultFrame = frameState.frameNumber
        tile.lastSelectionResult = .rendered

        // We'd like to refine but can't because we have no availability data for this tile's children,
        // so we have no idea if refinining would involve a load or an upsample. We'll have to finish
        // loading this tile first in order to find that out, so load this refinement blocker with
        // high priority.
        addTileToRenderList(tile)
        queueTileLoad(queue: &_tileLoadQueueHigh, tile: tile, frameState: frameState)

        traversalDetails.allAreRenderable = tile.renderable
        traversalDetails.anyWereRenderedLastFrame = lastFrameSelectionResult == .rendered
        traversalDetails.notYetRenderableCount = tile.renderable ? 0 : 1
    }
    
    /*
    private func visitTile(frameState: inout FrameState, tile: QuadtreeTile) {
        _debug.tilesVisited += 1
        
        _tileReplacementQueue.markTileRendered(tile)
        //tile.updateCustomData(frameNumber)
        
        if (tile.level > _debug.maxDepth) {
            _debug.maxDepth = tile.level
        }
        
        if screenSpaceError(&frameState, tile: tile) < maximumScreenSpaceError {
            // This tile meets SSE requirements, so render it.
            if tile.needsLoading == true {
                _tileLoadQueueMedium.append(tile)
            }
            addTileToRenderList(tile)
            return
        }
        
        let southwestChild = tile.southwestChild
        let southeastChild = tile.southeastChild
        let northwestChild = tile.northwestChild
        let northeastChild = tile.northeastChild
        let allAreRenderable = southwestChild.renderable && southeastChild.renderable &&
            northwestChild.renderable && northeastChild.renderable
        let allAreUpsampled = southwestChild.upsampledFromParent && southeastChild.upsampledFromParent &&
            northwestChild.upsampledFromParent && northeastChild.upsampledFromParent
        
        if allAreRenderable == true {
            if allAreUpsampled == true {
                // No point in rendering the children because they're all upsampled.  Render this tile instead.
                addTileToRenderList(tile)
                
                // Load the children even though we're (currently) not going to render them.
                // A tile that is "upsampled only" right now might change its tune once it does more loading.
                // A tile that is upsampled now and forever should also be done loading, so no harm done.
                queueChildLoadNearToFar(cameraPosition: frameState.camera?.positionCartographic, southwest: southwestChild, southeast: southeastChild, northwest: northwestChild, northeast: northwestChild)
                
                if tile.needsLoading == true {
                    // Rendered tile that's not waiting on children loads with medium priority.
                    _tileLoadQueueMedium.append(tile)
                }
            } else {
                // SSE is not good enough and children are loaded, so refine.
                // No need to add the children to the load queue because they'll be added (if necessary) when they're visited.
                visitVisibleChildrenNearToFar(southwest: southwestChild, southeast: southeastChild, northwest: northwestChild, northeast: northeastChild, frameState: &frameState)
                
                if tile.needsLoading == true {
                    // Tile is not rendered, so load it with low priority.
                    _tileLoadQueueLow.append(tile)
                }
            }
        } else {
            // We'd like to refine but can't because not all of our children are renderable.  Load the refinement blockers with high priority and
            // render this tile in the meantime.
            queueChildLoadNearToFar(cameraPosition: frameState.camera?.positionCartographic, southwest: southwestChild, southeast: southeastChild, northwest: northwestChild, northeast: northeastChild)
            addTileToRenderList(tile)
            
            if tile.needsLoading == true {
                // We will refine this tile when it's possible, so load this tile only with low priority.
                _tileLoadQueueLow.append(tile)
            }
        }
    }
 */
    
    /*
    func queueChildLoadNearToFar(cameraPosition: Cartographic?, southwest: QuadtreeTile, southeast: QuadtreeTile, northwest: QuadtreeTile, northeast: QuadtreeTile) {
        guard let cameraPosition = cameraPosition else { return }
        if cameraPosition.longitude < southwest.rectangle.east {
            if cameraPosition.latitude < southwest.rectangle.north {
                // Camera in southwest quadrant
                queueChildTileLoad(southwest)
                queueChildTileLoad(southeast)
                queueChildTileLoad(northwest)
                queueChildTileLoad(northeast)
            } else {
                // Camera in northwest quadrant
                queueChildTileLoad(northwest)
                queueChildTileLoad(southwest)
                queueChildTileLoad(northeast)
                queueChildTileLoad(southeast)
            }
        } else if (cameraPosition.latitude < southwest.rectangle.north) {
            // Camera southeast quadrant
            queueChildTileLoad(southeast)
            queueChildTileLoad(southwest)
            queueChildTileLoad(northeast)
            queueChildTileLoad(northwest)
        } else {
            // Camera in northeast quadrant
            queueChildTileLoad(northeast)
            queueChildTileLoad(northwest)
            queueChildTileLoad(southeast)
            queueChildTileLoad(southwest)
        }
    }
    */
    
    /*
    func queueChildTileLoad(_ childTile: QuadtreeTile) {
        _tileReplacementQueue.markTileRendered(childTile)
        if childTile.needsLoading == true {
            if childTile.renderable == true {
                _tileLoadQueueLow.append(childTile)
            } else {
                // A tile blocking refine loads with high priority
                _tileLoadQueueHigh.append(childTile)
            }
        }
    }
    */
    
    func visitVisibleChildrenNearToFar(southwest: QuadtreeTile, southeast: QuadtreeTile, northwest: QuadtreeTile, northeast: QuadtreeTile, frameState: inout FrameState, ancestorMeetsSse: inout Bool, traversalDetails: inout TraversalDetails) {
        
        guard let cameraPosition = frameState.camera?._positionCartographic else { return }
        let occluders = _occluders
        
        var quadDetails = traversalQuadsByLevel[southwest.level]

        if cameraPosition.longitude < southwest.rectangle.east {
            if cameraPosition.latitude < southwest.rectangle.north {
                // Camera in southwest quadrant
                visitIfVisible(tile: southwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.southwest)
                visitIfVisible(tile: southeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.southeast)
                visitIfVisible(tile: northwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.northwest)
                visitIfVisible(tile: northeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.northeast)
            } else {
                // Camera in northwest quadrant
                visitIfVisible(tile: northwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.northwest)
                visitIfVisible(tile: southwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.southwest)
                visitIfVisible(tile: northeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.northeast)
                visitIfVisible(tile: southeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.southeast)
            }
        } else if cameraPosition.latitude < southwest.rectangle.north {
            // Camera southeast quadrant
            visitIfVisible(tile: southeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.southeast)
            visitIfVisible(tile: southwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.southwest)
            visitIfVisible(tile: northeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.northeast)
            visitIfVisible(tile: northwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.northwest)
        } else {
            // Camera in northeast quadrant
            visitIfVisible(tile: northeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.northeast)
            visitIfVisible(tile: northwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.northwest)
            visitIfVisible(tile: southeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.southeast)
            visitIfVisible(tile: southwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &quadDetails.southwest)
        }
        traversalQuadsByLevel[southwest.level] = quadDetails
        traversalDetails = quadDetails.combine()
    }
    
    /*
    func visitVisibleChildrenNearToFar(southwest: QuadtreeTile, southeast: QuadtreeTile, northwest: QuadtreeTile, northeast: QuadtreeTile, frameState: inout FrameState) {
        guard let cameraPosition = frameState.camera?._positionCartographic else { return }
        let occluders = _occluders
        if cameraPosition.longitude < southwest.rectangle.east {
            if cameraPosition.latitude < southwest.rectangle.north {
                // Camera in southwest quadrant
                visitIfVisible(tile: southwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
                visitIfVisible(tile: southeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
                visitIfVisible(tile: northwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
                visitIfVisible(tile: northeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
            } else {
                // Camera in northwest quadrant
                visitIfVisible(tile: northwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
                visitIfVisible(tile: southwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
                visitIfVisible(tile: northeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
                visitIfVisible(tile: southeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
            }
        } else if cameraPosition.latitude < southwest.rectangle.north {
            // Camera southeast quadrant
            visitIfVisible(tile: southeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
            visitIfVisible(tile: southwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
            visitIfVisible(tile: northeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
            visitIfVisible(tile: northwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
        } else {
            // Camera in northeast quadrant
            visitIfVisible(tile: northeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
            visitIfVisible(tile: northwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
            visitIfVisible(tile: southeast, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
            visitIfVisible(tile: southwest, tileProvider: tileProvider, frameState: &frameState, occluders: occluders)
        }
        
    }
    */
    
    func containsNeededPosition(_ tile: QuadtreeTile) -> Bool {
        let rectangle = tile.rectangle
        return (cameraPositionCartographic != nil && rectangle.contains(cameraPositionCartographic!)) ||
            (cameraReferenceFrameOriginCartographic != nil && rectangle.contains(cameraReferenceFrameOriginCartographic!))
    }
    
    func visitIfVisible(tile: QuadtreeTile, tileProvider: GlobeSurfaceTileProvider, frameState: inout FrameState, occluders: QuadtreeOccluders?, ancestorMeetsSse: inout Bool, traversalDetails: inout TraversalDetails) {
        if tileProvider.computeTileVisibility(tile, frameState: &frameState, occluders: occluders) != .none {
            visitTile(frameState: &frameState, tile: tile, ancestorMeetsSse: &ancestorMeetsSse, traversalDetails: &traversalDetails)
            return
        }
        _debug.tilesCulled += 1
        _tileReplacementQueue.markTileRendered(tile)

        traversalDetails.allAreRenderable = true
        traversalDetails.anyWereRenderedLastFrame = false
        traversalDetails.notYetRenderableCount = 0

        if containsNeededPosition(tile) {
            // Load the tile(s) that contains the camera's position and
            // the origin of its reference frame with medium priority.
            // But we only need to load until the terrain is available, no need to load imagery.
            if tile.data == nil || tile.data!.vertexArray == nil {
                queueTileLoad(queue: &_tileLoadQueueMedium, tile: tile, frameState: frameState)
            }

            let lastFrame = lastSelectionFrameNumber
            let lastFrameSelectionResult = tile.lastSelectionResultFrame == lastFrame ? tile.lastSelectionResult : .none
            if lastFrameSelectionResult != .culled_but_needed && lastFrameSelectionResult != .rendered {
                _tilesToUpdateHeights.append(tile)
            }

            tile.lastSelectionResult = .culled_but_needed
        } else if preloadSiblings || tile.level == 0 {
            // Load culled level zero tiles with low priority.
            // For all other levels, only load culled tiles if preloadSiblings is enabled.
            queueTileLoad(queue: &_tileLoadQueueLow, tile: tile, frameState: frameState)
            tile.lastSelectionResult = .culled
        } else {
            tile.lastSelectionResult = .culled
        }

        tile.lastSelectionResultFrame = frameState.frameNumber
    }
    
    /*
    func visitIfVisible(tile: QuadtreeTile, tileProvider: GlobeSurfaceTileProvider, frameState: inout FrameState, occluders: QuadtreeOccluders) {
        if tileProvider.computeTileVisibility(tile, frameState: &frameState, occluders: _occluders) != .none {
            visitTile(frameState: &frameState, tile: tile)
        } else {
            _debug.tilesCulled += 1
            _tileReplacementQueue.markTileRendered(tile)
            
            // We've decided this tile is not visible, but if it's not fully loaded yet, we've made
            // this determination based on possibly-incorrect information.  We need to load this
            // culled tile with low priority just in case it turns out to be visible after all.
            if tile.needsLoading == true {
                _tileLoadQueueLow.append(tile)
            }
        }
    }
    */
    /*
    
    /**
    * Destroys the WebGL resources held by this object.  Destroying an object allows for deterministic
    * release of WebGL resources, instead of relying on the garbage collector to destroy this object.
    * <br /><br />
    * Once an object is destroyed, it should not be used; calling any function other than
    * <code>isDestroyed</code> will result in a {@link DeveloperError} exception.  Therefore,
    * assign the return value (<code>undefined</code>) to the object as done in the example.
    *
    * @memberof QuadtreePrimitive
    *
    * @returns {undefined}
    *
    * @exception {DeveloperError} This object was destroyed, i.e., destroy() was called.
    *
    * @see QuadtreePrimitive#isDestroyed
    *
    * @example
    * primitive = primitive && primitive.destroy();
    */
    QuadtreePrimitive.prototype.destroy = function() {
    this._tileProvider = this._tileProvider && this._tileProvider.destroy();
    };
    */

    func screenSpaceError(_ frameState: inout FrameState, tile: QuadtreeTile) -> Double {
        if frameState.mode == .scene2D {
            return screenSpaceError2D(&frameState, tile: tile)
        }
        
        let maxGeometricError = tileProvider.levelMaximumGeometricError(tile.level)
        
        let distance = tile.distance
        let height = frameState.context.height
        if let perspectiveFrustum = frameState.camera?.frustum as? PerspectiveFrustum {
            let sseDenominator = perspectiveFrustum.sseDenominator
            var error = (maxGeometricError * Double(height)) / (distance * sseDenominator)
            if frameState.fog.enabled == true {
                error -= Math.fog(distance, density: frameState.fog.density) * frameState.fog.sse
                return error
            }
        }

        // PERFORMANCE_IDEA: factor out stuff that's constant across tiles.
        return (maxGeometricError * Double(frameState.context.height)) / (2 * distance * tan(0.5 * frameState.camera!.frustum.fovy))
    }
    
    func screenSpaceError2D(_ frameState: inout FrameState, tile: QuadtreeTile) -> Double {
        let frustum = frameState.camera!.frustum

        let maxGeometricError = tileProvider.levelMaximumGeometricError(tile.level)
        let pixelSize = max(frustum.top - frustum.bottom, frustum.right - frustum.left) / max(Double(frameState.context.width), Double(frameState.context.height))
        return maxGeometricError / pixelSize
    }
    
    func addTileToRenderList(_ tile: QuadtreeTile) {
        _tilesToRender.append(tile)
    }
    
    func processTileLoadQueue(_ frameState: inout FrameState) {
        if _tileLoadQueueHigh.count == 0 && _tileLoadQueueMedium.count == 0 && _tileLoadQueueLow.count == 0 {
            return
        }
        
        // Remove any tiles that were not used this frame beyond the number
        // we're allowed to keep.
        _tileReplacementQueue.trimTiles(tileCacheSize)
        
        let endTime = Date(timeIntervalSinceNow: _loadQueueTimeSlice)
        
        // GlobeSurfaceTileNG
        var didSomeLoading = processSinglePriorityLoadQueue(frameState: &frameState, tileProvider: tileProvider, endTime: endTime, loadQueue: _tileLoadQueueHigh, didSomeLoading: false)
        didSomeLoading = processSinglePriorityLoadQueue(frameState: &frameState, tileProvider: tileProvider, endTime: endTime, loadQueue: _tileLoadQueueMedium, didSomeLoading: didSomeLoading)
        _ = processSinglePriorityLoadQueue(frameState: &frameState, tileProvider: tileProvider, endTime: endTime, loadQueue: _tileLoadQueueLow, didSomeLoading: didSomeLoading)
        
        /* Before GlobeSurfaceTileNG
        while _tileLoadQueueHigh.count > 0 {
            let tile = _tileLoadQueueHigh.removeFirst()
            _tileReplacementQueue.markTileRendered(tile)
            tileProvider.loadTile(tile, frameState: &frameState)
            if Date().compare(endTime) == ComparisonResult.orderedDescending {
                return
            }
        }
        while _tileLoadQueueMedium.count > 0 {
            let tile = _tileLoadQueueMedium.removeFirst()
            _tileReplacementQueue.markTileRendered(tile)
            tileProvider.loadTile(tile, frameState: &frameState)
            if Date().compare(endTime) == ComparisonResult.orderedDescending {
                return
            }
        }
        while _tileLoadQueueLow.count > 0 {
            let tile = _tileLoadQueueLow.removeFirst()
            _tileReplacementQueue.markTileRendered(tile)
            tileProvider.loadTile(tile, frameState: &frameState)
            if Date().compare(endTime) == ComparisonResult.orderedDescending {
                return
            }
        }
        */
    }
    
    private func sortByLoadPriority(a: QuadtreeTile, b: QuadtreeTile) -> Double {
        return a.loadPriority - b.loadPriority
    }

    func processSinglePriorityLoadQueue(frameState: inout FrameState, tileProvider: GlobeSurfaceTileProvider, endTime: Date, loadQueue: [QuadtreeTile], didSomeLoading: Bool) -> Bool {
        
        let sortedQueue = loadQueue.sorted{ (a, b) in
            a.loadPriority < b.loadPriority
        }
        
        var didLoad = didSomeLoading
        for tile in sortedQueue {
            _tileReplacementQueue.markTileRendered(tile)
            tileProvider.loadTile(tile, frameState: &frameState)
            didLoad = true
            if Date().compare(endTime) == .orderedDescending || !didLoad {
                break
            }
        }
        return didLoad

        /*
        for tile in loadQueue {
            _tileReplacementQueue.markTileRendered(tile)
            tileProvider.loadTile(tile, frameState: &frameState)
            if Date().compare(endTime) == ComparisonResult.orderedDescending {
                break
            }
        }
        */
    }
    
    func updateHeights(frameState: FrameState) {
        if !tileProvider.ready {
            return
        }
        var scratchRay = Ray()

        var tryNextFrame: [QuadtreeTile] = []
        let terrainProvider = tileProvider.terrainProvider

        let startTime = Date()
        let timeSlice = _updateHeightsTimeSlice
        let endTime = startTime + timeSlice

        let mode = frameState.mode
        let projection = frameState.mapProjection
        let ellipsoid = tileProvider.tilingScheme.ellipsoid
        var index = 0

        while _tilesToUpdateHeights.count > 0 {
            let tile = _tilesToUpdateHeights[0]
            if tile.data == nil || tile.data!.mesh == nil {
                // Tile isn't loaded enough yet, so try again next frame if this tile is still being rendered.
                let selectionResult = tile.lastSelectionResultFrame == lastSelectionFrameNumber ? tile.lastSelectionResult : .none
                if selectionResult == .rendered || selectionResult == .culled_but_needed {
                    tryNextFrame.append(tile)
                }
                let _ = _tilesToUpdateHeights.removeFirst()
                lastTileIndex = 0
                continue
            }
            let customData = tile.customData
            let customDataLength = customData.count

            var timeSliceMax = false
            for i in lastTileIndex..<customDataLength {
                index = i
                let data = customData[i]

                if tile.level > data.level {
                    if data.positionOnEllipsoidSurface == nil {
                        // cartesian has to be on the ellipsoid surface for `ellipsoid.geodeticSurfaceNormal`
                        data.positionOnEllipsoidSurface = Cartesian3.fromRadians(longitude: data.positionCartographic.longitude, latitude: data.positionCartographic.latitude, height: 0.0, ellipsoid: ellipsoid)
                    }

                    if mode == .scene3D {
                        let surfaceNormal = ellipsoid.geodeticSurfaceNormal(data.positionOnEllipsoidSurface!)
                        scratchRay.direction = surfaceNormal

                        // compute origin point

                        // Try to find the intersection point between the surface normal and z-axis.
                        // minimum height (-11500.0) for the terrain set, need to get this information from the terrain provider
                        let rayOrigin = ellipsoid.getSurfaceNormalIntersectionWithZAxis(data.positionOnEllipsoidSurface!, buffer: 11500.0)

                        // Theoretically, not with Earth datums, the intersection point can be outside the ellipsoid
                        if rayOrigin == nil {
                            // intersection point is outside the ellipsoid, try other value
                            // minimum height (-11500.0) for the terrain set, need to get this information from the terrain provider
                            var minimumHeight = 0.0
                            if let boundingRegion = tile.data?.tileBoundingRegion {
                                minimumHeight = boundingRegion.minimumHeight
                            }
                            let magnitude = min(minimumHeight, -11500.0)

                            // multiply by the *positive* value of the magnitude
                            let vectorToMinimumPoint = surfaceNormal.multiplyBy(scalar: abs(magnitude) + 1)
                            scratchRay.origin = data.positionOnEllipsoidSurface!.subtract(vectorToMinimumPoint)
                        } else {
                            scratchRay.origin = rayOrigin!
                        }
                    } else {
                        var scratchCartographic = data.positionCartographic

                        // minimum height for the terrain set, need to get this information from the terrain provider
                        scratchCartographic.height = -11500.0
                        var scratchPosition = projection.project(scratchCartographic)
                        //projection.project(scratchCartographic, scratchPosition);
                        scratchPosition = Cartesian3(x: scratchPosition.z, y: scratchPosition.x, z: scratchPosition.y)
                        //Cartesian3.fromElements(scratchPosition.z, scratchPosition.x, scratchPosition.y, scratchPosition);
                        scratchRay.origin = scratchPosition
                        //Cartesian3.clone(scratchPosition, scratchRay.origin);
                        scratchRay.direction = Cartesian3.unitX
                        //Cartesian3.clone(Cartesian3.UNIT_X, scratchRay.direction);
                    }

                    if let position = tile.data?.pick(scratchRay, mode: mode, projection: projection, cullBackFaces: false) {
                        data.callback(position)
                        data.level = tile.level
                    }
                } else if tile.level == data.level {
                    let children = tile.children
                    let childrenLength = children.count

                    var child: QuadtreeTile? = nil
                    for j in 0..<childrenLength {
                        child = children[j]
                        if let child = child, child.rectangle.contains(data.positionCartographic) {
                            break
                        }
                    }

                    if let child = child {
                        let tileDataAvailable = terrainProvider.getTileDataAvailable(x: child.x, y: child.y, level: child.level)
                        let parentTile = tile.parent
                        if let avail = tileDataAvailable, !avail {
                            data.removeFunc()
                        } else if let parent = parentTile, let pData = parent.data, let pTerrain = pData.terrainData, !pTerrain.isChildAvailable(parent.x, thisY: parent.y, childX: child.x, childY: child.y) {
                            data.removeFunc()
                        }
                    }
                }

                if Date() >= endTime {
                    timeSliceMax = true
                    break
                }
            }

            if timeSliceMax {
                _lastTileIndex = index
                break
            } else {
                _lastTileIndex = 0
                let _ = _tilesToUpdateHeights.removeFirst()
            }
        }
        for tryNextTile in tryNextFrame {
            _tilesToUpdateHeights.append(tryNextTile)
        }
    }
    
    /*
    func updateHeights(_ frameState: inout FrameState) {
        var scratchRay = Ray()

        let terrainProvider = tileProvider.terrainProvider
        
        let startTime = Date()
        let timeSlice = _updateHeightsTimeSlice
        let endTime = startTime + timeSlice
        
        let mode = frameState.mode;
        let projection = frameState.mapProjection
        let ellipsoid = projection.ellipsoid
        var tryNextFrame: [QuadtreeTile] = []
        var index = 0
        // FIXME: updateHeights
        while _tilesToUpdateHeights.count > 0 {
            let tile = _tilesToUpdateHeights[0]
            if tile.data == nil {
                // Tile isn't loaded enough yet, so try again next frame if this tile is still being rendered.
                /*
                 var selectionResult = tile._lastSelectionResultFrame === primitive._lastSelectionFrameNumber
                     ? tile._lastSelectionResult
                     : TileSelectionResult.NONE;
                 if (selectionResult === TileSelectionResult.RENDERED || selectionResult === TileSelectionResult.CULLED_BUT_NEEDED) {
                     tryNextFrame.push(tile);
                 }
                 */
                let tryNext = _tilesToUpdateHeights.removeFirst()
                tryNextFrame.append(tryNext)
                _lastTileIndex = 0
                continue
            }
            let customData = tile.customData
            let customDataLength = customData.count
            
            var timeSliceMax = false
            for i in _lastTileIndex..<customDataLength {
                index = i
                let data = customData[i]
                if tile.level > data.level {
                    if data.positionOnEllipsoidSurface == nil {
                        data.positionOnEllipsoidSurface = Cartesian3.fromRadians(longitude: data.positionCartographic.longitude, latitude: data.positionCartographic.latitude, height: 0.0, ellipsoid: ellipsoid)
                    }
                    if mode == .scene3D {
                        let surfaceNormal = ellipsoid.geodeticSurfaceNormal(data.positionOnEllipsoidSurface!)
                        scratchRay.direction = surfaceNormal

                        // compute origin point
                        // Try to find the intersection point between the surface normal and z-axis.
                        // minimum height (-11500.0) for the terrain set, need to get this information from the terrain provider
                        let rayOrigin = ellipsoid.getSurfaceNormalIntersectionWithZAxis(data.positionOnEllipsoidSurface!, buffer: 11500.0)

                        // Theoretically, not with Earth datums, the intersection point can be outside the ellipsoid
                        if rayOrigin == nil {
                            // intersection point is outside the ellipsoid, try other value
                            // minimum height (-11500.0) for the terrain set, need to get this information from the terrain provider
                            var minimumHeight = 0.0
                            if let boundingRegion = tile.data?.tileBoundingRegion {
                                minimumHeight = boundingRegion.minimumHeight
                            }
                            let magnitude = min(minimumHeight, -11500.0)
                            
                            // multiply by the *positive* value of the magnitude
                            let vectorToMinimumPoint = surfaceNormal.multiplyBy(scalar: abs(magnitude) + 1)
                            scratchRay.origin = data.positionOnEllipsoidSurface!.subtract(vectorToMinimumPoint)
                        } else {
                            scratchRay.origin = rayOrigin!
                        }
                    }
                    // mode != .scene3D is omitted
                    if let position = tile.data?.pick(scratchRay, mode: mode, projection: projection, cullBackFaces: false) {
                        data.callback(position)
                        data.level = tile.level
                    }
                } else if tile.level == data.level {
                    let children = tile.children
                    let childrenLength = children.count
                    
                    var child: QuadtreeTile? = nil
                    for j in 0..<childrenLength {
                        child = children[j]
                        if let child = child, child.rectangle.contains(data.positionCartographic) {
                            break
                        }
                    }
                    if let child = child {
                        let tileDataAvailable = terrainProvider.getTileDataAvailable(x: child.x, y: child.y, level: child.level)
                        let parentTile = tile.parent
                        if let avail = tileDataAvailable, !avail {
                            data.removeFunc()
                        } else if let parent = parentTile, let pData = parent.data, let pTerrain = pData.terrainData, !pTerrain.isChildAvailable(parent.x, thisY: parent.y, childX: child.x, childY: child.y) {
                            data.removeFunc()
                        }
                    }
                }
                
                if Date() >= endTime {
                    timeSliceMax = true
                    break
                }
            }
            if timeSliceMax {
                _lastTileIndex = index
                break
            } else {
                _lastTileIndex = 0
                let _ = _tilesToUpdateHeights.removeFirst()
            }
        }
        for i in 0..<tryNextFrame.count {
            _tilesToUpdateHeights.append(tryNextFrame[i])
        }
        /*while _tilesToUpdateHeights.count > 0 {
            var tile = tilesToUpdateHeights[tilesToUpdateHeights.length - 1];
            if (tile !== primitive._lastTileUpdated) {
                primitive._lastTileIndex = 0;
            }
            
            var customData = tile.customData;
            var customDataLength = customData.length;
            
            var timeSliceMax = false;
            for (var i = primitive._lastTileIndex; i < customDataLength; ++i) {
                var data = customData[i];
                
                if (tile.level > data.level) {
                    if (!defined(data.position)) {
                        data.position = ellipsoid.cartographicToCartesian(data.positionCartographic);
                    }
                    
                    if (mode === SceneMode.SCENE3D) {
                        Cartesian3.clone(Cartesian3.ZERO, scratchRay.origin);
                        Cartesian3.normalize(data.position, scratchRay.direction);
                    } else {
                        Cartographic.clone(data.positionCartographic, scratchCartographic);
                        
                        // minimum height for the terrain set, need to get this information from the terrain provider
                        scratchCartographic.height = -11500.0;
                        projection.project(scratchCartographic, scratchPosition);
                        Cartesian3.fromElements(scratchPosition.z, scratchPosition.x, scratchPosition.y, scratchPosition);
                        Cartesian3.clone(scratchPosition, scratchRay.origin);
                        Cartesian3.clone(Cartesian3.UNIT_X, scratchRay.direction);
                    }
                    
                    var position = tile.data.pick(scratchRay, mode, projection, false, scratchPosition);
                    if (defined(position)) {
                        data.callback(position);
                    }
                    
                    data.level = tile.level;
                } else if (tile.level === data.level) {
                    var children = tile.children;
                    var childrenLength = children.length;
                    
                    var child;
                    for (var j = 0; j < childrenLength; ++j) {
                        child = children[j];
                        if (Rectangle.contains(child.rectangle, data.positionCartographic)) {
                            break;
                        }
                    }
                    
                    var tileDataAvailable = terrainProvider.getTileDataAvailable(child.x, child.y, child.level);
         var parentTile = tile.parent;
                    if ((defined(tileDataAvailable) && !tileDataAvailable) ||
         (defined(parentTile) && defined(parentTile.data) && defined(parentTile.data.terrainData) &&
                                  !parentTile.data.terrainData.isChildAvailable(parentTile.x, parentTile.y, child.x, child.y))) {
                                data.removeFunc();
                    }
                }
                
                if (getTimestamp() >= endTime) {
                    timeSliceMax = true;
                    break;
                }
            }
            
            if (timeSliceMax) {
                primitive._lastTileUpdated = tile;
                primitive._lastTileIndex = i;
                break;
            } else {
                tilesToUpdateHeights.pop();
            }
        }*/
    }
    */
    
    func createRenderCommandsForSelectedTiles(_ frameState: inout FrameState) {
        for tile in tilesToRender {
            tileProvider.showTileThisFrame(tile, frameState: &frameState)
        }
        /* GlobeSufaceTileNG
        func tileDistanceSortFunction(_ a: QuadtreeTile, b: QuadtreeTile) -> Bool {
            return a.distance < b.distance
        }
        _tilesToRender.sort(by: tileDistanceSortFunction)
        
        for tile in _tilesToRender {
            tileProvider.showTileThisFrame(tile, frameState: &frameState)
            
            if tile.frameRendered != frameState.frameNumber - 1 {
                _tilesToUpdateHeights.append(tile)
            }
            tile.frameRendered = frameState.frameNumber
        }
        //updateHeights(&frameState)
        updateHeights(frameState: frameState) // GlobeSurfaceTileNG
        */
    }

}

class CallbackObject : Equatable {
    static func == (lhs: CallbackObject, rhs: CallbackObject) -> Bool {
        guard let lPos = lhs.position, let rPos = rhs.position else {
            return lhs.positionCartographic == rhs.positionCartographic && lhs.level == rhs.level
        }
        return lPos == rPos && lhs.level == rhs.level
    }
    
    var position: Cartesian3? = nil
    var positionCartographic: Cartographic = Cartographic()
    var positionOnEllipsoidSurface: Cartesian3? = nil
    var level: Int = -1
    var callback: (Cartesian3) -> ()
    var removeFunc: () -> () = {}

    init (callback: @escaping (Cartesian3) -> ()) {
        self.callback = callback
    }
}

struct TraversalDetails {
    /**
    * True if all selected (i.e. not culled or refined) tiles in this tile's subtree
    * are renderable. If the subtree is renderable, we'll render it; no drama.
    */
    var allAreRenderable: Bool = true
    /**
    * True if any tiles in this tile's subtree were rendered last frame. If any
    * were, we must render the subtree rather than this tile, because rendering
    * this tile would cause detail to vanish that was visible last frame, and
    * that's no good.
    */
    var anyWereRenderedLastFrame: Bool = false
    /**
    * Counts the number of selected tiles in this tile's subtree that are
    * not yet ready to be rendered because they need more loading. Note that
    * this value will _not_ necessarily be zero when
    * {@link TraversalDetails#allAreRenderable} is true, for subtle reasons.
    * When {@link TraversalDetails#allAreRenderable} and
    * {@link TraversalDetails#anyWereRenderedLastFrame} are both false, we
    * will render this tile instead of any tiles in its subtree and
    * the `allAreRenderable` value for this tile will reflect only whether _this_
    * tile is renderable. The `notYetRenderableCount` value, however, will still
    * reflect the total number of tiles that we are waiting on, including the
    * ones that we're not rendering. `notYetRenderableCount` is only reset
    * when a subtree is removed from the render queue because the
    * `notYetRenderableCount` exceeds the
    * {@link QuadtreePrimitive#loadingDescendantLimit}.
    */
    var notYetRenderableCount: Int = 0
}
struct TraversalQuadDetails {
    var southwest = TraversalDetails()
    var southeast = TraversalDetails()
    var northwest = TraversalDetails()
    var northeast = TraversalDetails()
    
    func combine() -> TraversalDetails {
        let allAreRenderable = southwest.allAreRenderable && southeast.allAreRenderable && northwest.allAreRenderable && northeast.allAreRenderable
        let anyWereRenderedLastFrame = southwest.anyWereRenderedLastFrame || southeast.anyWereRenderedLastFrame || northwest.anyWereRenderedLastFrame || northeast.anyWereRenderedLastFrame
        let notYetRenderableCount = southwest.notYetRenderableCount + southeast.notYetRenderableCount + northwest.notYetRenderableCount + northeast.notYetRenderableCount
        return TraversalDetails(allAreRenderable: allAreRenderable, anyWereRenderedLastFrame: anyWereRenderedLastFrame, notYetRenderableCount: notYetRenderableCount)
    }
}
