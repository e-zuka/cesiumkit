//
//  GlobeSurfaceTileNG.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/12/10.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

class GlobeSurfaceTile: QuadTreeTileData {
    /**
     * The {@link TileImagery} attached to this tile.
     * @type {TileImagery[]}
     * @default []
     */
    var imagery = [TileImagery]()
    
    var waterMaskTexture: Texture? = nil
    
    var waterMaskTranslationAndScale = Cartesian4(x: 0.0, y: 0.0, z: 1.0, w: 1.0)
    
    var terrainData: TerrainData? = nil
    var vertexArray: VertexArray? = nil
    var orientedBoundingBox: OrientedBoundingBox? = nil
    var boundingVolumeSourceTile: QuadtreeTile? = nil
    
    /**
    * A bounding region used to estimate distance to the tile. The horizontal bounds are always tight-fitting,
    * but the `minimumHeight` and `maximumHeight` properties may be derived from the min/max of an ancestor tile
    * and be quite loose-fitting and thus very poor for estimating distance. The {@link TileBoundingRegion#boundingVolume}
    * and {@link TileBoundingRegion#boundingSphere} will always be undefined; tiles store these separately.
    * @type {TileBoundingRegion}
    */
    var tileBoundingRegion: TileBoundingRegion? = nil
    var occludeePointInScaledSpace: Cartesian3? = Cartesian3()
    
    var terrainState: TerrainState = .unloaded
    var mesh: TerrainMesh? = nil
    var fill: TerrainFillMesh? = nil
    
    var pickBoundingSphere = BoundingSphere()
    
    var isClipped: Bool = true
    var clippedByBoundaries: Bool = false
    
    func eligibleForUnloading() -> Bool {
        // Do not remove tiles that are transitioning or that have
        // imagery that is transitioning.
        let loadingIsTransitioning = terrainState == .receiving || terrainState == .transforming
        
        var shouldRemoveTile = !loadingIsTransitioning
        
        for tileImagery in imagery {
            shouldRemoveTile = tileImagery.loadingImagery == nil || tileImagery.loadingImagery!.state != .transitioning
            if !shouldRemoveTile {
                break
            }
        }
        
        return shouldRemoveTile
    }
    
    var renderedMesh: TerrainMesh? {
        if vertexArray != nil {
            return mesh
        } else if fill != nil {
            return fill!.mesh
        }
        return nil
    }
    
    func getPosition(_ encoding: TerrainEncodingProtocol, mode: SceneMode? = nil, projection: MapProjection?, vertices: [Float], index: Int) -> Cartesian3 {
        
        var result = encoding.decodePosition(vertices, index: index)
        
        if mode != nil && mode != .scene3D {
            let positionCart = projection!.ellipsoid.cartesianToCartographic(result)
            result = projection!.project(positionCart!)
            result = Cartesian3(x: result.z, y: result.x, z: result.y)
        }
        
        return result
    }
    
    func pick (_ ray: Ray, mode: SceneMode?, projection: MapProjection?, cullBackFaces: Bool) -> Cartesian3? {
        
        guard let mesh = renderedMesh else {
            return nil
        }
        
        let vertices = mesh.vertices
        let indices = mesh.indices
        let encoding = mesh.encoding
        let length = indices.count
        for i in stride(from: 0, to: length, by: 3) {
            let i0 = indices[i]
            let i1 = indices[i + 1]
            let i2 = indices[i + 2]
            
            let v0 = getPosition(encoding, mode: mode, projection: projection, vertices: vertices, index: i0)
            let v1 = getPosition(encoding, mode: mode, projection: projection, vertices: vertices, index: i1)
            let v2 = getPosition(encoding, mode: mode, projection: projection, vertices: vertices, index: i2)
            
            let intersection = IntersectionTests.rayTriangle(ray, p0: v0, p1: v1, p2: v2, cullBackFaces: cullBackFaces)
            if intersection != nil {
                return intersection
            }
        }
        return nil
    }
    
    func freeResources () {
        waterMaskTexture = nil

        terrainData = nil

        terrainState = .unloaded
        mesh = nil
        if let fill = fill {
            fill.vertexArray = nil
        }
        fill = nil

        imagery.removeAll()

        freeVertexArray()
    }
    
    func freeVertexArray() {
        vertexArray = nil
    }
    
    class func initialize(tile: QuadtreeTile, terrainProvider: TerrainProvider, imageryLayerCollection: ImageryLayerCollection) {
        var surfaceTile = tile.data
        if surfaceTile == nil {
            surfaceTile = GlobeSurfaceTile()
            tile.data = surfaceTile
        }
        
        if tile.state == .start {
            prepareNewTile(tile: tile, terrainProvider: terrainProvider, imageryLayerCollection: imageryLayerCollection)
            tile.state = .loading
        }
    }
    
    class func processStateMachine(_ tile: QuadtreeTile, frameState: inout FrameState, terrainProvider: TerrainProvider, imageryLayerCollection: ImageryLayerCollection, vertexArraysToDestroy: [VertexArray] = [], terrainOnly: Bool = false) {
        
        GlobeSurfaceTile.initialize(tile: tile, terrainProvider: terrainProvider, imageryLayerCollection: imageryLayerCollection)

        let surfaceTile = tile.data!

        if tile.state == .loading {
            processTerrainStateMachine(tile: tile, frameState: &frameState, terrainProvider: terrainProvider, imageryLayerCollection: imageryLayerCollection, vertexArraysToDestroy: vertexArraysToDestroy);
        }

        // From here down we're loading imagery, not terrain. We don't want to load imagery until
        // we're certain that the terrain tiles are actually visible, though. We'll load terrainOnly
        // in these scenarios:
        //   * our bounding volume isn't accurate so we're not certain this tile is really visible (see GlobeSurfaceTileProvider#loadTile).
        //   * we want to upsample from this tile but don't plan to render it (see processTerrainStateMachine).
        if terrainOnly {
            return
        }

        let wasAlreadyRenderable = tile.renderable

        // The terrain is renderable as soon as we have a valid vertex array.
        tile.renderable = surfaceTile.vertexArray != nil

        // But it's not done loading until it's in the READY state.
        let isTerrainDoneLoading = surfaceTile.terrainState == .ready

        // If this tile's terrain and imagery are just upsampled from its parent, mark the tile as
        // upsampled only.  We won't refine a tile if its four children are upsampled only.
        tile.upsampledFromParent = surfaceTile.terrainData != nil && surfaceTile.terrainData!.wasCreatedByUpsampling()

        let isImageryDoneLoading = surfaceTile.processImagery(tile: tile, terrainProvider: terrainProvider, frameState: &frameState)

        if isTerrainDoneLoading && isImageryDoneLoading {
            /*
            let callbacks = tile._loadedCallbacks
            var newCallbacks: [String: Any] = [:]
            for (key, value) in callbacks {
                if value(tile) {
                    newCallbacks[key] = callbacks[key]
                }
            }
            */
            /*
            for(var layerId in callbacks) {
                if (callbacks.hasOwnProperty(layerId)) {
                    if(!callbacks[layerId](tile)) {
                        newCallbacks[layerId] = callbacks[layerId];
                    }
                }
            }
            tile._loadedCallbacks = newCallbacks
            */

            tile.state = .done
        }

        // Once a tile is renderable, it stays renderable, because doing otherwise would
        // cause detail (or maybe even the entire globe) to vanish when adding a new
        // imagery layer. `GlobeSurfaceTileProvider._onLayerAdded` sets renderable to
        // false for all affected tiles that are not currently being rendered.
        if wasAlreadyRenderable {
            tile.renderable = true
        }
    }

    func processImagery(tile: QuadtreeTile, terrainProvider: TerrainProvider, frameState: inout FrameState, skipLoading: Bool = false) -> Bool {
        let surfaceTile = tile.data
        var isUpsampledOnly = tile.upsampledFromParent
        var isRenderable = tile.renderable
        var isDoneLoading = true

        // Transition imagery states
        var i = 0
        var tileImageryCollection = surfaceTile?.imagery ?? []
        var len = tileImageryCollection.count
        while i < len {
            let tileImagery = tileImageryCollection[i]
            if tileImagery.loadingImagery == nil {
                isUpsampledOnly = false
                i += 1
                continue
            }
            
            if let loadingImagery = tileImagery.loadingImagery, loadingImagery.state == .placeHolder {
                let imageryLayer = loadingImagery.imageryLayer
                if imageryLayer.imageryProvider.ready {
                    // Remove the placeholder and add the actual skeletons (if any)
                    // at the same position.  Then continue the loop at the same index.
                    //tileImagery.freeResources()
                    _ = tileImageryCollection.remove(at: i)
                    _ = imageryLayer.createTileImagerySkeletons(tile, terrainProvider: terrainProvider, insertionPoint: i)
                    //i -= 1
                    len = tileImageryCollection.count
                    continue
                } else {
                    isUpsampledOnly = false
                }
            }
            
            let thisTileDoneLoading = tileImagery.processStateMachine(tile, frameState: &frameState, skipLoading: skipLoading)
            isDoneLoading = isDoneLoading && thisTileDoneLoading

            // The imagery is renderable as soon as we have any renderable imagery for this region.
            isRenderable = isRenderable && (thisTileDoneLoading || tileImagery.readyImagery != nil)

            isUpsampledOnly = isUpsampledOnly && tileImagery.loadingImagery != nil &&
                              (tileImagery.loadingImagery!.state == .failed || tileImagery.loadingImagery!.state == .invalid)
            
            i += 1
        }

        tile.upsampledFromParent = isUpsampledOnly
        tile.renderable = isRenderable

        return isDoneLoading
    }
    
    class func prepareNewTile(tile: QuadtreeTile, terrainProvider: TerrainProvider, imageryLayerCollection: ImageryLayerCollection) {
        
        var available = terrainProvider.getTileDataAvailable(x: tile.x, y: tile.y, level: tile.level)

        if available == nil && tile.parent != nil {
            // Provider doesn't know if this tile is available. Does the parent tile know?
            let parent = tile.parent!
            if let parentSurfaceTile = parent.data, let parentTerrainData = parentSurfaceTile.terrainData {
                available = parentTerrainData.isChildAvailable(parent.x, thisY: parent.y, childX: tile.x, childY: tile.y)
            }
        }

        if available == false {
            // This tile is not available, so mark it failed so we start upsampling right away.
            tile.data!.terrainState = .failed
        }

        // Map imagery tiles to this terrain tile
        for i in 0..<imageryLayerCollection.count {
            if let layer = imageryLayerCollection[i], layer.show {
                _ = layer.createTileImagerySkeletons(tile, terrainProvider: terrainProvider)
            }
        }
    }
    
    class func processTerrainStateMachine(tile: QuadtreeTile, frameState: inout FrameState, terrainProvider: TerrainProvider, imageryLayerCollection: ImageryLayerCollection, vertexArraysToDestroy: [VertexArray]) {
        
        guard let surfaceTile = tile.data else { return }

        // If this tile is FAILED, we'll need to upsample from the parent. If the parent isn't
        // ready for that, let's push it along.
        let parent = tile.parent
        if surfaceTile.terrainState == .failed && parent != nil {
            let parentReady = parent!.data != nil && parent!.data!.terrainData != nil && parent!.data!.terrainData!.canUpsample != false
            if !parentReady {
                GlobeSurfaceTile.processStateMachine(parent!, frameState: &frameState, terrainProvider: terrainProvider, imageryLayerCollection: imageryLayerCollection, terrainOnly: true)
            }
        }

        if surfaceTile.terrainState == .failed {
            surfaceTile.upsample(tile: tile, frameState: frameState, terrainProvider: terrainProvider, x: tile.x, y: tile.y, level: tile.level)
        }

        if surfaceTile.terrainState == .unloaded {
            surfaceTile.requestTileGeometry(terrainProvider: terrainProvider, x: tile.x, y: tile.y, level: tile.level)
        }

        if surfaceTile.terrainState == .received {
            surfaceTile.transform(frameState: frameState, terrainProvider: terrainProvider, x: tile.x, y: tile.y, level: tile.level)
        }

        if surfaceTile.terrainState == .transformed {
            surfaceTile.createResources(context: frameState.context, terrainProvider: terrainProvider, x: tile.x, y: tile.y, level: tile.level, vertexArraysToDestroy: vertexArraysToDestroy)
        }

        /*
        if surfaceTile.terrainState.rawValue >= TerrainState.received.rawValue && surfaceTile.waterMaskTexture == nil && terrainProvider.hasWaterMask {
            let terrainData = surfaceTile.terrainData
            if terrainData.waterMask != nil {
                createWaterMaskTextureIfNeeded(frameState.context, surfaceTile)
            } else {
                let sourceTile = surfaceTile.findAncestorTileWithTerrainData(tile)
                if sourceTile != nil && sourceTile.data.waterMaskTexture != nil {
                    surfaceTile.waterMaskTexture = sourceTile.data.waterMaskTexture
                    ++surfaceTile.waterMaskTexture.referenceCount;
                    surfaceTile._computeWaterMaskTranslationAndScale(tile, sourceTile, surfaceTile.waterMaskTranslationAndScale);
                }
            }
        }
        */
    }
    
    private func upsample(tile: QuadtreeTile, frameState: FrameState, terrainProvider: TerrainProvider, x: Int, y: Int, level: Int) {
        
        guard let parent = tile.parent else {
            // Trying to upsample from a root tile. No can do. This tile is a failure.
            tile.state = .failed
            return
        }

        let sourceX = parent.x
        let sourceY = parent.y
        let sourceLevel = parent.level

        guard let sourceData = parent.data?.terrainData else {
            // Parent is not available, so we can't upsample this tile yet.
            return
        }
        
        self.terrainState = .receiving

        _ = sourceData.upsample(frameState: frameState, tilingScheme: terrainProvider.tilingScheme, thisX: sourceX, thisY: sourceY, thisLevel: sourceLevel, descendantX: x, descendantY: y, descendantLevel: level) { terrainData in
            if let data = terrainData {
                self.terrainData = data
                self.terrainState = .received
            } else {
                self.terrainState = .failed
            }
        }
    }
    
    private func requestTileGeometry(terrainProvider: TerrainProvider, x: Int, y: Int, level: Int) {
        
        self.terrainState = .receiving
        _ = terrainProvider.requestTileGeometry(x: x, y: y, level: level, throttleRequests: true) { terrainData in
            if let terrainData = terrainData {
                self.terrainData = terrainData
                self.terrainState = .received
            } else {
                // Initially assume failure.  handleError may retry, in which case the state will
                // change to RECEIVING or UNLOADED.
                self.terrainState = .failed
                
                let message = "Failed to obtain terrain tile X: \(x) Y: \(y) Level: \(level) - terrain data request failed"
                logPrint(.debug, message)
            }
        }
    }
    
    private func transform(frameState: FrameState, terrainProvider: TerrainProvider, x: Int, y: Int, level: Int) {
        
        let tilingScheme = terrainProvider.tilingScheme

        self.terrainState = .transforming
        
        _ = self.terrainData!.createMesh(tilingScheme: tilingScheme, x: x, y: y, level: level, exaggeration: frameState.terrainExaggeration) { mesh in
            if let mesh = mesh {
                self.mesh = mesh
                self.orientedBoundingBox = mesh.orientedBoundingBox
                self.occludeePointInScaledSpace = mesh.occludeePointInScaledSpace
                self.terrainState = .transformed
            } else {
                self.terrainState = .failed
            }
        }
    }
    
    class func createVertexArrayForMesh(context: Context, mesh: TerrainMesh) -> VertexArray {
        let typedArray = mesh.vertices
        let buffer = Buffer(device: context.device, array: typedArray, componentDatatype: .float32, sizeInBytes: typedArray.sizeInBytes, label: "VertexBuffer")
        var attributes = mesh.encoding.getAttributes()
        attributes[0].buffer = buffer
        var mesh = mesh

        if mesh.indexBuffer == nil {
            let indexBuffer: Buffer?
            let indexCount = mesh.indices.count
            if indexCount < Math.SixtyFourKilobytes {
                let indicesShort = mesh.indices.map({ UInt16($0) })
                indexBuffer = Buffer(
                    device: context.device,
                    array: indicesShort,
                    componentDatatype: ComponentDatatype.unsignedShort,
                    sizeInBytes: indicesShort.sizeInBytes,
                    label: "IndexBuffer16"
                )
            } else {
                let indicesInt = mesh.indices.map({ UInt32($0) })
                indexBuffer = Buffer(
                    device: context.device,
                    array: indicesInt,
                    componentDatatype: ComponentDatatype.unsignedInt,
                    sizeInBytes: indicesInt.sizeInBytes,
                    label: "IndexBuffer32"
                )
            }
            mesh.indexBuffer = indexBuffer
        }
        /*
        if (!defined(indexBuffer) || indexBuffer.isDestroyed()) {
        } else {
            ++indexBuffer.referenceCount;
        }
        */

        return VertexArray(attributes: attributes, vertexCount: mesh.vertices.count, indexBuffer: mesh.indexBuffer!, indexCount: mesh.indices.count)
        /*
        return VertexArray(
            context: context,
            attributes: attributes,
            indexBuffer: mesh.indexBuffer!
        )
        */
    }
    
    func createResources(context: Context, terrainProvider: TerrainProvider, x: Int, y: Int, level: Int, vertexArraysToDestroy: [VertexArray]) {
        
        self.vertexArray = GlobeSurfaceTile.createVertexArrayForMesh(context: context, mesh: mesh!)
        self.terrainState = .ready
        // fill.destroy(vertexArraysToDestroy)
        self.fill = nil
    }

    private func findAncestorTileWithTerrainData(tile: QuadtreeTile) -> QuadtreeTile {
        
        var sourceTile = tile.parent
        
        while sourceTile != nil && sourceTile!.data == nil || sourceTile!.data!.terrainData == nil || sourceTile!.data!.terrainData!.wasCreatedByUpsampling() {
            sourceTile = sourceTile!.parent
        }

        return sourceTile!
    }
}
