//
//  ClippingPlaneCollection.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/12/11.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

class ClippingPlaneCollection {
    typealias IntersectTest = (Intersect) -> Bool
    
    var planes: [ClippingPlane] = []
    var dirtyIndex = -1
    var multipleDirtyPlanes = false
    var enabled = true
    var modelMatrix = Matrix4.identity
    var edgeColor = Color(red: 1, green: 1, blue: 1, alpha: 1)
    var edgeWidth = 0.0
    //var planeAdded: Event
    //var planeRemoved: Event
    //var owner = nil
    var unionClippingRegions = false
    var testIntersection: IntersectTest {
        unionClippingRegions ? unionIntersectFunction : defaultIntersectFunction
    }
    //var uint8View = nil
    //var float32View = nil
    var clippingPlanesTexture: Texture? = nil
    
    let unionIntersectFunction: IntersectTest = {intersect in intersect == Intersect.outside}
    let defaultIntersectFunction: IntersectTest = {intersect in intersect == Intersect.inside}
    
    var length: Int {
        planes.count
    }
    var texture: Texture? {
        clippingPlanesTexture
    }
    var clippingPlanesState: Int {
        unionClippingRegions ? planes.count : -planes.count
    }
    
    func computeIntersectionWithBoundingVolume(tileBoundingVolume: BoundingVolume, transform: Matrix4? = nil) -> Intersect {
        let length = planes.count
        if let transform = transform {
            modelMatrix = transform.multiply(modelMatrix)
        }
        
        // If the collection is not set to union the clipping regions, the volume must be outside of all planes to be
        // considered completely clipped. If the collection is set to union the clipping regions, if the volume can be
        // outside any the planes, it is considered completely clipped.
        // Lastly, if not completely clipped, if any plane is intersecting, more calculations must be performed.
        var intersection: Intersect = .inside
        if !unionClippingRegions && length > 0 {
            intersection = .outside
        }

        for plane in planes {
            let scratchPlane = plane.transform(modelMatrix) // ClippingPlane can be used for Plane math

            let value = tileBoundingVolume.intersectPlane(scratchPlane)
            if value == .intersecting {
                intersection = value
            } else if testIntersection(value) {
                return value
            }
        }

        return intersection
    }
}
