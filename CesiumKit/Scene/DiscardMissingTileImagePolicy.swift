//
//  DiscardMissingTileImagePolicy.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2018/07/10.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation
import Hydra

public class DiscardMissingTileImagePolicy : TileDiscardPolicy {
    let _pixelsToCheck: [Cartesian2]
    var _missingImagePixels: [UInt8]?
    var _missingImageByteLength: Int?
    var _isReady: Bool = false
    
    public var isReady: Bool {
        return _isReady
    }
    
    public func shouldDiscardImage(_ image: CGImage) -> Bool {
        if _isReady == false {
            assertionFailure("shouldDiscardImage must not be called before the discard policy is ready.")
            return false
        }

        if _missingImagePixels == nil {
            return false
        }
        
        if let missingImageByteLength = _missingImageByteLength, image.height * image.bytesPerRow != missingImageByteLength {
            return false
        }
        
        guard let (pixels, _) = image.renderToPixelArray(colorSpace: CGColorSpaceCreateDeviceRGB(), premultiplyAlpha: true, flipY: false) else {
            return false
        }
        
        for pos in _pixelsToCheck {
            let index = Int(pos.x) * 4 + Int(pos.y) * image.width
            for offset in 0..<4 {
                if pixels[index + offset] != _missingImagePixels![index + offset] {
                    return false
                }
            }
        }
        return true
    }
    
    public init(missingImageUrl: String, pixelsToCheck: [Cartesian2], disableCheckIfAllPixelsAreTransparent: Bool) {
        _pixelsToCheck = pixelsToCheck
        _missingImagePixels = nil
        _missingImageByteLength = nil
        
        guard let resource = Resource.createIfNeeded(resource: missingImageUrl) else { return }
        resource.fetchImage().then({ image in
            self._missingImageByteLength = image.bytesPerRow * image.height
            
            if let (pixels, _) = image.renderToPixelArray(colorSpace: CGColorSpaceCreateDeviceRGB(), premultiplyAlpha: true, flipY: false) {
                var allAreTransparent = true
                let width = image.width
                
                for pos in pixelsToCheck {
                    if allAreTransparent == false {
                        break
                    }
                    let index = Int(pos.x) * 4 + Int(pos.y) * width
                    let alpha = pixels[index + 3]
                    
                    if alpha > 0 {
                        allAreTransparent = false
                    }
                }
                
                if allAreTransparent == false {
                    self._missingImagePixels = pixels
                }
            }
            self._isReady = true
        }).catch({ error in
            logPrint(.error, error.localizedDescription)
            self._isReady = true
        })
    }
}
