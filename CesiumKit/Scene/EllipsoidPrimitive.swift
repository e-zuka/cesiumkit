//
//  EllipsoidPrimitive.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/11/28.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

class EllipsoidPrimitive {
    let center: Cartesian3
    let radii: Cartesian3
    var modelMatrix: Matrix4
    let show: Bool
    let material: Material
    var id: String? = nil
    var debugShowBoundingVolume: Bool = false
    var onlySunLighting: Bool
    
    var boundingSphere = BoundingSphere()
    var computedModelMatrix = Matrix4()
    var oneOverEllipsoidRadiiSquared: Cartesian3 = Cartesian3()
    
    private var _center = Cartesian3()
    private var _radii = Cartesian3()
    private var _modelMatrix = Matrix4()
    private var _material: Material? = nil
    private var _onlySunLighting: Bool = false
    private var translucent: Bool? = nil
    private var depthTest = RenderState.DepthTest(enabled: true, function: .less)
    private var useLogDepth = false
    private var sp: ShaderProgram? = nil
    private var rs: RenderState? = nil
    var va: VertexArray? = nil
    //private var pickSP
    //private var pickId
    //private var colorCommand: DrawCommand
    //private var pickCommand: DrawCommand
    private var uniforms: [String: Cartesian3] = [:]
    //private var pickUniforms: [String: Cartesian3] = [:]
    
    init(center: Cartesian3 = Cartesian3.zero, radii: Cartesian3, modelMatrix: Matrix4 = Matrix4.identity, show: Bool = true, material: Material?, onlySunLighting: Bool = false, id: String? = nil, depthTest: RenderState.DepthTest) {
        self.center = center
        self.radii = radii
        self.modelMatrix = modelMatrix
        self.show = show
        if let material = material {
            self.material = material
        } else {
            self.material = .init(fromType: ColorMaterialType())
        }
        self.id = id
        self.onlySunLighting = onlySunLighting
        self.depthTest = depthTest
        
        //colorCommand = DrawCommand()
        //colorCommand.owner = self
        //pickCommand = DrawCommand()
        
        translucent = false
        
        uniforms = ["u_radii": radii, "u_oneOverEllipsoidRadiiSquared": oneOverEllipsoidRadiiSquared]
    }
    
    private var attributeLocations = ["position" : 0]
    func getVertexArray(context: Context) -> VertexArray {
        if let cachedVertexArray = context.cache["ellipsoidPrimitive_vertexArray"] as? VertexArray {
            return cachedVertexArray
        }
        let geometry = BoxGeometry(
            fromDimensions: Cartesian3(x: 2.0, y: 2.0, z: 2.0),
            vertexFormat: .PositionOnly()).createGeometry(context)
        let vertexArray = VertexArray(fromGeometry: geometry, context: context, attributeLocations: attributeLocations, interleave: true)
        return vertexArray
    }
    
    func update(frameState: FrameState) {
        guard show, frameState.mode == .scene3D else { return }
        
        let context = frameState.context!
        //let translucent = material.isTranslucent
        let translucencyChanged: Bool = false
        /*
        if let _translucent = self.translucent {
            translucencyChanged = _translucent != translucent
        } else {
            translucencyChanged = true
        }
        */
        
        /*
        if rs == nil || translucencyChanged {
            //self.translucent = translucent
            
            // If this render state is ever updated to use a non-default
            // depth range, the hard-coded values in EllipsoidVS.glsl need
            // to be updated as well.
            rs = RenderState(device: context.device,
                             cullFace: .front,
                             depthTest: depthTest,//RenderState.DepthTest(enabled: false, function: .less),
                             depthMask: !translucent! && context.depthTexture
            )
            // blending translucent ? BlendingState.ALPHA_BLEND : undefined
        }
        */
        
        if va == nil {
            va = getVertexArray(context: context)
        }
        
        var boundingSphereDirty = false
        
        if radii != _radii {
            _radii = radii
            
            oneOverEllipsoidRadiiSquared.x = 1.0 / (radii.x * radii.x)
            oneOverEllipsoidRadiiSquared.y = 1.0 / (radii.y * radii.y)
            oneOverEllipsoidRadiiSquared.z = 1.0 / (radii.z * radii.z)

            boundingSphereDirty = true
        }
        
        if modelMatrix != _modelMatrix || center != _center {
            _modelMatrix = modelMatrix
            _center = center
            
            computedModelMatrix = modelMatrix.multiplyByTranslation(center)
            boundingSphereDirty = true
        }
        
        if boundingSphereDirty {
            boundingSphere.center = Cartesian3.zero
            boundingSphere.radius = radii.maximumComponent()
            boundingSphere = boundingSphere.transform(computedModelMatrix)
        }
        
        // TODO: material change handling
        //let materialChanged: Bool = _material == nil
        _material = material
        material.update(context)
        
        //let lightingChanged = onlySunLighting != _onlySunLighting
        _onlySunLighting = onlySunLighting
        
    }
}
