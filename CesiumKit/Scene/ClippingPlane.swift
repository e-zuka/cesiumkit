//
//  ClippingPlane.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/12/11.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

struct ClippingPlane {
    let distance: Double
    let normal: Cartesian3
    var index = -1
    
    init(distance: Double, normal: Cartesian3) {
        self.distance = distance
        self.normal = normal
    }
    
    init(from plane: Plane) {
        self.init(distance: plane.distance, normal: plane.normal)
    }
    
    func transform(_ matrix: Matrix4) -> Plane {
        let scratchNormal = matrix.multiplyByPointAsVector(self.normal).normalize()
        var scratchPosition = self.normal.multiplyBy(scalar: -self.distance)
        scratchPosition = matrix.multiplyByPoint(scratchPosition)
        
        return Plane(fromPoint: scratchPosition, normal: scratchNormal)
    }
}
