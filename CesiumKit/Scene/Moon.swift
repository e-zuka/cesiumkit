//
//  Moon.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/11/29.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

class Moon {
    struct EllipsoidUnirom {
        var radii: SIMD3<Float>
        var oneOverEllipsoidRadiiSquared: SIMD3<Float>
        var sunDirectionEC: SIMD3<Float>
    }
    struct TextureUniform {
        var color: SIMD4<Float>
        var _repeat: SIMD2<Float>
    }
    
    var texture: Texture? = nil
    var show: Bool {
        texture != nil
    }
    let width: Int
    let height: Int
    let onlySunLighting: Bool
    let ellipsoid: Ellipsoid
    var bufferSyncState: BufferSyncState = .zero
    private let ellipsoidPrimitive: EllipsoidPrimitive
    private let textureURL = "moonSmall.jpg"
    private let axes = IauOrientationAxes()
    
    var boundingVolume: BoundingVolume {
        ellipsoidPrimitive.boundingSphere
    }
    
    private var pipeline: MTLRenderPipelineState? = nil
    typealias BufferDict = [String: MTLBuffer]
    private var bufferPool: [BufferDict] = []
    private var currentBuffer: BufferDict {
        bufferPool[bufferSyncState.rawValue]
    }
    
    init(device: MTLDevice, viewport: (width: Int, height: Int), context: Context, ellipsoid: Ellipsoid = Ellipsoid.moon(), onlySunLighting: Bool = true) {
        
        self.width = viewport.width
        self.height = viewport.height
        self.ellipsoid = ellipsoid
        self.onlySunLighting = onlySunLighting
        
        self.bufferSyncState = context.bufferSyncState
        
        ellipsoidPrimitive = EllipsoidPrimitive(
            radii: ellipsoid.radii.multiplyBy(scalar: 3.0),
            material: nil,
            depthTest: RenderState.DepthTest(enabled: false, function: .less))
        
        if let moonImage = textureURL.loadImageForCubeMapSource() {
            texture = Texture(
                context: context,
                options: TextureOptions(
                    source: .image(moonImage),
                    pixelFormat: .rgba8Unorm,
                    flipY: true,
                    usage: .ShaderRead,
                    mipmapped: false))
        }
        
        guard let bundle = Bundle(identifier: "com.testtoast.CesiumKit") else {
            logPrint(.error, "Could not find CesiumKit bundle in executable")
            return
        }
        guard let libraryPath = bundle.url(forResource: "default", withExtension: "metallib")?.path else {
            logPrint(.error, "Could not find shader source from bundle")
            return
        }
        let shaderLibrary: MTLLibrary
        do {
            shaderLibrary = try device.makeLibrary(filepath: libraryPath)
        } catch let error as NSError {
            logPrint(.error, "Could not generate library from compiled shader lib: \(error.localizedDescription)")
            return
        }

        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
        pipelineDescriptor.depthAttachmentPixelFormat = .depth32Float_stencil8
        pipelineDescriptor.stencilAttachmentPixelFormat = .depth32Float_stencil8
        pipelineDescriptor.vertexFunction = shaderLibrary.makeFunction(name: "moon_vertex")
        pipelineDescriptor.fragmentFunction = shaderLibrary.makeFunction(name: "moon_fragment")
        pipelineDescriptor.label = "MoonPSO"
        do {
            pipeline = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch {
            assertionFailure("failed to create Moon PSO")
        }

        prepareBuffer(device: device)
    }
    
    private func prepareBuffer(device: MTLDevice) {
        for _ in 0..<3 {
            var dict: BufferDict = [:]
            let frustumBuffer = device.makeBuffer(length: MemoryLayout<FrustumUniformBufferLayout>.stride, options: .storageModeShared)!
            dict["frustum"] = frustumBuffer
            let ellipsoidBuffer = device.makeBuffer(length: MemoryLayout<EllipsoidUnirom>.size, options: .storageModeShared)!
            dict["ellipsoid"] = ellipsoidBuffer
            let texUniformBuffer = device.makeBuffer(length: MemoryLayout<TextureUniform>.stride, options: .storageModeShared)!
            dict["tex"] = texUniformBuffer
            bufferPool.append(dict)
        }
    }
    
    func update(frameState: FrameState) {
        guard show, let date = frameState.time else { return }
        var icrfToFixed = Matrix3()
        
        ellipsoidPrimitive.onlySunLighting = onlySunLighting
        
        
        if let fixedMatrix = Transforms.computeIcrfToFixedMatrix(date) {
            icrfToFixed = fixedMatrix
        }
        
        var rotation = axes.evaluate(date: date).transpose
        rotation = icrfToFixed.multiply(rotation)
        
        var translation = Simon1994PlanetaryPositions.sharedInstance.computeMoonPositionInEarthInertialFrame(date)
        translation = icrfToFixed.multiplyByVector(translation)
        
        ellipsoidPrimitive.modelMatrix = Matrix4(rotation: rotation, translation: translation)
        
        ellipsoidPrimitive.update(frameState: frameState)
    }
    
    func render(uniformState: UniformState, passDescriptor: MTLRenderPassDescriptor, with commandBuffer: MTLCommandBuffer) {
        guard let pipeline = pipeline, let vertexArray = ellipsoidPrimitive.va else { return }
        let frustumBuffer = currentBuffer["frustum"]!
        uniformState.model = ellipsoidPrimitive.computedModelMatrix
        uniformState.setFrustumUniforms(frustumBuffer)
        uniformState.model = Matrix4.identity
        
        let ellipsoidBuffer = currentBuffer["ellipsoid"]!
        var ellipsoidUniform = EllipsoidUnirom(radii: ellipsoidPrimitive.radii.floatRepresentation, oneOverEllipsoidRadiiSquared: ellipsoidPrimitive.oneOverEllipsoidRadiiSquared.floatRepresentation, sunDirectionEC: uniformState.sunDirectionEC.floatRepresentation)
        ellipsoidBuffer.write(from: &ellipsoidUniform, length: MemoryLayout<EllipsoidUnirom>.size)
        
        let texUniformBuffer = currentBuffer["tex"]
        var texUnifrom = TextureUniform(color: SIMD4<Float>(1, 1, 1, 1), _repeat: SIMD2<Float>(1, 1))
        texUniformBuffer?.write(from: &texUnifrom, length: MemoryLayout<TextureUniform>.size)
        
        let moonPassDescriptor = passDescriptor.copy() as! MTLRenderPassDescriptor
        moonPassDescriptor.colorAttachments[0].loadAction = .load
        let encoder = commandBuffer.makeRenderCommandEncoder(descriptor: moonPassDescriptor)!
        encoder.pushDebugGroup("Moon Render")
        encoder.label = "Moon"
        encoder.setCullMode(.front)
        //encoder.setDepthStencilState(depthStencilState)
        encoder.setFrontFacing(.counterClockwise)
        encoder.setViewport(MTLViewport(originX: 0, originY: 0, width: Double(width), height: Double(height), znear: 0.0, zfar: 1.0))
        encoder.setRenderPipelineState(pipeline)
        
        encoder.setVertexBuffer(vertexArray.attributes[0].buffer!.metalBuffer, offset: 0, index: 0)
        encoder.setVertexBuffer(frustumBuffer, offset: 0, index: 1)
        encoder.setVertexBuffer(ellipsoidBuffer, offset: 0, index: 2)
        
        encoder.setFragmentTexture(texture?.metalTexture, index: 0)
        encoder.setFragmentBuffer(frustumBuffer, offset: 0, index: 0)
        encoder.setFragmentBuffer(ellipsoidBuffer, offset: 0, index: 1)
        encoder.setFragmentBuffer(texUniformBuffer, offset: 0, index: 2)
        
        encoder.drawIndexedPrimitives(type: .triangle, indexCount: vertexArray.indexCount!, indexType: vertexArray.indexBuffer!.componentDatatype.toMTLIndexType(), indexBuffer: vertexArray.indexBuffer!.metalBuffer, indexBufferOffset: 0)
        
        encoder.endEncoding()
        encoder.popDebugGroup()
    }
}
