//
//  TerrainFillMesh.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2019/12/09.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation

struct HeightAndNormal {
    var height: Double = 0.0
    var encodedNormal = Cartesian2()
}
struct HeightRange {
    var minimumHeight: Double = 0.0
    var maximumHeight: Double = 0.0
}

class TerrainFillMesh {
    let tile: QuadtreeTile
    var frameLastUpdated: Int? = nil
    var westMeshes: [TerrainMesh] = []
    var westTiles: [QuadtreeTile] = []
    var southMeshes: [TerrainMesh] = []
    var southTiles: [QuadtreeTile] = []
    var eastMeshes: [TerrainMesh] = []
    var eastTiles: [QuadtreeTile] = []
    var northMeshes: [TerrainMesh] = []
    var northTiles: [QuadtreeTile] = []
    var southwestMesh: TerrainMesh? = nil
    var southwestTile: QuadtreeTile? = nil
    var southeastMesh: TerrainMesh? = nil
    var southeastTile: QuadtreeTile? = nil
    var northwestMesh: TerrainMesh? = nil
    var northwestTile: QuadtreeTile? = nil
    var northeastMesh: TerrainMesh? = nil
    var northeastTile: QuadtreeTile? = nil
    var changedThisFrame = true
    var visitedFrame: Int? = nil
    var enqueuedFrame: Int? = nil
    var mesh: TerrainMesh? = nil
    var vertexArray: VertexArray? = nil
    var waterMaskTexture: Texture? = nil
    var waterMaskTranslationAndScale = Cartesian4()
    
    private var cartographicScratch = Cartographic()
    private var heightmapBuffer: [UInt16] = [UInt16](repeating: 0, count: 9 * 9)
    private static let traversalQueue = Queue<QuadtreeTile>()
    
    init(tile: QuadtreeTile) {
        self.tile = tile
    }
    
    func update(tileProvider: GlobeSurfaceTileProvider, frameState: inout FrameState, vertexArraysToDestroy: [VertexArray] = []) {
        if changedThisFrame {
            createFillMesh(tileProvider: tileProvider, frameState: &frameState, tile: self.tile, vertexArraysToDestroy: vertexArraysToDestroy)
            changedThisFrame = false
        }
    }

    class func updateFillTiles(tileProvider: GlobeSurfaceTileProvider, renderedTiles:
        [QuadtreeTile], frameState: inout FrameState, vertexArraysToDestroy: [VertexArray] = []) {
        // We want our fill tiles to look natural, which means they should align perfectly with
        // adjacent loaded tiles, and their edges that are not adjacent to loaded tiles should have
        // sensible heights (e.g. the average of the heights of loaded edges). Some fill tiles may
        // be adjacent only to other fill tiles, and in that case heights should be assigned fanning
        // outward from the loaded tiles so that there are no sudden changes in height.

        // We do this with a breadth-first traversal of the rendered tiles, starting with the loaded
        // ones. Graph nodes are tiles and graph edges connect to other rendered tiles that are spatially adjacent
        // to those tiles. As we visit each node, we propagate tile edges to adjacent tiles. If there's no data
        // for a tile edge,  we create an edge with an average height and then propagate it. If an edge is partially defined
        // (e.g. an edge is adjacent to multiple more-detailed tiles and only some of them are loaded), we
        // fill in the rest of the edge with the same height.
        guard let quadtree = tileProvider.quadtree else { return }
        guard let levelZeroTiles = quadtree._levelZeroTiles else { return }
        let lastSelectionFrameNumber = quadtree.lastSelectionFrameNumber

        traversalQueue.clear()

        // Add the tiles with real geometry to the traversal queue.
        for renderedTile in renderedTiles {
            if renderedTile.data?.vertexArray != nil {
                traversalQueue.enqueue(renderedTile)
            }
        }

        var tile = traversalQueue.dequeue()
        
        while tile != nil {
            let tileToWest = tile!.findTileToWest(levelZeroTiles: levelZeroTiles)
            let tileToSouth = tile!.findTileToSouth(levelZeroTiles: levelZeroTiles)
            let tileToEast = tile!.findTileToEast(levelZeroTiles: levelZeroTiles)
            let tileToNorth = tile!.findTileToNorth(levelZeroTiles: levelZeroTiles)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: tile!, startTile: tileToWest, currentFrameNumber: lastSelectionFrameNumber, tileEdge: .east, downOnly: false, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: tile!, startTile: tileToSouth, currentFrameNumber: lastSelectionFrameNumber, tileEdge: .north, downOnly: false, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: tile!, startTile: tileToEast, currentFrameNumber: lastSelectionFrameNumber, tileEdge: .west, downOnly: false, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: tile!, startTile: tileToNorth, currentFrameNumber: lastSelectionFrameNumber, tileEdge: .south, downOnly: false, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)

            let tileToNorthwest = tileToWest?.findTileToNorth(levelZeroTiles: levelZeroTiles)
            let tileToSouthwest = tileToWest?.findTileToSouth(levelZeroTiles: levelZeroTiles)
            let tileToNortheast = tileToEast?.findTileToNorth(levelZeroTiles: levelZeroTiles)
            let tileToSoutheast = tileToEast?.findTileToSouth(levelZeroTiles: levelZeroTiles)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: tile!, startTile: tileToNorthwest, currentFrameNumber: lastSelectionFrameNumber, tileEdge: .southeast, downOnly: false, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: tile!, startTile: tileToNortheast, currentFrameNumber: lastSelectionFrameNumber, tileEdge: .southwest, downOnly: false, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: tile!, startTile: tileToSouthwest, currentFrameNumber: lastSelectionFrameNumber, tileEdge: .northeast, downOnly: false, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: tile!, startTile: tileToSoutheast, currentFrameNumber: lastSelectionFrameNumber, tileEdge: .northwest, downOnly: false, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)

            tile = traversalQueue.dequeue();
        }
    }
    
    static func visitRenderedTiles(tileProvider: GlobeSurfaceTileProvider, frameState: inout FrameState, sourceTile: QuadtreeTile, startTile: QuadtreeTile?, currentFrameNumber: Int, tileEdge: TileEdge, downOnly: Bool, traversalQueue: Queue<QuadtreeTile>, vertexArraysToDestroy: [VertexArray]) {
        
        guard let startTile = startTile else {
            // There are no tiles North or South of the poles.
            return
        }

        var tile: QuadtreeTile? = startTile
        while tile != nil && (tile!.lastSelectionResultFrame != currentFrameNumber || tile!.lastSelectionResult.wasKicked || tile!.lastSelectionResult.original == .culled) {
            // This tile wasn't visited or it was visited and then kicked, so walk up to find the closest ancestor that was rendered.
            // We also walk up if the tile was culled, because if siblings were kicked an ancestor may have been rendered.
            if downOnly {
                return
            }

            let parent = tile!.parent
            if tileEdge.rawValue >= TileEdge.northwest.rawValue && parent != nil {
                // When we're looking for a corner, verify that the parent tile is still relevant.
                // That is, the parent and child must share the corner in question.
                switch tileEdge {
                case .northwest:
                    tile = tile == parent!.northwestChild ? parent : nil
                case .northeast:
                    tile = tile == parent!.northeastChild ? parent : nil
                case .southwest:
                    tile = tile == parent!.southwestChild ? parent : nil
                case .southeast:
                    tile = tile == parent!.southeastChild ? parent : nil
                default:
                    assertionFailure("never to be here")
                }
            } else {
                tile = parent
            }
        }
        
        if tile == nil {
            return
        }

        if tile!.lastSelectionResult == .rendered {
            if tile!.data?.vertexArray != nil {
                // No further processing necessary for renderable tiles.
                return
            }
            visitTile(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, destinationTile: tile!, tileEdge: tileEdge, frameNumber: currentFrameNumber, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            return
        }

        if startTile.lastSelectionResult.original == .culled {
            return
        }

        // This tile was refined, so find rendered children, if any.
        // Visit the tiles in counter-clockwise order.
        switch tileEdge {
        case .west:
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.northwestChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.southwestChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
        case .east:
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.southeastChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.northeastChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
        case .south:
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.southwestChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.southeastChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
        case .north:
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.northeastChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.northwestChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
        case .northwest:
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.northwestChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
        case .northeast:
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.northeastChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
        case .southwest:
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.southwestChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
        case .southeast:
            visitRenderedTiles(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, startTile: startTile.southeastChild, currentFrameNumber: currentFrameNumber, tileEdge: tileEdge, downOnly: true, traversalQueue: traversalQueue, vertexArraysToDestroy: vertexArraysToDestroy)
        }
    }
    
    static func visitTile(tileProvider: GlobeSurfaceTileProvider, frameState: inout FrameState, sourceTile: QuadtreeTile, destinationTile: QuadtreeTile, tileEdge: TileEdge, frameNumber: Int, traversalQueue: Queue<QuadtreeTile>, vertexArraysToDestroy: [VertexArray]) {
        
        guard let destinationSurfaceTile = destinationTile.data else { return }

        if destinationSurfaceTile.fill == nil {
            destinationSurfaceTile.fill = TerrainFillMesh(tile: destinationTile)
        } else if destinationSurfaceTile.fill!.visitedFrame == frameNumber {
            // Don't propagate edges to tiles that have already been visited this frame.
            return
        }

        if destinationSurfaceTile.fill!.enqueuedFrame != frameNumber {
            // First time visiting this tile this frame, add it to the traversal queue.
            destinationSurfaceTile.fill!.enqueuedFrame = frameNumber
            destinationSurfaceTile.fill!.changedThisFrame = false
            traversalQueue.enqueue(destinationTile)
        }

        propagateEdge(tileProvider: tileProvider, frameState: &frameState, sourceTile: sourceTile, destinationTile: destinationTile, tileEdge: tileEdge, vertexArraysToDestroy: vertexArraysToDestroy)
    }
    
    static func propagateEdge(tileProvider: GlobeSurfaceTileProvider, frameState: inout FrameState, sourceTile: QuadtreeTile, destinationTile: QuadtreeTile, tileEdge: TileEdge, vertexArraysToDestroy: [VertexArray]) {
        
        guard let destinationFill = destinationTile.data?.fill else { return }

        let sourceMesh: TerrainMesh
        if let sourceFill = sourceTile.data?.fill {
            sourceFill.visitedFrame = frameState.frameNumber
            
            if !sourceFill.changedThisFrame && sourceFill.mesh == nil {
                sourceFill.changedThisFrame = true
            }

            // Source is a fill, create/update it if necessary.
            if sourceFill.changedThisFrame {
                sourceFill.createFillMesh(tileProvider: tileProvider, frameState: &frameState, tile: sourceTile, vertexArraysToDestroy: vertexArraysToDestroy)
                sourceFill.changedThisFrame = false
            }
            sourceMesh = sourceFill.mesh!
        } else {
            sourceMesh = sourceTile.data!.mesh!
        }
        
        var edgeMeshes: [TerrainMesh] = []
        var edgeTiles: [QuadtreeTile] = []

        switch tileEdge {
        case .west:
            edgeMeshes = destinationFill.westMeshes
            edgeTiles = destinationFill.westTiles
        case .south:
            edgeMeshes = destinationFill.southMeshes
            edgeTiles = destinationFill.southTiles
        case .east:
            edgeMeshes = destinationFill.eastMeshes
            edgeTiles = destinationFill.eastTiles
        case .north:
            edgeMeshes = destinationFill.northMeshes
            edgeTiles = destinationFill.northTiles
        // Corners are simpler.
        case .northwest:
            destinationFill.changedThisFrame = destinationFill.changedThisFrame || destinationFill.northwestMesh != sourceMesh
            destinationFill.northwestMesh = sourceMesh
            destinationFill.northwestTile = sourceTile
            return
        case .northeast:
            destinationFill.changedThisFrame = destinationFill.changedThisFrame || destinationFill.northeastMesh != sourceMesh
            destinationFill.northeastMesh = sourceMesh
            destinationFill.northeastTile = sourceTile
            return
        case .southwest:
            destinationFill.changedThisFrame = destinationFill.changedThisFrame || destinationFill.southwestMesh != sourceMesh
            destinationFill.southwestMesh = sourceMesh
            destinationFill.southwestTile = sourceTile
            return
        case .southeast:
            destinationFill.changedThisFrame = destinationFill.changedThisFrame || destinationFill.southeastMesh != sourceMesh
            destinationFill.southeastMesh = sourceMesh
            destinationFill.southeastTile = sourceTile
            return
        }
        
        func restoreToOriginal(tiles: [QuadtreeTile], meshes: [TerrainMesh]) {
            switch tileEdge {
            case .west:
                destinationFill.westMeshes = meshes
                destinationFill.westTiles = tiles
            case .south:
                destinationFill.southMeshes = meshes
                destinationFill.southTiles = tiles
            case .east:
                destinationFill.eastMeshes = meshes
                destinationFill.eastTiles = tiles
            case .north:
                destinationFill.northMeshes = meshes
                destinationFill.northTiles = tiles
            default:
                assertionFailure("never to be here")
            }
        }

        if sourceTile.level <= destinationTile.level {
            // Source edge completely spans the destination edge.
            //destinationFill.changedThisFrame = destinationFill.changedThisFrame || edgeMeshes[0] != sourceMesh || edgeMeshes.count != 1 // original
            destinationFill.changedThisFrame = destinationFill.changedThisFrame || (edgeMeshes.count > 0 && edgeMeshes[0] != sourceMesh) || edgeMeshes.count != 1 // safer
            edgeMeshes = [sourceMesh]
            edgeTiles = [sourceTile]
            restoreToOriginal(tiles: edgeTiles, meshes: edgeMeshes)
            return
        }
        
        // Source edge is a subset of the destination edge.
        // Figure out the range of meshes we're replacing.
        var startIndex: Int = 0
        var endIndex: Int = 0
        var existingTile: QuadtreeTile
        var existingRectangle: Rectangle
        let sourceRectangle = sourceTile.rectangle

        let epsilon: Double
        let destinationRectangle = destinationTile.rectangle

        switch tileEdge {
        case .west:
            epsilon = (destinationRectangle.north - destinationRectangle.south) * Math.Epsilon5

            for i in 0..<edgeTiles.count {
                startIndex = i
                existingTile = edgeTiles[startIndex]
                existingRectangle = existingTile.rectangle
                if Math.greaterThan(sourceRectangle.north, existingRectangle.south, epsilon) {
                    break
                }
                
            }
            for i in startIndex..<edgeTiles.count {
                endIndex = i
                existingTile = edgeTiles[endIndex]
                existingRectangle = existingTile.rectangle
                if Math.greaterThanOrEquals(sourceRectangle.south, existingRectangle.north, epsilon) {
                    break
                }
            }
        case .south:
            epsilon = (destinationRectangle.east - destinationRectangle.west) * Math.Epsilon5

            for i in 0..<edgeTiles.count {
                startIndex = i
                existingTile = edgeTiles[startIndex]
                existingRectangle = existingTile.rectangle
                if Math.lessThan(sourceRectangle.west, existingRectangle.east, epsilon) {
                    break
                }
            }
            for i in startIndex..<edgeTiles.count {
                endIndex = i
                existingTile = edgeTiles[endIndex]
                existingRectangle = existingTile.rectangle
                if Math.lessThanOrEquals(sourceRectangle.east, existingRectangle.west, epsilon) {
                    break
                }
            }
        case .east:
            epsilon = (destinationRectangle.north - destinationRectangle.south) * Math.Epsilon5

            for i in 0..<edgeTiles.count {
                startIndex = i
                existingTile = edgeTiles[startIndex]
                existingRectangle = existingTile.rectangle
                if Math.lessThan(sourceRectangle.south, existingRectangle.north, epsilon) {
                    break
                }
            }
            for i in startIndex..<edgeTiles.count {
                endIndex = i
                existingTile = edgeTiles[endIndex]
                existingRectangle = existingTile.rectangle
                if Math.lessThanOrEquals(sourceRectangle.north, existingRectangle.south, epsilon) {
                    break
                }
            }
        case .north:
            epsilon = (destinationRectangle.east - destinationRectangle.west) * Math.Epsilon5

            for i in 0..<edgeTiles.count {
                startIndex = i
                existingTile = edgeTiles[startIndex]
                existingRectangle = existingTile.rectangle
                if Math.greaterThan(sourceRectangle.east, existingRectangle.west, epsilon) {
                    break
                }
            }
            for i in startIndex..<edgeTiles.count {
                endIndex = i
                existingTile = edgeTiles[endIndex]
                existingRectangle = existingTile.rectangle
                if Math.greaterThanOrEquals(sourceRectangle.west, existingRectangle.east, epsilon) {
                    break
                }
            }
        default:
            assertionFailure("never to be here")
        }
        
        if (endIndex - startIndex) == 1 {
            destinationFill.changedThisFrame = destinationFill.changedThisFrame || edgeMeshes[startIndex] != sourceMesh
            edgeMeshes[startIndex] = sourceMesh
            edgeTiles[startIndex] = sourceTile
        } else {
            destinationFill.changedThisFrame = true
            edgeMeshes.removeSubrange(startIndex..<(startIndex + (endIndex - startIndex)))
            edgeMeshes.append(sourceMesh)
            //edgeMeshes.splice(startIndex, endIndex - startIndex, sourceMesh)
            edgeTiles.removeSubrange(startIndex..<(startIndex + (endIndex - startIndex)))
            edgeTiles.append(sourceTile)
            //edgeTiles.splice(startIndex, endIndex - startIndex, sourceTile)
        }
        restoreToOriginal(tiles: edgeTiles, meshes: edgeMeshes)
    }
    
    private func createFillMesh(tileProvider: GlobeSurfaceTileProvider, frameState: inout FrameState, tile: QuadtreeTile, vertexArraysToDestroy: [VertexArray]? = nil) {
        GlobeSurfaceTile.initialize(tile: tile, terrainProvider: tileProvider.terrainProvider, imageryLayerCollection: tileProvider.imageryLayers)
        
        let surfaceTile = tile.data!
        // TODO: needs to ensure fill != nil
        guard let fill = surfaceTile.fill else { return }
        let rectangle = tile.rectangle
        
        let ellipsoid = tile.tilingScheme.ellipsoid
        
        var swVertexScratch = HeightAndNormal()
        var seVertexScratch = HeightAndNormal()
        var nwVertexScratch = HeightAndNormal()
        var neVertexScratch = HeightAndNormal()
        var nwCorner = getCorner(terrainFillMesh: fill, ellipsoid: ellipsoid, u: 0.0, v: 1.0, cornerTile: fill.northwestTile, cornerMesh: fill.northwestMesh, previousEdgeTiles: fill.northTiles, previousEdgeMeshes: fill.northMeshes, nextEdgeTiles: fill.westTiles, nextEdgeMeshes: fill.westMeshes, vertex: &nwVertexScratch)
        var swCorner = getCorner(terrainFillMesh: fill, ellipsoid: ellipsoid, u: 0.0, v: 0.0, cornerTile: fill.southwestTile, cornerMesh: fill.southwestMesh, previousEdgeTiles: fill.westTiles, previousEdgeMeshes: fill.westMeshes, nextEdgeTiles: fill.southTiles, nextEdgeMeshes: fill.southMeshes, vertex: &swVertexScratch);
        var seCorner = getCorner(terrainFillMesh: fill, ellipsoid: ellipsoid, u: 1.0, v: 0.0, cornerTile: fill.southeastTile, cornerMesh: fill.southeastMesh, previousEdgeTiles: fill.southTiles, previousEdgeMeshes: fill.southMeshes, nextEdgeTiles: fill.eastTiles, nextEdgeMeshes: fill.eastMeshes, vertex: &seVertexScratch);
        var neCorner = getCorner(terrainFillMesh: fill, ellipsoid: ellipsoid, u: 1.0, v: 1.0, cornerTile: fill.northeastTile, cornerMesh: fill.northeastMesh, previousEdgeTiles: fill.eastTiles, previousEdgeMeshes: fill.eastMeshes, nextEdgeTiles: fill.northTiles, nextEdgeMeshes: fill.northMeshes, vertex: &neVertexScratch);
        
        nwCorner = fillMissingCorner(fill: fill, ellipsoid: ellipsoid, u: 0.0, v: 1.0, corner: nwCorner, adjacentCorner1: swCorner, adjacentCorner2: neCorner, oppositeCorner: seCorner, vertex: &nwVertexScratch)
        swCorner = fillMissingCorner(fill: fill, ellipsoid: ellipsoid, u: 0.0, v: 0.0, corner: swCorner, adjacentCorner1: nwCorner, adjacentCorner2: seCorner, oppositeCorner: neCorner, vertex: &swVertexScratch)
        seCorner = fillMissingCorner(fill: fill, ellipsoid: ellipsoid, u: 1.0, v: 1.0, corner: seCorner, adjacentCorner1: swCorner, adjacentCorner2: neCorner, oppositeCorner: nwCorner, vertex: &seVertexScratch)
        neCorner = fillMissingCorner(fill: fill, ellipsoid: ellipsoid, u: 1.0, v: 1.0, corner: neCorner, adjacentCorner1: seCorner, adjacentCorner2: nwCorner, oppositeCorner: swCorner, vertex: &neVertexScratch)
        
        // fillMissingCorner returns non optional. Safe to force unwrap
        let southwestHeight = swCorner!.height
        let southeastHeight = seCorner!.height
        let northwestHeight = nwCorner!.height
        let northeastHeight = neCorner!.height
        
        var minimumHeight = min(southwestHeight, southeastHeight, northwestHeight, northeastHeight)
        var maximumHeight = max(southwestHeight, southeastHeight, northwestHeight, northeastHeight)
        
        let middleHeight = (minimumHeight + maximumHeight) * 0.5
        
        // For low-detail tiles, our usual fill tile approach will create tiles that
        // look really blocky because they don't have enough vertices to account for the
        // Earth's curvature. But the height range will also typically be well within
        // the allowed geometric error for those levels. So fill such tiles with a
        // constant-height heightmap.
        let geometricError = tileProvider.levelMaximumGeometricError(tile.level)
        let minCutThroughRadius = ellipsoid.maximumRadius - geometricError
        var maxTileWidth = acos(minCutThroughRadius / ellipsoid.maximumRadius) * 4.0
        
        // When the tile width is greater than maxTileWidth as computed above, the error
        // of a normal fill tile from globe curvature alone will exceed the allowed geometric
        // error. Terrain won't change that much. However, we can allow more error than that.
        // A little blockiness during load is acceptable. For the WGS84 ellipsoid and
        // standard geometric error setup, the value here will have us use a heightmap
        // at levels 1, 2, and 3.
        maxTileWidth *= 1.5
        
        if rectangle.width > maxTileWidth && (maximumHeight - minimumHeight) <= geometricError {
            var heightMapStruct = HeightmapStructure()
            // Use the maximum as the constant height so that this tile's skirt
            // covers any cracks with adjacent tiles.
            heightMapStruct.heightOffset = maximumHeight
            let terrainData = HeightmapTerrainData(buffer: heightmapBuffer, width: 9, height: 9, structure: heightMapStruct)
            fill.mesh = terrainData.createMeshSync(tilingScheme: tile.tilingScheme, x: tile.x, y: tile.y, level: tile.level)
        } else {
            var encoding = TerrainEncodingEmpty()
            
            var centerCartographic = Cartographic()
            centerCartographic.longitude = (rectangle.east + rectangle.west) * 0.5
            centerCartographic.latitude = (rectangle.north + rectangle.south) * 0.5
            centerCartographic.height = middleHeight
            encoding.center = ellipsoid.cartographicToCartesian(centerCartographic)
            
            // At _most_, we have vertices for the 4 corners, plus 1 center, plus every adjacent edge vertex.
            // In reality there will be less most of the time, but close enough; better
            // to overestimate than to re-allocate/copy/traverse the vertices twice.
            // Also, we'll often be able to squeeze the index data into the extra space in the buffer.
            var maxVertexCount = 5
            var meshes: [TerrainMesh] = []
            
            meshes = fill.westMeshes
            for mesh in meshes {
                maxVertexCount += mesh.eastIndicesNorthToSouth.count
            }

            meshes = fill.southMeshes
            for mesh in meshes {
                maxVertexCount += mesh.northIndicesWestToEast.count
            }

            meshes = fill.eastMeshes
            for mesh in meshes {
                maxVertexCount += mesh.westIndicesSouthToNorth.count
            }

            meshes = fill.northMeshes
            for mesh in meshes {
                maxVertexCount += mesh.southIndicesEastToWest.count
            }
            
            var heightRange = HeightRange()
            heightRange.minimumHeight = minimumHeight
            heightRange.maximumHeight = maximumHeight
            
            //let encodingStride = encoding.getStride()
            //var typedArray = [Float32](repeating: 0.0, count: maxVertexCount * stride)
            var typedArray: [Float32] = []
            
            var nextIndex = 0
            let northwestIndex = nextIndex
            nextIndex = addVertexWithComputedPosition(ellipsoid: ellipsoid, rectangle: rectangle, encoding: encoding, buffer: &typedArray, index: nextIndex, u: 0.0, v: 1.0, height: nwCorner!.height, encodedNormal: nwCorner!.encodedNormal, webMercatorT: 1.0, heightRange: &heightRange)
            nextIndex = addEdge(terrainFillMesh: fill, ellipsoid: ellipsoid, encoding: encoding, typedArray: &typedArray, nextIndex: nextIndex, edgeTiles: fill.westTiles, edgeMeshes: fill.westMeshes, tileEdge: .east, heightRange: &heightRange)
            let southwestIndex = nextIndex
            nextIndex = addVertexWithComputedPosition(ellipsoid: ellipsoid, rectangle: rectangle, encoding: encoding, buffer: &typedArray, index: nextIndex, u: 0.0, v: 0.0, height: swCorner!.height, encodedNormal: swCorner!.encodedNormal, webMercatorT: 0.0, heightRange: &heightRange)
            nextIndex = addEdge(terrainFillMesh: fill, ellipsoid: ellipsoid, encoding: encoding, typedArray: &typedArray, nextIndex: nextIndex, edgeTiles: fill.southTiles, edgeMeshes: fill.southMeshes, tileEdge: .north, heightRange: &heightRange)
            let southeastIndex = nextIndex
            nextIndex = addVertexWithComputedPosition(ellipsoid: ellipsoid, rectangle: rectangle, encoding: encoding, buffer: &typedArray, index: nextIndex, u: 1.0, v: 0.0, height: seCorner!.height, encodedNormal: seCorner!.encodedNormal, webMercatorT: 0.0, heightRange: &heightRange)
            nextIndex = addEdge(terrainFillMesh: fill, ellipsoid: ellipsoid, encoding: encoding, typedArray: &typedArray, nextIndex: nextIndex, edgeTiles: fill.eastTiles, edgeMeshes: fill.eastMeshes, tileEdge: .west, heightRange: &heightRange)
            let northeastIndex = nextIndex
            nextIndex = addVertexWithComputedPosition(ellipsoid: ellipsoid, rectangle: rectangle, encoding: encoding, buffer: &typedArray, index: nextIndex, u: 1.0, v: 1.0, height: neCorner!.height, encodedNormal: neCorner!.encodedNormal, webMercatorT: 1.0, heightRange: &heightRange)
            nextIndex = addEdge(terrainFillMesh: fill, ellipsoid: ellipsoid, encoding: encoding, typedArray: &typedArray, nextIndex: nextIndex, edgeTiles: fill.northTiles, edgeMeshes: fill.northMeshes, tileEdge: .south, heightRange: &heightRange)

            minimumHeight = heightRange.minimumHeight
            maximumHeight = heightRange.maximumHeight
            
            let obb = OrientedBoundingBox.init(fromRectangle: rectangle, minimumHeight: minimumHeight, maximumHeight: maximumHeight, ellipsoid: tile.tilingScheme.ellipsoid)

            /*
            // Add a single vertex at the center of the tile.
            var southMercatorY = WebMercatorProjection.geodeticLatitudeToMercatorAngle(rectangle.south);
            var oneOverMercatorHeight = 1.0 / (WebMercatorProjection.geodeticLatitudeToMercatorAngle(rectangle.north) - southMercatorY);
            var centerWebMercatorT = (WebMercatorProjection.geodeticLatitudeToMercatorAngle(centerCartographic.latitude) - southMercatorY) * oneOverMercatorHeight;
            */

            let normalScratch = ellipsoid.geodeticSurfaceNormalCartographic(cartographicScratch)
            let centerEncodedNormal = AttributeCompression.octEncode(normalScratch)

            let centerIndex = nextIndex
            //encoding.encode(typedArray, nextIndex * stride, obb.center, Cartesian2.fromElements(0.5, 0.5, uvScratch), middleHeight, centerEncodedNormal, centerWebMercatorT);
            let uvScratch = Cartesian2(x: 0.5, y: 0.5)
            encoding.encode(&typedArray, position: obb.center, uv: uvScratch, height: middleHeight, normalToPack: centerEncodedNormal)
            nextIndex += 1

            let vertexCount = nextIndex

            /*
            let bytesPerIndex = vertexCount < 256 ? 1 : 2
            let indexDataBytes = indexCount * bytesPerIndex
            let availableBytesInBuffer = (typedArray.count - vertexCount * encodingStride) * MemoryLayout<Float32>.size
            */

            let indexCount = (vertexCount - 1) * 3 // one triangle per edge vertex
            var indices = [Int](repeating: 0, count: indexCount)
            /*
            if availableBytesInBuffer >= indexDataBytes {
                // Store the index data in the same buffer as the vertex data.
                var startIndex = vertexCount * stride * Float32Array.BYTES_PER_ELEMENT;
                indices = vertexCount < 256
                    ? new Uint8Array(typedArray.buffer, startIndex, indexCount)
                    : new Uint16Array(typedArray.buffer, startIndex, indexCount);
            } else {
                // Allocate a new buffer for the index data.
                indices = vertexCount < 256 ? new Uint8Array(indexCount) : new Uint16Array(indexCount);
            }
            typedArray = new Float32Array(typedArray.buffer, 0, vertexCount * stride)
            */

            var indexOut = 0;
            for i in 0..<(vertexCount - 2) {
                indices[indexOut] = centerIndex
                indices[indexOut + 1] = i
                indices[indexOut + 2] = i + 1
                indexOut += 3
            }

            indices[indexOut] = centerIndex
            indices[indexOut + 1] = vertexCount - 3//i
            indices[indexOut + 2] = 0
            indexOut += 3

            var westIndicesSouthToNorth: [Int] = []
            //for (i = southwestIndex; i >= northwestIndex; --i)
            for i in stride(from: southwestIndex, through: northwestIndex, by: -1) {
                westIndicesSouthToNorth.append(i)
            }

            var southIndicesEastToWest: [Int] = []
            //for (i = southeastIndex; i >= southwestIndex; --i)
            for i in stride(from: southeastIndex, through: southwestIndex, by: -1) {
                southIndicesEastToWest.append(i)
            }

            var eastIndicesNorthToSouth: [Int] = []
            //for (i = northeastIndex; i >= southeastIndex; --i)
            for i in stride(from: northeastIndex, through: southeastIndex, by: -1) {
                eastIndicesNorthToSouth.append(i)
            }

            var northIndicesWestToEast: [Int] = [0]
            //northIndicesWestToEast.push(0);
            //for (i = centerIndex - 1; i >= northeastIndex; --i)
            for i in stride(from: centerIndex - 1, through: northeastIndex, by: -1) {
                northIndicesWestToEast.append(i)
            }
            
            let occludeePoint = Cartesian3()
            fill.mesh = TerrainMesh(center: encoding.center, vertices: typedArray, indices: indices, minimumHeight: minimumHeight, maximumHeight: maximumHeight, boundingSphere3D: BoundingSphere.fromOrientedBoundingBox(obb), occludeePointInScaledSpace: occludeePoint, orientedBoundingBox: obb, encoding: encoding, exaggeration: frameState.terrainExaggeration, westIndicesSouthToNorth: westIndicesSouthToNorth, southIndicesEastToWest: southIndicesEastToWest, eastIndicesNorthToSouth: eastIndicesNorthToSouth, northIndicesWestToEast: northIndicesWestToEast)
        }
        let context = frameState.context!

        if let vertexArray = fill.vertexArray {
            if var vertexArraysToDestroy = vertexArraysToDestroy {
                vertexArraysToDestroy.append(vertexArray)
            } else {
                //GlobeSurfaceTile._freeVertexArray(fill.vertexArray);
                fill.vertexArray = nil
            }
        }

        fill.vertexArray = GlobeSurfaceTile.createVertexArrayForMesh(context: context, mesh: fill.mesh!)
        _ = surfaceTile.processImagery(tile: tile, terrainProvider: tileProvider.terrainProvider, frameState: &frameState, skipLoading: true)

        /*
        let oldTexture = fill.waterMaskTexture
        fill.waterMaskTexture = nil

        if tileProvider.terrainProvider.hasWaterMask {
            var waterSourceTile = surfaceTile._findAncestorTileWithTerrainData(tile);
            if (defined(waterSourceTile) && defined(waterSourceTile.data.waterMaskTexture)) {
                fill.waterMaskTexture = waterSourceTile.data.waterMaskTexture;
                ++fill.waterMaskTexture.referenceCount;
                surfaceTile._computeWaterMaskTranslationAndScale(tile, waterSourceTile, fill.waterMaskTranslationAndScale);
            }
        }

        if oldTexture != nil {
            oldTexture.referenceCount -= 1
            if (oldTexture.referenceCount === 0) {
                oldTexture.destroy();
            }
        }
        */
    }
    
    private func addVertexWithComputedPosition(ellipsoid: Ellipsoid, rectangle: Rectangle, encoding: TerrainEncodingProtocol, buffer: inout [Float], index: Int, u: Double, v: Double, height: Double, encodedNormal: Cartesian2, webMercatorT: Double, heightRange: inout HeightRange) -> Int {
        var cartographic = Cartographic()
        cartographic.longitude = Math.lerp(p: rectangle.west, q: rectangle.east, time: u)
        cartographic.latitude = Math.lerp(p: rectangle.south, q: rectangle.north, time: v)
        cartographic.height = height
        let position = ellipsoid.cartographicToCartesian(cartographic)

        var uv = Cartesian2()
        uv.x = u
        uv.y = v

        // buffer index ignored
        encoding.encode(&buffer, position: position, uv: uv, height: height, normalToPack: encodedNormal)

        heightRange.minimumHeight = min(heightRange.minimumHeight, height)
        heightRange.maximumHeight = max(heightRange.maximumHeight, height)

        return index + 1
    }
    
    private func addEdge(terrainFillMesh: TerrainFillMesh, ellipsoid: Ellipsoid, encoding: TerrainEncodingProtocol, typedArray: inout [Float], nextIndex: Int, edgeTiles: [QuadtreeTile], edgeMeshes: [TerrainMesh], tileEdge: TileEdge, heightRange: inout HeightRange) -> Int {
        
        var index = nextIndex
        for i in 0..<edgeTiles.count {
            index = addEdgeMesh(terrainFillMesh: terrainFillMesh, ellipsoid: ellipsoid, encoding: encoding, typedArray: &typedArray, nextIndex: index, edgeTile: edgeTiles[i], edgeMesh: edgeMeshes[i], tileEdge: tileEdge, heightRange: &heightRange)
        }
        return index
    }
    
    private func addEdgeMesh(terrainFillMesh: TerrainFillMesh, ellipsoid: Ellipsoid, encoding: TerrainEncodingProtocol, typedArray: inout [Float], nextIndex: Int, edgeTile: QuadtreeTile, edgeMesh: TerrainMesh, tileEdge: TileEdge, heightRange: inout HeightRange) -> Int {
        
        // Handle copying edges across the anti-meridian.
        var sourceRectangle = edgeTile.rectangle
        if tileEdge == .east && terrainFillMesh.tile.x == 0 {
            //sourceRectangle = Rectangle.clone(edgeTile.rectangle, sourceRectangleScratch);
            sourceRectangle.west -= Math.TwoPi
            sourceRectangle.east -= Math.TwoPi
        } else if tileEdge == .west && edgeTile.x == 0 {
            //sourceRectangle = Rectangle.clone(edgeTile.rectangle, sourceRectangleScratch);
            sourceRectangle.west += Math.TwoPi
            sourceRectangle.east += Math.TwoPi
        }
        
        //let targetRectangle = terrainFillMesh.tile.rectangle
        
        var lastU: Double = 0.0
        var lastV: Double = 0.0
        if (nextIndex > 0) {
            let uvScratch = encoding.decodeTextureCoordinates(typedArray, index: nextIndex - 1)
            lastU = uvScratch.x
            lastV = uvScratch.y
        }
        
        let indices: [Int]
        let compareU: Bool

        switch tileEdge {
        case .west:
            indices = edgeMesh.westIndicesSouthToNorth
            compareU = false
        case .north:
            indices = edgeMesh.northIndicesWestToEast
            compareU = true
        case .east:
            indices = edgeMesh.eastIndicesNorthToSouth
            compareU = false
        case .south:
            indices = edgeMesh.southIndicesEastToWest
            compareU = true
        default:
            assertionFailure("invalid tile edge: \(tileEdge)")
            indices = []
            compareU = false
        }
        
        let sourceTile = edgeTile
        let targetTile = terrainFillMesh.tile
        let sourceEncoding = edgeMesh.encoding
        let sourceVertices = edgeMesh.vertices
        //let targetStride = encoding.getStride()
        
        /*
        var southMercatorY;
        var oneOverMercatorHeight;
        if (sourceEncoding.hasWebMercatorT) {
            southMercatorY = WebMercatorProjection.geodeticLatitudeToMercatorAngle(targetRectangle.south);
            oneOverMercatorHeight = 1.0 / (WebMercatorProjection.geodeticLatitudeToMercatorAngle(targetRectangle.north) - southMercatorY);
        }
        */
        var returnIndex = nextIndex
        for index in indices {
            var uv = sourceEncoding.decodeTextureCoordinates(sourceVertices, index: index)
            uv = transformTextureCoordinates(sourceTile: sourceTile, targetTile: targetTile, coordinates: uv)
            let u = uv.x
            let v = uv.y
            let uOrV = compareU ? u : v
            
            if uOrV < 0.0 || uOrV > 1.0 {
                // Vertex is outside the target tile - skip it.
                continue
            }

            if abs(u - lastU) < Math.Epsilon5 && abs(v - lastV) < Math.Epsilon5 {
                // Vertex is very close to the previous one - skip it.
                continue
            }

            let nearlyEdgeU = abs(u) < Math.Epsilon5 || abs(u - 1.0) < Math.Epsilon5
            let nearlyEdgeV = abs(v) < Math.Epsilon5 || abs(v - 1.0) < Math.Epsilon5

            if nearlyEdgeU && nearlyEdgeV {
                // Corner vertex - skip it.
                continue
            }

            //var position = sourceEncoding.decodePosition(sourceVertices, index, cartesianScratch);
            let position = sourceEncoding.decodePosition(sourceVertices, index: index)
            let height = sourceEncoding.decodeHeight(sourceVertices, index: index)

            let normal: Cartesian2
            if sourceEncoding.hasVertexNormals {
                normal = sourceEncoding.octEncodedNormal(sourceVertices, index: index)
            } else {
                normal = Cartesian2(x: 0.0, y: 0.0)
            }

            /*
            var webMercatorT = v;
            if (sourceEncoding.hasWebMercatorT) {
                var latitude = CesiumMath.lerp(targetRectangle.south, targetRectangle.north, v);
                webMercatorT = (WebMercatorProjection.geodeticLatitudeToMercatorAngle(latitude) - southMercatorY) * oneOverMercatorHeight;
            }
            */

            //encoding.encode(typedArray, nextIndex * targetStride, position, uv, height, normal, webMercatorT);
            encoding.encode(&typedArray, position: position, uv: uv, height: height, normalToPack: normal)

            heightRange.minimumHeight = min(heightRange.minimumHeight, height)
            heightRange.maximumHeight = max(heightRange.maximumHeight, height)

            returnIndex += 1
        }
        return returnIndex
    }
    
    private func getCorner(terrainFillMesh: TerrainFillMesh, ellipsoid: Ellipsoid, u: Double, v: Double, cornerTile: QuadtreeTile?, cornerMesh: TerrainMesh?, previousEdgeTiles: [QuadtreeTile], previousEdgeMeshes: [TerrainMesh], nextEdgeTiles: [QuadtreeTile], nextEdgeMeshes: [TerrainMesh], vertex: inout HeightAndNormal) -> HeightAndNormal? {
        
        guard let cornerTile = cornerTile else { return nil }
        
        let gotCorner: Bool = (
            getCornerFromEdge(terrainFillMesh: terrainFillMesh, ellipsoid: ellipsoid, edgeMeshes: previousEdgeMeshes, edgeTiles: previousEdgeTiles, isNext: false, u: u, v: v, vertex: &vertex) ||
            getCornerFromEdge(terrainFillMesh: terrainFillMesh, ellipsoid: ellipsoid, edgeMeshes: nextEdgeMeshes, edgeTiles: nextEdgeTiles, isNext: true, u: u, v: v, vertex: &vertex)
        )
        if gotCorner {
            return vertex
        }
        
        if meshIsUsable(tile: cornerTile, mesh: cornerMesh) {
            // If usable, mesh is not nil
            guard let cornerMesh = cornerMesh else { return nil }
            let vertexIndex: Int
            // Corner mesh is valid, copy its corner vertex to this mesh.
            if u == 0.0 {
                if v == 0.0 {
                    // southwest destination, northeast source
                    vertexIndex = cornerMesh.eastIndicesNorthToSouth[0]
                } else {
                    // northwest destination, southeast source
                    vertexIndex = cornerMesh.southIndicesEastToWest[0]
                }
            } else if v == 0.0 {
                // southeast destination, northwest source
                vertexIndex = cornerMesh.northIndicesWestToEast[0]
            } else {
                // northeast destination, southwest source
                vertexIndex = cornerMesh.westIndicesSouthToNorth[0]
            }
            vertexFromTileAtCorner(sourceMesh: cornerMesh, sourceIndex: vertexIndex, u: u, v: v, vertex: &vertex)
            return vertex
        }
        
        // There is no precise vertex available from the corner or from either adjacent edge.
        // This is either because there are no tiles at all at the edges and corner, or
        // because the tiles at the edge are higher-level-number and don't extend all the way
        // to the corner.
        // Try to grab a height from the adjacent edges.
        var height: Double? = nil
        if u == 0.0 {
            if v == 0.0 {
                // southwest
                height = closestHeightToCorner(
                    previousMeshes: terrainFillMesh.westMeshes, previousTiles: terrainFillMesh.westTiles, previousEdge: .east, nextMeshes: terrainFillMesh.southMeshes, nextTiles: terrainFillMesh.southTiles, nextEdge: .north, u: u, v: v)
            } else {
                // northwest
                height = closestHeightToCorner(
                    previousMeshes: terrainFillMesh.northMeshes, previousTiles: terrainFillMesh.northTiles, previousEdge: .south, nextMeshes: terrainFillMesh.westMeshes, nextTiles: terrainFillMesh.westTiles, nextEdge: .east, u: u, v: v)
            }
        } else if v == 0.0 {
            // southeast
            height = closestHeightToCorner(
                previousMeshes: terrainFillMesh.southMeshes, previousTiles: terrainFillMesh.southTiles, previousEdge: .north, nextMeshes: terrainFillMesh.eastMeshes, nextTiles: terrainFillMesh.eastTiles, nextEdge: .west, u: u, v: v)
        } else {
            // northeast
            height = closestHeightToCorner(
                previousMeshes: terrainFillMesh.eastMeshes, previousTiles: terrainFillMesh.eastTiles, previousEdge: .west, nextMeshes: terrainFillMesh.northMeshes, nextTiles: terrainFillMesh.northTiles, nextEdge: .south, u: u, v: v)
        }

        if height != nil {
            vertexWithHeightAtCorner(terrainFillMesh: terrainFillMesh, ellipsoid: ellipsoid, u: u, v: v, height: height!, vertex: &vertex)
            return vertex
        }

        // No heights available that are closer than the adjacent corners.
        return nil
    }
    
    private func fillMissingCorner(fill: TerrainFillMesh, ellipsoid: Ellipsoid, u: Double, v: Double, corner: HeightAndNormal?, adjacentCorner1: HeightAndNormal?, adjacentCorner2: HeightAndNormal?, oppositeCorner: HeightAndNormal?, vertex: inout HeightAndNormal) -> HeightAndNormal {

        if let corner = corner {
            return corner
        }
        
        let height: Double
        if let adjacentCorner1 = adjacentCorner1, let adjacentCorner2 = adjacentCorner2 {
            height = (adjacentCorner1.height + adjacentCorner2.height) * 0.5
        } else if let adjacentCorner1 = adjacentCorner1 {
            height = adjacentCorner1.height
        } else if let adjacentCorner2 = adjacentCorner2 {
            height = adjacentCorner2.height
        } else if let oppositeCorner = oppositeCorner {
            height = oppositeCorner.height
        } else {
            let surfaceTile = fill.tile.data
            let tileBoundingRegion = surfaceTile?.tileBoundingRegion
            var minimumHeight = 0.0
            var maximumHeight = 0.0
            if let tileBoundingRegion = tileBoundingRegion {
                minimumHeight = tileBoundingRegion.minimumHeight
                maximumHeight = tileBoundingRegion.maximumHeight
            }
            height = (minimumHeight + maximumHeight) * 0.5
        }
        
        vertexWithHeightAtCorner(terrainFillMesh: fill, ellipsoid: ellipsoid, u: u, v: v, height: height, vertex: &vertex)
        return vertex
    }
    
    private func closestHeightToCorner(previousMeshes: [TerrainMesh], previousTiles: [QuadtreeTile], previousEdge: TileEdge, nextMeshes: [TerrainMesh], nextTiles: [QuadtreeTile], nextEdge: TileEdge, u: Double, v: Double) -> Double? {
        let height1 = nearestHeightOnEdge(meshes: previousMeshes, tiles: previousTiles, isNext: false, edge: previousEdge, u: u, v: v)
        let height2 = nearestHeightOnEdge(meshes: nextMeshes, tiles: nextTiles, isNext: true, edge: nextEdge, u: u, v: v)
        if height1 != nil && height2 != nil {
            // It would be slightly better to do a weighted average of the two heights
            // based on their distance from the corner, but it shouldn't matter much in practice.
            return (height1! + height2!) * 0.5
        } else if height1 != nil {
            return height1
        }
        return height2
    }
    
    private func nearestHeightOnEdge(meshes: [TerrainMesh], tiles: [QuadtreeTile], isNext: Bool, edge: TileEdge, u: Double, v: Double) -> Double? {
        let meshStart: Int
        let meshEnd: Int
        let meshStep: Int

        if isNext {
            meshStart = 0
            meshEnd = meshes.count
            meshStep = 1
        } else {
            meshStart = meshes.count - 1
            meshEnd = -1
            meshStep = -1
        }
        
        for meshIndex in stride(from: meshStart, to: meshEnd, by: meshStep) {
            let mesh = meshes[meshIndex]
            let tile = tiles[meshIndex]
            if !meshIsUsable(tile: tile, mesh: mesh) {
                continue
            }

            let indices: [Int]
            switch edge {
            case .west:
                indices = mesh.westIndicesSouthToNorth
            case .south:
                indices = mesh.southIndicesEastToWest
            case .east:
                indices = mesh.eastIndicesNorthToSouth
            case .north:
                indices = mesh.northIndicesWestToEast
            default:
                assertionFailure("invalid tile edge: \(edge)")
                indices = []
            }

            let index = indices.at(index: isNext ? 0 : indices.count - 1)
            //let index = indices[isNext ? 0 : indices.count - 1]
            if index != nil {
                return mesh.encoding.decodeHeight(mesh.vertices, index: index!)
            }
        }

        return nil
    }
    
    private func meshIsUsable(tile: QuadtreeTile?, mesh: TerrainMesh?) -> Bool {
        return mesh != nil && (tile?.data?.fill == nil || !tile!.data!.fill!.changedThisFrame)
    }
    
    private func getCornerFromEdge(terrainFillMesh: TerrainFillMesh, ellipsoid: Ellipsoid, edgeMeshes: [TerrainMesh], edgeTiles: [QuadtreeTile], isNext: Bool, u: Double, v: Double, vertex: inout HeightAndNormal) -> Bool {

        var edgeVertices: [Int] = []
        var compareU: Bool = false
        var increasing: Bool = false
        var vertexIndexIndex: Int = 0
        var vertexIndex: Int = 0
        let edgeIndex = isNext ? 0 : edgeMeshes.count - 1
        let testTile = edgeTiles.count < 1 ? nil : edgeTiles[edgeIndex]
        let testMesh = edgeMeshes.count < 1 ? nil : edgeMeshes[edgeIndex]
        /*
        let sourceTile = edgeTiles[isNext ? 0 : edgeMeshes.count - 1]
        let sourceMesh = edgeMeshes[isNext ? 0 : edgeMeshes.count - 1]
        */
        
        var uv: Cartesian2 = Cartesian2()
        
        if meshIsUsable(tile: testTile, mesh: testMesh) {
            guard let sourceMesh = testMesh, let sourceTile = testTile else { return false }
            // Previous mesh is valid, but we don't know yet if it covers this corner.
            if u == 0.0 {
                if v == 0.0 {
                    // southwest
                    edgeVertices = isNext ? sourceMesh.northIndicesWestToEast : sourceMesh.eastIndicesNorthToSouth
                    compareU = isNext
                    increasing = isNext
                } else {
                    // northwest
                    edgeVertices = isNext ? sourceMesh.eastIndicesNorthToSouth : sourceMesh.southIndicesEastToWest
                    compareU = !isNext
                    increasing = false
                }
            } else if v == 0.0 {
                // southeast
                edgeVertices = isNext ? sourceMesh.westIndicesSouthToNorth : sourceMesh.northIndicesWestToEast
                compareU = !isNext
                increasing = true
            } else {
                // northeast
                edgeVertices = isNext ? sourceMesh.southIndicesEastToWest : sourceMesh.westIndicesSouthToNorth
                compareU = isNext
                increasing = !isNext
            }
            
            if edgeVertices.count > 0 {
                // The vertex we want will very often be the first/last vertex so check that first.
                vertexIndexIndex = isNext ? 0 : edgeVertices.count - 1
                vertexIndex = edgeVertices[vertexIndexIndex]
                uv = sourceMesh.encoding.decodeTextureCoordinates(sourceMesh.vertices, index: vertexIndex)
                let targetUv = transformTextureCoordinates(sourceTile: sourceTile, targetTile: terrainFillMesh.tile, coordinates: uv)
                if targetUv.x == u && targetUv.y == v {
                    // Vertex is good!
                    vertexFromTileAtCorner(sourceMesh: sourceMesh, sourceIndex: vertexIndex, u: u, v: v, vertex: &vertex)
                    return true
                }
                
                // The last vertex is not the one we need, try binary searching for the right one.
                vertexIndexIndex = edgeVertices.binarySearch(compareU ? Int(u) : Int(v)) { (vIndex, texCoords) in
                    let uv = sourceMesh.encoding.decodeTextureCoordinates(sourceMesh.vertices, index: vIndex)
                    let targetUv = transformTextureCoordinates(sourceTile: sourceTile, targetTile: terrainFillMesh.tile, coordinates: uv)
                    if increasing {
                        if compareU {
                            return Int(targetUv.x - u)
                        }
                        return Int(targetUv.y - v)
                    } else if compareU {
                        return Int(u - targetUv.x)
                    }
                    return Int(v - targetUv.y)
                }

                if vertexIndexIndex < 0 {
                    vertexIndexIndex = ~vertexIndexIndex

                    if vertexIndexIndex > 0 && vertexIndexIndex < edgeVertices.count {
                        // The corner falls between two vertices, so interpolate between them.
                        interpolatedVertexAtCorner(ellipsoid: ellipsoid, sourceTile: sourceTile, targetTile: terrainFillMesh.tile, sourceMesh: sourceMesh, previousIndex: edgeVertices[vertexIndexIndex - 1], nextIndex: edgeVertices[vertexIndexIndex], u: u, v: v, interpolateU: compareU, vertex: &vertex);
                        return true
                    }
                } else {
                    // Found a vertex that fits in the corner exactly.
                    vertexFromTileAtCorner(sourceMesh: sourceMesh, sourceIndex: edgeVertices[vertexIndexIndex], u: u, v: v, vertex: &vertex);
                    return true
                }
            }
        }
        return false
    }
    
    private func interpolatedVertexAtCorner(ellipsoid: Ellipsoid, sourceTile: QuadtreeTile, targetTile: QuadtreeTile, sourceMesh: TerrainMesh, previousIndex: Int, nextIndex: Int, u: Double, v: Double, interpolateU: Bool, vertex: inout HeightAndNormal) {
        let sourceEncoding = sourceMesh.encoding
        let sourceVertices = sourceMesh.vertices

        let uv = sourceEncoding.decodeTextureCoordinates(sourceVertices, index: previousIndex)
        let previousUv = transformTextureCoordinates(sourceTile: sourceTile, targetTile: targetTile, coordinates: uv)
        let uv2 = sourceEncoding.decodeTextureCoordinates(sourceVertices, index: nextIndex)
        let nextUv = transformTextureCoordinates(sourceTile: sourceTile, targetTile: targetTile, coordinates: uv2)

        let ratio: Double
        if interpolateU {
            ratio = (u - previousUv.x) / (nextUv.x - previousUv.x)
        } else {
            ratio = (v - previousUv.y) / (nextUv.y - previousUv.y)
        }

        let height1 = sourceEncoding.decodeHeight(sourceVertices, index: previousIndex)
        let height2 = sourceEncoding.decodeHeight(sourceVertices, index: nextIndex)

        let targetRectangle = targetTile.rectangle
        cartographicScratch.longitude = Math.lerp(p: targetRectangle.west, q: targetRectangle.east, time: u)
        cartographicScratch.latitude = Math.lerp(p: targetRectangle.south, q: targetRectangle.north, time: v)
        cartographicScratch.height = Math.lerp(p: height1, q: height2, time: ratio)
        vertex.height = cartographicScratch.height

        if (sourceEncoding.hasVertexNormals) {
            let encodedNormal1 = sourceEncoding.octEncodedNormal(sourceVertices, index: previousIndex)
            let encodedNormal2 = sourceEncoding.octEncodedNormal(sourceVertices, index: nextIndex)
            let normal1 = AttributeCompression.octDecode(x: UInt8(encodedNormal1.x), y: UInt8(encodedNormal1.y))
            let normal2 = AttributeCompression.octDecode(x: UInt8(encodedNormal2.x), y: UInt8(encodedNormal2.y))
            let normal = normal1.lerp(normal2, t: ratio).normalize()
            vertex.encodedNormal = AttributeCompression.octEncode(normal)
        } else {
            let normal = ellipsoid.geodeticSurfaceNormalCartographic(cartographicScratch)
            vertex.encodedNormal = AttributeCompression.octEncode(normal)
        }
    }
    
    private func vertexFromTileAtCorner(sourceMesh: TerrainMesh, sourceIndex: Int, u: Double, v: Double, vertex: inout HeightAndNormal) {
        let sourceEncoding = sourceMesh.encoding
        let sourceVertices = sourceMesh.vertices

        vertex.height = sourceEncoding.decodeHeight(sourceVertices, index: sourceIndex)

        if sourceEncoding.hasVertexNormals {
            vertex.encodedNormal = sourceEncoding.octEncodedNormal(sourceVertices, index: sourceIndex)
        } else {
            vertex.encodedNormal.x = 0.0
            vertex.encodedNormal.y = 0.0
        }
    }
    
    private func vertexWithHeightAtCorner(terrainFillMesh: TerrainFillMesh, ellipsoid: Ellipsoid, u: Double, v: Double, height: Double, vertex: inout HeightAndNormal) {
        vertex.height = height
        let normal = ellipsoid.geodeticSurfaceNormalCartographic(cartographicScratch)
        vertex.encodedNormal = AttributeCompression.octEncode(normal)
    }
    
    private func transformTextureCoordinates(sourceTile: QuadtreeTile, targetTile: QuadtreeTile, coordinates: Cartesian2) -> Cartesian2
    {
        var sourceRectangle = sourceTile.rectangle
        let targetRectangle = targetTile.rectangle
        
        // Handle transforming across the anti-meridian.
        if targetTile.x == 0 && coordinates.x == 1.0 && sourceTile.x == sourceTile.tilingScheme.numberOfXTilesAt(level: sourceTile.level) - 1 {
            sourceRectangle = sourceTile.rectangle
            sourceRectangle.west -= Math.TwoPi
            sourceRectangle.east -= Math.TwoPi
        } else if sourceTile.x == 0 && coordinates.x == 0.0 && targetTile.x == targetTile.tilingScheme.numberOfXTilesAt(level: targetTile.level) - 1 {
            sourceRectangle = sourceTile.rectangle
            sourceRectangle.west += Math.TwoPi
            sourceRectangle.east += Math.TwoPi
        }
        
        let sourceWidth = sourceRectangle.east - sourceRectangle.west
        let umin = (targetRectangle.west - sourceRectangle.west) / sourceWidth
        let umax = (targetRectangle.east - sourceRectangle.west) / sourceWidth

        let sourceHeight = sourceRectangle.north - sourceRectangle.south
        let vmin = (targetRectangle.south - sourceRectangle.south) / sourceHeight
        let vmax = (targetRectangle.north - sourceRectangle.south) / sourceHeight

        var u = (coordinates.x - umin) / (umax - umin)
        var v = (coordinates.y - vmin) / (vmax - vmin)

        // Ensure that coordinates very near the corners are at the corners.
        if abs(u) < Math.Epsilon5 {
            u = 0.0
        } else if abs(u - 1.0) < Math.Epsilon5 {
            u = 1.0
        }

        if abs(v) < Math.Epsilon5 {
            v = 0.0
        } else if abs(v - 1.0) < Math.Epsilon5 {
            v = 1.0
        }
        
        return Cartesian2(x: u, y: v)
    }
    
    deinit {
        vertexArray = nil
    }
}
