//
//  sampleTerrain.swift
//  CesiumKit
//
//  Created by 飯塚淳 on 2018/07/18.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation
import Hydra
import os.signpost

struct TileRequest {
    var x: Int
    var y: Int
    var level: Int
    var tilingScheme: TilingScheme
    var terrainProvider: CesiumTerrainProvider
    var position: Cartographic
}

private let sampleTerrainLog = OSLog(subsystem: "com.ai.FS", category: "SampleTerrain")

extension Scene {
    public func sampleTerrainMostDetailed(position: Cartographic) -> Promise<Cartographic>? {
        guard let cesiumTerrainProvider = terrainProvider as? CesiumTerrainProvider else { return nil }
        guard let availability = cesiumTerrainProvider._availability else { return nil }
        let maxLevel = availability.computeMaximumLevelAtPosition(position: position)
        return sampleTerrain(level: (maxLevel > 14) ? 14 : maxLevel, position: position, terrainProvider: cesiumTerrainProvider)
    }
    
    func sampleTerrain(level: Int, position: Cartographic, terrainProvider: CesiumTerrainProvider) -> Promise<Cartographic>? {
        guard let xy = terrainProvider.tilingScheme.positionToTileXY(position: position, level: level) else { return nil }
        os_signpost(.begin, log: sampleTerrainLog, name: "sampleTerrain", "x:%d y:%d l:%d", xy.x, xy.y, level)
        let tileRequest = TileRequest(x: xy.x, y: xy.y, level: level, tilingScheme: terrainProvider.tilingScheme, terrainProvider: terrainProvider, position: position)
        
        guard let tilePromise = terrainProvider.requestTileGeometry(x: xy.x, y: xy.y, level: level) else { return nil }
        
        return Promise<Cartographic>({ resolve, reject, _ in
            tilePromise.then({ terrainData in
                os_signpost(.event, log: sampleTerrainLog, name: "sampleTerrain", "got terrainData")
                let rectangle = terrainProvider.tilingScheme.tileXYToRectangle(x: tileRequest.x, y: tileRequest.y, level: tileRequest.level)
                var pos = position
                guard let height = terrainData.interpolateHeight(rectangle, longitude: position.longitude, latitude: position.latitude) else {
                    os_signpost(.event, log: sampleTerrainLog, name: "sampleTerrain", "interpolate error => x:%d y:%d l:%d", xy.x, xy.y, level)
                    os_signpost(.end, log: sampleTerrainLog, name: "sampleTerrain", "x:%d y:%d l:%d", xy.x, xy.y, level)
                    reject(CesiumResourceError.general)
                    return
                }
                pos.height = height
                os_signpost(.end, log: sampleTerrainLog, name: "sampleTerrain", "x:%d y:%d l:%d", xy.x, xy.y, level)
                resolve(pos)
            }).catch({ error in
                os_signpost(.event, log: sampleTerrainLog, name: "sampleTerrain", "tilePromise error => x:%d y:%d l:%d", xy.x, xy.y, level)
                os_signpost(.end, log: sampleTerrainLog, name: "sampleTerrain", "x:%d y:%d l:%d", xy.x, xy.y, level)
                reject(error)
            })
        })
    }
}
