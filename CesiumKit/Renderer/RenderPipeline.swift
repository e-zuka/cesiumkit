//
//  RenderPipeline.swift
//  CesiumKit
//
//  Created by Ryan Walklin on 31/05/2015.
//  Copyright (c) 2015 Test Toast. All rights reserved.
//

import Metal

class RenderPipeline {
    
    let state: MTLRenderPipelineState!
    
    let shaderProgram: ShaderProgram
    
    var keyword: String {
        return state.label ?? ""
    }
    
    var blendingState: BlendingState? = nil
    
    var count: Int = 0
    
    var descriptor: MTLRenderPipelineDescriptor
    
    var stateForICB: MTLRenderPipelineState? = nil

    init (device: MTLDevice, shaderProgram: ShaderProgram, descriptor: MTLRenderPipelineDescriptor, vd: VertexDescriptor? = nil) {
        
        self.shaderProgram = shaderProgram
        self.descriptor = descriptor
        do {
            let state = try device.makeRenderPipelineState(descriptor: descriptor)
            self.state = state
        } catch let error as NSError  {
            state = nil
            assertionFailure("makeRenderPipelineState failed: \(error.localizedDescription)")
        }
        guard let vDesc = vd else { return }
        let icbDesc = descriptor.copy() as! MTLRenderPipelineDescriptor
        if vDesc.attributeCount == 1 {
            if let vertexFunction = shaderProgram.icbVertexQuantizedFunction, let fragmentFunction = shaderProgram.icbFragmentFunction {
                icbDesc.vertexFunction = vertexFunction
                icbDesc.fragmentFunction = fragmentFunction
                //icbDesc.supportIndirectCommandBuffers = true
                icbDesc.colorAttachments[1].pixelFormat = .rgba32Float
                icbDesc.colorAttachments[2].pixelFormat = .rgba32Float
                icbDesc.colorAttachments[3].pixelFormat = .rg16Float
                icbDesc.colorAttachments[4].pixelFormat = .rgba16Float
                icbDesc.label = "ICB Quantized"
                do {
                    stateForICB = try device.makeRenderPipelineState(descriptor: icbDesc)
                } catch {
                    print(error.localizedDescription)
                    // nop
                }
            }
        } else if vDesc.attributeCount == 2 {
            if let vertextFunction = shaderProgram.icbVertexFunction, let fragmentFunction = shaderProgram.icbFragmentFunction {
                icbDesc.vertexFunction = vertextFunction
                icbDesc.fragmentFunction = fragmentFunction
                //icbDesc.supportIndirectCommandBuffers = true
                icbDesc.colorAttachments[1].pixelFormat = .rgba32Float
                icbDesc.colorAttachments[2].pixelFormat = .rgba32Float
                icbDesc.colorAttachments[3].pixelFormat = .rg16Float
                icbDesc.colorAttachments[4].pixelFormat = .rgba16Float
                icbDesc.label = "ICB Raw"
                do {
                    stateForICB = try device.makeRenderPipelineState(descriptor: icbDesc)
                } catch {
                    print(error.localizedDescription)
                    // nop
                }
            }
        }
    }
    
    static func fromCache (context: Context, vertexShaderSource vss: ShaderSource, fragmentShaderSource fss: ShaderSource, vertexDescriptor vd: VertexDescriptor?, colorMask: ColorMask? = nil, depthStencil: Bool, blendingState: BlendingState? = nil, manualUniformStruct: String? = nil, uniformStructSize: Int? = nil) -> RenderPipeline {
        //FIXME: remove nil for manualUniformStruct
        return context.pipelineCache.getRenderPipeline(vertexShaderSource: vss, fragmentShaderSource: fss, vertexDescriptor: vd, colorMask: colorMask, depthStencil: depthStencil, blendingState: blendingState, manualUniformStruct: manualUniformStruct, uniformStructSize: uniformStructSize)
    }
    
    static func replaceCache (context: Context,  pipeline: RenderPipeline?, vertexShaderSource vss: ShaderSource, fragmentShaderSource fss: ShaderSource, vertexDescriptor vd: VertexDescriptor?, colorMask: ColorMask? = nil, depthStencil: Bool, blendingState: BlendingState? = nil) -> RenderPipeline? {
        
        return context.pipelineCache.replaceRenderPipeline(pipeline, vertexShaderSource: vss, fragmentShaderSource: fss, vertexDescriptor: vd, colorMask: colorMask, depthStencil: depthStencil, blendingState: blendingState)
    }
    
    static func withCompiledShader(_ context: Context, shaderSourceName: String, compiledMetalVertexName vertex: String, compiledMetalFragmentName fragment: String, uniformStructSize: Int, vertexDescriptor vd: VertexDescriptor?, colorMask: ColorMask? = nil, depthStencil: Bool, blendingState: BlendingState? = nil) -> RenderPipeline? {
        return context.pipelineCache.getRenderPipeline(shaderSourceName: shaderSourceName, compiledMetalVertexName: vertex, compiledMetalFragmentName: fragment, uniformStructSize: uniformStructSize, vertexDescriptor: vd, colorMask: colorMask, depthStencil: depthStencil, blendingState: blendingState)
    }
    
    func setUniforms(_ command: DrawCommand, device: MTLDevice, uniformState: UniformState) -> (fragmentOffset: Int, texturesValid: Bool, textures: [Texture]) {
        return shaderProgram.setUniforms(command, uniformState: uniformState)
    }
}
