//
//  AccelerationStructure.swift
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2019/11/12.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation
import MetalPerformanceShaders
import SceneKit

// buffer for airplane
// SCNGeometry* geometry = [airplaneNode geometry];
// SCNGeometrySource* source in [geometry geometrySources]
// airplaneVertex = [source semantic] == SCNGeometrySourceSemanticVertex
// airplaneNormal = [source semantic] == SCNGeometrySourceSemanticNormal
// for instanceAccelerationStructure??
// let fromENU = Transforms.eastNorthUpToFixedFrame(center, ellipsoid: ellipsoid)
// let toENU = fromENU.inverse
// transformMatrix (will be) = toENU * airplaneNode.worldTransform

// tile = instance

// buffer for tile
// vertexBuffer = [tile0.VB, tile1.VB, tile2.VB, ..., tileN.VB]
// instanceBuffer = [0, 1, 2, ..., N]
// for instanceAccelerationStructure

// tile transform matrix
// out.gl_Position = (frustumUniform.czm_f_projection * (tileUniform.u_modifiedModelView * float4(position, 1.0)));
// http://www.geisya.or.jp/~mwm48961/kou2/matrix3.html
// -> transformMatrix = frustumUniform.czm_f_projection * tileUniform.u_modifiedModelView
// out.gl_Position = transformMatrix * float4(position, 1.0)

public class AccelerationStructure {
    private let instanceAccelerationStructures: [MPSInstanceAccelerationStructure]
    private let transformBuffer: MTLBuffer
    private var bufferSyncState: BufferSyncState = .zero
    private let instanceCount: Int
    private var instanceIndexMap: [String: Int] = [:]
    
    public var accelerationStructure: MPSInstanceAccelerationStructure {
        instanceAccelerationStructures[bufferSyncState.rawValue]
    }
    
    private var pipelineState: MTLRenderPipelineState? = nil
    
    public init(device: MTLDevice, instances: [String: [SIMD3<Float>]]) {
        instanceCount = instances.count
        let group = MPSAccelerationStructureGroup(device: device)
        var triangleAccelerationStructures: [MPSTriangleAccelerationStructure] = []
        
        var instanceArray: [UInt] = []
        for (index, instance) in instances.enumerated() {
            let instanceName = instance.key
            let vertecies = instance.value
            instanceIndexMap[instanceName] = index
            
            instanceArray.append(UInt(index))
            let triangleStructure = MPSTriangleAccelerationStructure(group: group)
            triangleStructure.vertexBufferOffset = 0
            //let vertexBuffer = Buffer(device: device, array: vertecies, componentDatatype: .float32, sizeInBytes: MemoryLayout<SIMD3<Float>>.size * vertecies.count, label: "ASVertexBuffer")
            let vertexBuffer = device.makeBuffer(bytes: vertecies, length: MemoryLayout<SIMD3<Float>>.size * vertecies.count, options: .storageModeShared)!
            triangleStructure.vertexBuffer = vertexBuffer
            triangleStructure.triangleCount = vertecies.count / 3
            triangleStructure.rebuild()

            triangleAccelerationStructures.append(triangleStructure)
        }

        var structureBuffer: [MPSInstanceAccelerationStructure] = []
        //let instanceBuffer = Buffer(device: device, array: instanceArray, componentDatatype: .unsignedInt, sizeInBytes: 4 * instanceCount, label: "ASInstanceBuffer")
        let instanceBuffer = device.makeBuffer(bytes: instanceArray, length: 4 * instanceArray.count, options: .storageModeShared)
        
        // Allocate one extra buffer range (maxFramesInFlight + 1)
        //transformBuffer = Buffer(device: device, componentDatatype: .float32, sizeInBytes: MemoryLayout<float4x4>.size * instanceCount * 4)
        transformBuffer = device.makeBuffer(length: MemoryLayout<float4x4>.size * instanceCount * 4, options: .storageModeShared)!
        for _ in 0..<3 {
            let instanceStructure = MPSInstanceAccelerationStructure(group: group)
            instanceStructure.instanceCount = instanceCount
            instanceStructure.accelerationStructures = triangleAccelerationStructures
            instanceStructure.transformBuffer = transformBuffer
            instanceStructure.instanceBuffer = instanceBuffer
            structureBuffer.append(instanceStructure)
        }
        instanceAccelerationStructures = structureBuffer
        
        //preparePipeline(device: device)
    }
    
    func update(bufferSyncState: BufferSyncState, instances: [String: float4x4]) {
        self.bufferSyncState = bufferSyncState
        let instanceStructure = accelerationStructure
        let matrixSize = MemoryLayout<float4x4>.size
        let transformBufferOffset = bufferSyncState.rawValue * matrixSize * instanceCount
        
        // update transform
        for instance in instances {
            var transform = instance.value
            guard let index = instanceIndexMap[instance.key] else { continue }
            
            transformBuffer.write(from: &transform, length: MemoryLayout<float4x4>.size, offset: transformBufferOffset + index * matrixSize)
        }
        instanceStructure.transformBufferOffset = transformBufferOffset

        instanceStructure.rebuild()
    }
    
    
    func preparePipeline(device: MTLDevice) {
        guard let bundle = Bundle(identifier: "com.testtoast.CesiumKit") else {
            logPrint(.error, "Could not find CesiumKit bundle in executable")
            return
        }
        guard let libraryPath = bundle.url(forResource: "default", withExtension: "metallib")?.path else {
            logPrint(.error, "Could not find shader source from bundle")
            return
        }
        let shaderLibrary: MTLLibrary
        do {
            shaderLibrary = try device.makeLibrary(filepath: libraryPath)
        } catch let error as NSError {
            logPrint(.error, "Could not generate library from compiled shader lib: \(error.localizedDescription)")
            return
        }
        
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
        pipelineDescriptor.vertexFunction = shaderLibrary.makeFunction(name: "wire_vertex")
        pipelineDescriptor.fragmentFunction = shaderLibrary.makeFunction(name: "wire_fragment")
        pipelineDescriptor.depthAttachmentPixelFormat = .depth32Float_stencil8
        pipelineDescriptor.stencilAttachmentPixelFormat = .depth32Float_stencil8
        
        do {
            pipelineState = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch {
            assertionFailure("failed to create wire render pipeline state")
        }
    }
    func renderWire(context: Context, passState: PassState, frustumUniform: MTLBuffer, commandBuffer: MTLCommandBuffer) {
        let instanceStructure = accelerationStructure
        guard let triangleAS = instanceStructure.accelerationStructures?.first as? MPSTriangleAccelerationStructure, let pipeline = pipelineState else { return }
        
        let target = passState.framebuffer?.colorTextures![0] ?? context.defaultFramebuffer.colorTextures![0]
        let renderPassDescriptor = MTLRenderPassDescriptor()
        renderPassDescriptor.colorAttachments[0].texture = target.metalTexture
        renderPassDescriptor.colorAttachments[0].loadAction = .load
        renderPassDescriptor.colorAttachments[0].storeAction = .store
        renderPassDescriptor.depthAttachment.texture = passState.framebuffer.depthTexture?.metalTexture
        renderPassDescriptor.stencilAttachment.texture = passState.framebuffer.stencilTexture?.metalTexture
        
        let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
        renderEncoder.pushDebugGroup("AS wire")
        renderEncoder.setRenderPipelineState(pipeline)
        renderEncoder.setTriangleFillMode(.lines)
        renderEncoder.setViewport(MTLViewport(originX: 0, originY: 0, width: 1624, height: 750, znear: 0.0, zfar: 1.0))
        
        let matrixSize = MemoryLayout<float4x4>.size
        let transformBufferOffset = bufferSyncState.rawValue * matrixSize * instanceCount
        renderEncoder.setVertexBuffer(triangleAS.vertexBuffer, offset: 0, index: 0)
        renderEncoder.setVertexBuffer(frustumUniform, offset: 0, index: 1)
        renderEncoder.setVertexBuffer(transformBuffer, offset: transformBufferOffset, index: 2)

        renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: triangleAS.triangleCount * 3)
        renderEncoder.popDebugGroup()
        renderEncoder.endEncoding()
    }
}

public class AirplaneAccelerationStructure : AccelerationStructure {
    let airplane: SCNNode
    let heightOffset: Double
    let gears = ["frontGear", "leftGear", "rightGear"]

    public init(device: MTLDevice, airplane: SCNNode, heightOffset: Float = 0) {
        self.airplane = airplane
        self.heightOffset = Double(heightOffset)

        func vertecies(of node: SCNNode) -> [SIMD3<Float>] {
            return (node.geometry?.verticies() ?? []).map{ SIMD3<Float>($0) }
        }
        
        let instances = ["airplane": vertecies(of: airplane)]
        /*
        for gear in gears {
            guard let gearNode = airplane.childNode(withName: gear, recursively: false) else { continue }
            let originalVertecies = gearNode.geometry?.simd_verticies()
            //instances[gearNode.name!] = originalVertecies ?? []
            //instances.append(vertecies(of: gearNode))
            let childNodes = gearNode.childNodes { node, stop in return true}
            for child in childNodes {
                print(child.name ?? "noChildName")
                //instances.append(vertecies(of: child))
            }
        }
        */
        //super.init(device: device, vertecies: (airplane.geometry?.vertecies() ?? []).map{ SIMD3<Float>($0) })
        super.init(device: device, instances: instances)
    }
    
    func update(bufferSyncState: BufferSyncState, center: Cartesian3, headingVariation: Float) {
        // airplane transform
        var euler = airplane.presentation.worldOrientation.rotationMatrix().eulerAngles
        euler.z += .pi / 2
        euler.y += headingVariation / 180 * .pi
        let rotationQuat = euler.quaternion
        let rotationMatrix = float4x4(rotationQuat)

        var fromENU = Transforms.eastNorthUpToFixedFrame(center)
        var worldTranslation = fromENU.translation
        worldTranslation.y += heightOffset
        fromENU = fromENU.setTranslation(worldTranslation)

        let airplaneTransform = fromENU.floatRepresentation * rotationMatrix
        // transform array for update
        let transforms: [String: float4x4] = ["airplane": airplaneTransform]

        /*
        for gear in gears {
            guard let gearNode = airplane.childNode(withName: gear, recursively: false) else { continue }
            //let gearTransform = gearNode.simdConvertTransform(gearNode.simdTransform, to: airplane)
            let gearTransform = gearNode.simdTransform
            //transforms[gearNode.name!] = airplaneTransform * gearTransform
            //transforms.append(airplaneTransform * gearTransform)
            let childNodes = gearNode.childNodes { node, stop in return true}
            for child in childNodes {
                //let childTransform = child.simdConvertTransform(child.simdTransform, to: gearNode)
                //transforms.append(airplaneTransform * childTransform)
            }
        }
        */
        //super.update(bufferSyncState: bufferSyncState, transform: transform.floatRepresentation)
        super.update(bufferSyncState: bufferSyncState, instances: transforms)
    }
}
