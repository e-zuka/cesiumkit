//
//  IndirectCommandBuffer.swift
//  CesiumKitTests
//
//  Created by 飯塚淳 on 2019/10/07.
//  Copyright © 2019 Test Toast. All rights reserved.
//
import Foundation
import Metal

class ICBCesium {
    let maxTile: Int = 1000
    let device: MTLDevice
    var icb: MTLIndirectCommandBuffer {
        indirectCommandBuffer[bufferSyncState.rawValue]
    }
    var agb: ArgumentBufferDict {
        argumentBufferPool[bufferSyncState.rawValue]
    }
    var prev_agb: ArgumentBufferDict {
        let prevInflight = bufferSyncState.rawValue - 1 < 0 ? 2 : bufferSyncState.rawValue - 1
        return argumentBufferPool[prevInflight]
    }
    var computePipelineState: MTLComputePipelineState?
    
    let tileArgumentEncoder: MTLArgumentEncoder
    let qMeshArgumentEncoder: MTLArgumentEncoder
    let meshArgumentEncoder: MTLArgumentEncoder
    let materialArgumentEncoder: MTLArgumentEncoder
    let computeArgumentEncoder: MTLArgumentEncoder

    var indirectCommandBuffer: [MTLIndirectCommandBuffer] = []
    typealias ArgumentBufferDict = [String: MTLBuffer]// = ["tile": [], "qMesh": [], "mesh": [], "material": []]
    var argumentBufferPool: [ArgumentBufferDict] = []
    var bufferSyncState: BufferSyncState = .zero {
        willSet {
            if bufferSyncState != newValue {
                tileOffset = 0
            }
        }
        didSet {
            if bufferSyncState != oldValue {
                tileIndexes = [:]
            }
        }
    }
    var tileOffset: Int = 0
    typealias TileIndexMap = [String: Int] // tile_x_y_level: 0
    var _tileIndexes: [TileIndexMap] = [[:], [:], [:]]
    var tileIndexes: TileIndexMap {
        get {
            _tileIndexes[bufferSyncState.rawValue]
        }
        set {
            _tileIndexes[bufferSyncState.rawValue] = newValue
        }
    }
    var prevTileIndexes: TileIndexMap {
        let prevInflight = bufferSyncState.rawValue - 1 < 0 ? 2 : bufferSyncState.rawValue - 1
        return _tileIndexes[prevInflight]
    }

    var fragmentFunction: MTLFunction? = nil

    required init(with device: MTLDevice, commandQueue: MTLCommandQueue) {
        self.device = device

        let bundle = Bundle(identifier: "com.testtoast.CesiumKit")!
        let libraryPath = bundle.url(forResource: "default", withExtension: "metallib")!.path
        let library: MTLLibrary!
        do {
            library = try! device.makeLibrary(filepath: libraryPath)
        }
        
        fragmentFunction = library.makeFunction(name: "tileFragment")

        let icbDescriptor = MTLIndirectCommandBufferDescriptor()
        icbDescriptor.commandTypes = .drawIndexed
        //icbDescriptor.commandTypes = .draw
        icbDescriptor.inheritPipelineState = false
        icbDescriptor.inheritBuffers = false
        icbDescriptor.maxVertexBufferBindCount = 6
        icbDescriptor.maxFragmentBufferBindCount = 4
        
        let mainComputeFunction = library.makeFunction(name: "icb_tile")!
        //let mainComputeFunction = library.makeFunction(name: "icb_tile_encoder")!
        //let mainComputeFunction = library.makeFunction(name: "icb_tile_single")!
        computeArgumentEncoder = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.commandBufferContainer.rawValue)
        tileArgumentEncoder = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.tileObject.rawValue)
        qMeshArgumentEncoder = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.quantizedMesh.rawValue)
        meshArgumentEncoder = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.mesh.rawValue)
        materialArgumentEncoder = mainComputeFunction.makeArgumentEncoder(bufferIndex: CesiumKernelBufferIndex.material.rawValue)

        do {
            computePipelineState = try device.makeComputePipelineState(function: mainComputeFunction)
        } catch {
            assertionFailure("Failed to create compute pipeline state")
        }
        
        // ArgumentBuffer for triple buffering
        for _ in 0..<3 {
            //guard let icb = device.makeIndirectCommandBuffer(descriptor: icbDescriptor, maxCommandCount: maxTile, options: .storageModePrivate) else {
            guard let icb = device.makeIndirectCommandBuffer(descriptor: icbDescriptor, maxCommandCount: maxTile, options: .storageModeShared) else {
                assertionFailure("Error making icb")
                continue
            }
            indirectCommandBuffer.append(icb)
            argumentBufferPool.append(makeArgumentBufferCache())
        }
        if indirectCommandBuffer.count < 3 {
            assertionFailure("not enough icb buffer for triple buffering")
        }
    }
    
    func makeArgumentBufferCache() -> ArgumentBufferDict {
        let tileBuffer = device.makeBuffer(length: tileArgumentEncoder.encodedLength * maxTile, options: .storageModeShared)!
        let qMeshBuffer = device.makeBuffer(length: qMeshArgumentEncoder.encodedLength * maxTile, options: .storageModeShared)!
        let meshBuffer = device.makeBuffer(length: meshArgumentEncoder.encodedLength * maxTile, options: .storageModeShared)!
        let materialBuffer = device.makeBuffer(length: materialArgumentEncoder.encodedLength * maxTile, options: .storageModeShared)!
        let icbBuffer = device.makeBuffer(length: computeArgumentEncoder.encodedLength, options: .storageModeShared)!
        // prev buffer for FrustumUniform and TileUniform
        let frustumBuffer = device.makeBuffer(length: MemoryLayout<FrustumUniformBufferLayout>.stride * 5, options: .storageModeShared)!
        let tileUniformBuffer = device.makeBuffer(length: MemoryLayout<TileUniformStruct>.stride * maxTile, options: .storageModeShared)!
        return ["tile": tileBuffer, "qMesh": qMeshBuffer, "mesh": meshBuffer, "material": materialBuffer, "icb": icbBuffer, "frustum": frustumBuffer, "tileUniform": tileUniformBuffer]
    }
    
    func resetICB(context: Context, offset: Int, length: Int) {
        let commandBuffer = context.currentCommandBuffer()
        
        // reset icb
        guard let resetBlitEncoder = commandBuffer.makeBlitCommandEncoder() else { return }
        resetBlitEncoder.label = "Reset ICB Blit Encoder"
        resetBlitEncoder.resetCommandsInBuffer(icb, range: offset..<(offset + length))
        resetBlitEncoder.endEncoding()
    }

    func encode(commands: [DrawCommand], frustumUniform: MTLBuffer, passState: PassState, context: Context, shadow: Shadow? = nil) {
        guard let computePipelineState = computePipelineState, commands.count > 0 else { return }
        bufferSyncState = context.bufferSyncState
        let commandBuffer = context.currentCommandBuffer()
        
        let tileCount = commands.count
        resetICB(context: context, offset: tileOffset, length: tileCount)
        
        // compute encoder
        guard let computeEncoder = commandBuffer.makeComputeCommandEncoder() else { return }
        computeEncoder.label = "Render command encoder"

        // set uniform
        let uniformState = context.uniformState
        let firstCommand = commands[0]
        uniformState.model = firstCommand.modelMatrix ?? Matrix4.identity
        let autoUniform = context.automaticUniformBuffer()
        
        // pack vertexBuffer, indexBuffer, texture, tileUniform, into argument buffer
        //let tileCountBuffer = device.makeBuffer(length: MemoryLayout<UInt>.size, options: .storageModeShared)!
        //tileCountBuffer.contents().storeBytes(of: UInt(tileCount), as: UInt.self)
        //let tileArgumentBuffer = device.makeBuffer(length: tileArgumentEncoder.encodedLength * tileCount, options: .storageModeShared)!
        let tileArgumentBuffer = agb["tile"]!
        let qMeshArgumentBuffer = agb["qMesh"]!
        let meshArgmentBuffer = agb["mesh"]!
        let materialArgumentBuffer = agb["material"]!
        let icbArgumentBuffer = agb["icb"]!

        var resourceList: [MTLResource] = [tileArgumentBuffer, qMeshArgumentBuffer, meshArgmentBuffer, materialArgumentBuffer, icbArgumentBuffer, autoUniform, frustumUniform]
        var textureList: [Texture] = []
        for (i, command) in commands.enumerated() {
            guard let pipeline = command.pipeline?.stateForICB else {
                assertionFailure("no pipeline")
                continue
            }
            //let bufferParams = setUniforms(command, uniformState: uniformState)
            let bufferParams = setUniforms(command)
            // Don't render unless any textures required are available
            if !bufferParams.texturesValid {
                logPrint(.error, "invalid textures")
                assertionFailure("invalid textures")
                continue
            }
            guard let vertexArray = command.vertexArray else {
                assertionFailure("no vertexArray")
                continue
            }
            guard let indexBuffer = vertexArray.indexBuffer?.metalBuffer else {
                assertionFailure("only indexed vertecies are supported")
                continue
            }
            let count = command.count
            guard let _ = count ?? vertexArray.indexCount else {
                assertionFailure("index count not provided for indexed primitive")
                continue
            }
            guard let tileUniformBuffer = context.manualUniformBuffer(with: command.uniformMap?.uniformBufferProvider) else {
                assertionFailure("no tilemap uniform found")
                continue
            }
            guard let _ = command.pipeline?.state else {
                assertionFailure("no pipeline state")
                continue
            }
            
            resourceList.append(tileUniformBuffer)
            resourceList.append(indexBuffer)
            textureList.append(contentsOf: bufferParams.textures)
            
            tileArgumentEncoder.setArgumentBuffer(tileArgumentBuffer, startOffset: 0, arrayElement: tileOffset + i)
            qMeshArgumentEncoder.setArgumentBuffer(qMeshArgumentBuffer, startOffset: 0, arrayElement: tileOffset + i)
            meshArgumentEncoder.setArgumentBuffer(meshArgmentBuffer, startOffset: 0, arrayElement: tileOffset + i)
            materialArgumentEncoder.setArgumentBuffer(materialArgumentBuffer, startOffset: 0, arrayElement: tileOffset + i)
            
            tileArgumentEncoder.setBuffer(tileUniformBuffer, offset: 0, index: CesiumTileBufferIndex.tileUniform.rawValue)
            
            var attributeCount: UInt16 = 0
            for attribute in vertexArray.attributes {
                if let _ = attribute.buffer {
                    attributeCount += 1
                }
            }
            let _meshArgumentEncoder = attributeCount == 1 ? qMeshArgumentEncoder : meshArgumentEncoder
            tileArgumentEncoder.constantData(at: CesiumTileBufferIndex.vertexAttributeCount.rawValue).storeBytes(of: UInt16(attributeCount), as: UInt16.self)
            let applyFog: Bool = command.label != nil && command.label!.hasSuffix("_fog") ? true : false
            tileArgumentEncoder.constantData(at: CesiumTileBufferIndex.fog.rawValue).storeBytes(of: applyFog, as: Bool.self)
            
            for attribute in vertexArray.attributes {
                if let buffer = attribute.buffer {
                    resourceList.append(buffer.metalBuffer)
                    _meshArgumentEncoder.setBuffer(buffer.metalBuffer, offset: 0, index: attribute.bufferIndex)

                }
            }
            _meshArgumentEncoder.setBuffer(indexBuffer, offset: 0, index: CesiumMeshBufferIndex.indexBuffer.rawValue)
            _meshArgumentEncoder.constantData(at: CesiumMeshBufferIndex.indexCount.rawValue).storeBytes(of: UInt16(vertexArray.indexCount!), as: UInt16.self)
            
            materialArgumentEncoder.constantData(at: CesiumMaterialBufferIndex.textureCount.rawValue).storeBytes(of: UInt16(bufferParams.textures.count), as: UInt16.self)
            for (j, texture) in bufferParams.textures.enumerated() {
                materialArgumentEncoder.setTexture(texture.metalTexture, index: CesiumMaterialBufferIndex.texture_0.rawValue + j)
                materialArgumentEncoder.setSamplerState(texture.sampler.state, index: CesiumMaterialBufferIndex.sampler_0.rawValue + j)
            }
            if let shadow = shadow {
                materialArgumentEncoder.setTexture(shadow.randomTexture, index: CesiumMaterialBufferIndex.randomTexture.rawValue)
            }
            materialArgumentEncoder.setRenderPipelineState(pipeline, index: CesiumMaterialBufferIndex.pipelineState.rawValue)
        }

        // compute
        /*
        guard let argumentBuffer = device.makeBuffer(length: computeArgumentEncoder.encodedLength, options: .storageModeShared) else {
            assertionFailure("Error making argument buffer")
            return
        }
        */
        computeArgumentEncoder.setArgumentBuffer(icbArgumentBuffer, offset: 0)
        computeArgumentEncoder.setIndirectCommandBuffer(icb, index: CesiumArgumentBufferID.commandBuffer.rawValue)

        computeEncoder.setComputePipelineState(computePipelineState)
        
        computeEncoder.setBuffer(autoUniform, offset: 0, index: CesiumKernelBufferIndex.automaticUniform.rawValue)
        computeEncoder.setBuffer(frustumUniform, offset: 0, index: CesiumKernelBufferIndex.frustumUniform.rawValue)
        computeEncoder.setBuffer(tileArgumentBuffer, offset: 0, index: CesiumKernelBufferIndex.tileObject.rawValue)
        computeEncoder.setBuffer(icbArgumentBuffer, offset: 0, index: CesiumKernelBufferIndex.commandBufferContainer.rawValue)
        //computeEncoder.setBuffer(tileCountBuffer, offset: 0, index: CesiumKernelBufferIndex.tileCount.rawValue)
        var _tileCount = UInt16(tileCount)
        computeEncoder.setBytes(&_tileCount, length: 2, index: CesiumKernelBufferIndex.tileCount.rawValue)
        computeEncoder.setBuffer(qMeshArgumentBuffer, offset: 0, index: CesiumKernelBufferIndex.quantizedMesh.rawValue)
        computeEncoder.setBuffer(meshArgmentBuffer, offset: 0, index: CesiumKernelBufferIndex.mesh.rawValue)
        computeEncoder.setBuffer(materialArgumentBuffer, offset: 0, index: CesiumKernelBufferIndex.material.rawValue)
        var _tileOffset = UInt16(tileOffset)
        computeEncoder.setBytes(&_tileOffset, length: 2, index: CesiumKernelBufferIndex.tileOffset.rawValue)

        for res in resourceList {
            computeEncoder.useResource(res, usage: .read)
        }
        for tex in textureList {
            //computeEncoder.useResource(tex.metalTexture, usage: [.sample, .read])
            computeEncoder.useResource(tex.metalTexture, usage: .read)
        }
        computeEncoder.useResource(icb, usage: .write)
        let w = computePipelineState.threadExecutionWidth
        //let h = computePipelineState.maxTotalThreadsPerThreadgroup / w
        //let threadsPerThreadgroup = MTLSizeMake(w, h, 1)
        computeEncoder.dispatchThreads(MTLSize(width: tileCount, height: 1, depth: 1), threadsPerThreadgroup: MTLSize(width: w, height: 1, depth: 1))
        computeEncoder.endEncoding()
        
        
        // Encode command to optimize the indirect command buffer after encoding
        /*
        let optimizeBlitEncoder = commandBuffer.makeBlitCommandEncoder()!
        optimizeBlitEncoder.label = "Optimize ICB Blit Encoder"
        optimizeBlitEncoder.optimizeIndirectCommandBuffer(icb, range: 0..<tileCount)
        optimizeBlitEncoder.endEncoding()
        */

        // render
        let renderPass = context.createRenderPass(passState)
        let renderEncoder = renderPass.commandEncoder
        
        // set render state same across all tile (in other words, can not set with ICB)
        let renderState = firstCommand.renderState ?? context.defaultRenderState()
        //context.applyRenderState(renderPass, renderState: command.renderState ?? context.defaultRenderState(), passState: renderPass.passState)
        renderEncoder.setFrontFacing(renderState.windingOrder.toMetal())
        renderEncoder.setCullMode(renderState.cullFace.toMetal())
        if renderState.depthTest.enabled {
            renderEncoder.setDepthStencilState(renderState.depthStencilState())
        }
        // TODO: get rid of renderState
        renderState.applyViewport(renderEncoder, passState: passState)
        if renderState.wireFrame {
            renderEncoder.setTriangleFillMode(.lines)
        } else {
            renderEncoder.setTriangleFillMode(.fill)
        }

        renderEncoder.label = "Main Render Encoder"
        //renderEncoder.setRenderPipelineState(icbPipeline)
        renderEncoder.pushDebugGroup("useResource")
        renderEncoder.useResource(autoUniform, usage: .read)
        renderEncoder.useResource(frustumUniform, usage: .read)
        for res in resourceList {
            renderEncoder.useResource(res, usage: .read)
        }
        for tex in textureList {
            renderEncoder.useResource(tex.metalTexture, usage: .sample)
        }
        renderEncoder.popDebugGroup()
        //renderEncoder.drawIndexedPrimitives(type: .triangle, indexCount: indexCount, indexType: indexType, indexBuffer: indexBuffer, indexBufferOffset: 0)
        // Draw everything in the indirect command buffer
        renderEncoder.executeCommandsInBuffer(icb, range: tileOffset..<(tileOffset + tileCount))
        
        renderEncoder.endEncoding()
        
        tileOffset += tileCount
    }
    
    func setBuffer(p: UnsafeRawPointer, length: Int) -> MTLBuffer {
        return device.makeBuffer(bytes: p, length: length, options: .storageModeShared)!
    }
    
    func encodeOnCPU(commands: [DrawCommand], frustumUniform: MTLBuffer, frustumIndex: Int, passState: PassState, context: Context, shadow: Shadow? = nil) {
        bufferSyncState = context.bufferSyncState

        guard let fragmentFunction = fragmentFunction else { return }
        let argumentEncoder = fragmentFunction.makeArgumentEncoder(bufferIndex: 2) // Material
        let argumentBuffer = agb["material"]!

        // set uniform
        let uniformState = context.uniformState
        let firstCommand = commands[0]
        uniformState.model = firstCommand.modelMatrix ?? Matrix4.identity
        let autoUniform = context.automaticUniformBuffer()
        
        // pack vertexBuffer, indexBuffer, texture, tileUniform, into argument buffer
        let tileCount = commands.count
        //let tileCountBuffer = device.makeBuffer(length: MemoryLayout<UInt16>.size, options: .storageModeShared)!
        //tileCountBuffer.contents().storeBytes(of: UInt16(tileCount), as: UInt16.self)

        icb.reset(tileOffset..<(tileOffset + tileCount))

        var resourceList: [MTLResource] = [autoUniform, frustumUniform]
        var textureList: [Texture] = []
        for (i, command) in commands.enumerated() {
            //var resourceList: [MTLResource] = []
            
            guard let pipeline = command.pipeline?.stateForICB else {
                assertionFailure("invalid pipeline")
                continue
            }

            let uniformState = context.uniformState
            uniformState.model = command.modelMatrix ?? Matrix4.identity
            //let bufferParams = setUniforms(command, uniformState: uniformState)
            let bufferParams = setUniforms(command)
            
            // Don't render unless any textures required are available
            if !bufferParams.texturesValid {
                logPrint(.error, "invalid textures")
                assertionFailure("invalid textures")
                continue
            }

            let count = command.count
            guard let vertexArray = command.vertexArray,
                let ib = vertexArray.indexBuffer,
                let indexBuffer = vertexArray.indexBuffer?.metalBuffer,
                let indexCount = count ?? vertexArray.indexCount,
                let tileUniformBuffer = context.manualUniformBuffer(with: command.uniformMap?.uniformBufferProvider)//,
                //let argumentBuffer = device.makeBuffer(length: argumentEncoder.encodedLength, options: .storageModeShared)
                else {
                assertionFailure("invalid command")
                continue
            }
            argumentEncoder.setArgumentBuffer(argumentBuffer, offset: argumentEncoder.encodedLength * (tileOffset + i))
            resourceList.append(argumentBuffer)
            /*
            var attributeCount: UInt16 = 0
            for attribute in vertexArray.attributes {
                if let _ = attribute.buffer {
                    attributeCount += 1
                }
            }
            */
            //argumentEncoder.constantData(at: CesiumTileBufferIndex.vertexAttributeCount.rawValue).storeBytes(of: UInt16(attributeCount), as: UInt16.self)
            
            // Argument Buffer
            argumentEncoder.constantData(at: CesiumMaterialBufferIndex.textureCount.rawValue).storeBytes(of: UInt16(bufferParams.textures.count), as: UInt16.self)
            for (j, texture) in bufferParams.textures.enumerated() {
                argumentEncoder.setTexture(texture.metalTexture, index: CesiumMaterialBufferIndex.texture_0.rawValue + j)
                argumentEncoder.setSamplerState(texture.sampler.state, index: CesiumMaterialBufferIndex.sampler_0.rawValue + j)
            }
            if let shadow = shadow {
                argumentEncoder.setTexture(shadow.randomTexture, index: CesiumMaterialBufferIndex.randomTexture.rawValue)
            }
            argumentEncoder.setRenderPipelineState(pipeline, index: CesiumMaterialBufferIndex.pipelineState.rawValue)

            textureList.append(contentsOf: bufferParams.textures)
            
            let indexType = ib.componentDatatype.toMTLIndexType()
            var applyFog: Bool = command.label != nil && command.label!.hasSuffix("_fog") ? true : false
            let fogBuffer = setBuffer(p: &applyFog, length: 1)
            resourceList.append(fogBuffer)

            // Indirect Command Buffer
            let icbCommand = icb.indirectRenderCommandAt(tileOffset + i)
            // Vertex Buffer
            icbCommand.setRenderPipelineState(pipeline)
            icbCommand.setVertexBuffer(autoUniform, offset: 0, at: 0)
            icbCommand.setVertexBuffer(frustumUniform, offset: 0, at: 1)
            icbCommand.setVertexBuffer(tileUniformBuffer, offset: 0, at: 2)
            resourceList.append(tileUniformBuffer)
            for attribute in vertexArray.attributes {
                if let buffer = attribute.buffer {
                    resourceList.append(buffer.metalBuffer)
                    icbCommand.setVertexBuffer(buffer.metalBuffer, offset: 0, at: attribute.bufferIndex)
                }
            }
            icbCommand.setVertexBuffer(fogBuffer, offset: 0, at: 5)
            
            // Fragment Buffer
            icbCommand.setFragmentBuffer(autoUniform, offset: 0, at: 0)
            icbCommand.setFragmentBuffer(tileUniformBuffer, offset: bufferParams.fragmentOffset, at: 1)
            icbCommand.setFragmentBuffer(argumentBuffer, offset: argumentEncoder.encodedLength * (tileOffset + i), at: 2)
            icbCommand.setFragmentBuffer(fogBuffer, offset: 0, at: 3)

            icbCommand.drawIndexedPrimitives(.triangle, indexCount: indexCount, indexType: indexType, indexBuffer: indexBuffer, indexBufferOffset: 0, instanceCount: 1, baseVertex: 0, baseInstance: 0)
            //icbCommand.drawPrimitives(.triangle, vertexStart: 0, vertexCount: indexCount, instanceCount: 1, baseInstance: 0)
            resourceList.append(indexBuffer)
        }
        
        let renderState = firstCommand.renderState ?? context.defaultRenderState()
        
        let renderEncoder: MTLRenderCommandEncoder!
        if let shadow = shadow {
            renderEncoder = shadow.shadowRayRenderEncoder(with: context.currentCommandBuffer(), passState: passState, defaultFramebuffer: context.defaultFramebuffer, clear: (frustumIndex == 0))
            renderEncoder.useResource(shadow.randomTexture, usage: .read)
        } else {
            let renderPass = context.createRenderPass(passState)
            renderEncoder = renderPass.commandEncoder
        }
        renderEncoder.pushDebugGroup("ICBonCPU\(frustumIndex)")

        renderEncoder.setFrontFacing(renderState.windingOrder.toMetal())
        renderEncoder.setCullMode(renderState.cullFace.toMetal())
        if renderState.depthTest.enabled {
            renderEncoder.setDepthStencilState(renderState.depthStencilState())
        }
        // TODO: get rid of renderState
        renderState.applyViewport(renderEncoder, passState: passState)
        if renderState.wireFrame {
            renderEncoder.setTriangleFillMode(.lines)
        } else {
            renderEncoder.setTriangleFillMode(.fill)
        }
        // render
        renderEncoder.label = "Main Render Encoder"
        for res in resourceList {
            renderEncoder.useResource(res, usage: .read)
        }
        for tex in textureList {
            renderEncoder.useResource(tex.metalTexture, usage: .sample)
        }
        renderEncoder.executeCommandsInBuffer(icb, range: tileOffset..<(tileOffset + tileCount))
        renderEncoder.popDebugGroup()
        renderEncoder.endEncoding()
        
        tileOffset += tileCount

        // Encode command to optimize the indirect command buffer after encoding
        /*
        let optimizeBlitEncoder = commandBuffer.makeBlitCommandEncoder()!
        optimizeBlitEncoder.label = "Optimize ICB Blit Encoder"
        optimizeBlitEncoder.optimizeIndirectCommandBuffer(icb, range: 0..<tileCount)
        optimizeBlitEncoder.endEncoding()
        */
    }
    
    func encodeDirect(commands: [DrawCommand], frustumUniform: MTLBuffer, frustumIndex: Int, passState: PassState, context: Context, shadow: Shadow? = nil) {
        bufferSyncState = context.bufferSyncState
        guard let fragmentFunction = fragmentFunction else { return }
        let argumentEncoder = fragmentFunction.makeArgumentEncoder(bufferIndex: 2)
        let argumentBuffer = agb["material"]!
        let tileCount = commands.count
        
        let renderEncoder: MTLRenderCommandEncoder
        if let shadow = shadow {
            renderEncoder = shadow.shadowRayRenderEncoder(with: context.currentCommandBuffer(), passState: passState, defaultFramebuffer: context.defaultFramebuffer, clear: (frustumIndex == 0))
        } else {
            let renderPass = context.createRenderPass(passState)
            renderEncoder = renderPass.commandEncoder
        }
        renderEncoder.pushDebugGroup("encodeDirect\(frustumIndex)")

        
        for (i, command) in commands.enumerated() {
            var resourceList: [MTLResource] = []
            
            guard let pipeline = command.pipeline?.stateForICB else { return }
            let renderState = command.renderState ?? context.defaultRenderState()

            renderEncoder.setRenderPipelineState(pipeline)
            //context.applyRenderState(renderPass, renderState: command.renderState ?? context.defaultRenderState(), passState: renderPass.passState)
            renderEncoder.setFrontFacing(renderState.windingOrder.toMetal())
            renderEncoder.setCullMode(renderState.cullFace.toMetal())
            if renderState.depthTest.enabled {
                renderEncoder.setDepthStencilState(renderState.depthStencilState())
            }
            // TODO: get rid of renderState
            renderState.applyViewport(renderEncoder, passState: passState)
            if renderState.wireFrame {
                renderEncoder.setTriangleFillMode(.lines)
            } else {
                renderEncoder.setTriangleFillMode(.fill)
            }

            
            let uniformState = context.uniformState
            uniformState.model = command.modelMatrix ?? Matrix4.identity
            //let bufferParams = setUniforms(command, uniformState: uniformState)
            let bufferParams = setUniforms(command)
            
            // Don't render unless any textures required are available
            if !bufferParams.texturesValid {
                logPrint(.error, "invalid textures")
                continue
            }

            let count = command.count
            guard let vertexArray = command.vertexArray,
                let _ = vertexArray.attributes[0].buffer?.metalBuffer,
                let ib = vertexArray.indexBuffer,
                let indexBuffer = vertexArray.indexBuffer?.metalBuffer,
                let indexCount = count ?? vertexArray.indexCount,
                let tileUniformBuffer = context.manualUniformBuffer(with: command.uniformMap?.uniformBufferProvider)//,
                //let argumentBuffer = device.makeBuffer(length: argumentEncoder.encodedLength, options: .storageModeShared)
                else {
                assertionFailure("invalid command")
                continue
            }
            
            argumentEncoder.setArgumentBuffer(argumentBuffer, offset: argumentEncoder.encodedLength * (tileOffset + i))

            argumentEncoder.constantData(at: CesiumMaterialBufferIndex.textureCount.rawValue).storeBytes(of: UInt(bufferParams.textures.count), as: UInt.self)
            for (j, texture) in bufferParams.textures.enumerated() {
                argumentEncoder.setTexture(texture.metalTexture, index: CesiumMaterialBufferIndex.texture_0.rawValue + j)
                argumentEncoder.setSamplerState(texture.sampler.state, index: CesiumMaterialBufferIndex.sampler_0.rawValue + j)
            }
            if let shadow = shadow {
                argumentEncoder.setTexture(shadow.randomTexture, index: CesiumMaterialBufferIndex.randomTexture.rawValue)
                renderEncoder.useResource(shadow.randomTexture, usage: .read)
            }
            argumentEncoder.setRenderPipelineState(pipeline, index: CesiumMaterialBufferIndex.pipelineState.rawValue)
            
            let autoUniform = context.automaticUniformBuffer()
            let indexType = ib.componentDatatype.toMTLIndexType()
            var applyFog: Bool = command.label != nil && command.label!.hasSuffix("_fog") ? true : false
            
            // render
            renderEncoder.label = "Main Render Encoder"
            renderEncoder.setVertexBuffer(autoUniform, offset: 0, index: 0)
            renderEncoder.setVertexBuffer(frustumUniform, offset: 0, index: 1)
            renderEncoder.setVertexBuffer(tileUniformBuffer, offset: 0, index: 2)
            for attribute in vertexArray.attributes {
                if let buffer = attribute.buffer {
                    renderEncoder.setVertexBuffer(buffer.metalBuffer, offset: 0, index: attribute.bufferIndex)
                }
            }
            renderEncoder.setVertexBytes(&applyFog, length: 1, index: 5)
            
            renderEncoder.setFragmentBuffer(autoUniform, offset: 0, index: 0)
            renderEncoder.setFragmentBuffer(tileUniformBuffer, offset: bufferParams.fragmentOffset, index: 1)
            //renderEncoder.setFragmentBuffer(argumentBuffer, offset: 0, index: 2)
            renderEncoder.setFragmentBuffer(argumentBuffer, offset: argumentEncoder.encodedLength * (tileOffset + i), index: 2)
            renderEncoder.setFragmentBytes(&applyFog, length: 1, index: 3)
            for res in resourceList {
                renderEncoder.useResource(res, usage: .read)
            }
            for tex in bufferParams.textures {
                renderEncoder.useResource(tex.metalTexture, usage: .sample)
            }
            //renderEncoder.drawIndexedPrimitives(type: .triangle, indexCount: indexCount, indexType: indexType, indexBuffer: indexBuffer, indexBufferOffset: 0)
            renderEncoder.drawIndexedPrimitives(type: .triangle, indexCount: indexCount, indexType: indexType, indexBuffer: indexBuffer, indexBufferOffset: 0, instanceCount: 1, baseVertex: 0, baseInstance: 0)
        }
        renderEncoder.popDebugGroup()
        renderEncoder.endEncoding()
        
        tileOffset += tileCount
    }
    
    func encodeDirectNoAB(commands: [DrawCommand], frustumUniform: MTLBuffer, frustumIndex: Int, passState: PassState, context: Context, shadow: Shadow? = nil) {
        bufferSyncState = context.bufferSyncState
        let tileCount = commands.count
        
        // set current uniform for next frame
        let currentFrustumUniformBuffer = agb["frustum"]!
        let currentTileUniformBuffer = agb["tileUniform"]!
        currentFrustumUniformBuffer.copy(from: frustumUniform, length: MemoryLayout<FrustumUniformBufferLayout>.size, sourceOffset: 0, targetOffset: MemoryLayout<FrustumUniformBufferLayout>.stride * frustumIndex)
        
        let prevFrustumUniformBuffer = prev_agb["frustum"]!
        let prevTileUniformBuffer = prev_agb["tileUniform"]!

        let renderEncoder: MTLRenderCommandEncoder
        if let shadow = shadow {
            renderEncoder = shadow.shadowRayRenderEncoder(with: context.currentCommandBuffer(), passState: passState, defaultFramebuffer: context.defaultFramebuffer, clear: (frustumIndex == 0))
        } else {
            let renderPass = context.createRenderPass(passState)
            renderEncoder = renderPass.commandEncoder
        }
        renderEncoder.pushDebugGroup("encodeDirectNoAB\(frustumIndex)")

        
        for (i, command) in commands.enumerated() {

            guard let renderPL = command.pipeline, let pipeline = renderPL.stateForICB else {
                assertionFailure("no pipeline state for ICB")
                return
            }
            let renderState = command.renderState ?? context.defaultRenderState()

            renderEncoder.setRenderPipelineState(pipeline)
            //context.applyRenderState(renderPass, renderState: command.renderState ?? context.defaultRenderState(), passState: renderPass.passState)
            renderEncoder.setFrontFacing(renderState.windingOrder.toMetal())
            renderEncoder.setCullMode(renderState.cullFace.toMetal())
            if renderState.depthTest.enabled {
                renderEncoder.setDepthStencilState(renderState.depthStencilState())
            }
            // TODO: get rid of renderState
            renderState.applyViewport(renderEncoder, passState: passState)
            if renderState.wireFrame {
                renderEncoder.setTriangleFillMode(.lines)
            } else {
                renderEncoder.setTriangleFillMode(.fill)
            }

            
            let uniformState = context.uniformState
            uniformState.model = command.modelMatrix ?? Matrix4.identity
            let bufferParams = setUniforms(command)
            //let bufferParams = renderPL.setUniforms(command, device: device, uniformState: uniformState)
            
            // Don't render unless any textures required are available
            if !bufferParams.texturesValid {
                logPrint(.error, "invalid textures")
                continue
            }

            let count = command.count
            guard let vertexArray = command.vertexArray,
                let _ = vertexArray.attributes[0].buffer?.metalBuffer,
                let ib = vertexArray.indexBuffer,
                let indexBuffer = vertexArray.indexBuffer?.metalBuffer,
                let indexCount = count ?? vertexArray.indexCount,
                let tileUniformBuffer = context.manualUniformBuffer(bufferSyncState, with: command.uniformMap?.uniformBufferProvider)//,
                //let argumentBuffer = device.makeBuffer(length: argumentEncoder.encodedLength, options: .storageModeShared)
                else {
                assertionFailure("invalid command")
                continue
            }
            // set current tile uniform for next frame
            let tileIdentifier = command.label!.hasSuffix("_fog") ? String(command.label!.prefix(command.label!.count - 4)) : command.label!
            tileIndexes[tileIdentifier] = tileOffset + i
            currentTileUniformBuffer.copy(from: tileUniformBuffer, length: MemoryLayout<TileUniformStruct>.size, sourceOffset: 0, targetOffset: MemoryLayout<TileUniformStruct>.stride * (tileOffset + i))

            let autoUniform = context.automaticUniformBuffer(bufferSyncState)
            let indexType = ib.componentDatatype.toMTLIndexType()
            var applyFog: Bool = command.label != nil && command.label!.hasSuffix("_fog") ? true : false
            
            // render
            renderEncoder.label = "Main Render Encoder no AB"
            renderEncoder.setVertexBuffer(autoUniform, offset: 0, index: 0)
            renderEncoder.setVertexBuffer(frustumUniform, offset: 0, index: 1)
            renderEncoder.setVertexBuffer(tileUniformBuffer, offset: 0, index: 2)
            for attribute in vertexArray.attributes {
                if let buffer = attribute.buffer {
                    renderEncoder.setVertexBuffer(buffer.metalBuffer, offset: 0, index: attribute.bufferIndex)
                }
            }
            renderEncoder.setVertexBytes(&applyFog, length: 1, index: 5)
            
            if context.uniformState.frameState.frameNumber > 0 { // skip for first frame
                renderEncoder.setVertexBuffer(prevFrustumUniformBuffer, offset: MemoryLayout<FrustumUniformBufferLayout>.stride * frustumIndex, index: 6)
                renderEncoder.setVertexBuffer(prevTileUniformBuffer, offset: 0, index: 7)
            }
            var previousTileIndex = prevTileIndex(of: tileIdentifier)
            renderEncoder.setVertexBytes(&previousTileIndex, length: 4, index: 8)
            
            renderEncoder.setFragmentBuffer(autoUniform, offset: 0, index: 0)
            renderEncoder.setFragmentBuffer(frustumUniform, offset: 0, index: 1)
            renderEncoder.setFragmentBuffer(tileUniformBuffer, offset: bufferParams.fragmentOffset, index: 2)
            var textureCount = UInt(bufferParams.textures.count)
            renderEncoder.setFragmentBytes(&textureCount, length: 4, index: 3)
            renderEncoder.setFragmentBytes(&applyFog, length: 1, index: 4)
            
            for (j, tex) in bufferParams.textures.enumerated() {
                renderEncoder.setFragmentTexture(tex.metalTexture, index: j)
                renderEncoder.setFragmentSamplerState(tex.sampler.state, index: j)
                //renderEncoder.useResource(tex.metalTexture, usage: .sample)
            }
            
            if let shadow = shadow {
                renderEncoder.setFragmentTexture(shadow.randomTexture, index: 3)
            }
            //renderEncoder.drawIndexedPrimitives(type: .triangle, indexCount: indexCount, indexType: indexType, indexBuffer: indexBuffer, indexBufferOffset: 0)
            renderEncoder.drawIndexedPrimitives(type: .triangle, indexCount: indexCount, indexType: indexType, indexBuffer: indexBuffer, indexBufferOffset: 0, instanceCount: 1, baseVertex: 0, baseInstance: 0)
        }
        renderEncoder.popDebugGroup()
        renderEncoder.endEncoding()
        
        tileOffset += tileCount
    }
}

extension ICBCesium {
    // MARK: - Tile Indexing
    func prevTileIndex(of identifier: String) -> Int32 {
        let tileMap = prevTileIndexes
        return Int32(tileMap[identifier] ?? 1000)
    }
    
    //MARK:- Set uniforms
    
    func setUniforms (_ command: DrawCommand) -> (fragmentOffset: Int, texturesValid: Bool, textures: [Texture])
    {
        guard let map = command.uniformMap else {
            return (0, true, [Texture]())
        }
        
        if let map = map as? TileUniformMap {
            return setNativeUniforms(map)
        }
        
        if let _ = map as? LegacyUniformMap {
            return (0, false, [Texture]())
        }
        
        return (0, false, [Texture]())
    }
    
    func setNativeUniforms (_ map: TileUniformMap) -> (fragmentOffset: Int, texturesValid: Bool, textures: [Texture])
    {
        guard let buffer = map.uniformBufferProvider?.currentBuffer(bufferSyncState) else {
            return (0, false, [Texture]())
        }
        
        let textures = map.uniformUpdateBlock(buffer)
        buffer.signalWriteComplete()
        return (fragmentOffset: 0, texturesValid: true, textures: textures)
    }
}

extension MTLBuffer {
    func copy (from other: MTLBuffer, length copyLength: Int, sourceOffset: Int = 0, targetOffset: Int = 0) {
        assert(sourceOffset + copyLength <= other.length, "source buffer not large enough")
        assert(targetOffset + copyLength <= length, "This buffer is not large enough")
        memcpy(contents()+targetOffset, other.contents()+sourceOffset, copyLength)
    }
    func write (from data: UnsafeRawPointer, length writeLength: Int, offset: Int = 0) {
        assert(offset + writeLength <= length, "This buffer is not large enough")
        memcpy(contents()+offset, data, writeLength)
    }
}
