//
//  Shadow.swift
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2019/11/13.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation
import MetalPerformanceShaders

class Shadow {
    let intersector: MPSRayIntersector
    var shadowRayTexture: MTLTexture!
    var intersectionTexture: MTLTexture!
    var randomTexture: MTLTexture!
    var shadowTexture: MTLTexture!
    var composeTexture: MTLTexture!
    
    var storeTexturesForShadow: Bool = true

    private var shadowPipeline: MTLComputePipelineState? = nil
    private var composePipeline: MTLComputePipelineState? = nil
    private var denoiser: MPSSVGFDenoiser? = nil
    private var textureAllocator: MPSSVGFDefaultTextureAllocator? = nil
    private var depthNormalTexture: MTLTexture? = nil
    private var motionVectorTexture: MTLTexture? = nil
    private var previousDepthNormalTexture: MTLTexture? = nil
    private var previousTexture: MTLTexture? = nil
    
    private var TAA: MPSTemporalAA? = nil

    private var width: Int
    private var height: Int
    
    private var initialized: Bool = false
    
    required init(device: MTLDevice, viewport: Cartesian4) {
        intersector = MPSRayIntersector(device: device)
        intersector.rayDataType = .originMinDistanceDirectionMaxDistance
        intersector.intersectionDataType = .distance
        
        width = Int(viewport.width)
        height = Int(viewport.height)
        
        /*
        let textureDescriptor = MTLTextureDescriptor()
        textureDescriptor.width = width
        textureDescriptor.height = height
        
        textureDescriptor.textureType = .type2DArray
        textureDescriptor.pixelFormat = .rgba32Float
        textureDescriptor.usage = [.renderTarget, .shaderRead]
        textureDescriptor.storageMode = .private
        textureDescriptor.arrayLength = 2
        shadowRayTexture = device.makeTexture(descriptor: textureDescriptor)!
        
        textureDescriptor.usage = [.shaderRead, .shaderWrite]
        textureDescriptor.arrayLength = 1
        textureDescriptor.pixelFormat = .r32Float
        intersectionTexture = device.makeTexture(descriptor: textureDescriptor)!
        
        textureDescriptor.textureType = .type2D
        textureDescriptor.pixelFormat = .r8Uint
        textureDescriptor.usage = .shaderRead
        textureDescriptor.arrayLength = 1
        textureDescriptor.storageMode = .shared
        randomTexture = device.makeTexture(descriptor: textureDescriptor)!
        
        textureDescriptor.textureType = .type2D
        textureDescriptor.pixelFormat = .r16Float
        textureDescriptor.usage = [.shaderWrite, .shaderRead]
        textureDescriptor.storageMode = .private
        textureDescriptor.arrayLength = 1
        shadowTexture = device.makeTexture(descriptor: textureDescriptor)!
        
        textureDescriptor.textureType = .type2D
        textureDescriptor.pixelFormat = .bgra8Unorm
        textureDescriptor.usage = [.shaderWrite]
        textureDescriptor.storageMode = .private
        textureDescriptor.arrayLength = 1
        composeTexture = device.makeTexture(descriptor: textureDescriptor)!

        let dimension: Int = width * height
        var randomValue: [UInt8] = Array(repeating: 0, count: dimension)
        for i in 0..<randomValue.count {
            randomValue[i] = UInt8(arc4random() % 16)
        }
        randomTexture.replace(region: MTLRegionMake2D(0, 0, width, height), mipmapLevel: 0, withBytes: randomValue, bytesPerRow: MemoryLayout<UInt8>.size * width)
        */
        makeTexture(device: device)
        
        createPipeline(device: device)
        createDenoiser(device: device)
        
    }
    
    func makeTexture(device: MTLDevice) {
        let textureDescriptor = MTLTextureDescriptor()
        textureDescriptor.width = width
        textureDescriptor.height = height
        
        textureDescriptor.textureType = .type2DArray
        textureDescriptor.pixelFormat = .rgba32Float
        textureDescriptor.usage = [.renderTarget, .shaderRead]
        textureDescriptor.storageMode = .private
        textureDescriptor.arrayLength = 2
        shadowRayTexture = device.makeTexture(descriptor: textureDescriptor)!
        
        textureDescriptor.usage = [.shaderRead, .shaderWrite]
        textureDescriptor.arrayLength = 1
        textureDescriptor.pixelFormat = .r32Float
        intersectionTexture = device.makeTexture(descriptor: textureDescriptor)!
        
        textureDescriptor.textureType = .type2D
        textureDescriptor.pixelFormat = .r8Uint
        textureDescriptor.usage = .shaderRead
        textureDescriptor.arrayLength = 1
        textureDescriptor.storageMode = .shared
        randomTexture = device.makeTexture(descriptor: textureDescriptor)!
        
        textureDescriptor.textureType = .type2D
        textureDescriptor.pixelFormat = .r16Float
        textureDescriptor.usage = [.shaderWrite, .shaderRead]
        textureDescriptor.storageMode = .private
        textureDescriptor.arrayLength = 1
        shadowTexture = device.makeTexture(descriptor: textureDescriptor)!
        
        textureDescriptor.textureType = .type2D
        textureDescriptor.pixelFormat = .bgra8Unorm
        textureDescriptor.usage = [.shaderWrite]
        textureDescriptor.storageMode = .private
        textureDescriptor.arrayLength = 1
        composeTexture = device.makeTexture(descriptor: textureDescriptor)!

        let dimension: Int = width * height
        var randomValue: [UInt8] = Array(repeating: 0, count: dimension)
        for i in 0..<randomValue.count {
            randomValue[i] = UInt8(arc4random() % 16)
        }
        randomTexture.replace(region: MTLRegionMake2D(0, 0, width, height), mipmapLevel: 0, withBytes: randomValue, bytesPerRow: MemoryLayout<UInt8>.size * width)
    }
    
    func resizeTexture(device: MTLDevice, width: Int, height: Int) {
        self.width = width
        self.height = height
        makeTexture(device: device)
    }
    
    func createPipeline(device: MTLDevice) {
        guard let bundle = Bundle(identifier: "com.testtoast.CesiumKit") else {
            logPrint(.error, "Could not find CesiumKit bundle in executable")
            return
        }
        guard let libraryPath = bundle.url(forResource: "default", withExtension: "metallib")?.path else {
            logPrint(.error, "Could not find shader source from bundle")
            return
        }
        let shaderLibrary: MTLLibrary
        do {
            shaderLibrary = try device.makeLibrary(filepath: libraryPath)
        } catch let error as NSError {
            logPrint(.error, "Could not generate library from compiled shader lib: \(error.localizedDescription)")
            return
        }
        
        let computeDescriptor = MTLComputePipelineDescriptor()
        computeDescriptor.threadGroupSizeIsMultipleOfThreadExecutionWidth = true
        computeDescriptor.computeFunction = shaderLibrary.makeFunction(name: "shadowKernel")
        do {
            shadowPipeline = try device.makeComputePipelineState(descriptor: computeDescriptor, options: [], reflection: nil)
        } catch {
            assertionFailure("failed to create shadow pipeline state")
        }
        
        computeDescriptor.computeFunction = shaderLibrary.makeFunction(name: "compositeKernel")
        do {
            composePipeline = try device.makeComputePipelineState(descriptor: computeDescriptor, options: [], reflection: nil)
        } catch {
            assertionFailure("failed to create composite pipeline state")
        }
    }
    
    func createDenoiser(device: MTLDevice) {
        textureAllocator = MPSSVGFDefaultTextureAllocator(device: device)
        let svgf = MPSSVGF(device: device)
        svgf.channelCount = 1
        svgf.temporalWeighting = .exponentialMovingAverage
        svgf.temporalReprojectionBlendFactor = 0.1
        svgf.minimumFramesForVarianceEstimation = 2

        denoiser = MPSSVGFDenoiser(SVGF: svgf, textureAllocator: textureAllocator!)
        denoiser!.bilateralFilterIterations = 3
        
        TAA = MPSTemporalAA(device: device)

        previousDepthNormalTexture = textureAllocator!.texture(with: .rgba16Float, width: width, height: height)
        previousTexture = textureAllocator!.texture(with: .bgra8Unorm, width: width, height: height)
        //composeTexture = textureAllocator!.texture(with: .bgra8Unorm, width: width, height: height)!
    }
    func shadowRayRenderEncoder(with commandBuffer: MTLCommandBuffer, passState: PassState, defaultFramebuffer: Framebuffer, clear: Bool) -> MTLRenderCommandEncoder {
        let defaultPassDescriptor = passState.framebuffer?.renderPassDescriptor ?? defaultFramebuffer.renderPassDescriptor
        return shadowRayRenderEncoder(with: commandBuffer, passDescriptor: defaultPassDescriptor, clear: clear)
    }
    
    func shadowRayRenderEncoder(with commandBuffer: MTLCommandBuffer, passDescriptor: MTLRenderPassDescriptor, clear: Bool) -> MTLRenderCommandEncoder {
        let passDescriptor = passDescriptor.copy() as! MTLRenderPassDescriptor
        
        passDescriptor.colorAttachments[1].texture = shadowRayTexture
        passDescriptor.colorAttachments[1].slice = 0
        if clear {
            passDescriptor.colorAttachments[1].loadAction = .clear
        } else {
            passDescriptor.colorAttachments[1].loadAction = .load
        }
        passDescriptor.colorAttachments[1].clearColor = MTLClearColorMake(0.0, 0.0, 0.0, 0.0);
        if storeTexturesForShadow {
            passDescriptor.colorAttachments[1].storeAction = .store
        } else {
            passDescriptor.colorAttachments[1].loadAction = .dontCare
            passDescriptor.colorAttachments[1].storeAction = .dontCare
        }
        
        passDescriptor.colorAttachments[2].texture = shadowRayTexture
        passDescriptor.colorAttachments[2].slice = 1
        if clear {
            passDescriptor.colorAttachments[2].loadAction = .clear
        } else {
            passDescriptor.colorAttachments[2].loadAction = .load
        }
        // This clears the "max distance" field to -1 so the ray intersector
        // will exit immediately for pixels which are not covered by any geometry
        passDescriptor.colorAttachments[2].clearColor = MTLClearColorMake(0.0, 0.0, 0.0, -1.0)
        if storeTexturesForShadow {
            passDescriptor.colorAttachments[2].storeAction = .store
        } else {
            passDescriptor.colorAttachments[2].loadAction = .dontCare
            passDescriptor.colorAttachments[2].storeAction = .dontCare
        }
        
        // TODO: motionVector [[ color(3) ]], depthNormal [[ color(4) ]]
        if let textureAllocator = textureAllocator {
            if clear {
                motionVectorTexture = textureAllocator.texture(with: .rg16Float, width: width, height: height)
                depthNormalTexture = textureAllocator.texture(with: .rgba16Float, width: width, height: height)
            }

            passDescriptor.colorAttachments[3].texture = motionVectorTexture
            if clear {
                passDescriptor.colorAttachments[3].loadAction = .clear
            } else {
                passDescriptor.colorAttachments[3].loadAction = .load
            }
            passDescriptor.colorAttachments[3].clearColor = MTLClearColorMake(0.0, 0.0, 0.0, 0.0)
            if storeTexturesForShadow {
                passDescriptor.colorAttachments[3].storeAction = .store
            } else {
                passDescriptor.colorAttachments[3].loadAction = .dontCare
                passDescriptor.colorAttachments[3].storeAction = .dontCare
            }

            passDescriptor.colorAttachments[4].texture = depthNormalTexture
            if clear {
                passDescriptor.colorAttachments[4].loadAction = .clear
            } else {
                passDescriptor.colorAttachments[4].loadAction = .load
            }
            passDescriptor.colorAttachments[4].clearColor = MTLClearColorMake(1000.0, 0.0, 0.0, 0.0)
            if storeTexturesForShadow {
                passDescriptor.colorAttachments[4].storeAction = .store
            } else {
                passDescriptor.colorAttachments[4].loadAction = .dontCare
                passDescriptor.colorAttachments[4].storeAction = .dontCare
            }
        }
        
        return commandBuffer.makeRenderCommandEncoder(descriptor: passDescriptor)!
    }
    
    func computeIntersect(of accelerationStructure: AccelerationStructure, with commandBuffer: MTLCommandBuffer) {
        let instanceAS = accelerationStructure.accelerationStructure
        intersector.encodeIntersection(commandBuffer: commandBuffer, intersectionType: .any, rayTexture: shadowRayTexture, intersectionTexture: intersectionTexture, accelerationStructure: instanceAS)
    }
    
    func computeShadow(with commandBuffer: MTLCommandBuffer) -> MTLTexture {
        let shadowTexture: MTLTexture
        if let textureAllocator = textureAllocator {
            shadowTexture = textureAllocator.texture(with: .r16Float, width: width, height: height)!
        } else {
            shadowTexture = self.shadowTexture
        }
        guard let shadowPipeline = shadowPipeline else { return shadowTexture }
        
        let computeEncoder = commandBuffer.makeComputeCommandEncoder()!
        computeEncoder.label = "shadow compute"
        var viewport = SIMD2<UInt>(UInt(width), UInt(height))
        computeEncoder.setBytes(&viewport, length: MemoryLayout<SIMD2<UInt>>.size, index: 0)
        computeEncoder.setTexture(shadowRayTexture, index: 0)
        computeEncoder.setTexture(intersectionTexture, index: 1)
        computeEncoder.setTexture(shadowTexture, index: 2)
        
        computeEncoder.setComputePipelineState(shadowPipeline)
        
        let threadsPerThreadgroup = MTLSize(width: 8, height: 8, depth: 1)
        let threadgroups = MTLSizeMake(
            (width  + threadsPerThreadgroup.width  - 1) / threadsPerThreadgroup.width,
            (height + threadsPerThreadgroup.height - 1) / threadsPerThreadgroup.height,
            1);
        
        computeEncoder.dispatchThreadgroups(threadgroups, threadsPerThreadgroup: threadsPerThreadgroup)
        computeEncoder.endEncoding()
        
        return shadowTexture
    }
    
    func denoiseAndCompose(_ shadowTexture: MTLTexture, with commandBuffer: MTLCommandBuffer, context: Context, passState: PassState, temporalAA: Bool = true) {
        guard context.uniformState.frameState.frameNumber > 0,
            let _ = denoiser, let _ = motionVectorTexture, let _ = depthNormalTexture else {
            composeShadow(shadowTexture, with: commandBuffer, context: context, passState: passState)
            return
        }
        let denoisedTexture = denoise(shadowTexture: shadowTexture, with: commandBuffer)
        textureAllocator!.return(shadowTexture)
        
        let colorTexture = textureAllocator!.texture(with: .bgra8Unorm, width: width, height: height)!
        composeTexture = textureAllocator!.texture(with: .bgra8Unorm, width: width, height: height)
        
        composeShadow(denoisedTexture, with: commandBuffer, context: context, passState: passState, sourceColorTexture: colorTexture, finalBlit: !temporalAA)
        
        var AATexture = composeTexture!
        if temporalAA {
            if context.uniformState.frameState.frameNumber > 0, let taa = TAA {
                AATexture = textureAllocator!.texture(with: .bgra8Unorm, width: width, height: height)!
                taa.encode(to: commandBuffer,
                           sourceTexture: composeTexture,
                           previousTexture: previousTexture!,
                           destinationTexture: AATexture,
                           motionVectorTexture: motionVectorTexture,
                           depthTexture: depthNormalTexture)
                textureAllocator?.return(composeTexture)
    
                let target = passState.framebuffer?.colorTextures![0] ?? context.defaultFramebuffer.colorTextures![0]
                blit(from: AATexture, to: target.metalTexture, with: commandBuffer)
            }
        }
        
        textureAllocator?.return(denoisedTexture)
        textureAllocator?.return(colorTexture)

        textureAllocator!.return(previousDepthNormalTexture!)
        previousDepthNormalTexture = depthNormalTexture
        
        textureAllocator!.return(motionVectorTexture!)
        
        textureAllocator!.return(previousTexture!)
        previousTexture = AATexture
        
    }
    
    func denoise(shadowTexture: MTLTexture, with commandBuffer: MTLCommandBuffer) -> MTLTexture {
        if !initialized {
            initialized = true
            denoiser!.clearTemporalHistory()
        }
        let denoizedTexture = denoiser!.encode(
            commandBuffer: commandBuffer,
            sourceTexture: shadowTexture,
            motionVectorTexture: motionVectorTexture!,
            depthNormalTexture: depthNormalTexture!,
            previousDepthNormalTexture: previousDepthNormalTexture!)

        return denoizedTexture
    }
    
    func composeShadow(_ shadowTexture: MTLTexture, with commandBuffer: MTLCommandBuffer, context: Context, passState: PassState, sourceColorTexture: MTLTexture? = nil, finalBlit: Bool = true) {
        let target = passState.framebuffer?.colorTextures![0] ?? context.defaultFramebuffer.colorTextures![0]
        guard let composePipeline = composePipeline else { return }
        let autoUniform = context.automaticUniformBuffer()
        
        let sourceTexture: MTLTexture
        if let sourceColorTexture = sourceColorTexture {
            sourceTexture = sourceColorTexture
            blit(from: target.metalTexture, to: sourceTexture, with: commandBuffer)
        } else {
            sourceTexture = target.metalTexture
        }
        
        let threadsPerThreadgroup = MTLSize(width: 8, height: 8, depth: 1)
        let threadgroups = MTLSizeMake(
            (width  + threadsPerThreadgroup.width  - 1) / threadsPerThreadgroup.width,
            (height + threadsPerThreadgroup.height - 1) / threadsPerThreadgroup.height,
            1);
        
        let computeEncoder = commandBuffer.makeComputeCommandEncoder()!
        computeEncoder.label = "shadow compose"
        computeEncoder.setComputePipelineState(composePipeline)
        var viewport = SIMD2<UInt>(UInt(width), UInt(height))
        computeEncoder.setBytes(&viewport, length: MemoryLayout<SIMD2<UInt>>.size, index: 0)
        computeEncoder.setBuffer(autoUniform, offset: 0, index: 1)
        //computeEncoder.setTexture(target.metalTexture, index: 0)
        computeEncoder.setTexture(sourceTexture, index: 0)
        computeEncoder.setTexture(shadowTexture, index: 1)
        computeEncoder.setTexture(composeTexture, index: 2)
        computeEncoder.dispatchThreadgroups(threadgroups, threadsPerThreadgroup: threadsPerThreadgroup)
        computeEncoder.endEncoding()

        if finalBlit {
            blit(from: composeTexture, to: target.metalTexture, with: commandBuffer)
        }
        /*
        let origin = MTLOriginMake(0, 0, 0)
        let size = MTLSizeMake(width, height, 1)
        let blitEncoder = commandBuffer.makeBlitCommandEncoder()!
        blitEncoder.label = "shadow blit"
        blitEncoder.copy(from: composeTexture,
                         sourceSlice: 0,
                         sourceLevel: 0,
                         sourceOrigin: origin,
                         sourceSize: size,
                         to: target.metalTexture,
                         destinationSlice: 0,
                         destinationLevel: 0,
                         destinationOrigin: origin)
        blitEncoder.endEncoding()
        */
    }
    
    func blit(from source: MTLTexture, to destination: MTLTexture, with commandBuffer: MTLCommandBuffer) {
        let origin = MTLOriginMake(0, 0, 0)
        let size = MTLSizeMake(width, height, 1)
        let blitEncoder = commandBuffer.makeBlitCommandEncoder()!
        blitEncoder.label = "shadow blit"
        blitEncoder.copy(from: source,
                         sourceSlice: 0,
                         sourceLevel: 0,
                         sourceOrigin: origin,
                         sourceSize: size,
                         to: destination,
                         destinationSlice: 0,
                         destinationLevel: 0,
                         destinationOrigin: origin)
        blitEncoder.endEncoding()
    }
}
