//
//  SCNGeometry+vertecies.swift
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2019/11/13.
//  Copyright © 2019 Test Toast. All rights reserved.
//

import Foundation
import SceneKit

extension  SCNGeometry{
    /**
     Get the vertices (3d points coordinates) of the geometry.

     - returns: An array of SCNVector3 containing the vertices of the geometry.
     */
    func verticies() -> [SCNVector3]? {

        let sources = self.sources(for: .vertex)

        guard let source  = sources.first else { return nil }

        let stride = source.dataStride / source.bytesPerComponent
        let offset = source.dataOffset / source.bytesPerComponent
        let vectorCount = source.vectorCount
        
        //return source.data.withUnsafeBytes { (buffer : UnsafePointer<Float>) -> [SCNVector3] in
        return source.data.withUnsafeBytes { (b : UnsafeRawBufferPointer) -> [SCNVector3] in
            let buffer: UnsafePointer<Float> = b.baseAddress!.assumingMemoryBound(to: Float.self)

            var result = Array<SCNVector3>()
            for i in 0...vectorCount - 1 {
                let start = i * stride + offset
                let x = buffer[start]
                let y = buffer[start + 1]
                let z = buffer[start + 2]
                result.append(SCNVector3(x, y, z))
            }

            return result
        }
    }
    
    func simd_verticies() -> [SIMD3<Float>] {
        let sources = self.sources(for: .vertex)
        
        guard let source  = sources.first else { return [] }
        
        let stride = source.dataStride / source.bytesPerComponent
        let offset = source.dataOffset / source.bytesPerComponent
        let vectorCount = source.vectorCount
        
        //return source.data.withUnsafeBytes { (buffer : UnsafePointer<Float>) -> [SCNVector3] in
        return source.data.withUnsafeBytes { (b : UnsafeRawBufferPointer) -> [SIMD3<Float>] in
            let buffer: UnsafePointer<Float> = b.baseAddress!.assumingMemoryBound(to: Float.self)
            
            var result = Set<SIMD3<Float>>()
            for i in 0..<vectorCount {
                let start = i * stride + offset
                let x = buffer[start]
                let y = buffer[start + 1]
                let z = buffer[start + 2]
                result.insert(SIMD3<Float>(x, y, z))
            }
            return Array(result)
        }
    }
}
