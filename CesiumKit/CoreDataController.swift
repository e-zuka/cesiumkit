//
//  CoreDataController.swift
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2018/07/31.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController {
    let persistentContainer: NSPersistentContainer!
    
    init(completion: @escaping () -> ()) {
        let bundle = Bundle(identifier: "com.testtoast.CesiumKit")
        let modelURL = bundle!.url(forResource: "TerrainDataModel", withExtension: "momd")!
        persistentContainer = NSPersistentContainer(name: "TerrainDataModel", managedObjectModel: NSManagedObjectModel(contentsOf: modelURL)!)
        
        /*
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = paths.first!
        let storeUrl = documentDirectory.appendingPathComponent("TerrainDataModel.sqlite")
        if !FileManager.default.fileExists(atPath: (storeUrl.path)) {
            let seededDataUrl = Bundle.main.url(forResource: "TerrainDataModel", withExtension: "sqlite")
            try! FileManager.default.copyItem(at: seededDataUrl!, to: storeUrl)
        }
        let description = NSPersistentStoreDescription()
        description.shouldInferMappingModelAutomatically = true
        description.shouldMigrateStoreAutomatically = true
        description.url = storeUrl
        
        persistentContainer.persistentStoreDescriptions = [description]
        */
        
        persistentContainer.loadPersistentStores() { (description, error) in
            if let error = error {
                assertionFailure("CoreData PersistentContainer could not load. error: \(error)")
            }
            completion()
        }
    }
    
    func addTerrainData(key: String, navIdentifier: String, quantizedVertices: [UInt16], indices: [Int], encodedNormals: [UInt8]?, minimumHeight: Double, maximumHeight: Double, westIndices: [Int], southIndices: [Int], eastIndices: [Int], northIndices: [Int], westSkirtHeight: Double, southSkirtHeight: Double, eastSkirtHeight: Double, northSkirtHeight: Double, childTileMask: Int, waterMask: [UInt8]? = nil, createdByUpsampling: Bool = false, cartesianVertices: [Float]) {
        
        if UserDefaults.standard.bool(forKey: "enableCache") == false {
            return
        }

        //let context = persistentContainer.viewContext
        persistentContainer.performBackgroundTask() { context in
            let entity = NSEntityDescription.insertNewObject(forEntityName: "TerrainData", into: context) as! TerrainDataModel
            entity.key = key
            entity.navIdentifier = navIdentifier
            entity.cartesianVertices = cartesianVertices
            entity.quantizedVertices = quantizedVertices
            entity.indices = indices
            entity.encodedNormals = encodedNormals
            entity.minimumHeight = minimumHeight
            entity.maximumHeight = maximumHeight
            entity.westIndices = westIndices
            entity.southIndices = southIndices
            entity.eastIndices = eastIndices
            entity.northIndices = northIndices
            entity.westSkirtHeight = westSkirtHeight
            entity.southSkirtHeight = southSkirtHeight
            entity.eastSkirtHeight = eastSkirtHeight
            entity.northSkirtHeight = northSkirtHeight
            entity.childTileMask = Int16(childTileMask)
            entity.waterMask = waterMask
            entity.createdByUpsampling = createdByUpsampling
        
            do {
                try context.save()
            } catch {
                print("add & save failed: \(error.localizedDescription)")
                //assertionFailure("add & save failed: \(error.localizedDescription)")
            }
        }
    }
    
    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                print("terrain data saved")
            } catch {
                print("Failed to save context: \(error.localizedDescription)")
                //assertionFailure("Failed to save context: \(error.localizedDescription)")
            }
        }
    }
    
    func fetchTerrainData(key: String, childRectangle: Rectangle, ellipsoid: Ellipsoid) -> QuantizedMeshTerrainData? {
        if UserDefaults.standard.bool(forKey: "enableCache") == false {
            return nil
        }
        
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TerrainData")
        fetchRequest.predicate = NSPredicate(format: "key == %@", key)
        
        do {
            let fetchedTerrain = try context.fetch(fetchRequest) as! [TerrainDataModel]
            guard let terrainData = fetchedTerrain.first else { return nil }
            let boundingSphere = BoundingSphere.fromVertices(terrainData.cartesianVertices, center: Cartesian3.zero, stride: 3)
            let orientedBoundingBox = OrientedBoundingBox(fromRectangle: childRectangle, minimumHeight: terrainData.minimumHeight, maximumHeight: terrainData.maximumHeight, ellipsoid: ellipsoid)
            let occluder = EllipsoidalOccluder(ellipsoid: ellipsoid)
            let horizonOcclusionPoint = occluder.computeHorizonCullingPointFromVertices(directionToPoint: boundingSphere.center, vertices: terrainData.cartesianVertices, stride: 3, center: boundingSphere.center)!
            return QuantizedMeshTerrainData(quantizedVertices: terrainData.quantizedVertices, indices: terrainData.indices, encodedNormals: terrainData.encodedNormals, minimumHeight: terrainData.minimumHeight, maximumHeight: terrainData.maximumHeight, boundingSphere: boundingSphere, orientedBoundingBox: orientedBoundingBox, horizonOcclusionPoint: horizonOcclusionPoint, westIndices: terrainData.westIndices, southIndices: terrainData.southIndices, eastIndices: terrainData.eastIndices, northIndices: terrainData.northIndices, westSkirtHeight: terrainData.westSkirtHeight, southSkirtHeight: terrainData.southSkirtHeight, eastSkirtHeight: terrainData.eastSkirtHeight, northSkirtHeight: terrainData.northSkirtHeight, childTileMask: Int(terrainData.childTileMask), createdByUpsampling: terrainData.createdByUpsampling)
        } catch {
            print("Fetch TerrainData Failed: \(error.localizedDescription)")
            //assertionFailure("Fetch TerrainData Failed: \(error.localizedDescription)")
        }
        return nil
    }
    
    func deleteData(of navIdentifier: String?) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TerrainData")
        if navIdentifier != nil {
            fetchRequest.predicate = NSPredicate(format: "navIdentifier == %@", navIdentifier!)
        }
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        let context = persistentContainer.viewContext
        var success = false
        context.performAndWait {
            do {
                try context.execute(deleteRequest)
                if context.hasChanges {
                    try context.save()
                }
                success = true
            } catch {
                print("error on delete: \(error)")
            }
        }
        return success
        
        /*
        persistentContainer.performBackgroundTask { context in
            do {
                try self.persistentContainer.persistentStoreCoordinator.execute(deleteRequest, with: context)
                try context.save()
            } catch {
                print("error occured when deleting")
            }
        }
        */
    }
    
    
    func _deleteData(of navIdentifier: String?, callback: (_ success: Bool) -> Void) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TerrainData")
        if navIdentifier != nil {
            fetchRequest.predicate = NSPredicate(format: "navIdentifier == %@", navIdentifier!)
        }
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        let context = persistentContainer.viewContext
        var success = false
        context.performAndWait {
            do {
                try context.execute(deleteRequest)
                if context.hasChanges {
                    try context.save()
                    context.reset()
                }
                success = true
                //callback(true)
            } catch {
                print("error on delete: \(error)")
                //callback(false)
            }
        }
        callback(success)
    }
    
    func deleteData(of navIdentifier: String?, callback: @escaping (_ success: Bool) -> Void) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TerrainData")
        if navIdentifier != nil {
            fetchRequest.predicate = NSPredicate(format: "navIdentifier == %@", navIdentifier!)
        }
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        deleteRequest.resultType = .resultTypeObjectIDs
        
        let mainContext = persistentContainer.viewContext
        
        persistentContainer.performBackgroundTask { context in
            do {
                let result = try context.execute(deleteRequest) as? NSBatchDeleteResult
                guard let objectIDs = result?.result as? [NSManagedObjectID] else { return }
                let changes = [NSDeletedObjectsKey: objectIDs]
                NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [mainContext])
                callback(true)
            } catch {
                print("error on delete: \(error)")
                callback(false)
            }
        }
    }
}

