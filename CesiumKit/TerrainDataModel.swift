//
//  TerrainDataModel.swift
//  CesiumKit iOS
//
//  Created by 飯塚淳 on 2018/07/31.
//  Copyright © 2018 Test Toast. All rights reserved.
//

import Foundation
import CoreData


@objc(TerrainDataModel)
public class TerrainDataModel: NSManagedObject {
    
}

extension TerrainDataModel {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<TerrainDataModel> {
        return NSFetchRequest<TerrainDataModel>(entityName: "TerrainData")
    }
    
    @NSManaged public var cartesianVertices: [Float]
    @NSManaged public var quantizedVertices: [UInt16]
    @NSManaged public var indices: [Int]
    @NSManaged public var encodedNormals: [UInt8]?
    @NSManaged public var minimumHeight: Double
    @NSManaged public var maximumHeight: Double
    @NSManaged public var westIndices: [Int]
    @NSManaged public var southIndices: [Int]
    @NSManaged public var eastIndices: [Int]
    @NSManaged public var northIndices: [Int]
    @NSManaged public var westSkirtHeight: Double
    @NSManaged public var southSkirtHeight: Double
    @NSManaged public var eastSkirtHeight: Double
    @NSManaged public var northSkirtHeight: Double
    @NSManaged public var childTileMask: Int16
    @NSManaged public var createdByUpsampling: Bool
    @NSManaged public var waterMask: [UInt8]?
    @NSManaged public var key: String
    @NSManaged public var navIdentifier: String
    
}
